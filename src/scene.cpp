﻿#include "scene.h"
#include "scenario.h"
#include "DxLib.h"
#include "global.h"
#include "common.h"
#include "savedata.h"
#include "error_code_define.h"
#include <fstream>
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")
#include <list>

#include <typeinfo>
#include <iostream>


extern char common_dir[256];
extern bool is_hyper_skip;
using namespace Teng;

pair<int, string> Scene::gallery_files[TENG_GALLERY_MAX_NUMBER];

const DWORD Teng::Scene::SAVEDATA_CONFIRM_DIALOG_COLOR = DxLib::GetColor(255, 233, 199);
const DWORD Teng::Scene::SAVEDATA_CONFIRM_MSG_COLOR = DxLib::GetColor(53, 24, 4);
const DWORD Teng::Scene::SAVEDATA_STRING_DISABLED = DxLib::GetColor(130, 119, 92);
const DWORD Teng::Scene::SAVEDATA_STRING_ENABLE = DxLib::GetColor(195, 191, 166);
const DWORD Teng::Scene::SAVEDATA_STRING_FOCUS = DxLib::GetColor(255, 227, 159);


Teng::Scene::Scene():
    is_backlog_enable(false),
	frame(0),
	now_scene(eScene_logo),
	before_scene(eScene_none),
	loaded(false),
	auto_frame(0),
	wait_frame(0),
	scene_changing(false),
	gallery_page_num(1),
	movie_playing(false),
	movie_fading(false),
	saveload_pagenum(1),
	saveload_before_pagenum(1),
	saveload_confirm_fadein(false),
	saveload_confirm_fadeout(false),
	saveload_confirm_fadeout_reverse(false),
	keep_load_number(-1),
	hyperskip_doing(false),
	is_effect_do(false),
	effect_type("stop")
{
	strcpy_s(window_title, 128, TENG_DEFAULT_WINDOW_TITLE);
	makeGalleryFilesList();
}

void Teng::Scene::afterInit(){
	color_black = DxLib::GetColor(0, 0, 0);
	color_white = DxLib::GetColor(255, 255, 255);
	saveload_confirm_dialog = DxLib::MakeScreen(1024, SAVEDATA_CONFIRM_DIALOG_HEIGHT, TRUE);
	//saveload_dialog_back = LoadGraph("C:\\projects\\at_engine\\save_dialog_back.png");
	char fullpath[512];
	sprintf_s(fullpath, "%s%s%s", common_dir, TENG_DIR_SYSTEM_IMAGE, "saveload_confirm_dummy.png");
	saveload_dialog_dummy = LoadGraph(fullpath);
	tmp_screen = DxLib::MakeScreen(1024, 768);
}

Teng::Scene::~Scene(){

}

void Teng::Scene::addFrame(){
	if (frame < ULONG_MAX){
		frame++;
	}
	else{
		frame = 0;
	}
}

unsigned long Teng::Scene::getFrame(){
	return frame;
}


void Teng::Scene::drawScreen(Teng::GraphicManager* graphic_manager, Teng::Scenario* scenario, Teng::CmdMessage* message, Teng::Config* config, Teng::Input* input, Teng::SoundManager* s_manager, Teng::Window* window){
	ULONG trans_frame = 255;
	switch (now_scene){
	case eScene_scenario:


		// 右クリックメニュー→画面切り替え時に一瞬シナリオが見える問題対策
		if (clicked_button != eScene_scenario_save && clicked_button != eScene_scenario_load){
			graphic_manager->drawBackGroundGraphic(input);
			graphic_manager->drawCharacterGraphic(input);

			// 背景エフェクトは背景＋立ち絵のみ

			bool is_nexttag_do = doBackEffect(graphic_manager, input);
			message->setForceClickFlg(is_nexttag_do);


			if (!message->getMsgWindowHideFlg() && !graphic_manager->getTransitionDoing()){


				//DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, message->getMsgWindowTransFrame());
				////メッセージウィンドウ
				//DxLib::DrawGraph(
				//	graphic_manager->system_graphics[Teng::SystemGraphic::arrnum_msgwindow].getX(),
				//	graphic_manager->system_graphics[Teng::SystemGraphic::arrnum_msgwindow].getY(),
				//	graphic_manager->system_graphics[Teng::SystemGraphic::arrnum_msgwindow].getHandle(),
				//	TRUE);
				//DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

				graphic_manager->drawSystemGraphic(now_scene);
				graphic_manager->drawFaceGraphic();
				//message->update(frame, config, scenario);
				//message->render(TENG_MESSAGE_FULL_WIDTH, TENG_MESSAGE_FONT_SIZE, graphic_manager, scenario);
			}


		}
		//シナリオ画面でロードボタンを押した
		if (scene_changing && clicked_button == eScene_scenario_load){
			trans_frame = 255;
			if (process_status == 2 && DxLib::GetASyncLoadNum() == 0) decodeSavedataThumbnail2(config, input, graphic_manager, s_manager);
			if (process_status == 3 && DxLib::GetASyncLoadNum() == 0) decodeSavedataThumbnail3(config, input, graphic_manager, s_manager);
			if (process_status == 4 && DxLib::GetASyncLoadNum() == 0) decodeSavedataThumbnail4(config, input, graphic_manager, s_manager);
			//シナリオ→ロード フェードアウト
			if (frame < 30){
				trans_frame = 255 - frame * 8;
				DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);
			}
			//シナリオ→ロード フェードアウト完了直後
			else{
				if (process_status == 5){
					before_scene = eScene_scenario;
					now_scene = eScene_scenario_load;
					frame = 0;
					resetTitleButtonFlag();
				}
				DxLib::SetDrawBright(0, 0, 0);
			}
		}
		//シナリオ画面でセーブボタンを押した
		else if (scene_changing && clicked_button == eScene_scenario_save){
			trans_frame = 255;
			if (process_status == 2 && DxLib::GetASyncLoadNum() == 0) decodeSavedataThumbnail2(config, input, graphic_manager, s_manager);
			if (process_status == 3 && DxLib::GetASyncLoadNum() == 0) decodeSavedataThumbnail3(config, input, graphic_manager, s_manager);
			if (process_status == 4 && DxLib::GetASyncLoadNum() == 0) decodeSavedataThumbnail4(config, input, graphic_manager, s_manager);
			//シナリオ→セーブ フェードアウト
			if (frame < 30){
				trans_frame = 255 - frame * 8;
				DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);
			}
			//シナリオ→セーブ フェードアウト完了直後
			else{
				if (process_status == 5 && DxLib::GetASyncLoadNum() == 0){
					before_scene = eScene_scenario;
					now_scene = eScene_scenario_save;
					frame = 0;
					resetTitleButtonFlag();
				}
				DxLib::SetDrawBright(0, 0, 0);
			}
		}
		//ロード画面で戻るを押した（シナリオ画面フェードイン）
		else if (scene_changing && before_scene == eScene_scenario_load){
			trans_frame = 0;
			if (frame < 30){
				trans_frame = frame * 8;
			}
			//トラン完了直後
			else{
				before_scene = eScene_scenario;
				now_scene = eScene_scenario;
				frame = 0;
				scene_changing = false;
				trans_frame = 255;
				clicked_button = -1;
			}
			DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);
		}
		//セーブ画面で戻るを押した（シナリオ画面フェードイン）
		else if (scene_changing && before_scene == eScene_scenario_save){
			trans_frame = 0;
			if (frame < 30){
				trans_frame = frame * 8;
			}
			//トラン完了直後
			else{
				before_scene = eScene_scenario;
				now_scene = eScene_scenario;
				frame = 0;
				scene_changing = false;
				trans_frame = 255;
				clicked_button = -1;
			}
			DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);
		}
		//ハイパースキップ開始
		else if (scene_changing && clicked_button == eScene_hyperskip){
			// ハイパースキップ処理（ループ）はdoScenarioに依存せず、doHyperSkipで行う
			if (frame < 30){
				trans_frame = 255 - frame * 8;
				DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);
			}
			//シナリオ→セーブ フェードアウト完了直後
			else{
				before_scene = eScene_scenario;
				now_scene = eScene_hyperskip;
				frame = 0;
				hyperskip_doing = true;
				is_hyper_skip = true;
				resetTitleButtonFlag();
				DxLib::SetDrawBright(0, 0, 0);
				clicked_button = -1;
			}
		}
		//ハイパースキップ完了後、シナリオ再開
		else if (scene_changing && before_scene == eScene_hyperskip){
			trans_frame = 0;
			if (frame < 30){
				trans_frame = frame * 8;
			}
			//トラン完了直後
			else{
				//is_hyper_skip = true;
				//scenario->reloadHyperSkip(scenario, message, graphic_manager, s_manager, window, config, this);
				graphic_manager->setTransitionDoing(false);
				before_scene = eScene_scenario;
				now_scene = eScene_scenario;
				frame = 0;
				scene_changing = false;
				trans_frame = 255;
				clicked_button = -1;
			}
			DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);
		}
		else if (scene_changing && before_scene == eScene_scenario_config){
			trans_frame = 0;
			if (frame < 30){
				trans_frame = frame * 8;
			}
			//トラン完了直後
			else{
				before_scene = eScene_scenario;
				now_scene = eScene_scenario;
				frame = 0;
				scene_changing = false;
				trans_frame = 255;
				clicked_button = -1;
				resetTitleButtonFlag();
			}
			DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);
		}

		break;
	case eScene_hyperskip:
		//if (hyperskip_doing){
		//	//ハイパースキップ処理
		//	hyperskip_doing = false;
		//}
		//else{
			//シナリオ画面に戻る
			frame = 0;
			before_scene = eScene_hyperskip;
			now_scene = eScene_scenario;
			scene_changing = true;
		//}

		break;
	case eScene_logo:
		graphic_manager->drawSystemGraphic(now_scene);
		break;
	case eScene_title:
		//タイトルからギャラリーへ遷移
		trans_frame = 255;


		// ロゴからタイトルへ（真っ黒からフェードイン）
		if (scene_changing && before_scene == eScene_logo){
			int trans_frame = frame * 8;
			DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);
			if (trans_frame >= 255) scene_changing = false;
		}


		//回想ボタンを押した
		else if (scene_changing && before_scene == eScene_title && button_4_clicked){
			//トラン中
			if (frame < 30){
				trans_frame = 255 - frame * 8;
			}
			//トラン完了後
			else{
				before_scene = eScene_title;
				now_scene = eScene_gallery;
				//DxLib::SetDrawBright(255, 255, 255);
				trans_frame = 255 - frame * 8;
				frame = 0;
				gallery_page_num = 1;
				gallery_large_mode = false;
			}
			DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);
		}
		//コンフィグからタイトルへ戻るとき
		else if (scene_changing && before_scene == eScene_title_config){
			//トラン中
			if (frame < 30){
				trans_frame = frame * 8;
			}
			//トラン完了後
			else{
				scene_changing = false;
				clicked_button = -1;
				frame = 0;
				trans_frame = 255;
				before_scene = eScene_title;
				now_scene = eScene_title;
			}
			DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);

		}
		//ギャラリーからタイトルへ戻るとき
		else if (scene_changing && before_scene == eScene_gallery){
			//トラン中
			if (frame < 30){
				trans_frame = frame * 8;
			}
			//トラン完了後
			else{
				scene_changing = false;
				clicked_button = -1;
				frame = 0;
				trans_frame = 255;
				before_scene = eScene_title;
				now_scene = eScene_title;
			}
			DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);

		}
		// 右クリックメニューからタイトルへ戻るとき
		else if (scene_changing && before_scene == eScene_scenario_menu){
			//トラン中
			if (frame < 30){
				trans_frame = frame * 8;
			}
			//トラン完了後
			else{
				scene_changing = false;
				clicked_button = -1;
				frame = 0;
				trans_frame = 255;
				before_scene = eScene_title;
				now_scene = eScene_title;
			}
			DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);

		}
		//タイトル→設定フェードアウト
		else if (scene_changing && before_scene == eScene_title && button_3_clicked){
			//フェードアウト中
			if (frame < 80){
				trans_frame = 255 - frame * 5;
				int volume = 255;//ここの値を変えたら、Window::drawCloseDialog()内での値を調整すること
				//static int bgm_handle = s_manager->system_sound[SystemSound::arrnum_bgm_title].getHandle();
				//double vol =
				//	(double)(config->getSetting().master_volume) *
				//	(double)(config->getSetting().bgm_volume) / 65025 * volume;
				//DxLib::ChangeVolumeSoundMem(vol - (int)frame * 2, bgm_handle);
				input->disableAllKeys();
			}
			//フェードアウト完了後、設定へ
			else{
				trans_frame = 0;
				before_scene = eScene_title;
				now_scene = eScene_title_config;
				this->sceneConfigInitialize(window, config, input, graphic_manager, s_manager);
				//DxLib::StopSoundMem(s_manager->system_sound[SystemSound::arrnum_bgm_title].getHandle());
				frame = 0;
				scene_changing = true;
			}
			DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);
		}
		//タイトル→ロードフェードアウト
		else if (scene_changing && before_scene == eScene_title && button_2_clicked){
			//サムネイルデコードの非同期版
			if (process_status == 2 && DxLib::GetASyncLoadNum() == 0) decodeSavedataThumbnail2(config, input, graphic_manager, s_manager);
			if (process_status == 3 && DxLib::GetASyncLoadNum() == 0) decodeSavedataThumbnail3(config, input, graphic_manager, s_manager);
			if (process_status == 4 && DxLib::GetASyncLoadNum() == 0) decodeSavedataThumbnail4(config, input, graphic_manager, s_manager);
			//フェードアウト中
			if (frame < 80){
				trans_frame = 255 - frame * 5;
				int volume = 255;//ここの値を変えたら、Window::drawCloseDialog()内での値を調整すること
				static int bgm_handle = s_manager->system_sound[SystemSound::arrnum_bgm_title].getHandle();
				double vol =
					(double)(config->getSetting().master_volume) *
					(double)(config->getSetting().bgm_volume) / 65025 * volume;
				DxLib::ChangeVolumeSoundMem(vol - (int)frame * 2, bgm_handle);
			}
			//フェードアウト完了後
			else{
				trans_frame = 0;
				//非同期デコードが完了するまで待つ
				if (process_status == 5){
					trans_frame = 0;
					before_scene = eScene_title;
					now_scene = eScene_title_load;
					DxLib::StopSoundMem(s_manager->system_sound[SystemSound::arrnum_bgm_title].getHandle());
					frame = 0;
					clicked_button = -1;
					scene_changing = true;
					resetTitleButtonFlag();
				}
			}
			DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);
		}
		//タイトル→シナリオスタート（フェードアウト）
		else if (scene_changing && before_scene == eScene_title && button_1_clicked){
			//フェードアウト中
			if (frame < 80){
				trans_frame = 255 - frame * 5;
				int volume = 255;//ここの値を変えたら、Window::drawCloseDialog()内での値を調整すること
				static int bgm_handle = s_manager->system_sound[SystemSound::arrnum_bgm_title].getHandle();
				double vol =
					(double)(config->getSetting().master_volume) *
					(double)(config->getSetting().bgm_volume) / 65025 * volume;
				DxLib::ChangeVolumeSoundMem(vol - (int)frame * 2, bgm_handle);
			}
			//フェードアウト完了後、シナリオへ
			else{
				trans_frame = 255;
				before_scene = eScene_title;
				now_scene = eScene_scenario;
				DxLib::StopSoundMem(s_manager->system_sound[SystemSound::arrnum_bgm_title].getHandle());
				scene_changing = false;
				clicked_button = -1;
			}
			DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);
		}
		//ロード→タイトル（フェードイン）
		else if (scene_changing && before_scene == eScene_title_load){
			//トラン中
			if (frame < 30){
				trans_frame = frame * 8;
			}
			//トラン完了後
			else{
				scene_changing = false;
				frame = 0;
				trans_frame = 255;
				before_scene = eScene_title;
				now_scene = eScene_title;
			}
			DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);
		}
		//通常表示
		else{
			DxLib::SetDrawBright(255, 255, 255);
		}
		graphic_manager->drawSystemGraphic(now_scene);
		break;
	case eScene_gallery:
		//画像のロード待機
		while (DxLib::GetASyncLoadNum() > 0){
			DxLib::ProcessMessage();
			Sleep(1);
		}

		trans_frame = 0;
		//タイトルから遷移した直後
		if (scene_changing && before_scene == eScene_title){
			//トラン中
			if (frame < 30){
				trans_frame = frame * 8;
			}
			//トラン完了後
			else{
				scene_changing = false;
				clicked_button = -1;
				frame = 0;
				trans_frame = 255;
			}
			DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);
		}
		//回想からタイトルへ戻る
		else if (scene_changing && before_scene == eScene_gallery){
			//トラン中
			if (frame < 30){
				trans_frame = 255 - frame * 8;
			}
			//トラン完了後
			else{
				before_scene = eScene_gallery;
				now_scene = eScene_title;
				trans_frame = 255 - frame * 8;
				frame = 0;
				resetTitleButtonFlag();
				//タイトルにて明るくするトランを実行
			}
			DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);
		}
		//通常表示
		else{
			DxLib::SetDrawBright(255, 255, 255);
		}

		graphic_manager->drawSystemGraphic(eScene_gallery);
		drawPictures(config, input, graphic_manager);
		break;
	case eScene_scenario_load:
		trans_frame = 0;
		//シナリオからロード画面に来た
		if (scene_changing && before_scene == eScene_scenario && clicked_button != eScene_scenario){
			if (frame < 30){
				trans_frame = frame * 8;
			}
			//トラン完了直後
			else{
				scene_changing = false;
				clicked_button = -1;
				frame = 0;
				trans_frame = 255;
			}
			DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);
		}
		//ロード画面からシナリオへ（フェードアウト）
		else if (scene_changing && clicked_button == eScene_scenario){
			if (frame < 40){
				trans_frame = 255 - frame * 8;
			}
			//トラン完了直後
			else{
				frame = 0;
				before_scene = eScene_scenario_load;
				now_scene = eScene_scenario;
				clicked_button = -1;
				if (process_status == 9){
					Teng::SaveData *savedata = new Teng::SaveData;
					// シナリオからのロード
					savedata->load(keep_load_number, scenario, message, graphic_manager, s_manager, window, config, this);
					delete savedata;
					keep_load_number = -1;
					process_status = 0;
					for (int i = 0; i < TENG_SAVE_SAVEDATA_MAX; i++){
						DxLib::DeleteGraph(Teng::SaveData::getThumbnailHandle(i));
						Teng::SaveData::setThumbnailHandle(i, -1);
					}
				}
			}
			DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);
		}
		break;
	case eScene_title_load:
		trans_frame = 0;
		if (scene_changing && before_scene == eScene_title && clicked_button != eScene_title){
			//トラン中
			if (frame < 30){
				trans_frame = frame * 8;
			}
			//トラン完了後
			else{
				scene_changing = false;
				clicked_button = -1;
				frame = 0;
				trans_frame = 255;
			}
			DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);
		}
		//ロードからタイトルに向けてフェードアウト
		else if (scene_changing && clicked_button == eScene_title){// before_scene == eScene_title_load){
			//トラン中
			if (frame < 30){
				trans_frame = 255 - frame * 8;
			}
			//トラン完了直後
			else{
				frame = 0;
				trans_frame = 0;
				now_scene = eScene_title;
				before_scene = eScene_title_load;
				//サムネイル削除
				for (int i = 0; i < TENG_SAVE_SAVEDATA_MAX; i++){
					DxLib::DeleteGraph(Teng::SaveData::getThumbnailHandle(i));
					Teng::SaveData::setThumbnailHandle(i, -1);
				}
			}
			DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);
		}
		else if (scene_changing && clicked_button == eScene_scenario){
			if (frame < 40){
				trans_frame = 255 - frame * 8;
			}
			//トラン完了直後
			else{
				frame = 0;
				before_scene = eScene_scenario_load;
				now_scene = eScene_scenario;
				clicked_button = -1;
				if (process_status == 9){
					Teng::SaveData *savedata = new Teng::SaveData;
					// タイトルからのロード
					savedata->load(keep_load_number, scenario, message, graphic_manager, s_manager, window, config, this);
					delete savedata;
					keep_load_number = -1;
					process_status = 0;
					for (int i = 0; i < TENG_SAVE_SAVEDATA_MAX; i++){
						DxLib::DeleteGraph(Teng::SaveData::getThumbnailHandle(i));
						Teng::SaveData::setThumbnailHandle(i, -1);
					}
				}
			}
			DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);
		}
		break;
	case eScene_scenario_menu:

		////シナリオ画面でセーブボタンを押した
		//if (scene_changing && clicked_button == eScene_scenario_save){
		//	trans_frame = 255;
		//	if (process_status == 2 && DxLib::GetASyncLoadNum() == 0) decodeSavedataThumbnail2(config, input, graphic_manager, s_manager);
		//	if (process_status == 3 && DxLib::GetASyncLoadNum() == 0) decodeSavedataThumbnail3(config, input, graphic_manager, s_manager);
		//	if (process_status == 4 && DxLib::GetASyncLoadNum() == 0) decodeSavedataThumbnail4(config, input, graphic_manager, s_manager);
		//	//シナリオ→セーブ フェードアウト
		//	if (frame < 30){
		//		trans_frame = 255 - frame * 8;
		//		DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);
		//	}
		//	//シナリオ→セーブ フェードアウト完了直後
		//	else{
		//		if (process_status == 5 && DxLib::GetASyncLoadNum() == 0){
		//			before_scene = eScene_scenario;
		//			now_scene = eScene_scenario_save;
		//			frame = 0;
		//			resetTitleButtonFlag();
		//		}
		//		DxLib::SetDrawBright(0, 0, 0);
		//	}
		//}
		break;
	case eScene_scenario_save:
		trans_frame = 0;
		//シナリオからセーブ画面に来た
		if (scene_changing && before_scene == eScene_scenario && clicked_button != eScene_scenario){
			if (frame < 30){
				trans_frame = frame * 8;
			}
			//トラン完了直後
			else{
				scene_changing = false;
				clicked_button = -1;
				frame = 0;
				trans_frame = 255;
			}
			DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);
		}
		//セーブ画面からシナリオへ（フェードアウト）
		else if (scene_changing && clicked_button == eScene_scenario){
			if (frame < 30){
				trans_frame = 255 - frame * 8;
			}
			//トラン完了直後
			else{
				frame = 0;
				before_scene = eScene_scenario_save;
				now_scene = eScene_scenario;
				clicked_button = -1;
			}
			DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);
		}
		break;
	case eScene_title_config:
		trans_frame = 0;
		//タイトルからフェードアウトした後、設定画面をフェードイン
		if (scene_changing && button_3_clicked){
			if (frame < 80){
				trans_frame = frame * 5;
				//input->disableAllKeys();
			}
			else{
				trans_frame = 255;
				before_scene = eScene_title; //設定画面では、OK/キャンセルボタンでnow/beforeを入れ替えるため注意
				now_scene = eScene_title_config;
				scene_changing = false;
				resetTitleButtonFlag();
			}
			DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);
		}
		break;
	//case eScene_scenario_config:
	//	trans_frame = 0;
	//	//シナリオからフェードアウトした後、設定画面をフェードイン
	//	if (scene_changing){
	//		if (frame < 80){
	//			trans_frame = frame * 5;
	//			//input->disableAllKeys();
	//		}
	//		else{
	//			trans_frame = 255;
	//			before_scene = eScene_scenario; //設定画面では、OK/キャンセルボタンでnow/beforeを入れ替えるため注意
	//			now_scene = eScene_scenario_config;
	//			scene_changing = false;
	//			resetTitleButtonFlag();
	//		}
	//		DxLib::SetDrawBright(trans_frame, trans_frame, trans_frame);
	//	}
	//	break;
	default:
		break;
	}


}


//タイトル画面のボタン押下色状態のリセット
//タイトルに遷移するときは、必ずこれを呼ぶこと
void Scene::resetTitleButtonFlag(){
	button_1_clicked = false;
	button_2_clicked = false;
	button_3_clicked = false;
	button_4_clicked = false;
	button_5_clicked = false;
	cursor_on_button = false;
	saveload_cursor_is_hand = false;
	is_dialog_draw = false;
	saveload_page_changing = false;
}

//セーブデータのサムネイルを表示できる形に展開、サムネイルハンドルをセット
void Teng::Scene::decodeSavedataThumbnail(
	Window* window,
	Config* config,
	Input* input,
	GraphicManager* g_manager,
	SoundManager* s_manager)
{
	//DxLib::SetUseASyncLoadFlag(FALSE);
	//サムネイルの復元
	for (int i = 0; i < TENG_SAVE_SAVEDATA_MAX; i++){
		char savefile[TENG_FILE_NAME_MAX];
		sprintf_s(savefile, TENG_FILE_NAME_MAX, TENG_SAVE_FILENAME, i);

		if (SaveData::getSavedataExist(i)){
			LONGLONG filesize = DxLib::FileRead_size(savefile);
			void *buffer = malloc((size_t)filesize);
			int filehandle = DxLib::FileRead_open(savefile);
			while (DxLib::CheckHandleASyncLoad(filehandle) != FALSE){
				DxLib::ProcessMessage();
				DEBUG printf("[INFO] wait(FileRead_open)\n");
				Sleep(10);
			}
			DxLib::FileRead_read(buffer, (int)filesize, filehandle);
			while (DxLib::CheckHandleASyncLoad(filehandle) != FALSE){
				DxLib::ProcessMessage();
				DEBUG printf("[INFO] wait(FileRead_read)\n");
				Sleep(10);
			}
			DxLib::FileRead_close(filehandle);
			Teng::SaveDataStruct *data = (Teng::SaveDataStruct *)buffer;
			int img_handle = DxLib::CreateGraphFromMem(data->thumbnail, sizeof(data->thumbnail));

			Teng::SaveData::setThumbnailHandle(i, img_handle);


			DEBUG printf("[INFO] %s:成功\n", savefile);
		}
		else{
			//DEBUG printf("[INFO] %s:存在しません\n", savefile);
		}

	}
	//DxLib::SetUseASyncLoadFlag(TRUE);

}

//クリックした箇所のみデコードする
void Teng::Scene::decodeSavedataThumbnailOnlyClicked(
	Window* window,
	Config* config,
	Input* input,
	GraphicManager* g_manager,
	SoundManager* s_manager,
	int number)
{
	DxLib::SetUseASyncLoadFlag(TRUE);
	char savefile[TENG_FILE_NAME_MAX];
	sprintf_s(savefile, TENG_FILE_NAME_MAX, TENG_SAVE_FILENAME, number);
	if (SaveData::getSavedataExist(number)){
		LONGLONG filesize = DxLib::FileRead_size(savefile);
		void *buffer = malloc((size_t)filesize);
		int filehandle = DxLib::FileRead_open(savefile, TRUE);
		while (DxLib::CheckHandleASyncLoad(filehandle) != FALSE){
			DxLib::WaitTimer(10);
		}
		DxLib::FileRead_read(buffer, (int)filesize, filehandle);

		while (DxLib::CheckHandleASyncLoad(filehandle) != FALSE){
			DxLib::WaitTimer(10);
		}


		char* c_save = reinterpret_cast<char*>(buffer);
		for (int i = 0; i < sizeof(SaveDataStruct); i++){
			//c_save[i] = c_save[i] ^ TENG_SAVEDATA_PASSWORD;
		}
		memcpy(buffer, c_save, sizeof(SaveDataStruct));


		DxLib::FileRead_close(filehandle);
		Teng::SaveDataStruct *data = (Teng::SaveDataStruct *)buffer;
		int img_handle = DxLib::CreateGraphFromMem(data->thumbnail, sizeof(data->thumbnail));

		Teng::SaveData::setThumbnailHandle(number, img_handle);

		DEBUG printf("[INFO] %s:成功A\n", savefile);
	}
	DxLib::SetUseASyncLoadFlag(FALSE);

}



void Teng::Scene::decodeSavedataThumbnail1(
	Window* window,
	Config* config,
	Input* input,
	GraphicManager* g_manager,
	SoundManager* s_manager)
{
	DxLib::SetUseASyncLoadFlag(TRUE);
	//サムネイルの復元
	for (int i = 1; i < TENG_SAVE_SAVEDATA_MAX; i++){
		//ファイル名を生成

		string path = (string(Config::savedata_dir) + string("data_1.sav"));
		sprintf_s(decode_savefile[i], TENG_FILE_NAME_MAX, TENG_SAVE_FILENAME, i);
		if (SaveData::getSavedataExist(i)){
			decode_filesize[i] = DxLib::FileRead_size(decode_savefile[i]);
			decode_buffer[i] = (char *)malloc((size_t)decode_filesize[i]);
			decode_filehandle[i] = DxLib::FileRead_open(decode_savefile[i], TRUE);
		}
	}
	process_status = 2;
}

void Teng::Scene::decodeSavedataThumbnail2(
	Config* config,
	Input* input,
	GraphicManager* g_manager,
	SoundManager* s_manager)
{
	for (int i = 1; i < TENG_SAVE_SAVEDATA_MAX; i++){
		if (SaveData::getSavedataExist(i)){
			DxLib::FileRead_read(decode_buffer[i], (int)decode_filesize[i], decode_filehandle[i]);
		}
	}
			//while (GetASyncLoadNum() != 0){
			//	WaitTimer(10);
			//}
	process_status = 3;
}

void Teng::Scene::decodeSavedataThumbnail3(
	Config* config,
	Input* input,
	GraphicManager* g_manager,
	SoundManager* s_manager)
{
	DxLib::SetUseASyncLoadFlag(TRUE);
	for (int i = 1; i < TENG_SAVE_SAVEDATA_MAX; i++){
		if (SaveData::getSavedataExist(i)){
			
			char* c_save = reinterpret_cast<char*>(decode_buffer[i]);
			
			for (int j = 0; j < sizeof(SaveDataStruct); j++){
				//c_save[j] = c_save[j] ^ TENG_SAVEDATA_PASSWORD;
			}
			memcpy(decode_buffer[i], c_save, sizeof(SaveDataStruct));

			//if (decode_filehandle[i] > 0){
				DxLib::FileRead_close(decode_filehandle[i]);
				Teng::SaveDataStruct *data = (Teng::SaveDataStruct *)decode_buffer[i];
				decode_img_handle[i] = DxLib::CreateGraphFromMem(data->thumbnail, sizeof(data->thumbnail));
			//}
		}
	}
	process_status = 4;
}

void Teng::Scene::decodeSavedataThumbnail4(
	Config* config,
	Input* input,
	GraphicManager* g_manager,
	SoundManager* s_manager)
{
	//DxLib::SetUseASyncLoadFlag(TRUE);
	for (int i = 1; i < TENG_SAVE_SAVEDATA_MAX; i++){
		if (SaveData::getSavedataExist(i)){
			Teng::SaveData::setThumbnailHandle(i, decode_img_handle[i]);
			try{
				//free(decode_buffer[i]);
				DEBUG printf("[INFO] %s:成功\n", decode_savefile[i]);
			}
			catch (exception e){
				DEBUG printf("%s", e.what());
			}
		}
	}
	process_status = 5;
}
void Teng::Scene::doScenarioLoad(
	Window* window,
	Config* config,
	Input* input,
	GraphicManager* g_manager,
	SoundManager* s_manager,
	Scenario* scenario,
	CmdMessage* message)
{
	doLoadCommon(window, config, input, g_manager, s_manager, scenario, message);

}

void Teng::Scene::doTitleLoad(
	Window* window,
	Config* config,
	Input* input,
	GraphicManager* g_manager,
	SoundManager* s_manager,
	Scenario* scenario,
	CmdMessage* message)
{
	doLoadCommon(window, config, input, g_manager, s_manager, scenario, message);

}


void Teng::Scene::doScenarioConfig(
	Window* window,
	Config* config,
	Input* input,
	GraphicManager* g_manager,
	SoundManager* s_manager)
{
	doConfigCommon(window, config, input, g_manager, s_manager);
}

void Teng::Scene::doTitleConfig(
	Window* window,
	Config* config,
	Input* input,
	GraphicManager* g_manager,
	SoundManager* s_manager)
{
	doConfigCommon(window, config, input, g_manager, s_manager);
}


//オートモードの進行判定
bool Teng::Scene::checkAutoMsg(Config* config, Input* input){
	if (loop_flg) return true;

	//checkAutoMsgの1回めはリセット無視、2回めのみリセット
	static bool check_flg = false;
	//オートモードONのとき
	if (config->getAutoMsgFlag()){
		int auto_spd = 11 - config->getSetting().auto_message_wait;

		if (auto_frame > (unsigned long)auto_spd * 30){
			if (check_flg){
				auto_frame = 0;
				check_flg = false;
				return true;
			}
			check_flg = true;
			return true;
		}
		else{
			return false;
		}

	}
	else{
		auto_frame = 0;
		return false;
	}

	return false;
}

const int Teng::Scene::getThumbnailX(const int& data_in_page){
	if (data_in_page < 0) return -1;
	switch (data_in_page % 4){
	case 1: return SAVEDATA_X_1;
	case 2: return SAVEDATA_X_2;
	case 3: return SAVEDATA_X_3;
	case 0: return SAVEDATA_X_4;
	default: return -1;
	}
}


const int Teng::Scene::getThumbnailY(const int& data_in_page){
	if (data_in_page == 0)  return SAVEDATA_Y_3;
	if (data_in_page <= 4)  return SAVEDATA_Y_1;
	if (data_in_page <= 8)  return SAVEDATA_Y_2;
	if (data_in_page <= 12) return SAVEDATA_Y_3;
	return -1;
}
