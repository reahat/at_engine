#include "scene.h"
#include "scenario.h"
#include "DxLib.h"
#include "global.h"
#include "common.h"
#include "savedata.h"
#include "error_code_define.h"
#include <fstream>
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")
#include <list>

#include <typeinfo>
#include <iostream>


extern char common_dir[256];
extern bool is_hyper_skip;
using namespace Teng;

void Teng::Scene::doTitle(Window* window, Config* config, Input* input, GraphicManager* g_manager, SoundManager* s_manager){
	const int BUTTON_X = 14;
	//const int BUTTON_Y_1 = 250;
	//const int BUTTON_Y_2 = 295;
	//const int BUTTON_Y_3 = 335;
	//const int BUTTON_Y_4 = 380;
	//const int BUTTON_Y_5 = 427;
	const int BUTTON_Y_1 = 287;
	const int BUTTON_Y_2 = 331;
	const int BUTTON_Y_3 = 374;
	const int BUTTON_Y_4 = 416;
	const int BUTTON_Y_5 = 461;
	static bool doing_exit_flg = false;





	//BGM再生
	static int bgm_handle = s_manager->system_sound[SystemSound::arrnum_bgm_title].getHandle();
	int volume = 255;//ここの値を変えたら、Window::drawCloseDialog()内での値を調整すること
	double vol =
		(double)(config->getSetting().master_volume) *
		(double)(config->getSetting().bgm_volume) / 65025 * volume;
	DxLib::ChangeVolumeSoundMem((int)vol, bgm_handle);
	if (DxLib::CheckSoundMem(bgm_handle) == 0){
		DxLib::SetSoundCurrentTime(1200, bgm_handle);
		DxLib::PlaySoundMem(bgm_handle, DX_PLAYTYPE_LOOP, FALSE);
	}

	double vol_focus =
		(double)(config->getSetting().master_volume) *
		(double)(config->getSetting().sound_volume) / 65025 * 255;
	DxLib::ChangeVolumeSoundMem(vol_focus, s_manager->system_sound[SystemSound::arrnum_se_button_title_focus].getHandle());
	double vol_click =
		(double)(config->getSetting().master_volume) *
		(double)(config->getSetting().sound_volume) / 65025 * 255;
	DxLib::ChangeVolumeSoundMem(vol_click, s_manager->system_sound[SystemSound::arrnum_se_button_title_clicked].getHandle());

	//終了ボタンが押されたら
	if (doing_exit_flg){
		if (frame * 5 < 255){
			int bright = 255 - (int)frame * 5;
			DxLib::SetDrawBright(bright, bright, bright);
			int volume = 255;//ここの値を変えたら、Window::drawCloseDialog()内での値を調整すること
			double vol =
				(double)(config->getSetting().master_volume) *
				(double)(config->getSetting().bgm_volume) / 65025 * volume;
			DxLib::ChangeVolumeSoundMem(vol - (int)frame * 5, bgm_handle);
		}
		else{
			DEBUG printf("[INFO] 正常終了します。\n");
			throw ENGINE_SYSTEM_STANDARD_EXIT;
		}
	}

	//タイトル背景
	DxLib::DrawGraph(0, 0, g_manager->system_graphics[Teng::SystemGraphic::arrnum_bg_title].getHandle(), FALSE);

	window->setCursorType(LoadCursor(NULL, IDC_ARROW));
	SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());

	//シーン切り替え中はキー操作無効
	if (scene_changing){
		input->disableAllKeys();
	}
	//Common::putCursorPoint();
	bool in_button = false;
	//開始ボタン
	if (!button_1_clicked){
		if (Common::cursorIn(BUTTON_X, BUTTON_X + 129, BUTTON_Y_1, BUTTON_Y_1 + 31) && !scene_changing){
			in_button = true;
			window->setCursorType(LoadCursor(NULL, IDC_HAND));
			SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
			DxLib::DrawGraph(BUTTON_X, BUTTON_Y_1,
				g_manager->system_graphics[SystemGraphic::arrnum_button_title_start_focus].getHandle(), TRUE);
			if (!cursor_on_button){
				cursor_on_button = true;
				DxLib::PlaySoundMem(s_manager->system_sound[SystemSound::arrnum_se_button_title_focus].getHandle(), DX_PLAYTYPE_BACK);
			}
			if (input->getKeepMouseLeft() != 0){
				window->setCursorType(LoadCursor(NULL, IDC_ARROW));
				SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
				button_1_clicked = true;
				before_scene = eScene_title;
				now_scene = eScene_title;
				frame = 0;
				scene_changing = true;
				DxLib::PlaySoundMem(s_manager->system_sound[SystemSound::arrnum_se_button_title_clicked].getHandle(), DX_PLAYTYPE_BACK);




			}
		}
		else{
			DxLib::DrawGraph(BUTTON_X, BUTTON_Y_1,
				g_manager->system_graphics[SystemGraphic::arrnum_button_title_start_normal].getHandle(), TRUE);
		}
	}
	else{
		DxLib::DrawGraph(BUTTON_X, BUTTON_Y_1,
			g_manager->system_graphics[SystemGraphic::arrnum_button_title_start_click].getHandle(), TRUE);
	}
	//ロードボタン
	if (!button_2_clicked){
		if (Common::cursorIn(BUTTON_X, BUTTON_X + 125, BUTTON_Y_2, BUTTON_Y_2 + 30) && !scene_changing){
			in_button = true;
			window->setCursorType(LoadCursor(NULL, IDC_HAND));
			SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
			DxLib::DrawGraph(BUTTON_X, BUTTON_Y_2,
				g_manager->system_graphics[SystemGraphic::arrnum_button_title_load_focus].getHandle(), TRUE);
			if (!cursor_on_button){
				cursor_on_button = true;
				DxLib::PlaySoundMem(s_manager->system_sound[SystemSound::arrnum_se_button_title_focus].getHandle(), DX_PLAYTYPE_BACK);
			}
			if (input->getKeepMouseLeft() != 0){
				window->setCursorType(LoadCursor(NULL, IDC_ARROW));
				SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
				process_status = 1;
				button_2_clicked = true;
				before_scene = eScene_title;
				now_scene = eScene_title;
				scene_changing = true;
				frame = 0;
				saveload_pagenum = 1;
				saveload_page_changing = false;
				//DxLib::PlaySoundMem(s_manager->system_sound[SystemSound::arrnum_se_button_title_clicked].getHandle(), DX_PLAYTYPE_BACK);
				DEBUG printf("[INFO] セーブデータサムネイル展開\n");
				//サムネイルを表示できる形に復元・サムネイルハンドルをセット
				process_status = 1;
				decodeSavedataThumbnail1(window, config, input, g_manager, s_manager);
			}
		}
		else{
			DxLib::DrawGraph(BUTTON_X, BUTTON_Y_2,
				g_manager->system_graphics[SystemGraphic::arrnum_button_title_load_normal].getHandle(), TRUE);
		}
	}
	else{
		DxLib::DrawGraph(BUTTON_X, BUTTON_Y_2,
			g_manager->system_graphics[SystemGraphic::arrnum_button_title_load_click].getHandle(), TRUE);

	}
	//設定ボタン
	if (!button_3_clicked){
		if (Common::cursorIn(BUTTON_X, BUTTON_X + 148, BUTTON_Y_3, BUTTON_Y_3 + 38) && !scene_changing){
			in_button = true;
			window->setCursorType(LoadCursor(NULL, IDC_HAND));
			SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
			DxLib::DrawGraph(BUTTON_X, BUTTON_Y_3,
				g_manager->system_graphics[SystemGraphic::arrnum_button_title_config_focus].getHandle(), TRUE);
			if (!cursor_on_button){
				cursor_on_button = true;
				DxLib::PlaySoundMem(s_manager->system_sound[SystemSound::arrnum_se_button_title_focus].getHandle(), DX_PLAYTYPE_BACK);
			}
			if (input->getKeepMouseLeft() != 0){
				window->setCursorType(LoadCursor(NULL, IDC_ARROW));
				SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
				button_3_clicked = true;
				before_scene = eScene_title;
				now_scene = eScene_title;
				scene_changing = true;
				frame = 0;
				//DxLib::PlaySoundMem(s_manager->system_sound[SystemSound::arrnum_se_button_title_clicked].getHandle(), DX_PLAYTYPE_BACK);
			}
		}
		else{
			DxLib::DrawGraph(BUTTON_X, BUTTON_Y_3,
				g_manager->system_graphics[SystemGraphic::arrnum_button_title_config_normal].getHandle(), TRUE);
		}
	}
	else{
		DxLib::DrawGraph(BUTTON_X, BUTTON_Y_3,
			g_manager->system_graphics[SystemGraphic::arrnum_button_title_config_click].getHandle(), TRUE);

	}
	//回想ボタン
	//if (config->getSetting().draw_gallery_menu){
	//	if (!button_4_clicked){
	//		if (Common::cursorIn(BUTTON_X, BUTTON_X + 159, BUTTON_Y_4, BUTTON_Y_4 + 38) && !scene_changing){
	//			in_button = true;
	//			DxLib::DrawGraph(BUTTON_X, BUTTON_Y_4,
	//				g_manager->system_graphics[SystemGraphic::arrnum_button_title_gallery_focus].getHandle(), TRUE);
	//			window->setCursorType(LoadCursor(NULL, IDC_HAND));
	//			SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
	//			if (!cursor_on_button){
	//				cursor_on_button = true;
	//				DxLib::PlaySoundMem(s_manager->system_sound[SystemSound::arrnum_se_button_title_focus].getHandle(), DX_PLAYTYPE_BACK);
	//			}
	//			if (input->getKeepMouseLeft() != 0){
	//				window->setCursorType(LoadCursor(NULL, IDC_ARROW));
	//				SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
	//				loadPictures(g_manager);
	//				before_scene = eScene_title;
	//				scene_changing = true;
	//				frame = 0;
	//				button_4_clicked = true;
	//				//DxLib::PlaySoundMem(s_manager->system_sound[SystemSound::arrnum_se_button_title_clicked].getHandle(), DX_PLAYTYPE_BACK);
	//				//ギャラリー画像ロード（縮小・通常共に）

	//			}
	//			//ボタン押下後は矢印に戻す
	//			if (scene_changing){
	//				window->setCursorType(LoadCursor(NULL, IDC_ARROW));
	//				SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
	//			}
	//		}
	//		//ボタン外
	//		else{
	//			DxLib::DrawGraph(BUTTON_X, BUTTON_Y_4,
	//				g_manager->system_graphics[SystemGraphic::arrnum_button_title_gallery_normal].getHandle(), TRUE);
	//			//window->setCursorType(LoadCursor(NULL, IDC_ARROW));
	//			//SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
	//		}
	//	}
	//	else{
	//		DxLib::DrawGraph(BUTTON_X, BUTTON_Y_4,
	//			g_manager->system_graphics[SystemGraphic::arrnum_button_title_gallery_click].getHandle(), TRUE);
	//	}
	//}

	//終了ボタン
	if (!button_5_clicked){
		if (Common::cursorIn(BUTTON_X, BUTTON_X + 104, BUTTON_Y_5, BUTTON_Y_5 + 31) && !scene_changing){
			in_button = true;
			DxLib::DrawGraph(BUTTON_X, BUTTON_Y_5,
				g_manager->system_graphics[SystemGraphic::arrnum_button_title_exit_focus].getHandle(), TRUE);
			window->setCursorType(LoadCursor(NULL, IDC_HAND));
			SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
			if (!cursor_on_button){
				cursor_on_button = true;
				DxLib::PlaySoundMem(s_manager->system_sound[SystemSound::arrnum_se_button_title_focus].getHandle(), DX_PLAYTYPE_BACK);
			}
			if (input->getKeepMouseLeft() != 0){
				window->setCursorType(LoadCursor(NULL, IDC_ARROW));
				SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
				frame = 0;
				button_5_clicked = true;
				doing_exit_flg = true;
				scene_changing = true;
				now_scene = eScene_title;
				before_scene = eScene_title;
				//DxLib::PlaySoundMem(s_manager->system_sound[SystemSound::arrnum_se_button_title_clicked].getHandle(), DX_PLAYTYPE_BACK);
				// 終了ボタンが押された後のフェード処理は、この関数の上のほう
			}
			//ボタン押下後は矢印に戻す
			if (scene_changing){
				window->setCursorType(LoadCursor(NULL, IDC_ARROW));
				SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
			}
		}
		//ボタン外
		else{
			DxLib::DrawGraph(BUTTON_X, BUTTON_Y_5,
				g_manager->system_graphics[SystemGraphic::arrnum_button_title_exit_normal].getHandle(), TRUE);
			//window->setCursorType(LoadCursor(NULL, IDC_ARROW));
			//SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
		}
	}
	else{
		DxLib::DrawGraph(BUTTON_X, BUTTON_Y_5,
			g_manager->system_graphics[SystemGraphic::arrnum_button_title_exit_click].getHandle(), TRUE);
	}



	// 鳥かごボタン
	const int KAGO_X = 44;
	const int KAGO_Y = 673;
	const int KAGO_WIDTH = 63;
	const int KAGO_HEIGHT = 81;

	if (Common::cursorIn(KAGO_X, KAGO_X + KAGO_WIDTH, KAGO_Y, KAGO_Y + KAGO_HEIGHT)){
		DxLib::DrawGraph(KAGO_X, KAGO_Y, g_manager->system_graphics[SystemGraphic::arrnum_button_title_kago_focus].getHandle(), TRUE);
		window->setCursorType(LoadCursor(NULL, IDC_HAND));
		SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
		if (input->getKeepMouseLeft() != 0){
			ShellExecute(NULL, "open", "http://ccpie.jp/", NULL, NULL, SW_SHOWNORMAL);
		}
	}
	else{
		DxLib::DrawGraph(KAGO_X, KAGO_Y, g_manager->system_graphics[SystemGraphic::arrnum_button_title_kago_normal].getHandle(), TRUE);
	}

	if (!in_button){
		cursor_on_button = false;
	}

}

