﻿#include "scene.h"
#include "common.h"
#include "input.h"
#include <functional>
#include "tag/cmd_message.h"
#include "error_code_define.h"
#pragma once
extern char common_dir[256];

using namespace Teng;
using namespace std;

void Scene::doMovie(
	CmdMessage* message,
	Window* window,
	Config* config,
	Input* input,
	GraphicManager* g_manager,
	SoundManager* s_manager)
{
	DxLib::DrawGraph(0, 0, movie_handle, FALSE);
	WaitTimer(10);
	if (DxLib::GetMovieStateToGraph(movie_handle) != 1 || input->getKeepMouseLeft() != 0){
		DxLib::PauseMovieToGraph(movie_handle);
		DxLib::DeleteGraph(movie_handle);
		now_scene = eScene_scenario;
		before_scene = eScene_movie;
		frame = 0;
		movie_fading = true;
		message->clear();
	}


}

void Scene::setPlayMovie(CmdMessage* message, SoundManager* s_manager, const char* command){
	//暗転して、トラン完了したらシーンをmovieに移して再生
	movie_playing = true;
	movie_fading = true;
	message->setMsgWindowStatus(CmdMessage::eMsgWindow::MSG_WINDOW_TRANS_TYPE_FADEOUT);
	s_manager->setBgmFadePower(2.0);
	s_manager->isBgmFading(true);
	char movie_path[512];
	frame = 0;

	sprintf_s(movie_path, "%s%s%s", common_dir, TENG_DIR_MOVIE, command);

	movie_handle = DxLib::LoadGraph(movie_path);
	if (movie_handle == -1){
		throw ENGINE_MOVIE_LOAD_FAILURE;
	}
}

int Scene::playMovie(){

	return 0;
}

void Scene::checkFadeForMovie(CmdMessage* message, SoundManager* s_manager){
	//再生前のフェードアウト
	if (movie_fading && before_scene != eScene_movie){
		int draw_frame = 255 - (int)frame * 3;
		DxLib::SetDrawBright(draw_frame, draw_frame, draw_frame);
		if (draw_frame <= -150){
			movie_fading = false;
			s_manager->isBgmStopComplete(true);
		}
	}
	//フェードアウト完了後、動画再生する
	if (movie_playing && s_manager->isBgmStopComplete() && !movie_fading){
		now_scene = eScene_movie;
		before_scene = eScene_scenario;
		DxLib::SetDrawBright(255, 255, 255);
		if (DxLib::PlayMovieToGraph(movie_handle) == -1){
			throw ENGINE_MOVIE_PLAY_FAILURE;
		}
	}
	//再生後、画面を明るくする
	if (movie_fading && before_scene == eScene_movie){
		int draw_frame = (int)frame * 3;
		if (draw_frame > 255) draw_frame = 255;
		DxLib::SetDrawBright(draw_frame, draw_frame, draw_frame);
		if (draw_frame == 255){
			movie_fading = false;
			movie_playing = false;
			message->setMsgWindowStatus(CmdMessage::eMsgWindow::MSG_WINDOW_TRANS_TYME_FADEIN);
			s_manager->isBgmStopComplete(false);
			DxLib::DeleteGraph(movie_handle);
			frame = 0;
			//両方ともeScene_scenarioにしておかないと、2回目の動画再生に失敗する
			now_scene = eScene_scenario;
			before_scene = eScene_scenario;
		}
	}

}