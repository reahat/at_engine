﻿#ifndef GLOBAL_H_INCLUDED
#define GLOBAL_H_INCLUDED

#ifdef GLOBAL_VALUE_DEFINE
#define GLOBAL
#define GLOBAL_VAL(v) = (v)
#else
#define GLOBAL extern
#define GLOBAL_VAL(v)
#endif
#include "define.h"
#include "class_header.h"

//グローバル変数定義
//GLOBAL Teng::SystemGraphic system_graphics[TENG_GRAPHIC_SYSTEM_MAX];//システム画像

//カーソルがボタン上の場合などはクリックでシナリオが進まないようにする
GLOBAL bool cursor_on_button;
GLOBAL bool loop_flg;



#undef GLOBAL
#undef GLOBAL_VAL
#endif 