﻿//シナリオ管理クラス

//#include "class_header.h"
#pragma once

#include "input.h"
#include "define.h"
//#include "scene.h"
#include "tag\cmd_message.h"
#include <string>
#include <map>
#include "sound\sound_manager.h"
#include "savedata.h"
#include <vector>
using namespace std;
namespace Teng{
	class Scene;

	class Scenario{
	public:
		Scenario();
		
		bool getChangeTitleFlag(){ return change_title_flg; }
		//シナリオファイルのロード
		Scenario(char *file);

		virtual ~Scenario();
		int decodeScript(Teng::Input* input, Teng::Scene* scene, Teng::Config* config, Teng::CmdMessage* msg);

		//タグの実行を実際に行う
		//ループ可否を返す
		bool executeTag(
			Teng::Input* input,
			Teng::Scene* scn,
			Teng::CmdMessage* msg,
			Teng::GraphicManager* graphic,
			Teng::Scenario* subscenario,
			Teng::SoundManager* sound,
			Teng::Config* config);

		void splitString(int line);
		int tagToConstNum(char *cmd);

		//キーの入力状態および前フレームの状態を確認
		int checkInput(Teng::Input* input, Teng::Scene* scn);

		//getter
		int getCurrentLine();

		//Scenario::next_tagに次のタグ番号をセット
		void setNextTag(int tagnum);

		void importScenario(Teng::Input* input,
			Teng::Scene* scene,
			Teng::CmdMessage* msg,
			Teng::GraphicManager* graphic,
			char *importfile,
			Teng::Scenario* subscenario);

		void cmdJump(Teng::Input* input,
			Teng::Scene* scene,
			Teng::CmdMessage* msg,
			Teng::GraphicManager* graphic,
			char *importfile,
			Teng::Scenario* this_scenario);

		//@goto処理
		void gotoScenario(const char *cmdlabel);

		//@goto内など、引数のラベルを探索し行番号を返す
		int searchLabel(const char *cmdlabel);

		//タイトルに戻るタグ
		int sceneChange(const char* cmd_scene);

		//シナリオ内でボタンがクリックされたときの処理
		void checkButtonClick(
			Teng::Input* input, Teng::Window* window,
			Teng::Scene* scene, Teng::Config* config,
			Teng::GraphicManager* g_manager, Teng::SoundManager* s_manager, Teng::CmdMessage* message);

		void returnScenario(Teng::Scenario* main_scenario);
		void setIsImportScenario(bool scenario_type);
		bool getIsImportScenario();
		void makeCommandMap();
		char* getScenarioFile();
		void setScenarioFile(const char* filename);
		void setCurrentLine(const int line);
		bool checkForceSkip(Input* input, Config* config, CmdMessage* msg);
		void reloadHyperSkip(Scenario* scenario, CmdMessage* message, GraphicManager* g_manager, SoundManager* s_manager, Window* window, Config* config, Scene* scene);

		//scenario->command[1]を返す
		//char* getCommandOne(){ return command[1];}
		//char* getCommandZero(){ return command[0];}
		//char* getCommandTwo(){ return command[2];}
		char* getCommandOne();
		char* getCommandZero();
		char* getCommandTwo();

		void setFlagResetFlg(bool is_reset){flag_reset_flg = is_reset;}
		bool getFlagResetFlg(){return flag_reset_flg;}
		int  getNextTag(){ return next_tag; }
		bool startedFromTitle(){ return started_from_title; }
		void startedFromTitle(bool do_auto_click){ started_from_title = do_auto_click; }
		int  calcSelectedCount(short current_line_read_flag, int idx);
		bool isSelectionSelected(short current_line_read_flag, int idx);
		bool isHyperSkipButtonPushed(){ return is_hyper_skip_button_pushed; }
		void isHyperSkipButtonPushed(const bool now_skipping){ is_hyper_skip_button_pushed = now_skipping; }
		//既読行かどうかを判定
		bool isAlreadyReadLine(const int line);
		void incrementCurrentLine(){ current_line++; }
	private:
		bool change_title_flg;
		bool is_initialized;
		char scenario_file[TENG_FILE_NAME_MAX];//シナリオファイル名
		bool is_import_scenario;
		int  max_line;//シナリオファイルの最大行数
		int  current_line;//現在のシナリオ行
		char script[TENG_SCRIPT_MAX_LINE][TENG_SCRIPT_MAX_STRING_LENGTH];//ロードしたシナリオファイル内容のコピー
		char msg_buffer[TENG_SCRIPT_MESSAGE_MAX_LINE][TENG_SCRIPT_MESSAGE_MAX_LENGTH];//出力用メッセージバッファ

		int next_tag;
		int executed_tag;
		char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER+1][TENG_SCRIPT_COMMAND_MAX_LENGTH];//現在行のコマンド・引数一覧

		static std::map<std::string, int> command_map;

		//シナリオ内の行が既読かどうかを格納
		short already_read_flag[TENG_SCRIPT_MAX_LINE];
		std::vector<std::map<std::string, int>> characters_love;

		bool flag_reset_flg;

		//最初からシナリオを開始したとき、最初の1回は自動クリック
		bool started_from_title;

		//名前入力の制限を超えているかどうか
		bool is_name_valid;
		
		//ハイパースキップボタンが押された
		bool is_hyper_skip_button_pushed;

		bool is_x_clicked;
	public:
		bool isXClicked(){ return is_x_clicked; }
	
	/********** cmd_love **********/
	public:
		void resetLove();
		void setLove(char (*command)[TENG_SCRIPT_COMMAND_MAX_LENGTH]);
		int* backupLove();
		void restoreLove(const int* love_array);
		void clearLove();
		string getTopLoveCharName();
		void splitCommandAndCharName(string cmd_charname, string& before, string& after);

	/********** cmd_if **********/
	public:
		void cmdIf(char (*command)[TENG_SCRIPT_COMMAND_MAX_LENGTH]);


	private:
		map<string, int> char_status;
		bool checkCharName(string name);
		bool checkLoveArg(string love_arg2);
		int  getNumber(const char* number_with_sign);


	/******************* 名前入力 *********************/

	public:
		struct Btn{
			int now;
			int normal;
			int focus;
			int click;
			int x;
			int y;
			int width;
			int height;
			int power;
			int status;
		};
		void nameEntryInitialize(GraphicManager* g_manager);
	private:
		Btn btn_name_entry_base, btn_name_entry_init, btn_name_entry_decision;

	public:
		// メッセージボックス初期化
		void msgBoxInitialize(GraphicManager* g_manager);
		bool inButton(Btn& btn);
	private:
		Btn btn_textbox_base;
		Btn btn_textbox_config, btn_textbox_hskip, btn_textbox_qload;
		Btn btn_textbox_qsave, btn_textbox_skip, btn_textbox_x, btn_textbox_auto;
		int cursor_x, cursor_y;

	};



}