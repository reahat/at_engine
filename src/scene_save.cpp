#include "scene.h"
#include "scenario.h"
#include "DxLib.h"
#include "global.h"
#include "common.h"
#include "savedata.h"
#include "error_code_define.h"
#include <fstream>
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")
#include <list>

#include <typeinfo>
#include <iostream>
#include <time.h>


extern char common_dir[256];
extern bool is_hyper_skip;
using namespace Teng;

void Scene::doScenarioSave(
	Window* window,
	Config* config,
	Input* input,
	GraphicManager* g_manager,
	SoundManager* s_manager,
	Scenario* scenario,
	CmdMessage* message)
{
	if (scene_changing){
		input->disableAllKeys();
	}

	static int before_frame_mouse_right_clicking = 0;
	int this_frame_mouse_right_clicking = input->getKeepMouseRight();
	bool mouse_right_clicked = (before_frame_mouse_right_clicking == 0 && this_frame_mouse_right_clicking != 0);
	before_frame_mouse_right_clicking = this_frame_mouse_right_clicking;


	static bool input_disabled = false;
	bool cursor_is_hand = false;
	static int keep_overwrite_number = -1;
	static int keep_delete_number = -1;
	int  tmp_saveload_pagenum = saveload_pagenum;
	int  tmp_saveload_before_pagenum = saveload_before_pagenum;
	bool tmp_saveload_page_changing = saveload_page_changing;
	int  tmp_clicked_button = clicked_button;
	ULONG tmp_frame = frame;
	static char disp_date[32] = { 0 };
	static char disp_msg[3][128] = { 0, 0, 0 };
	static time_t timer;
	static struct tm t_st;
	static int before_save_thumbnail = -1;

	//ロードボタン押下時にサムネイルを削除するフラグ
	bool load_selected = false;

	//メッセージと時刻を取り出し、ダイアログに表示する
	list<BacklogMessage> logs = message->getLog().logs();
	auto it = logs.end();
	it--;
	list<string> msgs = *it;
	auto it_str = msgs.begin();
	int i = 0;
	//for (int j = 0; j < 3; j++){
	//	disp_msg[j][0] = 0;
	//}
	//for (auto it_str = msgs.begin(); it_str != msgs.end(); it_str++){
	//	strcpy_s(disp_msg[i++], it_str->c_str());
	//}


	//背景描画
	g_manager->drawSystemGraphic(now_scene);


	SystemGraphic *s_graphics = g_manager->getSystemGraphics();
	
	auto button = [this, &s_graphics, &input, &window, &cursor_is_hand, &tmp_saveload_pagenum, &tmp_saveload_before_pagenum, &tmp_saveload_page_changing, &tmp_frame](int arrnum, int pagenum){
		int size_x, size_y;
		DxLib::GetGraphSize(s_graphics[arrnum].getHandle(), &size_x, &size_y);
		if (tmp_saveload_pagenum == pagenum){
			// ページボタンの描画
			//通常時
			if (!tmp_saveload_page_changing){
				DxLib::DrawGraph(s_graphics[arrnum].getX(), s_graphics[arrnum].getY(), s_graphics[arrnum + 1].getHandle(), TRUE);
			}
			//ページ切り替え中はボタンをピンクに
			else{
				DxLib::DrawGraph(s_graphics[arrnum].getX(), s_graphics[arrnum].getY(), s_graphics[arrnum + 2].getHandle(), TRUE);
			}
		}
		else if (!is_dialog_draw && Common::cursorIn(s_graphics[arrnum].getX(), s_graphics[arrnum].getX() + size_x, s_graphics[arrnum].getY(), s_graphics[arrnum].getY() + size_y)){
			cursor_is_hand = true;
			if (input->getKeepMouseLeft() != 0 && !tmp_saveload_page_changing && tmp_frame > 0){
				tmp_saveload_before_pagenum = tmp_saveload_pagenum;
				tmp_saveload_pagenum = pagenum;
				tmp_saveload_page_changing = true;
				tmp_frame = 0;
			}

			// ページボタンの描画（フォーカス）
			DxLib::DrawGraph(s_graphics[arrnum].getX(), s_graphics[arrnum].getY(), s_graphics[arrnum + 1].getHandle(), TRUE);
		}
		else{
			DxLib::DrawGraph(s_graphics[arrnum].getX(), s_graphics[arrnum].getY(), s_graphics[arrnum].getHandle(), TRUE);
		}

	};
	

	//ボタン処理
	button(SystemGraphic::arrnum_button_save_page1_normal, 1);
	button(SystemGraphic::arrnum_button_save_page2_normal, 2);
	button(SystemGraphic::arrnum_button_save_page3_normal, 3);
	button(SystemGraphic::arrnum_button_save_page4_normal, 4);
	button(SystemGraphic::arrnum_button_save_page5_normal, 5);
	//バラの描画
	static const int BARA_X = 31;
	auto get_button_arrnum = [](int pagenum){
		switch (pagenum){
		case 1: return SystemGraphic::arrnum_button_save_page1_normal;
		case 2: return SystemGraphic::arrnum_button_save_page2_normal;
		case 3: return SystemGraphic::arrnum_button_save_page3_normal;
		case 4: return SystemGraphic::arrnum_button_save_page4_normal;
		case 5: return SystemGraphic::arrnum_button_save_page5_normal;
		}
		return -1;
	};
	DxLib::DrawGraph(BARA_X, s_graphics[get_button_arrnum(saveload_pagenum)].getY() + 3, g_manager->system_graphics[SystemGraphic::arrnum_button_save_icon_bara].getHandle(), TRUE);


	//戻るボタン
	int back_left, back_right, back_top, back_bottom;
	int back_width, back_height;
	DxLib::GetGraphSize(g_manager->system_graphics[SystemGraphic::arrnum_button_save_back_normal].getHandle(), &back_width, &back_height);
	back_left = g_manager->system_graphics[SystemGraphic::arrnum_button_save_back_normal].getX();
	back_right = back_left + back_width;
	back_top = g_manager->system_graphics[SystemGraphic::arrnum_button_save_back_normal].getY();
	back_bottom = back_top + back_height;
	//戻るボタン フォーカス
	if (!is_dialog_draw && !scene_changing && Common::cursorIn(back_left, back_right, back_top, back_bottom)){
		cursor_is_hand = true;
		DxLib::DrawGraph(back_left, back_top, g_manager->system_graphics[SystemGraphic::arrnum_button_save_back_focus].getHandle(), TRUE);
		if (input->getKeepMouseLeft() != 0 && !scene_changing){
			frame = 0;
			scene_changing = true;
			//before_scene = now_scene;
			//シナリオに移動するボタンを押した
			if(before_scene == eScene_scenario) clicked_button = eScene_scenario;
		}
		
	}
	else if (before_scene != eScene_scenario){
		if (tmp_clicked_button != eScene_scenario){
			DxLib::DrawGraph(back_left, back_top, g_manager->system_graphics[SystemGraphic::arrnum_button_save_back_click].getHandle(), TRUE);
		}
		else{
			DxLib::DrawGraph(back_left, back_top, g_manager->system_graphics[SystemGraphic::arrnum_button_save_back_normal].getHandle(), TRUE);
		}
	}
	//戻るボタン 通常時
	else{
		DxLib::DrawGraph(back_left, back_top, g_manager->system_graphics[SystemGraphic::arrnum_button_save_back_normal].getHandle(), TRUE);
	}


	saveload_pagenum = tmp_saveload_pagenum;
	saveload_before_pagenum = tmp_saveload_before_pagenum;
	saveload_page_changing = tmp_saveload_page_changing;
	if(!scene_changing) frame = tmp_frame;

	// TODO これ何に使うんだ？
	while (DxLib::GetASyncLoadNum() > 0){
		DEBUG printf("[INFO] Savedata loading...\n");
		DxLib::WaitTimer(10);
	}


	//トラン中は前ページ・次（現在）ページのクロスフェード
	if (saveload_page_changing){
		if (frame < 255){
			input_disabled = true;
			// 次ページサムネイル
			for (int i = 1; i <= 12; i++){
				int data_no = 12 * (saveload_pagenum - 1) + i;
				int before_page_datano = 12 * (saveload_before_pagenum - 1) + i;
				if (SaveData::getSavedataExist(before_page_datano)){
					DxLib::DrawGraph(getThumbnailX(i), getThumbnailY(i), SaveData::getThumbnailHandle(data_no), FALSE);
				}
				else{
					DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, frame);
					DxLib::DrawGraph(getThumbnailX(i), getThumbnailY(i), SaveData::getThumbnailHandle(data_no), FALSE);
					DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
				}
			}

			//前ページ
			DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255 - frame);
			for (int i = 1; i <= 12; i++){
				int data_no = 12 * (saveload_before_pagenum - 1) + i;
				DxLib::DrawGraph(getThumbnailX(i), getThumbnailY(i), SaveData::getThumbnailHandle(data_no), FALSE);
			}
			DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
			if(!scene_changing) frame += 10;
		}
		// トラン完了した瞬間
		else{
			saveload_page_changing = false;
			//DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
			input_disabled = false;
			for (int i = 1; i <= 12; i++){
				int data_no = 12 * (saveload_pagenum - 1) + i;
				DxLib::DrawGraph(getThumbnailX(i), getThumbnailY(i), SaveData::getThumbnailHandle(data_no), FALSE);
			}
			saveload_before_pagenum = saveload_pagenum;
		}
	}
	// 通常時
	else{
		//トラン中以外は現在ページ表示
		for (int i = 1; i <= 12; i++){
			int data_no = 12 * (saveload_pagenum - 1) + i;
			//通常時
			if (!saveload_confirm_fadeout_reverse){
				DxLib::DrawGraph(getThumbnailX(i), getThumbnailY(i), SaveData::getThumbnailHandle(data_no), FALSE);
			}
			//YESボタン押下フェード時
			else{
				if (keep_overwrite_number != data_no){
					DxLib::DrawGraph(getThumbnailX(i), getThumbnailY(i), SaveData::getThumbnailHandle(data_no), FALSE);
				}
				else{
					DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, frame+92);
					DxLib::DrawGraph(getThumbnailX(i), getThumbnailY(i), SaveData::getThumbnailHandle(data_no), FALSE);
					DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
				}
			}
		}
	}

	//サムネイル描画
	int x, y;
	DxLib::GetMousePoint(&x, &y);
	//DEBUG printf("x:%d, y:%d\n", x, y);

	static char* text = "";
	cursor_is_hand = false;
	string focused_selection = "";
	bool draw_save_load_delete_selection = false;
	int focused_num = -1;
	// 選択肢の幅取得
	const static int TEXT_WIDTH_SAVE =
		DxLib::GetDrawStringWidthToHandle("save", 4, message->getFontHandle(CmdMessage::MsgType::SAVE_ENABLED));
	const static int TEXT_WIDTH_LOAD =
		DxLib::GetDrawStringWidthToHandle("load", 4, message->getFontHandle(CmdMessage::MsgType::SAVE_DISABLED));
	const static int TEXT_WIDTH_DELETE =
		DxLib::GetDrawStringWidthToHandle("delete", 6, message->getFontHandle(CmdMessage::MsgType::SAVE_ENABLED));
	//サムネイル未クリックのとき
	if (keep_overwrite_number == -1 && keep_delete_number == -1){
		for (int i = 1; i <= 12; i++){
			int datanum = 12 * (saveload_pagenum - 1) + i;
			if (!is_dialog_draw && Common::cursorIn(getThumbnailX(i), getThumbnailX(i) + SAVEDATA_WIDTH, getThumbnailY(i), getThumbnailY(i) + SAVEDATA_HEIGHT)){
				//フォーカス状態のサムネイルを暗くする
				focused_num = i;
				SetDrawBlendMode(DX_BLENDMODE_MULA, 110);
				DrawBox(getThumbnailX(i), getThumbnailY(i), getThumbnailX(i) + TENG_SAVE_SCREENSHOT_WIDTH, getThumbnailY(i) + TENG_SAVE_SCREENSHOT_HEIGHT, GetColor(16, 16, 16), TRUE);
				SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
				draw_save_load_delete_selection = true;

				// saveにフォーカス
				if (Common::cursorIn(getThumbnailX(i) + 25, getThumbnailX(i) + 25 + TEXT_WIDTH_SAVE, getThumbnailY(i) + 38, getThumbnailY(i) + 38 + 30)){
					focused_selection = "save";

					//saveをクリックした
					if (input->getKeepMouseLeft() != 0){
						keep_overwrite_number = datanum;
						saveload_confirm_fadein = true;
						is_dialog_draw = true;
						frame = 0;
						// 時刻保存
						(void)time(&timer);
						localtime_s(&t_st, &timer);
						char *wday[] = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };
						int y = t_st.tm_year;
						sprintf_s(disp_date, "%d.%d.%d %s. %d:%02d",
							t_st.tm_year + 1900,
							t_st.tm_mon + 1,
							t_st.tm_mday,
							wday[t_st.tm_wday],
							t_st.tm_hour,
							t_st.tm_min);

						text = SaveData::getSavedataExist(keep_overwrite_number) ? "上書きしますか？" : "セーブしますか？";
					}
				} // end saveフォーカス時

				// deleteフォーカス
				else if (Common::cursorIn(getThumbnailX(i) + 78, getThumbnailX(i) + 78 + TEXT_WIDTH_DELETE, getThumbnailY(i) + 78, getThumbnailY(i) + 78 + 30)){
					focused_selection = "delete";

					//deleteをクリックした
					if (input->getKeepMouseLeft() != 0 && SaveData::getSavedataExist(datanum)){
						keep_delete_number = datanum;
						saveload_confirm_fadein = true;
						is_dialog_draw = true;
						frame = 0;

						text = "データを削除しますか？";

						SaveDataStruct savedata = SaveData::get(datanum);
						strcpy_s(disp_date, 32, savedata.date);
						strcpy_s(disp_msg[0], 128, savedata.messages[0]);
						strcpy_s(disp_msg[1], 128, savedata.messages[1]);
						strcpy_s(disp_msg[2], 128, savedata.messages[2]);
					}

				}


			}
		}
	}
	//サムネイルクリック済みのとき
	else if (keep_delete_number == -1 || keep_overwrite_number == -1){
		//サムネイルを暗く表示
		int thumb_in_page;

		if (!saveload_confirm_fadeout && !saveload_confirm_fadeout_reverse){
			if (keep_overwrite_number != -1){
				thumb_in_page = keep_overwrite_number % 12;
			}
			else{
				thumb_in_page = keep_delete_number % 12;
			}
			SetDrawBlendMode(DX_BLENDMODE_MULA, 110);
			DrawBox(getThumbnailX(thumb_in_page % 12), getThumbnailY(thumb_in_page % 12), getThumbnailX(thumb_in_page % 12) + TENG_SAVE_SCREENSHOT_WIDTH, getThumbnailY(thumb_in_page % 12) + TENG_SAVE_SCREENSHOT_HEIGHT, GetColor(16, 16, 16), TRUE);
			SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		}
		const static char* TEXT_YES = "Yes";
		const static char* TEXT_NO = "No";
		const static int WIDTH_YES = DxLib::GetDrawStringWidthToHandle(TEXT_YES, 3, message->getFontHandle(CmdMessage::MsgType::SAVE_YESNO));
		const static int WIDTH_NO = DxLib::GetDrawStringWidthToHandle(TEXT_NO, 2, message->getFontHandle(CmdMessage::MsgType::SAVE_YESNO));
		//帯専用画面
		DxLib::SetDrawScreen(saveload_confirm_dialog);
		DxLib::ClearDrawScreen();
		//帯の背景は半透明
		DxLib::SetDrawBlendMode(DX_BLENDMODE_PMA_ALPHA, 166); // 65%
		//淡黄色の背景にする
		DxLib::DrawBox(0, 0, 1024, SAVEDATA_CONFIRM_DIALOG_HEIGHT, SAVEDATA_CONFIRM_DIALOG_COLOR, TRUE);
		//帯上の文字は非透明茶色
		DxLib::SetDrawBlendMode(DX_BLENDMODE_PMA_ALPHA, 255);
		//日付
		DxLib::DrawStringToHandle(SAVEDATA_CONFIRM_MSG_DATE_X, SAVEDATA_CONFIRM_MSG_DATE_Y, disp_date, SAVEDATA_CONFIRM_MSG_COLOR, message->getFontHandle(CmdMessage::MsgType::SAVE_DATE));
		//テキスト
		int confirm_text_x = SAVEDATA_CONFIRM_MSG_QUESTION_X;
		if (keep_overwrite_number != 0 && keep_delete_number == -1){
			for (auto it_str = msgs.begin(); it_str != msgs.end(); it_str++){
				strcpy_s(disp_msg[i++], it_str->c_str());
			}
			confirm_text_x += 20;
		}
		DxLib::DrawStringToHandle(SAVEDATA_CONFIRM_MSG_TEXT_X, SAVEDATA_CONFIRM_MSG_TEXT_Y, disp_msg[0], SAVEDATA_CONFIRM_MSG_COLOR, message->getFontHandle(CmdMessage::MsgType::SAVE_MSG));
		DxLib::DrawStringToHandle(SAVEDATA_CONFIRM_MSG_TEXT_X, SAVEDATA_CONFIRM_MSG_TEXT_Y + 30, disp_msg[1], SAVEDATA_CONFIRM_MSG_COLOR, message->getFontHandle(CmdMessage::MsgType::SAVE_MSG));
		DxLib::DrawStringToHandle(SAVEDATA_CONFIRM_MSG_TEXT_X, SAVEDATA_CONFIRM_MSG_TEXT_Y + 60, disp_msg[2], SAVEDATA_CONFIRM_MSG_COLOR, message->getFontHandle(CmdMessage::MsgType::SAVE_MSG));
		//セーブしますか？ / 削除しますか？ 確認メッセージ

		// セーブの場合は少し右へ
		
		DxLib::DrawStringToHandle(confirm_text_x, SAVEDATA_CONFIRM_MSG_QUESTION_Y, text, SAVEDATA_CONFIRM_MSG_COLOR, message->getFontHandle(CmdMessage::MsgType::SAVE_CONFIRM));
		//YES NO
		DxLib::DrawStringToHandle(SAVEDATA_CONFIRM_MSG_YES_X, SAVEDATA_CONFIRM_MSG_YESNO_Y, TEXT_YES, SAVEDATA_CONFIRM_MSG_COLOR, message->getFontHandle(CmdMessage::MsgType::SAVE_YESNO));
		DxLib::DrawStringToHandle(SAVEDATA_CONFIRM_MSG_NO_X, SAVEDATA_CONFIRM_MSG_YESNO_Y, TEXT_NO, SAVEDATA_CONFIRM_MSG_COLOR, message->getFontHandle(CmdMessage::MsgType::SAVE_YESNO));
		//YES NOの矢印
		int yesno_y = SAVEDATA_CONFIRM_MSG_YESNO_Y + SAVEDATA_CONFIRM_DIALOG_Y;
		//YES
		if (Common::cursorIn(SAVEDATA_CONFIRM_MSG_YES_X, SAVEDATA_CONFIRM_MSG_YES_X + WIDTH_YES, yesno_y, yesno_y + 35)){
			DxLib::DrawGraph(SAVEDATA_CONFIRM_ARROW_YES_X, SAVEDATA_CONFIRM_ARROW_YESNO_Y, g_manager->system_graphics[SystemGraphic::arrnum_button_save_icon_arrow].getHandle(), TRUE);
			//YESボタンを押した
			if (input->getKeepMouseLeft() != 0 && !saveload_confirm_fadeout_reverse){
				// セーブデータ作成
				if (keep_overwrite_number != -1 && keep_delete_number == -1){
					Teng::SaveData *savedata = new Teng::SaveData(false);
					savedata->save(keep_overwrite_number, scenario, g_manager, s_manager, window, message, this);
					delete savedata;
					DEBUG printf("[INFO] セーブデータサムネイル再展開\n");
					//存在確認フラグをtrueにする
					SaveData::setSavedataExist(keep_overwrite_number, true);

					//クロスフェード用にセーブ前のサムネイルを一時保存
					before_save_thumbnail = SaveData::getThumbnailHandle(keep_overwrite_number);

					//サムネイルを表示できる形に復元・サムネイルハンドルをセット
					decodeSavedataThumbnailOnlyClicked(window, config, input, g_manager, s_manager, keep_overwrite_number);

					frame = 0;

					saveload_confirm_fadeout_reverse = true;
					//keep_overwrite_number = -1;
					keep_delete_number = -1;
				}
				// セーブデータ削除
				if (keep_overwrite_number == -1 && keep_delete_number != -1){
					window->setCursorType(LoadCursor(NULL, IDC_HAND));
					SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
						//DxLib::DeleteGraph(Teng::SaveData::getThumbnailHandle(keep_delete_number));
						//Teng::SaveData::setThumbnailHandle(keep_delete_number, -1);
						//Teng::SaveData::setSavedataExist(keep_delete_number, false);
						//char filename[TENG_FILE_NAME_MAX];
						//sprintf_s(filename, TENG_FILE_NAME_MAX, TENG_SAVE_FILENAME, keep_delete_number);
						//if (remove(filename) == 0){
						//	DEBUG printf("[INFO] data_%d.savを削除しました。\n", keep_delete_number);
						//}
						//else{
						//	DEBUG printf("[WARN] data_%d.savの削除に失敗しました。\n", keep_delete_number);
						//}
						//keep_overwrite_number = -1;
						//keep_delete_number = -1;
						saveload_confirm_fadeout_reverse = true;
						frame = 0;
				}
			}
			cursor_is_hand = true;
		}
		//NO
		if (Common::cursorIn(SAVEDATA_CONFIRM_MSG_NO_X, SAVEDATA_CONFIRM_MSG_NO_X + WIDTH_NO, yesno_y, yesno_y + 35) ||	mouse_right_clicked){
			if(!mouse_right_clicked) DxLib::DrawGraph(SAVEDATA_CONFIRM_ARROW_NO_X, SAVEDATA_CONFIRM_ARROW_YESNO_Y, g_manager->system_graphics[SystemGraphic::arrnum_button_save_icon_arrow].getHandle(), TRUE);
			if(!saveload_confirm_fadeout) cursor_is_hand = true;
			if ((input->getKeepMouseLeft() != 0 && !saveload_confirm_fadeout) || mouse_right_clicked){
				//keep_overwrite_number = -1;
				frame = 0;
				saveload_confirm_fadeout = true;
				cursor_is_hand = false;
			}
		}

		DxLib::SetDrawScreen(DX_SCREEN_BACK);

		//確認ダイアログの表示
		//フェードイン
		if (saveload_confirm_fadein){
			//DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, frame);
			DxLib::SetDrawBlendMode(DX_BLENDMODE_PMA_ALPHA, frame);
			DxLib::DrawGraph(0, SAVEDATA_CONFIRM_DIALOG_Y, saveload_confirm_dialog, TRUE);
			if (frame < 255){
				frame += 12;
			}
			//トラン完了直後
			else{
				frame = 0;
				saveload_confirm_fadein = false;
			}
		}
		//フェードアウト
		else if (saveload_confirm_fadeout){
			DxLib::SetDrawBlendMode(DX_BLENDMODE_PMA_ALPHA, 255 - frame);
			DxLib::DrawGraph(0, SAVEDATA_CONFIRM_DIALOG_Y, saveload_confirm_dialog, TRUE);
			if (frame < 255){
				frame += 12;
			}
			//フェードアウト完了直後
			else{
				frame = 0;
				saveload_confirm_fadeout = false;
				saveload_confirm_fadeout_reverse = false;
				keep_overwrite_number = -1;
				keep_delete_number = -1;
				is_dialog_draw = false;
			}
		}
		//フェードアウト(右に消える)
		else if (saveload_confirm_fadeout_reverse){
			int number = keep_overwrite_number != -1 ? keep_overwrite_number : keep_delete_number;

			//フェード中
			if (frame < 255){
				DxLib::DrawBlendGraph(
					0,
					SAVEDATA_CONFIRM_DIALOG_Y,
					saveload_confirm_dialog,
					TRUE,
					g_manager->system_graphics[SystemGraphic::arrnum_rule_save_confirm_dialog_reverse].getHandle(),
					frame,
					64);
				int boxcolor = 110 - frame * 2;
				SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255-frame);
				DrawGraph(getThumbnailX(number%12), getThumbnailY(number % 12), before_save_thumbnail, TRUE);
				if (before_save_thumbnail < 0){
					SetDrawBlendMode(DX_BLENDMODE_MULA, boxcolor);
					if (frame > 0)DrawBox(getThumbnailX(number % 12), getThumbnailY(number % 12), getThumbnailX(number % 12) + TENG_SAVE_SCREENSHOT_WIDTH, getThumbnailY(number % 12) + TENG_SAVE_SCREENSHOT_HEIGHT, GetColor(16, 16, 16), TRUE);
				}
				SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
				frame += 7;
			}
			//フェードアウト完了直後
			else{
				DxLib::DeleteGraph(Teng::SaveData::getThumbnailHandle(keep_delete_number));
				Teng::SaveData::setThumbnailHandle(keep_delete_number, -1);
				Teng::SaveData::setSavedataExist(keep_delete_number, false);
				char filename[TENG_FILE_NAME_MAX];
				sprintf_s(filename, TENG_FILE_NAME_MAX, TENG_SAVE_FILENAME, keep_delete_number);
				if (remove(filename) == 0){
					DEBUG printf("[INFO] data_%d.savを削除しました。\n", keep_delete_number);
				}
				else{
					DEBUG printf("[WARN] data_%d.savの削除に失敗しました。\n", keep_delete_number);
				}

				frame = 0;
				saveload_confirm_fadeout_reverse = false;
				saveload_confirm_fadeout = false;
				keep_overwrite_number = -1;
				keep_delete_number = -1;
				before_save_thumbnail = -1;
				is_dialog_draw = false;
			}
		}
		//非フェード時
		else{
			DxLib::DrawGraph(
				0,
				SAVEDATA_CONFIRM_DIALOG_Y,
				saveload_confirm_dialog,
				TRUE);
		}
		DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

	}
	if (focused_num != -1 || keep_overwrite_number != -1 || keep_delete_number != -1){
		int i = keep_overwrite_number;
		if (i == -1) i = keep_delete_number;
		if (i == -1) i = focused_num;


		if (!saveload_confirm_fadeout){
			// saveテキスト
			int text_color_save;
			// フォーカスまたはクリック済み
			if (focused_selection == "save" || keep_overwrite_number != -1){
				text_color_save = SAVEDATA_STRING_ENABLE;
				if (focused_selection == "save") cursor_is_hand = true;
				// save横の矢印
				DrawGraph(
					getThumbnailX(i) + 10,
					getThumbnailY(i % 12) + 49,
					g_manager->system_graphics[SystemGraphic::arrnum_button_save_thumbnail_arrow].getHandle(),
					TRUE);
				// クリック済みの場合
				if (keep_overwrite_number != -1) text_color_save = SAVEDATA_STRING_FOCUS;
			}
			// 通常時
			else{
				text_color_save = SAVEDATA_STRING_ENABLE;
			}

			DrawStringToHandle(
				getThumbnailX(i) + 25,
				getThumbnailY(i % 12) + 38,
				"save",
				text_color_save,
				message->getFontHandle(CmdMessage::MsgType::SAVE_ENABLED)
				);

			// loadは非活性
			DrawStringToHandle(
				getThumbnailX(i) + 101,
				getThumbnailY(i % 12) + 38,
				"load",
				SAVEDATA_STRING_DISABLED,
				message->getFontHandle(CmdMessage::MsgType::SAVE_DISABLED)
				);

			//DrawGraph(
			//	getThumbnailX(i) + 88,
			//	getThumbnailY(i) + 49,
			//	g_manager->system_graphics[SystemGraphic::arrnum_button_save_thumbnail_arrow].getHandle(),
			//	TRUE);




			// delete
			int text_color_delete;
		
			int tmp;
			if (keep_delete_number != -1) tmp = keep_delete_number;
			else tmp = i + (saveload_pagenum - 1) * 12;

			if (!SaveData::getSavedataExist(tmp)){
				text_color_delete = SAVEDATA_STRING_DISABLED;
			}
			// フォーカス
			else if (keep_delete_number != -1 || focused_selection == "delete"){

				text_color_delete = SAVEDATA_STRING_ENABLE;
				if (focused_selection == "delete") cursor_is_hand = true;
				focused_selection = "delete";
				DrawGraph(
					getThumbnailX(i) + 64,
					getThumbnailY(i % 12) + 87,
					g_manager->system_graphics[SystemGraphic::arrnum_button_save_thumbnail_arrow].getHandle(),
					TRUE);
				if (keep_delete_number != -1) text_color_delete = SAVEDATA_STRING_FOCUS;
			}
			else{
				text_color_delete = SAVEDATA_STRING_ENABLE;
			}
			DrawStringToHandle(
				getThumbnailX(i) + 78,
				getThumbnailY(i % 12) + 75,
				"delete",
				text_color_delete,
				message->getFontHandle(CmdMessage::MsgType::SAVE_FOCUS)
				);


			// 境界線
			DxLib::DrawBox(
				getThumbnailX(i) + 18,
				getThumbnailY(i % 12) + 72,
				getThumbnailX(i) + 155,
				getThumbnailY(i % 12) + 74, SAVEDATA_STRING_ENABLE, TRUE);

		}
	}


	//データ削除
	if (keep_delete_number == -1 && keep_overwrite_number == -1){

	}
	else if (keep_overwrite_number == -1){

	}


	if (cursor_is_hand){
		//前フレームではハンドではない
		if (!saveload_cursor_is_hand){
			window->setCursorType(LoadCursor(NULL, IDC_HAND));
			SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
		}
		saveload_cursor_is_hand = cursor_is_hand;
	}
	else{
		//前フレームではハンドだった
		if (saveload_cursor_is_hand){
			window->setCursorType(LoadCursor(NULL, IDC_ARROW));
			SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
		}
		saveload_cursor_is_hand = cursor_is_hand;
	}


	//サムネイルを削除
	//if (load_selected){
	//	for (int i = 0; i < TENG_SAVE_SAVEDATA_MAX; i++){
	//		DxLib::DeleteGraph(Teng::SaveData::getThumbnailHandle(i));
	//		Teng::SaveData::setThumbnailHandle(i, -1);
	//	}
	//}




	if (input->getKeepMouseRight() != 0 && !scene_changing && keep_overwrite_number == -1 && keep_delete_number == -1){
		frame = 0;
		scene_changing = true;
		//before_scene = now_scene;
		//シナリオに移動するボタンを押した
		if (before_scene == eScene_scenario) clicked_button = eScene_scenario;
		//if (before_scene == eScene_title) clicked_button = eScene_title;
	}





	//サムネイル描画
	//DxLib::DrawGraph(168, 332, Teng::SaveData::getThumbnailHandle(0), TRUE);
	//DxLib::DrawGraph(540, 332, Teng::SaveData::getThumbnailHandle(1), TRUE);

	//int x, y;
	//DxLib::GetMousePoint(&x, &y);
	////DEBUG printf("x:%d, y:%d\n", x, y);

	////上書き・削除ダイアログが出ていない状態
	//if (keep_overwrite_number == -1 && keep_delete_number == -1){
	//	//データ左
	//	if (x > 166 && x < 368 && y > 332 && y < 482){
	//		window->setCursorType(LoadCursor(NULL, IDC_HAND));
	//		SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
	//		if (input->getKeepMouseLeft() != 0){
	//			keep_overwrite_number = 0;
	//		}
	//	}
	//	//データ右
	//	else if (x > 539 && x < 741 && y > 332 && y < 482){
	//		window->setCursorType(LoadCursor(NULL, IDC_HAND));
	//		SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
	//		if (input->getKeepMouseLeft() != 0){
	//			keep_overwrite_number = 1;
	//		}
	//	}
	//	else{
	//		window->setCursorType(LoadCursor(NULL, IDC_ARROW));
	//		SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
	//	}
	//}
	////サムネイルクリック後
	//else if (keep_delete_number == -1){

	//	//既に存在するなら上書き確認ダイアログを出す
	//	if (SaveData::getSavedataExist(keep_overwrite_number)){

	//		DxLib::DrawGraph(
	//			TENG_GRAPHIC_SYSTEM_DIALOG_BOX_X,
	//			TENG_GRAPHIC_SYSTEM_DIALOG_BOX_Y,
	//			g_manager->system_graphics[Teng::SystemGraphic::arrnum_dialog_overwrite].getHandle(),
	//			FALSE);

	//		//YESボタン
	//		if (x > 360 && x < 465 && y > 405 && y < 458){
	//			//TODO 毎フレーム実行していいのだろうか？
	//			window->setCursorType(LoadCursor(NULL, IDC_HAND));
	//			SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
	//			if (input->getKeepMouseLeft()){


	//				Teng::SaveData *savedata = new Teng::SaveData;
	//				savedata->save(keep_overwrite_number, scenario, g_manager, s_manager, window, message, this);
	//				delete savedata;
	//				DEBUG printf("[INFO] セーブデータサムネイル再展開\n");
	//				//存在確認フラグをtrueにする
	//				SaveData::setSavedataExist(keep_overwrite_number, true);
	//				//サムネイルを表示できる形に復元・サムネイルハンドルをセット
	//				decodeSavedataThumbnail(window, config, input, g_manager, s_manager);


	//				keep_overwrite_number = -1;
	//				keep_delete_number = -1;
	//			}
	//		}
	//		//NOボタン
	//		else if (x > 520 && x < 670 && y > 405 && y < 458){
	//			//TODO 毎フレーム実行していいのだろうか？
	//			window->setCursorType(LoadCursor(NULL, IDC_HAND));
	//			SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
	//			if (input->getKeepMouseLeft()){
	//				keep_overwrite_number = -1;
	//				keep_delete_number = -1;
	//			}
	//		}
	//		//ボタン外
	//		else{
	//			//TODO 毎フレーム実行していいのだろうか？
	//			window->setCursorType(LoadCursor(NULL, IDC_ARROW));
	//			SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
	//		}
	//	}
	//	else{
	//		//存在しないデータ枠なら、ダイアログを出さずにすぐセーブする
	//		Teng::SaveData *savedata = new Teng::SaveData;
	//		savedata->save(keep_overwrite_number, scenario, g_manager, s_manager, window, message, this);
	//		delete savedata;
	//		DEBUG printf("[INFO] セーブデータサムネイル再展開\n");
	//		//存在確認フラグをtrueにする
	//		SaveData::setSavedataExist(keep_overwrite_number, true);
	//		//サムネイルを表示できる形に復元・サムネイルハンドルをセット
	//		decodeSavedataThumbnail(window, config, input, g_manager, s_manager);
	//		keep_overwrite_number = -1;
	//		keep_delete_number = -1;
	//	}
	//}
	////削除ボタンを押したとき
	//else if (keep_overwrite_number == -1){
	//	DxLib::DrawGraph(
	//		TENG_GRAPHIC_SYSTEM_DIALOG_BOX_X,
	//		TENG_GRAPHIC_SYSTEM_DIALOG_BOX_Y,
	//		g_manager->system_graphics[Teng::SystemGraphic::arrnum_dialog_delete].getHandle(),
	//		FALSE);

	//	//YESボタン
	//	if (x > 360 && x < 465 && y > 405 && y < 458){
	//		//TODO 毎フレーム実行していいのだろうか？
	//		window->setCursorType(LoadCursor(NULL, IDC_HAND));
	//		SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
	//		if (input->getKeepMouseLeft()){
	//			DxLib::DeleteGraph(Teng::SaveData::getThumbnailHandle(keep_delete_number));
	//			Teng::SaveData::setThumbnailHandle(keep_delete_number, -1);
	//			Teng::SaveData::setSavedataExist(keep_delete_number, false);
	//			char filename[TENG_FILE_NAME_MAX];
	//			sprintf_s(filename, TENG_FILE_NAME_MAX, TENG_SAVE_FILENAME, keep_delete_number);
	//			if (remove(filename) == 0){
	//				DEBUG printf("[INFO] data_%d.savを削除しました。\n", keep_delete_number);
	//			}
	//			else{
	//				DEBUG printf("[WARN] data_%d.savの削除に失敗しました。\n", keep_delete_number);
	//			}
	//			keep_overwrite_number = -1;
	//			keep_delete_number = -1;
	//		}
	//	}
	//	//NOボタン
	//	else if (x > 520 && x < 670 && y > 405 && y < 458){
	//		//TODO 毎フレーム実行していいのだろうか？
	//		window->setCursorType(LoadCursor(NULL, IDC_HAND));
	//		SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
	//		if (input->getKeepMouseLeft()){
	//			keep_overwrite_number = -1;
	//			keep_delete_number = -1;
	//		}
	//	}
	//	//ボタン外
	//	else{
	//		//TODO 毎フレーム実行していいのだろうか？
	//		window->setCursorType(LoadCursor(NULL, IDC_ARROW));
	//		SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
	//	}
	//}

	//データ削除
	if (input->getKeepKeyF5() != 0 && SaveData::getSavedataExist(0)){
		keep_delete_number = 0;
	}
	if (input->getKeepKeyF9() != 0 && SaveData::getSavedataExist(1)){
		keep_delete_number = 1;
	}

	//右クリックで前画面に戻る
	//if (input->getKeepMouseRight() != 0){
	//	keep_delete_number = -1;
	//	keep_overwrite_number = -1;
	//	//サムネイル削除
	//	for (int i = 0; i < TENG_SAVE_SAVEDATA_MAX; i++){
	//		DxLib::DeleteGraph(Teng::SaveData::getThumbnailHandle(i));
	//		Teng::SaveData::setThumbnailHandle(i, -1);
	//	}
	//	remove("tmp");
	//	int tmp = now_scene;
	//	now_scene = before_scene;
	//	before_scene = tmp;
	//}
}