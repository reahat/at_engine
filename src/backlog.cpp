﻿#include "define.h"
#include "DxLib.h"
#include "error_code_define.h"
#include "backlog.h"
#include <memory>

using namespace Teng;

Backlog::Backlog(){
	list<BacklogMessage> backlogs;
}

Backlog::~Backlog(){

}

// ログ１回分を追加
void Backlog::push(BacklogMessage& log){
	if(backlogs.size() >= TENG_BACKLOG_RECORD_LIMIT){
		backlogs.pop_front();
	}
	backlogs.push_back(log);
}

void Backlog::clear(){
	backlogs.clear();
}

// ログ１行を追加（最大３行あるので、３回程度繰り返す）
void BacklogMessage::add(string message){
	this->push_back(message);
}

