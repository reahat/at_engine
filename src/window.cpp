﻿#include "window.h"
#include "DxLib.h"
#include "graphic\system_graphic.h"
#include "input.h"
#include "error_code_define.h"
#include "common.h"
#include "scene.h"
#include <memory>
#include "tag/cmd_message.h"
const char Teng::Window::dx_key_archive_string[13] = TENG_DX_ARCHIVE_KEY;
bool Teng::Window::dialog_drawing = false;
extern std::shared_ptr<Teng::CmdMessage> message;

Teng::Window::Window(){

	window_title = TENG_DEFAULT_WINDOW_TITLE;
	cursor_type = LoadCursor(NULL, IDC_ARROW);
	SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)cursor_type);
	dialog_drawing = false;
}

Teng::Window::~Window(){
}

void Teng::Window::setDxlibSetting(Teng::Config* cnf){

	const char *software_version = __DATE__;
	char window_fullname[128] = TENG_DEFAULT_WINDOW_TITLE;
	//DEBUG strcat_s(window_fullname, sizeof(window_fullname), " | DEBUG MODE - ");
	//DEBUG strcat_s(window_fullname, sizeof(window_fullname), software_version);

	//ウィンドウ設定
	//setWindowMode(cnf->getSetting().window_mode);
	//setWindowSize(cnf->getSetting().window_size);
	setWindowMode(TRUE);
	setWindowSize(TRUE);
	//ウィンドウ枠タイプの設定
	DxLib::SetWindowStyleMode(0);
	//DirectInputの使用可否
	DxLib::SetUseDirectInputFlag(FALSE);
	//アイコンファイル読み込み

	//非同期ロード
	DxLib::SetUseASyncLoadFlag(TRUE);
	//アーカイブ設定
	DxLib::SetDXArchiveExtension("data");
	DxLib::SetDXArchiveKeyString(dx_key_archive_string);
	//DxLibのログ出力
	DxLib::SetOutApplicationLogValidFlag(FALSE);
	//非アクティブ時に動作停止するかどうか
	DxLib::SetAlwaysRunFlag(FALSE);
	//×ボタンで終了しない
	DxLib::SetWindowUserCloseEnableFlag(FALSE);
	//デフォルトのウィンドウテキスト
	DxLib::SetMainWindowText(window_fullname);
	//描画スクリーンの作成
	screen = DxLib::MakeScreen(1024, 768);

	//DxLibの初期化
	DxLib::DxLib_Init();

	//初期化後の設定
	setDxlibSettingAfterInit();
}

//DxLib_Init後の設定
void Teng::Window::setDxlibSettingAfterInit(){
	DxLib::SetMouseDispFlag(TRUE);

	//FILE *console;
	//DEBUG AllocConsole();
	//DEBUG freopen_s(&console, "CONOUT$", "w", stdout);
	//DEBUG freopen_s(&console, "CONIN$", "r", stdin);
	//画面モード変更時にグラフィックハンドルをリセットしない
	DxLib::SetChangeScreenModeGraphicsSystemResetFlag(FALSE);
}

void Teng::Window::setWindowMode(bool window_mode){
	DxLib::ChangeWindowMode(window_mode);
}

void Teng::Window::setWindowSize(bool large){
	if(large){
		//pow = TENG_SCREEN_SIZE_POWER_NORMAL;
		DxLib::SetGraphMode(TENG_SCREEN_WIDTH_LARGE, TENG_SCREEN_HEIGHT_LARGE, TENG_SCREEN_COLOR_BIT);
	}else{
		//pow = TENG_SCREEN_SIZE_POWER_SMALL;
		DxLib::SetGraphMode(TENG_SCREEN_WIDTH_SMALL, TENG_SCREEN_HEIGHT_SMALL, TENG_SCREEN_COLOR_BIT);
	}
}

//描画前の画面処理
void Teng::Window::previousProcess(Teng::Config* cnf){
	DxLib::SetDrawScreen(DX_SCREEN_BACK);
	DxLib::ClearDrawScreen();
	if(cnf->getSetting().window_size == TENG_SCREEN_SMALL){
		DrawExtendGraph(0, 0, 800, 600, screen, FALSE);
		SetDrawScreen(screen);
		ClearDrawScreen();
		SetDrawMode(DX_DRAWMODE_BILINEAR);
	}
}

//描画後の画面処理
void Teng::Window::postProcess(){
	DxLib::ScreenFlip();
}

void Teng::Window::drawCloseDialog(Teng::GraphicManager* g_manager, Teng::Input* input, Teng::Scene* scene, Teng::SoundManager* s_manager, Teng::Config* config){
	bool is_close_pushed = DxLib::GetWindowUserCloseFlag(TRUE);
	if(is_close_pushed){
		setDialogDrawing(true);
	}
	if (!dialog_drawing) return;


	int x, y;
	DxLib::GetMousePoint(&x, &y);
	//DEBUG printf("x:%d, y:%d\n", x, y);
	static int frame = 0;
	static bool dialog_yes_clicked = false;
	static bool dialog_no_clicked = false;
	static string dialog_fade_type = "fadein";

	static bool is_dialog_initialized = false;
	if (!is_dialog_initialized){
		btn_dialog_base.now = btn_dialog_base.normal = 
			g_manager->system_graphics[Teng::SystemGraphic::arrnum_dialog_base].getHandle();
		btn_dialog_yes.now = btn_dialog_yes.normal =
			g_manager->system_graphics[Teng::SystemGraphic::arrnum_dialog_yes_normal].getHandle();
		btn_dialog_no.now = btn_dialog_no.normal =
			g_manager->system_graphics[Teng::SystemGraphic::arrnum_dialog_no_normal].getHandle();
		btn_dialog_yes.click =
			g_manager->system_graphics[Teng::SystemGraphic::arrnum_dialog_yes_click].getHandle();
		btn_dialog_no.click =
			g_manager->system_graphics[Teng::SystemGraphic::arrnum_dialog_no_click].getHandle();
		btn_dialog_base.x = 298;
		btn_dialog_base.y = 259;
		btn_dialog_yes.x = 406;
		btn_dialog_yes.y = 356;
		btn_dialog_no.x = 556;
		btn_dialog_no.y = 356;
		DxLib::GetGraphSize(btn_dialog_yes.normal, &btn_dialog_yes.width, &btn_dialog_yes.height);
		DxLib::GetGraphSize(btn_dialog_no.normal, &btn_dialog_no.width, &btn_dialog_no.height);
		is_dialog_initialized = true;
	}

	//if (input->getKeepMouseRight() != 0){
	//	dialog_no_clicked = true;
	//	input->disableAllKeys();
	//}

	static int back_fade_power = 0;
	//×ボタンが押されたら(押された瞬間しか検知できない）
	if(is_close_pushed){
		setDialogDrawing(true);
		dialog_fade_type = "fadein";
		frame = 0;
		dialog_yes_clicked = false;
		dialog_no_clicked = false;
	}
	if (getDialogDrawing() && scene->getNowScene() != Scene::eScene_title){
		int back_trans_color = GetColor(27, 8, 5);
		int trans_per_60 = 102;
		// フェードイン
		if (scene->getNowScene() != Scene::eScene_scenario_menu && dialog_fade_type == "fadein"){
			// ダイアログ以外、画面全体を茶色っぽくする
			if (back_fade_power < 102){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, back_fade_power);
				back_fade_power += 7;
				DxLib::DrawBox(0, 0, 1024, 768, back_trans_color, TRUE);
			}
			else{
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, 102);
				DxLib::DrawBox(0, 0, 1024, 768, back_trans_color, TRUE);
			}
			DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		}
		// フェード完了後
		else if (scene->getNowScene() != Scene::eScene_scenario_menu && dialog_fade_type == "no_fade"){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, 102);
				DxLib::DrawBox(0, 0, 1024, 768, back_trans_color, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		}
		// フェードアウト（戻る）
		else if (scene->getNowScene() != Scene::eScene_scenario_menu && dialog_no_clicked){
			// ダイアログ以外、画面全体を茶色っぽくする
			if (back_fade_power > 0){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, back_fade_power);
				back_fade_power -= 7;
				DxLib::DrawBox(0, 0, 1024, 768, back_trans_color, TRUE);
			}
			else{
				dialog_fade_type == "fadein";
				frame = 0;
				back_fade_power = 0;
				dialog_yes_clicked = false;
				dialog_no_clicked = false;
				dialog_drawing = false;
				btn_dialog_yes.now = btn_dialog_yes.normal;
				btn_dialog_no.now = btn_dialog_no.normal;
			}
			DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		}
		DxLib::DrawGraph(btn_dialog_base.x, btn_dialog_base.y, btn_dialog_base.normal, TRUE);

		//YESボタン
		if (Common::cursorIn(
			btn_dialog_yes.x, btn_dialog_yes.x + btn_dialog_yes.width,
			btn_dialog_yes.y, btn_dialog_yes.y + btn_dialog_yes.height))
		{
			//TODO 毎フレーム実行していいのだろうか？
			cursor_type = LoadCursor(NULL, IDC_HAND);
			SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)cursor_type);
			if(input->getKeepMouseLeft()){
				//throw ENGINE_SYSTEM_STANDARD_EXIT;
				dialog_yes_clicked = true;
				btn_dialog_yes.now = btn_dialog_yes.click;
			}
		}
		//NOボタン
		else if (Common::cursorIn(
			btn_dialog_no.x, btn_dialog_no.x + btn_dialog_no.width,
			btn_dialog_no.y, btn_dialog_no.y + btn_dialog_no.height))
		{
			//TODO 毎フレーム実行していいのだろうか？
			cursor_type = LoadCursor(NULL, IDC_HAND);
			SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)cursor_type);
			if(input->getKeepMouseLeft()){
				btn_dialog_no.now = btn_dialog_no.click;
				dialog_no_clicked = true;
				back_fade_power = 102;
				dialog_fade_type = "fadeout";
				// ただし右メニュー中にダイアログを出して閉じるなら即閉じる
				if (scene->getNowScene() == Scene::eScene_scenario_menu){
					dialog_fade_type == "fadein";
					frame = 0;
					back_fade_power = 0;
					dialog_yes_clicked = false;
					dialog_no_clicked = false;
					dialog_drawing = false;
					btn_dialog_yes.now = btn_dialog_yes.normal;
					btn_dialog_no.now = btn_dialog_no.normal;
					cursor_type = LoadCursor(NULL, IDC_ARROW);
					SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)cursor_type);
				}
			}

		}
		//ボタン外
		else{
			//TODO 毎フレーム実行していいのだろうか？
			cursor_type = LoadCursor(NULL, IDC_ARROW);
			SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)cursor_type);
		}

		DxLib::DrawGraph(btn_dialog_yes.x, btn_dialog_yes.y, btn_dialog_yes.now, TRUE);
		DxLib::DrawGraph(btn_dialog_no.x, btn_dialog_no.y, btn_dialog_no.now, TRUE);
		DxLib::DrawStringToHandle(
			379, 311,
			"ゲームを終了しますか?",
			GetColor(236, 226, 200),
			message->getFontHandle(CmdMessage::MsgType::DIALOG_TEXT));
	}

	//タイトル画面の場合、フェードして終了
	if ((getDialogDrawing() && scene->getNowScene() == Scene::eScene_title) || dialog_yes_clicked){
		input->disableAllKeys();
		frame += 5;
		int bright = 255 - frame;
		DxLib::SetDrawBright(bright, bright, bright);
		int bgm_handle = s_manager->system_sound[SystemSound::arrnum_bgm_title].getHandle();

		int volume = 255;//ここの値を変えたら、Scene::doTitle()内での値を調整すること
		double vol =
			(double)(config->getSetting().master_volume) *
			(double)(config->getSetting().bgm_volume) / 65025 * volume;
		DxLib::ChangeVolumeSoundMem((int)vol-frame, bgm_handle);

		if (frame > 255){
			throw ENGINE_SYSTEM_STANDARD_EXIT;
		}
	}

}