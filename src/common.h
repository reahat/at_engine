﻿#pragma once
#include <DxLib.h>
#include "define.h"
#include "window.h"

namespace Teng{
	class Common{
	public:
		//カーソルが指定範囲内にあるかどうかを拡大率を考慮して判定
		static bool cursorIn(int x_left, int x_right, int y_top, int y_bottom);

		//カーソル位置をコンソールに出力。デバッグモード時のみ動作
		static void putCursorPoint();


	private:



	};

}