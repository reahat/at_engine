﻿#include "scene.h"
#include "common.h"
#include "input.h"
#include <functional>
#pragma once


using namespace Teng;
using namespace std;

int Scene::isGalleryPicture(const string filename){
	for each (pair<int, string> pic in gallery_files){
		if (pic.second.empty()){ return -1; }
		if (pic.second == filename){ return pic.first; }
	}
	return -1;
}

void Scene::makeGalleryFilesList(){
	auto set_gallery = [this](int num, string file) { gallery_files[num] = make_pair(num, file); };
	//ギャラリー対象の画像
	//縮小画像対象外も含む
	set_gallery(cg_00, "event_1-1.png");
	set_gallery(cg_01, "event_1-2.png");
	set_gallery(cg_02, "event_2-1.png");
	set_gallery(cg_03, "event_2-2.png");
	set_gallery(cg_04, "event_2-3.png");
	set_gallery(cg_05, "event_3-1.png");
	set_gallery(cg_06, "");
	set_gallery(cg_07, "");
	set_gallery(cg_08, "");
	set_gallery(cg_09, "");
	set_gallery(cg_10, "");
	set_gallery(cg_11, "");
	set_gallery(cg_12, "");
	set_gallery(cg_13, "");
	set_gallery(cg_14, "");
	set_gallery(cg_15, "");
	set_gallery(cg_16, "");
	set_gallery(cg_17, "");
	set_gallery(cg_18, "");
	set_gallery(cg_19, "");
	set_gallery(cg_20, "");
	set_gallery(cg_21, "");
	set_gallery(cg_22, "");
	set_gallery(cg_23, "");
	set_gallery(cg_24, "");
	set_gallery(cg_25, "");
	set_gallery(cg_26, "");
	set_gallery(cg_27, "");
	set_gallery(cg_28, "");
	set_gallery(cg_29, "");
	set_gallery(cg_30, "");
	set_gallery(cg_31, "");
	set_gallery(cg_32, "");
	set_gallery(cg_33, "");
	set_gallery(cg_34, "");
	set_gallery(cg_35, "");
	set_gallery(cg_36, "");
	set_gallery(cg_37, "");
	set_gallery(cg_38, "");
	set_gallery(cg_39, "");
	set_gallery(cg_40, "");
	set_gallery(cg_41, "");
	set_gallery(cg_42, "");
	set_gallery(cg_43, "");
	set_gallery(cg_44, "");
	set_gallery(cg_45, "");
	set_gallery(cg_46, "");
	set_gallery(cg_47, "");
	set_gallery(cg_48, "");
	set_gallery(cg_49, "");
	set_gallery(cg_50, "");
	set_gallery(cg_51, "");
	set_gallery(cg_52, "");
	set_gallery(cg_53, "");
	set_gallery(cg_54, "");
	set_gallery(cg_55, "");
	set_gallery(cg_56, "");
	set_gallery(cg_57, "");
	set_gallery(cg_58, "");
	set_gallery(cg_59, "");
	set_gallery(cg_60, "");
	set_gallery(cg_61, "");
	set_gallery(cg_62, "");
	set_gallery(cg_63, "");
}


void Scene::doGallery(
	Window* window,
	Config* config,
	Input* input,
	GraphicManager* g_manager,
	SoundManager* s_manager,
	Scenario* scenario,
	CmdMessage* message)
{
	//trueのとき、キー操作を受け付けない
	static bool input_disabled = false;
	//キー押しっぱなし対策。一旦マウスを離せば入力を許可する
	if (input->getKeepMouseLeft() == 0 && input->getKeepMouseRight() == 0) input_disabled = false;

	//シーン・ページ切り替え中はキー操作無効
	if (scene_changing || page_changing){
		input->disableAllKeys();
	}
	static bool back_to_list = false;

	//画像サムネイルのフォーカス・クリック処理
	static auto inPicture = [this, window, input, config](int pic_no, function<void()> func){
		if (config->getSetting().cg_collected[pic_no]){
			window->setCursorType(LoadCursor(NULL, IDC_HAND));
			SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
			if (input->getKeepMouseLeft() != 0 && !input_disabled){
				window->setCursorType(LoadCursor(NULL, IDC_ARROW));
				SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
				gallery_large_mode = true;
				frame = 0;
				gallery_large_mode_picno = pic_no;
				func();
				back_to_list = false;
				input_disabled = true;
			}
		}
	};
	//ページボタンのフォーカス・クリック処理
	static auto inPageButton = [this, window, input](function<void()> func){
		window->setCursorType(LoadCursor(NULL, IDC_HAND));
		SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
		if (input->getKeepMouseLeft() != 0 && !input_disabled){
			window->setCursorType(LoadCursor(NULL, IDC_ARROW));
			SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
			gallery_large_mode = false;
			frame = 0;
			func();
			back_to_list = false;
			page_changing = true;
			input_disabled = true;
		}
	};

	if (!gallery_large_mode){
		//戻るボタン
		if (Common::cursorIn(890, 983, 660, 740)){
			window->setCursorType(LoadCursor(NULL, IDC_HAND));
			SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
			if (input->getKeepMouseLeft() != 0 && !input_disabled){
				window->setCursorType(LoadCursor(NULL, IDC_ARROW));
				SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());

				before_scene = eScene_gallery;
				now_scene = eScene_gallery;
				scene_changing = true;
				frame = 0;
			}
		}
		//1ページ目ボタン
		else if (Common::cursorIn(220, 315, 666, 745)){
			if (gallery_page_num != 1 && !page_changing){
				inPageButton([this](){
					gallery_page_num_before = gallery_page_num;
					gallery_page_num = 1;
				});
			}
		}
		//2ページ目ボタン
		else if (Common::cursorIn(378, 472, 666, 745)){
			if (gallery_page_num != 2 && !page_changing){
				inPageButton([this](){
					gallery_page_num_before = gallery_page_num;
					gallery_page_num = 2;
				});
			}
		}
		//3ページ目ボタン
		else if (Common::cursorIn(532, 625, 666, 745)){
			if (gallery_page_num != 3 && !page_changing){
				inPageButton([this](){
					gallery_page_num_before = gallery_page_num;
					gallery_page_num = 3;
				});
			}
		}
		//画像01
		else if (Common::cursorIn(200, 450, 100, 288)){
			if (gallery_page_num == 1){
				inPicture(cg_00, [this](){});
			}
			else if (gallery_page_num == 2){
				inPicture(cg_05, [this](){});
			}
		}
		//画像02
		else if (Common::cursorIn(600, 850, 100, 288)){
			inPicture(cg_02, [this](){});
		}
		//ボタン外
		else{
			window->setCursorType(LoadCursor(NULL, IDC_ARROW));
			SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
		}
	}

	static bool change_next_picture = false;

	if (!gallery_large_mode || (!change_next_picture)){
		//ここで一覧の画像を表示
		drawScreen(g_manager, scenario, message, config, input, s_manager, window);
	}
	
	static int before, next;
	
	//ギャラリー拡大モード中
	if (gallery_large_mode){
		//1枚目の画像を表示
		if (!change_next_picture){
			if (frame < 255){
				input_disabled = true;
				//最初にafter画像を描画
				DxLib::DrawGraph(0, 0, g_manager->getSystemGraphic(
					SystemGraphic::gallery_pic_large[gallery_large_mode_picno]).getHandle(), FALSE);

				//before画像とルール画像を合成して描画
				DxLib::DrawBlendGraph(0, 0,
					g_manager->getSystemGraphic(SystemGraphic::arrnum_bg_gallery).getHandle(),
					FALSE,
					g_manager->getSystemGraphic(SystemGraphic::arrnum_gallery_rule_slide).getHandle(),
					frame,
					128);

				DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255 - frame * 2);
				drawPictures(config, input, g_manager);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

				frame += 10; //フレーム加算加速
			}
			else{
				DxLib::DrawGraph(0, 0, g_manager->getSystemGraphic(SystemGraphic::gallery_pic_large[gallery_large_mode_picno]).getHandle(), FALSE);
			}
			//クリックしたら次の画像へ
			if (frame >= 255 && input->getKeepMouseLeft() != 0 && !input_disabled){
				frame = 0;
				int res = nextPictureExist(config, g_manager, SystemGraphic::gallery_pic_large[gallery_large_mode_picno]);
				//次の画像があるとき
				if (res >= 0){
					change_next_picture = true;
					before = gallery_large_mode_picno;
					next = res;
				}
				//次の画像がないとき
				else{
					back_to_list = true;
					next = gallery_large_mode_picno;
				}

			}
			//次の画像がないとき、または右クリックされたとき一覧に戻る
			if (input->getKeepMouseRight() != 0 && !back_to_list && frame >= 255 && !input_disabled){
				back_to_list = true;
				frame = 0;
				next = gallery_large_mode_picno;
			}
			if (back_to_list){
				//トラン中はクロスフェード
				if (frame < 255){
					input_disabled = true;
					DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255 - frame);
					DxLib::DrawGraph(0, 0, g_manager->getSystemGraphic(SystemGraphic::gallery_pic_large[next]).getHandle(), FALSE);
					DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, frame);
					DxLib::DrawGraph(0, 0, g_manager->getSystemGraphic(SystemGraphic::arrnum_bg_gallery).getHandle(), FALSE);
					drawPictures(config, input, g_manager);
					DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
					frame += 10;

				}
				//トラン完了後
				else{
					DxLib::DrawGraph(0, 0, g_manager->getSystemGraphic(SystemGraphic::arrnum_bg_gallery).getHandle(), FALSE);
					drawPictures(config, input, g_manager);
					change_next_picture = false;
					before = -1;
					next = -1;
					back_to_list = false;
					gallery_large_mode = false;
				}
			}
		}
		//2枚目以降の画像に切り替える
		else{
			//2枚目以降に切り替える時はクロスフェードする
			if (frame < 255 && !back_to_list){
				input_disabled = true;
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255 - frame);
				DxLib::DrawGraph(0, 0, g_manager->getSystemGraphic(SystemGraphic::gallery_pic_large[before]).getHandle(), FALSE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, frame);
				DxLib::DrawGraph(0, 0, g_manager->getSystemGraphic(SystemGraphic::gallery_pic_large[next]).getHandle(), FALSE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

				//フレーム加算加速
				frame += 10;
			}
			//フェード完了後
			else{
				DxLib::DrawGraph(0, 0, g_manager->getSystemGraphic(SystemGraphic::gallery_pic_large[next]).getHandle(), FALSE);
				//次の画像に進む、または一覧に戻る
				if (input->getKeepMouseLeft() != 0 && !back_to_list && !input_disabled){
					int res = nextPictureExist(config, g_manager, SystemGraphic::gallery_pic_large[next]);
					frame = 0;
					if (res >= 0){
						change_next_picture = true;
						before = next;
						next = res;
					}
					else{
						back_to_list = true;
					}
				}
				//右クリックで一覧に戻る
				if (input->getKeepMouseRight() != 0 && !back_to_list && frame >= 255 && !input_disabled){
					frame = 0;
					back_to_list = true;
				}
				//一覧に戻る際の描画処理
				if (back_to_list){
					//トラン中
					if (frame < 255){
						input_disabled = true;
						DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255 - frame);
						DxLib::DrawGraph(0, 0, g_manager->getSystemGraphic(SystemGraphic::gallery_pic_large[next]).getHandle(), FALSE);
						DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, frame);
						DxLib::DrawGraph(0, 0, g_manager->getSystemGraphic(SystemGraphic::arrnum_bg_gallery).getHandle(), FALSE);
						drawPictures(config, input, g_manager);
						DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
						frame += 10;

					}
					//トラン完了後
					else{
						DxLib::DrawGraph(0, 0, g_manager->getSystemGraphic(SystemGraphic::arrnum_bg_gallery).getHandle(), FALSE);
						drawPictures(config, input, g_manager);
						change_next_picture = false;
						before = -1;
						next = -1;
						back_to_list = false;
						gallery_large_mode = false;
					}
				}
			}
		}
	}
}

//縮小画像一覧の描画
void Scene::drawPictures(
	Config* config,
	Input* input,
	GraphicManager* g_manager)
{

	static auto drawPic = [this, g_manager](int pic_no) {
		DxLib::DrawGraph(
			g_manager->getSystemGraphic(SystemGraphic::gallery_pic_small[pic_no]).getX(),
			g_manager->getSystemGraphic(SystemGraphic::gallery_pic_small[pic_no]).getY(),
			g_manager->getSystemGraphic(SystemGraphic::gallery_pic_small[pic_no]).getHandle(),
			FALSE);

	};

	static auto drawPage = [this, config](int pagenum, function<void()> func){
		switch (pagenum){
		case 1:
			if (config->getSetting().cg_collected[cg_00]) drawPic(cg_00);
			if (config->getSetting().cg_collected[cg_02]) drawPic(cg_02);
			break;
		case 2:
			if (config->getSetting().cg_collected[cg_05]) drawPic(cg_05);
			break;
		case 3:

			break;
		default:
			break;
		}
	};

	switch (gallery_page_num)
	{
	case 1:
		if (page_changing){
			if (frame < 255){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255 - frame);
				drawPage(gallery_page_num_before, []{});
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, frame);
				drawPage(1, []{});
				DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
				frame += 10;
			}
			else{
				frame = 0;
				page_changing = false;
				drawPage(1, []{});
			}
		}
		else{
			drawPage(1, []{});
		}
		break;
	case 2:
		if (page_changing){
			if (frame < 255){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255 - frame);
				drawPage(gallery_page_num_before, []{});
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, frame);
				drawPage(2, []{});
				DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
				frame += 10;
			}
			else{
				frame = 0;
				page_changing = false;
				drawPage(2, []{});
			}
		}
		else{
			drawPage(2, []{});
		}
		break;
	case 3:
		if (page_changing){
			if (frame < 255){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255 - frame);
				drawPage(gallery_page_num_before, []{});
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, frame);
				drawPage(3, []{});
				DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
				frame += 10;
			}
			else{
				frame = 0;
				page_changing = false;
				drawPage(3, []{});
			}
		}
		else{
			drawPage(3, []{});
		}
		break;
	default:
		break;
	}


}

int Scene::loadPictures(GraphicManager* g_manager){
	auto load_pic_s = [this, g_manager](int num, char* filename, int x, int y){
		SystemGraphic::gallery_pic_small[num] = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_gallery_pic_small, filename, x, y);

	};

	//縮小画像のロード
	load_pic_s(cg_00, "event_1-1.png", 200, 100);
	load_pic_s(cg_02, "event_2-1.png", 600, 100);
	load_pic_s(cg_05, "event_3-1.png", 200, 100);


	//大きい画像は差分画像あり
	auto load_pic_l = [this, g_manager](int num, char* filename, int parent_num, int child_num){
		SystemGraphic::gallery_pic_large[num] = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_gallery_pic_large, filename, 0, 0, parent_num, child_num);
	};
	load_pic_l(cg_00, "event_1-1.png", cg_00, 1);
	load_pic_l(cg_01, "event_1-2.png", cg_00, 2);
	load_pic_l(cg_02, "event_2-1.png", cg_02, 1);
	load_pic_l(cg_03, "event_2-2.png", cg_02, 2);
	load_pic_l(cg_04, "event_2-3.png", cg_02, 3);
	load_pic_l(cg_05, "event_3-1.png", cg_05, 1);

	return 0;
}

int Scene::nextPictureExist(Config* config, GraphicManager* g_manager, arrnum now_pic){
	//現在表示中の拡大画像オブジェクトを取得
	SystemGraphic picture = g_manager->getSystemGraphic(now_pic);
	int parent = picture.getBranchParent();
	int child = picture.getBranchChild();

	SystemGraphic *s_graphics = g_manager->getSystemGraphics();
	for (int i = 0; i < TENG_GRAPHIC_SYSTEM_MAX; i++){
		//全ての拡大画像オブジェクトから、同じ親を持ち、現在よりも後の子画像を検索、その配列番号を返す
		if (parent >= 0 && child >= 0 && s_graphics[i].getBranchParent() == parent){
			if (s_graphics[i].getBranchChild() > child){
				for (int j = 0; j < TENG_GALLERY_MAX_NUMBER; j++){
					if (SystemGraphic::gallery_pic_large[j] == i){
						if (config->getSetting().cg_collected[j]){
							return j;
						}
					}
				}

			}
		}
	}
	return -1;
}