﻿#include "graphic.h"
#include "define.h"
#include "global.h"
#include "error_code_define.h"
#include <Windows.h>
#include <iostream>
Teng::Graphic::Graphic(){
	handle = 0;
	x = 0;
	y = 0;

	image_type = eGraphicType_null;
}

Teng::Graphic::~Graphic(){
	handle = 0;
	x = 0;
	y = 0;
	image_type = eGraphicType_null;
}

void Teng::Graphic::setHandle(int handle){
	this->handle = handle;
}

void Teng::Graphic::setImageType(int type){
	this->image_type = type;
}

int Teng::Graphic::getHandle(){
	return handle;
}

int Teng::Graphic::getX(){
	return x;
}

int Teng::Graphic::getY(){
	return y;
}

void Teng::Graphic::setX(int x){
	this->x = x;
}

void Teng::Graphic::setY(int y){
	this->y = y;
}






