﻿#include "define.h"
#include "DxLib.h"
#include "error_code_define.h"
#include "backlog.h"
#include <memory>
#include "click_wait.h"
#include "graphic\graphic_manager.h"

using namespace Teng;

ClickWait::ClickWait(GraphicManager* g_manager){
	f = 0;
	handle_now = handle_1 = g_manager->system_graphics[SystemGraphic::arrnum_wait_1].getHandle();
	handle_2 = g_manager->system_graphics[SystemGraphic::arrnum_wait_2].getHandle();
	handle_3 = g_manager->system_graphics[SystemGraphic::arrnum_wait_3].getHandle();
	handle_4 = g_manager->system_graphics[SystemGraphic::arrnum_wait_4].getHandle();
	handle_5 = g_manager->system_graphics[SystemGraphic::arrnum_wait_5].getHandle();
	handle_6 = g_manager->system_graphics[SystemGraphic::arrnum_wait_6].getHandle();
	handle_7 = g_manager->system_graphics[SystemGraphic::arrnum_wait_7].getHandle();
	handle_8 = g_manager->system_graphics[SystemGraphic::arrnum_wait_8].getHandle();
	handle_9 = g_manager->system_graphics[SystemGraphic::arrnum_wait_9].getHandle();
	handle_10 = g_manager->system_graphics[SystemGraphic::arrnum_wait_10].getHandle();
	handle_11 = g_manager->system_graphics[SystemGraphic::arrnum_wait_11].getHandle();
	handle_12 = g_manager->system_graphics[SystemGraphic::arrnum_wait_12].getHandle();
	handle_off = g_manager->system_graphics[SystemGraphic::arrnum_wait_off].getHandle();
}

ClickWait::~ClickWait(){}

void ClickWait::drawFall(){
	if (f < 120) handle_now = handle_1;
	else if (f < 126) handle_now = handle_2;
	else if (f < 132) handle_now = handle_3;
	else if (f < 138) handle_now = handle_4;
	else if (f < 144) handle_now = handle_5;
	else if (f < 150) handle_now = handle_6;
	else if (f < 156) handle_now = handle_7;
	else if (f < 162) handle_now = handle_8;
	else if (f < 168) handle_now = handle_9;
	else if (f < 174) handle_now = handle_10;
	else if (f < 180) handle_now = handle_11;
	else if (f < 186) handle_now = handle_12;
	else if (f >= 186){
		handle_now = handle_1;
		f = 1;
	}

	DrawGraph(946, 707, handle_now, TRUE);

	f++;
}

void ClickWait::drawFade(const int x, const int y){
	if (f < 120){
		DrawGraph(x, y, handle_off, TRUE);
	}
	else if (f < 150){
		DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255 - ((f - 120) * 8));
		DrawGraph(x, y, handle_off, TRUE);
		DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	}
	else{
		f = 0;
	}
	f++;
}