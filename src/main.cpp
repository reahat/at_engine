﻿#include "define.h"
#include "exception.h"

#define GLOBAL_VALUE_DEFINE
#include "global.h"
#include "class_header.h"

#include <Windows.h>
#include "resource.h"
#include <ImageHlp.h>
#pragma comment(lib, "imagehlp.lib")

//テスト用
#include "savedata.h"

#include <memory>

void initializeGraphic(Teng::GraphicManager* manager);
//void loadSystemGraphics(Teng::GraphicManager* manager);


//TODO 開発時のみ使用
char common_dir[256];
void setCommonDir();
bool is_hyper_skip = false;
#include <fstream>
#include <iostream>
#include "error_code_define.h"
std::shared_ptr<Teng::GraphicManager> graphic_manager;
std::shared_ptr<Teng::CmdMessage> message;//DxLib初期化後でないとフォントが生成できない
std::unique_ptr<Teng::Config> config;
std::unique_ptr<Teng::Input> input;
////

int WINAPI WinMain(HINSTANCE,HINSTANCE,LPSTR,int){
	// 画面初期色を白に
	DxLib::SetBackgroundColor(255, 255, 255);
	//アイコン指定
	DxLib::SetWindowIconID(IDI_ICON1);

	std::unique_ptr<Teng::Window> window(new Teng::Window);
	//std::unique_ptr<Teng::Config> config(new Teng::Config);
	config.reset(new Teng::Config);
	input.reset(new Teng::Input);
	//std::unique_ptr<Teng::Input> input(new Teng::Input);
	//std::unique_ptr<Teng::GraphicManager> graphic_manager(new Teng::GraphicManager);
	graphic_manager.reset(new Teng::GraphicManager);
	std::unique_ptr<Teng::Scene> scene(new Teng::Scene);
	std::unique_ptr<Teng::SoundManager> sound(new Teng::SoundManager);

	//Scenarioでunique_ptrを使うのは@importの都合上よくない
	Teng::Scenario *scenario = NULL;
	Teng::Scenario *subscenario = NULL;

	

	try{

		///////// commonディレクトリ位置のロード ///////////
		setCommonDir();
		////////////////////////////////////////////////

		//設定ファイルのロード・セット
		config->loadConfig(TENG_CONFIG_FILE);
		//DxLibのウィンドウ等基本設定
		window->setDxlibSetting(config.get());
		message.reset(new Teng::CmdMessage);//DxLib初期化後でないとフォントが生成できない
		message->clear();
		scenario = new Teng::Scenario("k001");
		//scenario = new Teng::Scenario("myscenario");
		scenario->setIsImportScenario(false);
		subscenario = new Teng::Scenario(TENG_SCENARIO_DUMMYFILE);
		cursor_on_button = false;
		scenario->resetLove();
		scene->afterInit();
		DxLib::SetFontSize(25);
		DxLib::SetFontThickness(3);
		//画像の初期化、初期読み込み
		initializeGraphic(graphic_manager.get());
		//最初から開始したとき一度だけ自動クリックするため
		scenario->startedFromTitle(true);

		while (DxLib::ProcessMessage() == 0)
		{
			window->previousProcess(config.get());
			input->checkKeyInput();
			input->checkKeepMouseLeft(scene.get());
			input->checkKeepMouseRight(scene.get());
			input->checkKeepKeyReturn(scene.get());
			input->checkKeepKeyF5(scene.get());
			input->checkKeepKeyF9(scene.get());
			input->checkKeepWheelDown(scene.get());
			input->checkKeepWheelUp(scene.get());

			//if Scenario
			if(scene->getNowScene() == Teng::Scene::eScene_scenario){
				scene->doScenario(message.get(), window.get(), config.get(),
					input.get(), graphic_manager.get(), sound.get(), scenario, subscenario);


				//scene->drawScreen(graphic_manager.get(), scenario, message.get(), config.get(), input.get(), sound.get(), window.get());
				if (true){
					//セーブ・ロード等のボタン押下・オンマウスチェック
					scenario->checkButtonClick(input.get(), window.get(), scene.get(), config.get(), graphic_manager.get(), sound.get(), message.get());
				}

				scene->doScenarioBacklog(window.get(), config.get(), input.get(),
					graphic_manager.get(), sound.get(), scenario, message.get());
				
				//テキスト表示完了・トラン完了状態のみオートモードのフレーム加算
				if(!message->getRenderingFlg() &&
				   !graphic_manager->background_graphics[Teng::BackGroundGraphic::after_trans_arrnum].getChanging())
				{
					scene->addAutoFrame();
				}else if (message->getRenderingFlg() && message->plus_stopping){
					scene->addAutoFrame();
				}

			}//Scenario

			//if Scenario-load
			else if(scene->getNowScene() == Teng::Scene::eScene_scenario_load){
				scene->doScenarioLoad(window.get(), config.get(), input.get(),
					graphic_manager.get(), sound.get(), scenario, message.get());
				scene->drawScreen(graphic_manager.get(), scenario, message.get(), config.get(), input.get(), sound.get(), window.get());
			}//Scenario-load


			//if Title-load
			else if(scene->getNowScene() == Teng::Scene::eScene_title_load){
				scene->doTitleLoad(window.get(), config.get(), input.get(),
					graphic_manager.get(), sound.get(), scenario, message.get());
				scene->drawScreen(graphic_manager.get(), scenario, message.get(), config.get(), input.get(), sound.get(), window.get());
			}//Title-load

			//if Scenario-save
			else if(scene->getNowScene() == Teng::Scene::eScene_scenario_save){
				scene->doScenarioSave(window.get(), config.get(), input.get(),
					graphic_manager.get(), sound.get(), scenario, message.get());
				scene->drawScreen(graphic_manager.get(), scenario, message.get(), config.get(), input.get(), sound.get(), window.get());
			}//Scenario-save

			//if Logo
			else if(scene->getNowScene() == Teng::Scene::eScene_logo){
				scene->doLogo(window.get(),	config.get(),
					input.get(), graphic_manager.get(),	sound.get());
				scene->drawScreen(graphic_manager.get(), scenario, message.get(), config.get(), input.get(), sound.get(), window.get());
			}//Logo

			//if Title
			else if(scene->getNowScene() == Teng::Scene::eScene_title){
				scene->doTitle(window.get(), config.get(), input.get(),
					graphic_manager.get(), sound.get());
				scene->drawScreen(graphic_manager.get(), scenario, message.get(), config.get(), input.get(), sound.get(), window.get());
			}//Title

			//if Title-config
			else if(scene->getNowScene() == Teng::Scene::eScene_title_config){
				scene->doTitleConfig(window.get(), config.get(), input.get(),
					graphic_manager.get(), sound.get());
				scene->drawScreen(graphic_manager.get(), scenario, message.get(), config.get(), input.get(), sound.get(), window.get());
			}//Title-config

			//if Scenario-config
			else if(scene->getNowScene() == Teng::Scene::eScene_scenario_config){
				scene->doScenarioConfig(window.get(), config.get(), input.get(),
					graphic_manager.get(), sound.get());
			}//Scenario-config

			else if(scene->getNowScene() == Teng::Scene::eScene_scenario_menu){
				scene->doScenarioMenu(window.get(), config.get(), input.get(),
					graphic_manager.get(), sound.get(), scenario, message.get());
				scene->doScenarioBacklog(window.get(), config.get(), input.get(),
					graphic_manager.get(), sound.get(), scenario, message.get());
			}//Scenario-menu

			//ハイパースキップ
			else if (scene->getNowScene() == Teng::Scene::eScene_hyperskip){
				scene->doHyperskip(message.get(), window.get(), config.get(), input.get(), graphic_manager.get(),
					sound.get(), scenario, subscenario);
				scene->drawScreen(graphic_manager.get(), scenario, message.get(), config.get(), input.get(), sound.get(), window.get());
			}
			//else if(scene->getNowScene() == Teng::Scene::eScene_scenario_backlog){
			//	scene->doScenarioBacklog(window.get(), config.get(), input.get(),
			//		graphic_manager.get(), sound.get(), scenario, message.get());
			//}
			else if (scene->getNowScene() == Teng::Scene::eScene_gallery){
				scene->doGallery(window.get(), config.get(), input.get(),
					graphic_manager.get(), sound.get(), scenario, message.get());
				
			}
			else if (scene->getNowScene() == Teng::Scene::eScene_movie){
				scene->doMovie(message.get(), window.get(), config.get(), input.get(), graphic_manager.get(), sound.get());
			}

			//ダイアログボックス表示
			window->drawCloseDialog(graphic_manager.get(), input.get(), scene.get(), sound.get(), config.get());

			window->postProcess();
			scene->addFrame();
		} // while


		DxLib::DxLib_End();
		delete scenario;
		delete subscenario;

		return 0;

	}catch(int errnum){
		//正常終了
		if(errnum == ENGINE_SYSTEM_STANDARD_EXIT){
			DEBUG printf("[INFO] システムを正常終了します。\n");
			//remove("tmp");
			string tmp_path = string(config->savedata_dir) + "tmp";
			remove(tmp_path.c_str());
			//delete scenario;
			//delete subscenario;
			DxLib::ProcessMessage();
			DxLib::DxLib_End();
			return 0;
		}
		//異常終了
		else{
			//エラーウィンドウを出力
			Teng::Exception err_obj;
			err_obj.putError(errnum);
			string tmp_path = string(config->savedata_dir) + "tmp";
			//remove("tmp");
			remove(tmp_path.c_str());
			DEBUG printf("[WARN] システムを異常終了します。\n");
			delete scenario;
			delete subscenario;
			return 1;
		}

		return -1;
	}
} // WinMain


//画像の初期化処理
void initializeGraphic(Teng::GraphicManager* manager){
	//ロゴ画面だけは起動時にロードする必要がある
	Teng::SystemGraphic::arrnum_bg_logo_1 = manager->setSystemGraphic(
		Teng::SystemGraphic::eGraphicSys_bg_logo_1,
		TENG_GRAPHIC_BACKGROUND_LOGO_1, 0, 0);
	Teng::SystemGraphic::arrnum_bg_white = manager->setSystemGraphic(
		Teng::SystemGraphic::eGraphicSys_bg_white,
		TENG_GRAPHIC_BACKGROUND_WHITE, 0, 0);
	Teng::SystemGraphic::arrnum_bg_blank = manager->setSystemGraphic(
		Teng::SystemGraphic::eGraphicSys_bg_blank,
		TENG_GRAPHIC_BACKGROUND_BLANK, 0, 0);


}



//開発時のみ使用
//共通資材ディレクトリ位置をロード
void setCommonDir(){
	//std::ifstream ifs("./common_dir.txt", std::ios::in);

	////読み込み失敗時
	//if(ifs.fail()){
	//	throw ENGINE_ERROR_DEFAULT;

	//}
	////設定ファイル読み込み成功時
	//else{
	//	ifs.read((char *)&common_dir, 256);
	//	ifs.close();
	//}

	HKEY hKey;
	HKEY hParentKey = HKEY_CURRENT_USER;
	LPCSTR lpszSubKey = "Software\\Cherry Creampie\\Ephialtes";
	LPCSTR installDirKey = "InstallDir";
	DWORD dwType = REG_SZ;
	LONG rc;
	char szText[256];
	DWORD dwByte = sizeof(szText) / sizeof(szText[0]);
	rc = RegOpenKeyEx(hParentKey, lpszSubKey, 0, KEY_ALL_ACCESS, &hKey);
	if (rc != ERROR_SUCCESS) throw ENGINE_ERROR_TEST;
	rc = RegQueryValueEx(hKey, installDirKey, NULL, &dwType, (LPBYTE)szText, &dwByte);
	RegCloseKey(hKey);
	if (rc != ERROR_SUCCESS) throw ENGINE_ERROR_TEST;

	strcpy_s(common_dir, szText);

	//セーブデータ保存ディレクトリが存在しなければ作る
	ExpandEnvironmentStrings(TENG_DIR_SAVEDATA, Teng::Config::savedata_dir, sizeof(Teng::Config::savedata_dir));
	if (MakeSureDirectoryPathExists(Teng::Config::savedata_dir)) {
		//なにもしない
	}
	else {
		throw ENGINE_SAVE_DIR_MAKE_FAILED;
	}
}