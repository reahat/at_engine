﻿//主にmain関数内で使用するものを扱う
#pragma once
#include <string>
#include "define.h"
#include "config.h"
#include <Windows.h>
#include "graphic\graphic_manager.h"
//#include "input.h"

namespace Teng{
	class Input;
	class Scene;
	class SoundManager;
	class Window
	{
	public:
		Window();
		virtual ~Window();

		//DxLibの基本設定
		void setDxlibSetting(Teng::Config* cnf);

		//DxLibの基本設定（DxLib_Init()の後に設定すべきもの）
		void setDxlibSettingAfterInit();

		//ウィンドウモード設定
		void setWindowMode(bool window_mode);

		//ウィンドウサイズ設定
		void setWindowSize(bool large);

		//描画前の画面処理
		void previousProcess(Teng::Config* cnf);

		//描画後の画面処理
		void postProcess();

		//終了ダイアログ
		void drawCloseDialog(Teng::GraphicManager* g_manager, Teng::Input* input, Scene* scene, SoundManager* s_manager, Config* config);

		//削除ダイアログ
		void drawDeleteDialog();

		//上書きダイアログ
		void drawOverwriteDialog();

		//ロードダイアログ
		void drawLoadDialog();

		//accessor
		int getScreen(){return screen;}
		void setCursorType(HCURSOR type){ cursor_type = type;}
		HCURSOR getCursorType(){ return cursor_type;}
		static void setDialogDrawing(bool drawing){ dialog_drawing = drawing;}
		static bool getDialogDrawing(){ return dialog_drawing;}
		//static void setPow(double power){ pow = power;}
		//static double getPow(){ return pow;}

	private:
		//dxlib用アーカイブ解凍パス（12桁）
		static const char dx_key_archive_string[13];

		//ウィンドウタイトル
		std::string window_title;

		//スクリーンハンドル（DX_SCREEN_BACKの代わりとして使用する）
		int screen;

		//拡大率（逆数）
		//static double pow;

		static bool dialog_drawing;

		HCURSOR cursor_type;


		struct Btn{
			int now;
			int normal;
			int focus;
			int click;
			int x;
			int y;
			int width;
			int height;
			int power;
			int status;
		};
		// ダイアログボックス用
		Btn btn_dialog_yes, btn_dialog_no, btn_dialog_base;
	
	};
}