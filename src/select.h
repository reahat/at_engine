﻿//キー入力処理クラス
#pragma once

#include "tag/cmd_message.h"
#include <string>
using std::string;

namespace Teng{
	class Select{
	public:
		Select();
		~Select();
		void   setMsg(string msg){select_message = msg;}
		void   setLabal(string lbl){label = lbl;}
		const string getMsg(){return select_message;}
		string getLabel(){return label;}
	private:
		string select_message;
		string label;
	};
}