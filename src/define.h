﻿#ifndef DEFINE_H_INCLUDED
#define DEFINE_H_INCLUDED

#ifdef _DEBUG
#define DEBUG //デバッグ時のみ使用する関数 例：DEBUG printf("...");
#define IF_DEBUG_TRUE TRUE
#define IF_DEBUG_FALSE FALSE
#else
#define DEBUG 1 ? (void)0 :
#define IF_DEBUG_TRUE FALSE
#define IF_DEBUG_FALSE TRUE
#endif

#include <string>
#define NC '\0' //念のためNULLを使わない

//型定義
typedef int arrnum;

//定数定義

#define TENG_VERSION "@0.0.1a"
#define TENG_SAVEDATA_PASSWORD 'teng'
#define TENG_DEFAULT_WINDOW_TITLE "Ephialtes" //デフォルトのウィンドウタイトル
#define TENG_DX_ARCHIVE_KEY "@T3nG1Ne_cC_" //DxLibのアーカイブ解凍パス12桁

#define TENG_CONFIG_FILE "config.cf"

//ファイル関係
#define TENG_FILE_NAME_MAX 256

//ディレクトリ
#define TENG_DIR_COMMON          "./common/"
#define TENG_DIR_CHARACTER_IMAGE "ci\\"
#define TENG_DIR_BACKGROUND_IMAGE "bi\\"
#define TENG_DIR_SYSTEM_IMAGE	 "si\\"
#define TENG_DIR_SYSTEM_SOUND    "ss\\"
#define TENG_DIR_RULE_IMAGE      "ri\\"
#define TENG_DIR_FACE_IMAGE      "fi\\"
#define TENG_DIR_SCENARIO		 "st\\"
#define TENG_DIR_BGM             "bm\\"
#define TENG_DIR_SE              "se\\"
#define TENG_DIR_FONT            "ft\\"
#define TENG_DIR_GALLERY		 "gi\\"
#define TENG_SCENARIO_DUMMYFILE  "dummy"
#define TENG_DIR_MOVIE           "mv\\"


//#define TENG_DIR_COMMON          "./common/"
//#define TENG_DIR_CHARACTER_IMAGE "./character_image/"
//#define TENG_DIR_BACKGROUND_IMAGE "background_image\\"
//#define TENG_DIR_SYSTEM_IMAGE	 "./system_image/"
//#define TENG_DIR_SYSTEM_SOUND    "./system_sound/"
//#define TENG_DIR_RULE_IMAGE      "./rule_image/"
//#define TENG_DIR_FACE_IMAGE      "./face_image/"
//#define TENG_DIR_SCENARIO		 "./script/"
//#define TENG_DIR_BGM             "./bgm/"
//#define TENG_DIR_SE              "./se/"
//#define TENG_DIR_FONT            "font\\"
//#define TENG_DIR_GALLERY		 "gallery_image\\"
//#define TENG_SCENARIO_DUMMYFILE  "dummy"
//#define TENG_DIR_MOVIE           "movie\\"
#define TENG_DIR_SAVEDATA		 "%APPDATA%\\Cherry Creampie\\Ephialtes\\data\\"

//動画
#define TENG_MOVIE_OPENING_FILE_NAME "movie.mp4"

//シナリオ関係
#define TENG_SCRIPT_MAX_LINE				2000 //シナリオファイルの最大行数
#define TENG_SCRIPT_MAX_STRING_LENGTH	200 //シナリオファイルの1行の最大文字数
#define TENG_SCRIPT_COMMAND_MAX_LENGTH	200 //コマンドの最大長さ
//#define TENG_SCRIPT_ARGUMENT_MAX_LENGTH	64 //引数の最大長さ
#define TENG_SCRIPT_ARGUMENT_MAX_NUMBER	10 //引数の最大個数
#define TENG_SCRIPT_COMMAND_PREFIX		"@"//コマンドのprefix
#define TENG_SCRIPT_DELIMITER			" /,"//コマンドおよび引数の区切り文字
#define TENG_SCRIPT_MESSAGE_MAX_LENGTH	60 //メッセージの一行の最大バイト数
#define TENG_SCRIPT_MESSAGE_MAX_LINE		4  //メッセージ１件の最大行数
const int TENG_BACKLOG_RECORD_LIMIT = 100;
#define TENG_SCRIPT_SCENARIO_FILE_SUFFIX ".asc"

//メッセージ設定
#define TENG_MESSAGE_FONT_SIZE 24
#define TENG_MESSAGE_MIN_WIDTH (TENG_MESSAGE_FONT_SIZE * 10)
#define TENG_MESSAGE_FULL_WIDTH 650
#define TENG_MESSAGE_POINT_X 289
//#define TENG_MESSAGE_POINT_Y 590
#define TENG_MESSAGE_POINT_Y 610
const int TENG_MESSAGE_SPEAKER_NAME_X = 289;
const int TENG_MESSAGE_SPEAKER_NAME_Y = 569;
#define TENG_MESSAGE_RENDER_SPEED_SUPER 19 //描画中にクリックした際のスピード（必ず奇数にする）
#define TENG_MESSAGE_FONT_NAME "KozMinPro-Bold"
#define TENG_MESSAGE_FONT_FILE_NAME "KozMinPro-Bold.otf"
#define TENG_MESSAGE_FONT_ANTIALIAS_TYPE DX_FONTTYPE_ANTIALIASING
#define TENG_RUBI_FONT_NAME "KozMinPro-Bold"
const int TENG_RUBI_FONT_SIZE = 10;
#define TENG_RUBI_FONT_ANTIALIAS_TYPE DX_FONTTYPE_ANTIALIASING
const char TENG_MESSAGE_SPACE_CHARACTER = '_';
const int TENG_BACKLOG_POINT_X    = 265;
const int TENG_BACKLOG_POINT_Y    = 72;
const int TENG_BACKLOG_LINE_SPACE = 13;
const int TENG_MESSAGE_CHARACTER_NAME_MAXLENGTH = 12;
#define TENG_MESSAGE_CHARACTER_NAME_DEFAULT "デフォルト"
const int TENG_MESSAGE_WINDOW_TRANS_WAIT_FRAME = 50;//メッセージウィンドウが再表示されてから、メッセージ表示が開始されるまでの時間
const int TENG_MESSAGE_WINDOW_TRANS_SPEED = 20;


//画面設定
#define TENG_SCREEN_LARGE		1
#define TENG_SCREEN_SMALL		0
#define TENG_SCREEN_WIDTH_LARGE	1024
#define TENG_SCREEN_HEIGHT_LARGE	768
#define TENG_SCREEN_WIDTH_SMALL	800
#define TENG_SCREEN_HEIGHT_SMALL	600
#define TENG_SCREEN_COLOR_BIT	16
#define TENG_SCREEN_SIZE_POWER_NORMAL 1.0
#define TENG_SCREEN_SIZE_POWER_SMALL	 1.28
const int TENG_GALLERY_MAX_NUMBER = 64;

//画像設定
#define TENG_GRAPHIC_SYSTEM_MAX		200
#define TENG_GRAPHIC_BACKGROUND_MAX	20
#define TENG_GRAPHIC_RULE_MAX		10
#define TENG_GRAPHIC_CHARACTER_MAX	30
#define TENG_GRAPHIC_FACE_MAX		30

// メッセージボックス
#define TENG_GRAPHIC_SYSTEM_GRAPHIC_MSG_WINDOW_FILE	"textbox/text_box_base.png"//メッセージウィンドウ
#define TENG_GRAPHIC_SYSTEM_GRAPHIC_MSG_WINDOW_X		0
#define TENG_GRAPHIC_SYSTEM_GRAPHIC_MSG_WINDOW_Y		500
const int TENG_GRAPHIC_SYSTEM_DIALOG_BOX_X = 300;
const int TENG_GRAPHIC_SYSTEM_DIALOG_BOX_Y = 250;
#define TENG_GRAPHIC_TEXTBOX_BASE "textbox/text_box_base.png"
#define TENG_GRAPHIC_TEXTBOX_CONFIG "textbox/config.png"
#define TENG_GRAPHIC_TEXTBOX_CONFIG_ON "textbox/config_on.png"
#define TENG_GRAPHIC_TEXTBOX_HSKIP "textbox/H-skip.png"
#define TENG_GRAPHIC_TEXTBOX_HSKIP_ON "textbox/H-skip_on.png"
#define TENG_GRAPHIC_TEXTBOX_QLOAD "textbox/Q-load.png"
#define TENG_GRAPHIC_TEXTBOX_QLOAD_ON "textbox/Q-load_on.png"
#define TENG_GRAPHIC_TEXTBOX_QSAVE "textbox/Q-save.png"
#define TENG_GRAPHIC_TEXTBOX_QSAVE_ON "textbox/Q-save_on.png"
#define TENG_GRAPHIC_TEXTBOX_SKIP "textbox/skip.png"
#define TENG_GRAPHIC_TEXTBOX_SKIP_ON "textbox/skip_on.png"
#define TENG_GRAPHIC_TEXTBOX_X "textbox/x.png"
#define TENG_GRAPHIC_TEXTBOX_X_ON "textbox/x_on.png"
#define TENG_GRAPHIC_TEXTBOX_AUTO "textbox/auto.png"
#define TENG_GRAPHIC_TEXTBOX_AUTO_ON "textbox/auto_on.png"

// バックログ
#define TENG_GRAPHIC_BACKLOG_BASE "backlog/base.png"
#define TENG_GRAPHIC_BACKLOG_BASE_BAR "backlog/base_bar.png"
#define TENG_GRAPHIC_BACKLOG_BAR_NORMAL "backlog/bar.png"
#define TENG_GRAPHIC_BACKLOG_BAR_FOCUS "backlog/bar_on.png"
#define TENG_GRAPHIC_BACKLOG_BAR_CLICK "backlog/bar_select.png"
#define TENG_GRAPHIC_BACKLOG_DOWN_NORMAL "backlog/down.png"
#define TENG_GRAPHIC_BACKLOG_DOWN_FOCUS "backlog/down_select.png"
#define TENG_GRAPHIC_BACKLOG_DOWN_CLICK "backlog/down_on.png"
#define TENG_GRAPHIC_BACKLOG_UP_NORMAL "backlog/up.png"
#define TENG_GRAPHIC_BACKLOG_UP_FOCUS "backlog/up_select.png"
#define TENG_GRAPHIC_BACKLOG_UP_CLICK "backlog/up_on.png"
#define TENG_GRAPHIC_BACKLOG_BACK_NORMAL "backlog/Back.png"
#define TENG_GRAPHIC_BACKLOG_BACK_FOCUS "backlog/Back.png"
#define TENG_GRAPHIC_BACKLOG_BACK_CLICK "backlog/Back2.png"



#define TENG_GRAPHIC_TRANSITION_NOEFFECT  0
#define TENG_GRAPHIC_TRANSITION_FADEIN    1
#define TENG_GRAPHIC_TRANSITION_CROSSFADE 2
#define TENG_GRAPHIC_TRANSITION_RULE      3
#define TENG_GRAPHIC_TRANSITION_FADEOUT   4
#define TENG_GRAPHIC_TRANSITON_THRESHOLD_DEFAULT 64
#define TENG_GRAPHIC_TRANSITION_SPEED_DEFAULT 10

#define TENG_GRAPHIC_BACKGROUND_TITLE  "start_tate.jpg"
#define TENG_GRAPHIC_BACKGROUND_LOGO_1 "logo/logogamen.jpg"
#define TENG_GRAPHIC_BACKGROUND_BLANK "logo/blank.png"
#define TENG_GRAPHIC_BACKGROUND_WHITE "logo/white.jpg"
#define TENG_GRAPHIC_BACKGROUND_SAVE_1 "save.jpg"
#define TENG_GRAPHIC_BACKGROUND_LOAD_1 "load.jpg"
#define TENG_GRAPHIC_BACKGROUND_CONFIG "config_back.jpg"
#define TENG_GRAPHIC_SYSTEM_SELECT_NORMAL "selection/off.png"
#define TENG_GRAPHIC_SYSTEM_SELECT_FOCUS "selection/on.png"
#define TENG_GRAPHIC_SYSTEM_SELECT_SELECTED "selection/off.png"
#define TENG_GRAPHIC_SYSTEM_MESSAGE_ARROW "arrow.png"
#define TENG_GRAPHIC_BACKGROUND_GALLERY "gallery_back.png"
#define TENG_GRAPHIC_SYSTEM_GALLERY_RULE_SLIDE "kosuri.png"
// ダイアログボックス
#define TENG_GRAPHIC_SYSTEM_DIALOG_BASE "dialog/base.png"
#define TENG_GRAPHIC_SYSTEM_DIALOG_YES_NORMAL "dialog/Yes.png"
#define TENG_GRAPHIC_SYSTEM_DIALOG_YES_CLICK "dialog/Yes_on.png"
#define TENG_GRAPHIC_SYSTEM_DIALOG_NO_NORMAL "dialog/No.png"
#define TENG_GRAPHIC_SYSTEM_DIALOG_NO_CLICK "dialog/No_on.png"

//タイトル画面のボタン類
#define TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_START_NORMAL   "start_0.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_START_FOCUS    "start_01.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_START_CLICK    "start_02.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_LOAD_NORMAL    "load_0.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_LOAD_FOCUS     "load_01.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_LOAD_CLICK     "load_02.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_CONFIG_NORMAL  "confg_0.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_CONFIG_FOCUS   "confg_01.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_CONFIG_CLICK   "confg_02.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_GALLERY_NORMAL "gallery_0.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_GALLERY_FOCUS  "gallery_01.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_GALLERY_CLICK  "gallery_02.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_EXIT_NORMAL    "exit_0.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_EXIT_FOCUS     "exit_01.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_EXIT_CLICK     "exit_02.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_KAGO_NORMAL    "title/title_icon_off.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_KAGO_FOCUS     "title/title_icon_on.png"

//セーブ・ロード画面のボタン類
#define TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_1_NORMAL "1_defo.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_1_FOCUS  "1_select.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_1_CLICK  "1_on.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_2_NORMAL "2_defo.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_2_FOCUS  "2_select.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_2_CLICK  "2_on.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_3_NORMAL "3_defo.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_3_FOCUS  "3_select.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_3_CLICK  "3_on.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_4_NORMAL "4_defo.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_4_FOCUS  "4_select.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_4_CLICK  "4_on.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_5_NORMAL "5_defo.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_5_FOCUS  "5_select.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_5_CLICK  "5_on.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_BACK_NORMAL   "back_0.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_BACK_FOCUS    "back_01.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_BACK_CLICK    "back_02.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_ICON_BARA     "bara.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_ICON_ARROW    "yazirushi.png"
#define TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_THUMBNAIL_ARROW    "saveload_thunbnail_arrow.png"
#define TENG_GRAPHIC_SYSTEM_RULE_SAVE_CONFIRM_DIALOG  "rule_confirm_dialog.png"
#define TENG_GRAPHIC_SYSTEM_RULE_SAVE_CONFIRM_DIALOG_REVERSE "rule_confirm_dialog_rev.png"

//メニュー画面
#define SYSTEM_MENU_BUTTON_SAVE_NORMAL "menu/save_0.png"
#define SYSTEM_MENU_BUTTON_LOAD_NORMAL "menu/load_0.png"
#define SYSTEM_MENU_BUTTON_HSKIP_NORMAL "menu/HyperSkip_0.png"
#define SYSTEM_MENU_BUTTON_CONFIG_NORMAL "menu/confg_0.png"
#define SYSTEM_MENU_BUTTON_BACKLOG_NORMAL "menu/BackLog_0.png"
#define SYSTEM_MENU_BUTTON_TITLE_NORMAL "menu/Title_0.png"
#define SYSTEM_MENU_BUTTON_EXIT_NORMAL "menu/exit_0.png"
#define SYSTEM_MENU_BUTTON_BACK_NORMAL "menu/Back_0.png"
#define SYSTEM_MENU_BUTTON_SAVE_FOCUS "menu/save_01.png"
#define SYSTEM_MENU_BUTTON_LOAD_FOCUS "menu/load_01.png"
#define SYSTEM_MENU_BUTTON_HSKIP_FOCUS "menu/HyperSkip_01.png"
#define SYSTEM_MENU_BUTTON_CONFIG_FOCUS "menu/confg_01.png"
#define SYSTEM_MENU_BUTTON_BACKLOG_FOCUS "menu/BackLog_01.png"
#define SYSTEM_MENU_BUTTON_TITLE_FOCUS "menu/Title_01.png"
#define SYSTEM_MENU_BUTTON_EXIT_FOCUS "menu/exit_01.png"
#define SYSTEM_MENU_BUTTON_BACK_FOCUS "menu/Back_01.png"
#define SYSTEM_MENU_BUTTON_SAVE_CLICK "menu/save_02.png"
#define SYSTEM_MENU_BUTTON_LOAD_CLICK "menu/load_02.png"
#define SYSTEM_MENU_BUTTON_HSKIP_CLICK "menu/HyperSkip_02.png"
#define SYSTEM_MENU_BUTTON_CONFIG_CLICK "menu/confg_02.png"
#define SYSTEM_MENU_BUTTON_BACKLOG_CLICK "menu/BackLog_02.png"
#define SYSTEM_MENU_BUTTON_TITLE_CLICK "menu/Title_02.png"
#define SYSTEM_MENU_BUTTON_EXIT_CLICK "menu/exit_02.png"
#define SYSTEM_MENU_BUTTON_BACK_CLICK "menu/Back_02.png"
#define SYSTEM_MENU_BASE "menu/menu_base.png"



//コンフィグ画面
#define SYSTEM_BUTTON_CONFIG_BAR_BASE "base_bar.png"
#define SYSTEM_BUTTON_CONFIG_MINUS_NORMAL "-.png"
#define SYSTEM_BUTTON_CONFIG_MINUS_FOCUS "-_select.png"
#define SYSTEM_BUTTON_CONFIG_MINUS_CLICK "-_on.png"
#define SYSTEM_BUTTON_CONFIG_PLUS_NORMAL "+.png"
#define SYSTEM_BUTTON_CONFIG_PLUS_FOCUS "+_select.png"
#define SYSTEM_BUTTON_CONFIG_PLUS_CLICK "+_on.png"
#define SYSTEM_BUTTON_CONFIG_SKIP_ALL_NORMAL "all.png"
#define SYSTEM_BUTTON_CONFIG_SKIP_ALL_FOCUS "all_select.png"
#define SYSTEM_BUTTON_CONFIG_SKIP_ALL_CLICK "all_on.png"
#define SYSTEM_BUTTON_CONFIG_SKIP_ALREADY_NORMAL "already.png"
#define SYSTEM_BUTTON_CONFIG_SKIP_ALREADY_FOCUS "already_select.png"
#define SYSTEM_BUTTON_CONFIG_SKIP_ALREADY_CLICK "already_on.png"
#define SYSTEM_BUTTON_CONFIG_BACK_NORMAL "back_0.png"
#define SYSTEM_BUTTON_CONFIG_BACK_FOCUS "back_01.png"
#define SYSTEM_BUTTON_CONFIG_BACK_CLICK "back_02.png"
#define SYSTEM_BUTTON_CONFIG_BAR_NORMAL "bar.png"
#define SYSTEM_BUTTON_CONFIG_BAR_FOCUS "bar_on.png"
#define SYSTEM_BUTTON_CONFIG_BAR_CLICK "bar_select.png"

// 名前入力
#define TENG_GRAPHIC_SYSTEM_NAME_ENTRY_BOX "name_entry/base.png"
#define TENG_GRAPHIC_SYSTEM_NAME_ENTRY_INIT_NORMAL "name_entry/initialization.png"
#define TENG_GRAPHIC_SYSTEM_NAME_ENTRY_INIT_CLICK "name_entry/initialization_on.png"
#define TENG_GRAPHIC_SYSTEM_NAME_ENTRY_DECISION_NORMAL "name_entry/decision.png"
#define TENG_GRAPHIC_SYSTEM_NAME_ENTRY_DECISION_CLICK "name_entry/decision_on.png"

// クリック待ち
#define TENG_GRAPHIC_WAIT_1 "wait/1.png"
#define TENG_GRAPHIC_WAIT_2 "wait/2.png"
#define TENG_GRAPHIC_WAIT_3 "wait/3.png"
#define TENG_GRAPHIC_WAIT_4 "wait/4.png"
#define TENG_GRAPHIC_WAIT_5 "wait/5.png"
#define TENG_GRAPHIC_WAIT_6 "wait/6.png"
#define TENG_GRAPHIC_WAIT_7 "wait/7.png"
#define TENG_GRAPHIC_WAIT_8 "wait/8.png"
#define TENG_GRAPHIC_WAIT_9 "wait/9.png"
#define TENG_GRAPHIC_WAIT_10 "wait/10.png"
#define TENG_GRAPHIC_WAIT_11 "wait/11.png"
#define TENG_GRAPHIC_WAIT_12 "wait/12.png"
#define TENG_GRAPHIC_WAIT_OFF "wait/off.png"

//画像表示位置座標
#define TENG_GRAPHIC_LOCATION_LEFT_LEFT     100
#define TENG_GRAPHIC_LOCATION_LEFT_MIDDLE   150
#define TENG_GRAPHIC_LOCATION_LEFT_RIGHT    200
#define TENG_GRAPHIC_LOCATION_CENTER_LEFT   250
#define TENG_GRAPHIC_LOCATION_CENTER_MIDDLE 300
#define TENG_GRAPHIC_LOCATION_CENTER_RIGHT  350
#define TENG_GRAPHIC_LOCATION_RIGHT_LEFT    400
#define TENG_GRAPHIC_LOCATION_RIGHT_MIDDLE  450
#define TENG_GRAPHIC_LOCATION_RIGHT_RIGHT   500
#define TENG_GRAPHIC_LOCATION_Y             200

#define TENG_GRAPHIC_FACE_X 100
#define TENG_GRAPHIC_FACE_Y 500

#define TENG_GRAPHIC_SELECT_TOP_X 182
#define TENG_GRAPHIC_SELECT_TOP_Y 181
#define TENG_GRAPHIC_SELECT_DISTANCE 80
#define TENG_GRAPHIC_SELECT_WIDTH 658
#define TENG_GRAPHIC_SELECT_HEIGHT 58
const int TENG_GRAPHIC_NAME_ENTRY_BOX_X = 300;
const int TENG_GRAPHIC_NAME_ENTRY_BOX_Y = 200;
const int TENG_GRAPHIC_NAME_ENTRY_CURSOR_X = 330;
const int TENG_GRAPHIC_NAME_ENTRY_CURSOR_Y = 230;

//音声
#define TENG_SOUND_SYSTEM_MAX_COUNT 20
#define TENG_SOUND_BGM_MAX_COUNT 10
#define TENG_SOUND_SE_MAX_COUNT  10
#define TENG_SOUND_BGM_VOLUME_DEFAULT 255
#define TENG_SOUND_SE_VOLUME_DEFAULT 255
#define TENG_SOUND_SE_CONFIG_DEMO "pi.wav"
#define TENG_SOUND_SE_SELECT_CLICKED "select_clicked.wav"
#define TENG_SOUND_SE_SELECT_FOCUS "sentakushi.mp3"
#define TENG_SOUND_BGM_TITLE "op.ogg"
#define TENG_SOUND_SYSTEM_BUTTON_TITLE_CLICKED "start.mp3"
#define TENG_SOUND_SYSTEM_BUTTON_TITLE_FOCUS "button.mp3"
const int TENG_SOUND_SE_SELECT_CLICKED_VOLUME = 255;
const int TENG_SOUND_SE_SELECT_FOCUS_VOLUME   = 255;


//コンフィグ設定デフォルト値
#define TENG_CONFIG_DEFAULT_AUTO_MESSAGE_WAIT 5 //100
#define TENG_CONFIG_DEFAULT_BGM_VOLUME		  200
#define TENG_CONFIG_DEFAULT_MASTER_VOLUME	  200
#define TENG_CONFIG_DEFAULT_MESSAGE_WAIT      5
#define TENG_CONFIG_DEFAULT_FORCE_SKIP		  0
#define TENG_CONFIG_DEFAULT_SOUND_VOLUME      200
#define TENG_CONFIG_DEFAULT_WINDOW_SIZE		  1
#define TENG_CONFIG_DEFAULT_WINDOW_MODE		  1

//セーブ関係
const int TENG_SAVE_SCREENSHOT_WIDTH  = 174;
const int TENG_SAVE_SCREENSHOT_HEIGHT = 128;
const int TENG_SAVE_SAVEDATA_MAX      = 64;
//#define TENG_SAVE_FILENAME "data/data_%d.sav"
#define TENG_SAVE_FILENAME (string(Config::savedata_dir)+string("data_%d.sav")).c_str()
//#define TENG_DIR_ALREADY_READ_FILE "data/"
#define TENG_FILE_ALREADY_READ_FILE_EXTENSION ".ard"

//好感度用キャラクター名
const int TENG_CHARACTER_MAX = 10;
const std::string TENG_CHARACTER_NAME_TAG_1 = "jrd";
const std::string TENG_CHARACTER_NAME_TAG_2 = "anz";
const std::string TENG_CHARACTER_NAME_TAG_3 = "drz";
const std::string TENG_CHARACTER_NAME_TAG_4 = "stf";
const std::string TENG_CHARACTER_NAME_TAG_5 = "";
const std::string TENG_CHARACTER_NAME_TAG_6 = "";
const std::string TENG_CHARACTER_NAME_TAG_7 = "";
const std::string TENG_CHARACTER_NAME_TAG_8 = "";
const std::string TENG_CHARACTER_NAME_TAG_9 = "";

//選択肢
const int TENG_SELECT_AFTER_CLICK_WAIT_TIME = 60;


//スクリプトタグ
#define TENG_TAG_COMMENT	  0
#define TENG_TAG_MESSAGE	  1
#define TENG_TAG_LOADCHAR	  2
#define TENG_TAG_DRAWCHAR	  3
#define TENG_TAG_HIDECHAR     4
#define TENG_TAG_FREECHAR     5
#define TENG_TAG_IMPORT       6
#define TENG_TAG_RETURN       7
#define TENG_TAG_LABEL        8
#define TENG_TAG_GOTO         9
#define TENG_TAG_LOADBGIMAGE 10
#define TENG_TAG_DRAWBGIMAGE 11
#define TENG_TAG_LOADRULE    12
#define TENG_TAG_FREEBGIMAGE 13
#define TENG_TAG_FREERULE    14
#define TENG_TAG_LOADFACE    15
#define TENG_TAG_DRAWFACE    16
#define TENG_TAG_HIDEFACE    17
#define TENG_TAG_FREEFACE    18
#define TENG_TAG_LOADBGM     19
#define TENG_TAG_PLAYBGM     20
#define TENG_TAG_STOPBGM     21
#define TENG_TAG_FREEBGM     22
#define TENG_TAG_LOADSE      23
#define TENG_TAG_PLAYSE      24
#define TENG_TAG_STOPSE      25
#define TENG_TAG_FREESE      26
#define TENG_TAG_SELECT      27
#define TENG_TAG_LOVE        28
#define TENG_TAG_IF          29
#define TENG_TAG_NAMESET     30
#define TENG_TAG_SAY         31
#define TENG_TAG_SETTITLE    32
#define TENG_TAG_RESETTITLE  33
#define TENG_TAG_WAIT        34
#define TENG_TAG_HIDEMSG     35
#define TENG_TAG_DISPMSG     36
#define TENG_TAG_PLAYMOVIE   37
#define TENG_TAG_JUMP        38
#define TENG_TAG_CHANGECHAR  39
#define TENG_TAG_DISPGALLERY 40
#define TENG_TAG_EXMESSAGE   41
#define TENG_TAG_EFFECTFLASH   42
#define TENG_TAG_EFFECTSEPIA   43
#define TENG_TAG_EFFECTSHAKE   44
#define TENG_TAG_CHANGESCENE   45

#endif //DEFINE_H_INCLUDED