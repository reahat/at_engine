﻿#define _CRT_SECURE_NO_WARNINGS

#include "scenario.h"
#include "DxLib.h"
#include "error_code_define.h"
#include "scene.h"
#include "tag\cmd_message.h"
#include "graphic.h"
#include <string>
#include <map>
#include "sound\sound_manager.h"
#include "global.h"
#include <memory>

extern bool is_hyper_skip;
extern char common_dir[256];
extern std::shared_ptr<Teng::GraphicManager> graphic_manager;

std::map<std::string, int> Teng::Scenario::command_map;

Teng::Scenario::Scenario(){
	DEBUG printf("[ERROR] Scenarioの引数なしコンストラクタは使用しないこと\n");
	throw ENGINE_ERROR_DEFAULT;
}

Teng::Scenario::~Scenario(){}


Teng::Scenario::Scenario(char *file){
	change_title_flg = false;
	is_x_clicked = false;
	is_initialized = false;
	int pos = 0;
	int line = 0;
	char c;
	//スクリプトファイル
	int fp;
	flag_reset_flg = false;
	is_name_valid = true;
	is_hyper_skip_button_pushed = false;
	

	//コマンドリストの生成
	//TODO コマンドマップが存在しない場合のみ作成する必要あり
	makeCommandMap();

	//変数のクリア
	for(int s = 0; s < TENG_SCRIPT_ARGUMENT_MAX_NUMBER+1; s++){
		command[s][0] = '\0';
	}
	for(int m = 0; m < TENG_SCRIPT_MESSAGE_MAX_LINE; m++){
		msg_buffer[m][0] = '\0';
	}
	for(int i = 0; i < TENG_SCRIPT_MAX_LINE; i++){
		already_read_flag[i] = false;
	}

	//TODO
	//////開発用
	char fullpath[256];
	strcpy_s(fullpath, 256, common_dir);
	strcat_s(fullpath, 256, TENG_DIR_SCENARIO);
	strcat_s(fullpath, 256, file);
	strcat_s(fullpath, 256, TENG_SCRIPT_SCENARIO_FILE_SUFFIX);

	//スクリプトファイルを開く
	fp = DxLib::FileRead_open(fullpath);
	while(DxLib::CheckHandleASyncLoad(fp) != FALSE){
		DxLib::ProcessMessage();
		DEBUG printf("[INFO] シナリオファイルロード中\n");
		Sleep(300);
	}

	if( fp == NULL ) {
		//ファイル読み込みに失敗
		DEBUG printf("[ERROR] スクリプトの読み込みに失敗しました: %s\n", file);
		throw ENGINE_SCRIPT_FILE_READ_FAILURE;
	}

	//script書き込み時に使用
	pos = 0;

	while(true) {
		//一文字読み込み
		c = DxLib::FileRead_getc( fp );
		//ファイルの終わりかどうか
		if( DxLib::FileRead_eof( fp ) ) {
			script[line][pos] = c;
			script[line][pos+1] = '\0';
			for(int i = line+1; i < TENG_SCRIPT_MAX_LINE; i++){
				script[i][0] = '\0';
			}
			break;
		}
		//文章先頭の空白部分を読み飛ばす
		while( (c == ' ' || c == '\t') && pos == 0 && !FileRead_eof( fp ) ) {
			c = DxLib::FileRead_getc( fp );
		}

		if( pos >= TENG_SCRIPT_MAX_STRING_LENGTH - 1 ) {
			//1行の文字数が多すぎる
			DEBUG printf("スクリプトの文字数が多すぎます: %s (%d行目)\n", file, line+1);
			throw ENGINE_SCRIPT_FILE_STRING_TOO_MUCH;
		}

		//改行文字が出てきた場合，次の行へ移動
		if( c == '\n' || c == '\r') {
			//空行は読み飛ばす
			if( pos == 0 ) {
				continue;
			}
			//\0を文字列の最後に付ける
			script[line][pos] = '\0';
			//次の行に移動
			line++;
			//書き込み位置を0にする
			pos = 0;
		}else {
			//書き込み
			script[line][pos] = c;
			//文字書き込み位置をずらす
			pos++;
		}
	}
	//最大行数
	max_line = line;

	//読み込み時用の現在ラインを初期化
	current_line = 0;

	//スクリプトファイル名を設定

	char ext_name[256];
	strcpy_s(ext_name, TENG_FILE_NAME_MAX, file);
	size_t filename_len = strnlen_s(ext_name, TENG_FILE_NAME_MAX);
	for (size_t cur = filename_len; cur > 0; cur--){
		if (ext_name[cur] == '.'){
			ext_name[cur] = NC;
			break;
		}
	}

	strcpy_s(scenario_file, TENG_FILE_NAME_MAX, ext_name);

	FileRead_close(fp);


	//既読ファイルを読む
	FILE *already_fp;
	char already_read_file_full_path[TENG_FILE_NAME_MAX];
	
	sprintf_s(already_read_file_full_path, TENG_FILE_NAME_MAX, "%s%s%s", Config::savedata_dir, ext_name, TENG_FILE_ALREADY_READ_FILE_EXTENSION);
	if(fopen_s(&already_fp, already_read_file_full_path, "rb") != 0){
		DEBUG printf("[INFO] 既読情報ファイルを作成します：%s%s\n", ext_name, TENG_FILE_ALREADY_READ_FILE_EXTENSION);
		fopen_s(&already_fp, already_read_file_full_path, "wb");
		fwrite(&already_read_flag, sizeof(short)*TENG_SCRIPT_MAX_LINE, 1, already_fp);
		fclose(already_fp);
	}else{
		DEBUG printf("[INFO] 既読情報ファイル読込成功：%s%s\n", ext_name, TENG_FILE_ALREADY_READ_FILE_EXTENSION);
		fread_s(already_read_flag, sizeof(short)*TENG_SCRIPT_MAX_LINE, sizeof(short), TENG_SCRIPT_MAX_LINE, already_fp);
		fclose(already_fp);
	}

}

void Teng::Scenario::makeCommandMap(){

	command_map.insert(std::pair<std::string, int>("#", TENG_TAG_COMMENT));
	command_map.insert(std::pair<std::string, int>("@msg", TENG_TAG_MESSAGE));
	command_map.insert(std::pair<std::string, int>("@message", TENG_TAG_MESSAGE));
	command_map.insert(std::pair<std::string, int>("@char.load", TENG_TAG_LOADCHAR));
	command_map.insert(std::pair<std::string, int>("@char.draw", TENG_TAG_DRAWCHAR));
	command_map.insert(std::pair<std::string, int>("@char.hide", TENG_TAG_HIDECHAR));
	command_map.insert(std::pair<std::string, int>("@char.free", TENG_TAG_FREECHAR));
	command_map.insert(std::pair<std::string, int>("@char.change", TENG_TAG_CHANGECHAR));
	command_map.insert(std::pair<std::string, int>("@import", TENG_TAG_IMPORT));
	command_map.insert(std::pair<std::string, int>("@return", TENG_TAG_RETURN));
	command_map.insert(std::pair<std::string, int>("@label", TENG_TAG_LABEL));
	command_map.insert(std::pair<std::string, int>("@goto", TENG_TAG_GOTO));
	command_map.insert(std::pair<std::string, int>("@bg.load", TENG_TAG_LOADBGIMAGE));
	command_map.insert(std::pair<std::string, int>("@bg.draw", TENG_TAG_DRAWBGIMAGE));
	command_map.insert(std::pair<std::string, int>("@bg.change", TENG_TAG_DRAWBGIMAGE));
	command_map.insert(std::pair<std::string, int>("@rule.load", TENG_TAG_LOADRULE));
	command_map.insert(std::pair<std::string, int>("@rule.free", TENG_TAG_FREERULE));
	command_map.insert(std::pair<std::string, int>("@bg.free", TENG_TAG_FREEBGIMAGE));
	command_map.insert(std::pair<std::string, int>("@face.load", TENG_TAG_LOADFACE));
	command_map.insert(std::pair<std::string, int>("@face.draw", TENG_TAG_DRAWFACE));
	command_map.insert(std::pair<std::string, int>("@face.hide", TENG_TAG_HIDEFACE));
	command_map.insert(std::pair<std::string, int>("@face.free", TENG_TAG_FREEFACE));
	command_map.insert(std::pair<std::string, int>("@bgm.load", TENG_TAG_LOADBGM));
	command_map.insert(std::pair<std::string, int>("@bgm.play", TENG_TAG_PLAYBGM));
	command_map.insert(std::pair<std::string, int>("@bgm.stop", TENG_TAG_STOPBGM));
	command_map.insert(std::pair<std::string, int>("@bgm.free", TENG_TAG_FREEBGM));
	command_map.insert(std::pair<std::string, int>("@se.load", TENG_TAG_LOADSE));
	command_map.insert(std::pair<std::string, int>("@se.play", TENG_TAG_PLAYSE));
	command_map.insert(std::pair<std::string, int>("@se.stop", TENG_TAG_STOPSE));
	command_map.insert(std::pair<std::string, int>("@se.free", TENG_TAG_FREESE));
	command_map.insert(std::pair<std::string, int>("@select", TENG_TAG_SELECT));
	command_map.insert(std::pair<std::string, int>("@char.love", TENG_TAG_LOVE));
	command_map.insert(std::pair<std::string, int>("@if", TENG_TAG_IF));
	command_map.insert(std::pair<std::string, int>("@nameset", TENG_TAG_NAMESET));
	command_map.insert(std::pair<std::string, int>("@say", TENG_TAG_SAY));
	command_map.insert(std::pair<std::string, int>("@window.settitle", TENG_TAG_SETTITLE));
	command_map.insert(std::pair<std::string, int>("@window.resettitle", TENG_TAG_RESETTITLE));
	command_map.insert(std::pair<std::string, int>("@wait", TENG_TAG_WAIT));
	command_map.insert(std::pair<std::string, int>("@msg.hide",TENG_TAG_HIDEMSG));
	command_map.insert(std::pair<std::string, int>("@msg.display",TENG_TAG_DISPMSG));
	command_map.insert(std::pair<std::string, int>("@movie.play", TENG_TAG_PLAYMOVIE));
	command_map.insert(std::pair<std::string, int>("@jump", TENG_TAG_JUMP));
	command_map.insert(std::pair<std::string, int>("@flag.displayGalleryMenu", TENG_TAG_DISPGALLERY));
	command_map.insert(std::pair<std::string, int>("@msg.ex", TENG_TAG_EXMESSAGE));
	command_map.insert(std::pair<std::string, int>("@effect.flash", TENG_TAG_EFFECTFLASH));
	command_map.insert(std::pair<std::string, int>("@effect.sepia", TENG_TAG_EFFECTSEPIA));
	command_map.insert(std::pair<std::string, int>("@effect.shake", TENG_TAG_EFFECTSHAKE));
	command_map.insert(std::pair<std::string, int>("@scene.change", TENG_TAG_CHANGESCENE));



}

//スクリプト行を解析、コマンドおよび引数を保存する
//コマンドコードを返す
//TODO 引数のscenarioはthisのことではないのか？
int Teng::Scenario::decodeScript(Teng::Input* input, Teng::Scene* scene, Teng::Config* config, Teng::CmdMessage* msg){
	int tagnum = 0;

	//クリックしたら解析
	if(!scene->isBacklogEnable() && input->getKeepMouseLeft() != 0 ||
		scene->checkAutoMsg(config, input) ||
		checkForceSkip(input, config, msg) ||
		is_hyper_skip ||
		msg->getForceClickFlg()){
			msg->setRenderingFlg(true);
			//スクリプトの分割処理
			splitString(this->current_line);

			//分割に失敗した場合
			if(this->script[this->current_line][0] == '\0'){
				DEBUG printf("スクリプトの分割処理に失敗しました: %d行目\n", this->current_line);
				throw ENGINE_SCRIPT_SPLIT_FAILURE;
			}

			//読み込んだ行をデバッグ表示
			//DEBUG printf("[NEXT SCRIPT #%d] %s\n", this->current_line, this->script[this->current_line]);

			//コマンドコードの取得
			tagnum = tagToConstNum(command[0]);

			//次のシナリオ行へ
			//FIXME オブジェクト指向的にこれでいいのか？
			//scenario.current_line++;
	}
	return tagnum;
}

//文字列分割(1行の最大文字数は SCRIPT_MAX_STRING_LENGTH)
//src : 分割したい文字列
//dest: 分割された文字列
//delim: 区切り文字
//splitNum : 最大分割数
//void splitString(const char* src, char* dest[], const char* delim, int splitNum)
void Teng::Scenario::splitString(int line)
{
	//FIXME strtok_sを使うこと
	int i;
	char* cp;
	char* copySrc;
	char* dst;

	const char* command_prefix = TENG_SCRIPT_COMMAND_PREFIX;
	const char* delim = TENG_SCRIPT_DELIMITER;



	//元の文章をコピーする
	copySrc = (char*)malloc( sizeof(int) * TENG_SCRIPT_MAX_STRING_LENGTH + 1);

	strncpy_s(copySrc, TENG_SCRIPT_MAX_STRING_LENGTH, this->script[line], TENG_SCRIPT_MAX_STRING_LENGTH );
	cp = copySrc;

	//strtokを使って copySrc をdelim区切りで分割する
	for( i = 0; i < TENG_SCRIPT_ARGUMENT_MAX_NUMBER+1 ; i++ ) {
		//分割対象文字列が無くなるまで分割
		//if( (dst = strtok(cp, delim)) == NULL ) {
		if( (dst = strtok(cp, TENG_SCRIPT_DELIMITER)) == NULL ) {
			//strcpy(this->command[i], dst);
			break;

		}
		strcpy(this->command[i], dst);
		//2回目にstrtokを呼び出す時は，cpをNULLにする
		cp = NULL;
	}
	//分割された文字列の最後の要素はNULLとしておく
	this->command[i][0] = NULL;

	free(copySrc);
}

int Teng::Scenario::tagToConstNum(char *cmd){
	if (cmd[0] == '#'){
		return command_map["#"];
	}
	//else if ((UCHAR)cmd[0] == 0x81 && (UCHAR)cmd[1] == 0x97){
	//	//全角＠から始まるもの
	//	return command_map["@msg.ex"];
	//}
	else if((UCHAR)cmd[0] == '@' && command_map.find(cmd) == command_map.end()){
		DEBUG printf("無効なコマンドが存在します: %s", cmd);
		throw ENGINE_SCRIPT_INVALID_COMMAND;
	}
	else if ((UCHAR)cmd[0] == '@'){
		return command_map[cmd];
	}
	else{
		return command_map["@msg.ex"];
	}
}

int Teng::Scenario::getCurrentLine(){
	return current_line;
}

void Teng::Scenario::setNextTag(int tagnum){
	next_tag = tagnum;
}
void Teng::Scenario::setIsImportScenario(bool scenario_type){
	is_import_scenario = scenario_type;
}
bool Teng::Scenario::getIsImportScenario(){
	return is_import_scenario;
}
char* Teng::Scenario::getScenarioFile(){
	return scenario_file;
}
void Teng::Scenario::setScenarioFile(const char* filename){
	strcpy_s(scenario_file, TENG_FILE_NAME_MAX, filename);
}
void Teng::Scenario::setCurrentLine(const int line){
	current_line = line;
}



//コマンド実行
bool Teng::Scenario::executeTag(
	Teng::Input* input,
	Teng::Scene* scn,
	Teng::CmdMessage* msg,
	Teng::GraphicManager* graphic,
	Teng::Scenario* subscenario,
	Teng::SoundManager* sound,
	Teng::Config* config){

		//画像処理やコメント等、クリックが不要なものを処理したらtrueになる
		bool loop_flg = false;

		if(input->getKeepMouseLeft() != 0 || scn->checkAutoMsg(config, input) || checkForceSkip(input, config, msg) || is_hyper_skip || msg->getForceClickFlg()){
			if(msg->getForceClickFlg()) msg->setForceClickFlg(false);

			//クリック時にオートモードのタイミングをリセット
			if(input->getKeepMouseLeft() != 0){
				scn->resetAutoFrame();
			}

			// ハイパースキップ時、未読ポイントで停止
			if(!already_read_flag[current_line]){
				is_hyper_skip = false;
			}

			//既読情報の出力
			char* already_flag_text;
			if(already_read_flag[current_line] > 0){
				already_flag_text = "(既読)";
			}else{
				already_flag_text = "(未読)";
			}
			if (already_read_flag[current_line] == 0){
				already_read_flag[current_line] = 1;
			}
			FILE *already_fp;
			char already_read_file_full_path[TENG_FILE_NAME_MAX];


			sprintf_s(already_read_file_full_path, TENG_FILE_NAME_MAX, "%s%s%s", Config::savedata_dir, scenario_file, TENG_FILE_ALREADY_READ_FILE_EXTENSION);

			if(fopen_s(&already_fp, already_read_file_full_path, "wb") != 0){
				DEBUG printf("[ERROR] 既読情報ファイルの書き込みに失敗しました：%s%s\n", scenario_file, TENG_FILE_ALREADY_READ_FILE_EXTENSION);
				throw ENGINE_SAVE_READ_FLAG_WRITE_FAILED;
			}else{
				fwrite(&already_read_flag, sizeof(short)*TENG_SCRIPT_MAX_LINE, 1, already_fp);
				fclose(already_fp);
			}


			DEBUG printf("[SCRIPT#%d]%s %s",this->getCurrentLine(), already_flag_text, this->command[0]);
			int script_arg_max_num = 0;
			DEBUG script_arg_max_num = TENG_SCRIPT_ARGUMENT_MAX_NUMBER;
			for(int i = 1; i < TENG_SCRIPT_ARGUMENT_MAX_NUMBER; i++){
				if(command[i][0] == '\0') break;
				DEBUG printf(", %s", command[i]);
			}
			DEBUG printf("\n");

			//commandを各コマンドへ振り分け
			bool is_effect_none = false;

			//タグを追加した際は、ここに追記すること
			switch(this->next_tag){
			case TENG_TAG_COMMENT:
				//何もしない
				loop_flg = true;
				break;
			case TENG_TAG_EXMESSAGE:
				if(!is_hyper_skip){
					msg->clear();
					msg->setExMessage(command[0]);
				}
				loop_flg = false;
				break;
			case TENG_TAG_MESSAGE:
				if(!is_hyper_skip){
					msg->clear();
					msg->setDrawMessage(this->command[1], false);
				}
				loop_flg = false;
				break;
			case TENG_TAG_SAY:
				if(!is_hyper_skip){
					msg->clear();
					msg->setDrawMessage(command[2], true, string(command[1]));
				}
				loop_flg = false;
				break;
			case TENG_TAG_LOADCHAR:
				graphic->loadCharacterGraphic(this->command[1]);
				loop_flg = true;
				break;
			case TENG_TAG_DRAWCHAR:
				is_effect_none = graphic->setCharacterGraphic(this->command);
				loop_flg = is_effect_none;
				break;
			case TENG_TAG_HIDECHAR:
				graphic->hideCharacterGraphic(this->command);
				loop_flg = true;
				break;
			case TENG_TAG_FREECHAR:
				graphic->freeCharacterImage(this->command);
				loop_flg = true;
				break;
			case TENG_TAG_CHANGECHAR:
				graphic->changeCharacterGraphic(this->command);
				loop_flg = false;
				break;
			case TENG_TAG_IMPORT:
				importScenario(input, scn, msg, graphic, this->command[1], subscenario);
				loop_flg = true;
				break;
			case TENG_TAG_JUMP:
				cmdJump(input, scn, msg, graphic, this->command[1], this);
				loop_flg = true;
				break;
			case TENG_TAG_RETURN:
				returnScenario(subscenario);
				loop_flg = true;
				break;
			case TENG_TAG_LABEL:
				loop_flg = true;
				break;
			case TENG_TAG_GOTO:
				gotoScenario(this->command[1]);
				loop_flg = true;
				break;
			case TENG_TAG_LOADBGIMAGE:
				graphic->loadBackGroundGraphic(this->command[1]);
				loop_flg = true;
				break;
			case TENG_TAG_DRAWBGIMAGE:
				graphic->setBackGroundGraphic(this->command, config);
				loop_flg = true;
				break;
			case TENG_TAG_LOADRULE:
				graphic->loadRuleGraphic(this->command[1]);
				loop_flg = true;
				break;
			case TENG_TAG_FREEBGIMAGE:
				graphic->freeBackGroundGraphic(this->command[1]);
				loop_flg = true;
				break;
			case TENG_TAG_FREERULE:
				graphic->freeRuleGraphic(this->command[1]);
				loop_flg = true;
				break;
			case TENG_TAG_LOADFACE:
				graphic->loadFaceGraphic(this->command[1]);
				loop_flg = true;
				break;
			case TENG_TAG_DRAWFACE:
				graphic->setFaceGraphic(this->command[1]);
				loop_flg = true;
				break;
			case TENG_TAG_HIDEFACE:
				graphic->hideFaceGraphic();
				loop_flg = true;
				break;
			case TENG_TAG_FREEFACE:
				graphic->freeFaceGraphic(this->command[1]);
				loop_flg = true;
				break;
			case TENG_TAG_LOADBGM:
				sound->loadBgm(this->command[1], atoi(command[2]));
				loop_flg = true;
				break;
			case TENG_TAG_PLAYBGM:
				sound->playBgm(config, command);
				loop_flg = true;
				break;
			case TENG_TAG_STOPBGM:
				//sound->stopBgm(command);
				//loop_flg = false;
				loop_flg = sound->stopBgm(command);
				break;
			case TENG_TAG_FREEBGM:
				sound->freeBgm(command[1]);
				loop_flg = true;
				break;
			case TENG_TAG_LOADSE:
				sound->loadSe(this->command[1], atoi(command[2]));
				loop_flg = true;
				break;
			case TENG_TAG_PLAYSE:
				sound->playSe(config, command);
				loop_flg = true;
				break;
			case TENG_TAG_STOPSE:
				sound->stopSe(command[1]);
				loop_flg = true;
				break;
			case TENG_TAG_FREESE:
				sound->freeSe(command[1]);
				loop_flg = true;
				break;
			case TENG_TAG_SELECT:
				msg->setSelect(command);
				loop_flg = true;
				break;
			case TENG_TAG_LOVE:
				setLove(command);
				loop_flg = true;
				break;
			case TENG_TAG_IF:
				cmdIf(command);
				loop_flg = true;
				break;
			case TENG_TAG_NAMESET:
				msg->cmdNameSet();
				loop_flg = false;
				break;
			case TENG_TAG_SETTITLE:
				scn->setTitle(command);
				loop_flg = true;
				break;
			case TENG_TAG_RESETTITLE:
				scn->resetTitle();
				loop_flg = true;
				break;
			case TENG_TAG_WAIT:
				scn->wait(msg, command);
				loop_flg = false;
				break;
			case TENG_TAG_HIDEMSG:
				// msg->setMsgWindowHideFlg(true);
				//msg->setMsgWindowStatus(CmdMessage::eMsgWindow::MSG_WINDOW_TRANS_TYPE_FADEOUT);
				msg->setMsgWindowStatus(CmdMessage::eMsgWindow::MSG_WINDOW_TRANS_TYPE_FADEOUT);
				loop_flg = true;
				break;
			case TENG_TAG_DISPMSG:
				loop_flg = false;
				msg->clear();
				// msg->setMsgWindowHideFlg(false);
				msg->setMsgWindowStatus(CmdMessage::eMsgWindow::MSG_WINDOW_TRANS_TYME_FADEIN);
				if (input->getKeyControl() != 0){
					msg->setMsgWindowHideFlg(false);
					msg->setMsgWindowStatus(CmdMessage::eMsgWindow::MSG_WINDOW_TRANS_COMPLETED);
					msg->setMsgWindowTransFrame(255);
				}
				break;
			case TENG_TAG_PLAYMOVIE:
				loop_flg = false;
				scn->setPlayMovie(msg, sound, command[1]);
				break;
			case TENG_TAG_DISPGALLERY:
				loop_flg = true;
				config->setDisplayGalleryMenuFlag();
				config->outputConfig();
				break;
			case TENG_TAG_EFFECTFLASH:
				scn->setEffectFlash(command);
				loop_flg = false;
				break;
			case TENG_TAG_EFFECTSEPIA:
				scn->setEffectSepia(command);
				loop_flg = false;
				break;
			case TENG_TAG_CHANGESCENE:
				sceneChange(command[1]);
				loop_flg = false;
				break;
			default:
				DEBUG printf("無効なコマンドが存在します: [TAG] %d", this->next_tag);
				throw ENGINE_SCRIPT_INVALID_COMMAND;
			}

			this->current_line++;
		}

		memset(command, 0, (TENG_SCRIPT_ARGUMENT_MAX_NUMBER + 1) * TENG_SCRIPT_COMMAND_MAX_LENGTH);
		if (is_hyper_skip) loop_flg = true;
		if (is_hyper_skip && next_tag == TENG_TAG_SELECT){
			is_hyper_skip = false;
		}
		return loop_flg;
}

void Teng::Scenario::checkButtonClick(
	Teng::Input* input,
	Teng::Window* window,
	Teng::Scene* scene,
	Teng::Config* config,
	Teng::GraphicManager* g_manager,
	Teng::SoundManager* s_manager,
	Teng::CmdMessage* message)
{
	static bool is_qload_clicked = false;
	static bool is_qsave_clicked = false;
	static bool is_config_clicked = false;
	static bool is_auto_clicked = false;
	static bool is_skip_clicked = false;
	static bool is_hskip_clicked = false;
	//static bool is_x_clicked = false;

	if (is_x_clicked){
		if (input->getKeepMouseLeft() != 0){
			message->setMsgWindowStatus(CmdMessage::eMsgWindow::MSG_WINDOW_TRANS_TYME_FADEIN);
		}
	}
	if (message->getMsgWindowStatus() == CmdMessage::eMsgWindow::MSG_WINDOW_TRANS_TYPE_DISPLAY){
		is_x_clicked = false;
	}

	if (message->getMsgWindowHideFlg()){
		return;
	}

	if (!is_initialized){
		msgBoxInitialize(g_manager);
		is_initialized = true;
	}

	//int x, y;
	DxLib::GetMousePoint(&cursor_x, &cursor_y);
	//DEBUG printf("x:%d, y:%d\n", x, y);
	static int clicked_select_num = -1;
	static bool select_focused[4] = {false, false, false, false};

	//最後にボタンの上にカーソルが乗ったフレーム
	static unsigned long last_button_in_frame_load = -1;
	static unsigned long last_button_in_frame_save = -1;

	if (scene->getBeforeScene() != Scene::eScene_scenario){
		is_qload_clicked = false;
		is_qsave_clicked = false;
		is_config_clicked = false;
		is_auto_clicked = false;
		is_skip_clicked = false;
		is_hskip_clicked = false;
		is_x_clicked = false;
		btn_textbox_qload.now = btn_textbox_qload.normal;
		btn_textbox_qsave.now = btn_textbox_qsave.normal;
		btn_textbox_config.now = btn_textbox_config.normal;
		btn_textbox_hskip.now = btn_textbox_hskip.normal;
		btn_textbox_x.now = btn_textbox_x.normal;

	}

	static unsigned long frame = 0;

	if(flag_reset_flg){
		select_focused[0] = false;
		select_focused[1] = false;
		select_focused[2] = false;
		select_focused[3] = false;
		last_button_in_frame_load = -1;
		last_button_in_frame_save = -1;
		is_hyper_skip = true;
		flag_reset_flg = false;
	}

	if(input->getKeepKeyF5() != 0 && !scene->getSceneChanging()){
		//is_hyper_skip = true;
		//is_hyper_skip_button_pushed = true;
		scene->setSceneChanging(true);
		scene->setClickedButton(Scene::eScene_hyperskip);
		scene->resetFrame();
	}

	if (!scene->getSceneChanging()){
		scene->setBeforeScene(Scene::eScene_scenario);
	}


	if (config->getSkipModeFlag()){
		if (input->getKeepMouseLeft()){
			config->setSkipModeFlag(false);
		}
		else if (message->isDrawSelect()){
			config->setSkipModeFlag(false);
		}
	}


	DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, message->getMsgWindowTransFrame());

	/* ダイアログボックスが表示されていないときのみ！ */
	if(!Window::getDialogDrawing())
	{
		// メッセージボックスベース
		DxLib::DrawGraph(btn_textbox_base.x, btn_textbox_base.y, btn_textbox_base.normal, TRUE);
		
		message->update(frame, config, this, scene);
		message->render(TENG_MESSAGE_FULL_WIDTH, TENG_MESSAGE_FONT_SIZE, g_manager, this);

		//ロードボタン
		if(inButton(btn_textbox_qload) && !scene->isBacklogEnable()){
			//はじめてボタン上にカーソルが乗ったとき
			if(scene->getFrame()-1 != last_button_in_frame_load){
				window->setCursorType(LoadCursor(NULL, IDC_HAND));
				SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
			}
			last_button_in_frame_load = scene->getFrame();
			cursor_on_button = true;
			scene->cursorOnButton(true);
			//クリックしたらロード画面へ
			if(input->getKeepMouseLeft() && !scene->getSceneChanging()){
				is_qload_clicked = true;
				//scene->setSceneChanging(true);

				//scene->setClickedButton(Scene::eScene::eScene_scenario_load);
				//scene->setBeforeScene(Teng::Scene::eScene_scenario);
				//scene->setNowScene(Teng::Scene::eScene_scenario);
				scene->resetFrame();
				frame = 0;
				////scene->setBeforeScene(Teng::Scene::eScene_scenario);
				////scene->setNowScene(Teng::Scene::eScene_scenario_load);
				////サムネイルを表示できる形に復元・サムネイルハンドルをセット
				////DxLib::SetUseASyncLoadFlag(FALSE);
				////scene->decodeSavedataThumbnail(window, config, input, g_manager, s_manager);
				//scene->decodeSavedataThumbnail1(window, config, input, g_manager, s_manager);
				////DxLib::SetUseASyncLoadFlag(TRUE);
			}
		}
		//セーブボタン
		else if(inButton(btn_textbox_qsave) && !scene->isBacklogEnable() && message->isMsgDrawCompleted()){
			//はじめてボタン上にカーソルが乗ったとき
			if(scene->getFrame()-1 != last_button_in_frame_save){
				window->setCursorType(LoadCursor(NULL, IDC_HAND));
				SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
			}
			last_button_in_frame_save = scene->getFrame();
			cursor_on_button = true;
			scene->cursorOnButton(true);
			//クリックしたらロード画面へ
			if(input->getKeepMouseLeft() && !scene->getSceneChanging()){
				is_qsave_clicked = true;
				frame = 0;
				//画面遷移前にスクリーンショットを取得しておく
				DxLib::SetUseASyncLoadFlag(FALSE);
				SaveData::makeScreenShotShumbnail();
				DxLib::SetUseASyncLoadFlag(TRUE);
				Teng::SaveData *savedata = new Teng::SaveData(false);
				savedata->save(65, this, g_manager, s_manager, window, message, scene);
				delete savedata;

				//scene->setSceneChanging(true);
				//scene->setClickedButton(Scene::eScene::eScene_scenario_save);
				//scene->setBeforeScene(Teng::Scene::eScene_scenario);
				//scene->setNowScene(Teng::Scene::eScene_scenario);
				//scene->resetFrame();
				//scene->decodeSavedataThumbnail1(window, config, input, g_manager, s_manager);

				//サムネイルを表示できる形に復元・サムネイルハンドルをセット
				//scene->decodeSavedataThumbnail(window, config, input, g_manager, s_manager);
			}
		}
		//コンフィグボタン
		else if(inButton(btn_textbox_config) && !scene->isBacklogEnable()){
			//はじめてボタン上にカーソルが乗ったとき
			if(scene->getFrame()-1 != last_button_in_frame_load){
				window->setCursorType(LoadCursor(NULL, IDC_HAND));
				SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
			}
			last_button_in_frame_load = scene->getFrame();
			cursor_on_button = true;
			scene->cursorOnButton(true);
			//クリックしたらコンフィグ画面へ
			if(input->getKeepMouseLeft()){
				scene->sceneConfigInitialize(window, config, input, g_manager, s_manager);
				is_config_clicked = true;
				frame = 0;
			}

		}
		//オートモードボタン
		else if(inButton(btn_textbox_auto) && !scene->isBacklogEnable()){
			cursor_on_button = true;
			scene->cursorOnButton(true);
			//はじめてボタン上にカーソルが乗ったとき
			if(scene->getFrame()-1 != last_button_in_frame_load){
				window->setCursorType(LoadCursor(NULL, IDC_HAND));
				SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
			}
			if(input->getKeepMouseLeft()){
				if(config->getAutoMsgFlag()){
					config->setAutoMsgFlag(false);
				}else{
					config->setAutoMsgFlag(true);
				}
			}
		}
		//スキップボタン
		else if(inButton(btn_textbox_skip) && !scene->isBacklogEnable() && !message->isDrawSelect()){
			cursor_on_button = true;
			scene->cursorOnButton(true);
			//はじめてボタン上にカーソルが乗ったとき
			if(scene->getFrame()-1 != last_button_in_frame_load){
				window->setCursorType(LoadCursor(NULL, IDC_HAND));
				SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
			}
			if(input->getKeepMouseLeft()){
				if(config->getSkipModeFlag()){
					config->setSkipModeFlag(false);
				}else{
					config->setSkipModeFlag(true);
				}
			}
		}
		// ハイパースキップボタン
		else if(inButton(btn_textbox_hskip) && !scene->isBacklogEnable()){
			//はじめてボタン上にカーソルが乗ったとき
			if(scene->getFrame()-1 != last_button_in_frame_save){
				window->setCursorType(LoadCursor(NULL, IDC_HAND));
				SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
			}
			last_button_in_frame_save = scene->getFrame();
			cursor_on_button = true;
			scene->cursorOnButton(true);
			//クリックしたらハイパースキップへ
			if(input->getKeepMouseLeft() && !scene->getSceneChanging()){
				is_hskip_clicked = true;
				frame = 0;
			}
		}
		// ×ボタン
		else if(inButton(btn_textbox_x) && !scene->isBacklogEnable()){
			//はじめてボタン上にカーソルが乗ったとき
			if(scene->getFrame()-1 != last_button_in_frame_save){
				window->setCursorType(LoadCursor(NULL, IDC_HAND));
				SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
			}
			last_button_in_frame_save = scene->getFrame();
			cursor_on_button = true;
			scene->cursorOnButton(true);
			if(input->getKeepMouseLeft() && !scene->getSceneChanging()){
				frame = 0;
				message->setMsgWindowStatus(CmdMessage::eMsgWindow::MSG_WINDOW_TRANS_TYPE_FADEOUT);
				is_x_clicked = true;
			}
		}



		else{
			//TODO 毎フレーム実行していいのだろうか？
			window->setCursorType(LoadCursor(NULL, IDC_ARROW));
			SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
			cursor_on_button = false;
			scene->cursorOnButton(false);
		}

		if (!inButton(btn_textbox_auto) && !scene->isBacklogEnable()){
			if (input->getKeepMouseLeft() != 0){
					is_auto_clicked = false;
					config->setAutoMsgFlag(false);
			}
		}


		if (is_qload_clicked){
			btn_textbox_qload.now = btn_textbox_qload.click;
			window->setCursorType(LoadCursor(NULL, IDC_ARROW));
			SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
			cursor_on_button = false;
			scene->cursorOnButton(false);
		}
		if (is_config_clicked){
			btn_textbox_config.now = btn_textbox_config.click;
			window->setCursorType(LoadCursor(NULL, IDC_ARROW));
			SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
			cursor_on_button = false;
			scene->cursorOnButton(false);
		}
		if (config->getAutoMsgFlag()){
			btn_textbox_auto.now = btn_textbox_auto.click;
			//window->setCursorType(LoadCursor(NULL, IDC_ARROW));
			//SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
			//cursor_on_button = false;
			//scene->cursorOnButton(false);
		}
		else{
			btn_textbox_auto.now = btn_textbox_auto.normal;
		}

		if (config->getSkipModeFlag()){
			btn_textbox_skip.now = btn_textbox_skip.click;
		}
		else{
			btn_textbox_skip.now = btn_textbox_skip.normal;
		}

		if (is_qsave_clicked && frame < 45){
			btn_textbox_qsave.now = btn_textbox_qsave.click;
			window->setCursorType(LoadCursor(NULL, IDC_ARROW));
			SetClassLongPtr(DxLib::GetMainWindowHandle(), GCL_HCURSOR, (LONG)window->getCursorType());
			cursor_on_button = false;
			scene->cursorOnButton(false);
		}
		else if (is_qsave_clicked && frame >= 45){
			btn_textbox_qsave.now = btn_textbox_qsave.normal;
			frame = 0;
			is_qsave_clicked = false;
		}

		// HSKIP押されたあと
		if (is_hskip_clicked){
			btn_textbox_hskip.now = btn_textbox_hskip.click;
			//フェードアウト
			if (frame < 255){
				//DxLib::SetDrawBright(255 - frame, 255 - frame, 255 - frame);
				frame += 8;
			}
			else{
				is_hskip_clicked = false;
				btn_textbox_hskip.now = btn_textbox_hskip.normal;
				isHyperSkipButtonPushed(true);
				scene->setNowScene(Scene::eScene_hyperskip);
			}
		}

		DxLib::DrawGraph(btn_textbox_auto.x, btn_textbox_auto.y, btn_textbox_auto.now, TRUE);
		DxLib::DrawGraph(btn_textbox_qsave.x, btn_textbox_qsave.y, btn_textbox_qsave.now, TRUE);
		DxLib::DrawGraph(btn_textbox_qload.x, btn_textbox_qload.y, btn_textbox_qload.now, TRUE);
		DxLib::DrawGraph(btn_textbox_config.x, btn_textbox_config.y, btn_textbox_config.now, TRUE);
		int res = DxLib::DrawGraph(btn_textbox_skip.x, btn_textbox_skip.y, btn_textbox_skip.now, TRUE);
		DxLib::DrawGraph(btn_textbox_hskip.x, btn_textbox_hskip.y, btn_textbox_hskip.now, TRUE);
		DxLib::DrawGraph(btn_textbox_x.x, btn_textbox_x.y, btn_textbox_x.now, TRUE);


		//TODO 仮
		//if(config->getAutoMsgFlag()){
		//	DxLib::DrawString(210, 703, "AUTO ■", GetColor(255,255,255));
		//}else{
		//	DxLib::DrawString(210, 703, "AUTO □", GetColor(255,255,255));
		//}

		//選択肢
		static int selection_back_trans_frame = 0;
		if((message->isDrawSelect() && !message->getRenderingFlg()) || message->getSelectClickWaiting()){
			
			// ダイアログ以外、画面全体を茶色っぽくする
			// 選択肢がおわるとき
			if (message->getSelectClickWaiting()){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, selection_back_trans_frame);
				if(selection_back_trans_frame > 0) selection_back_trans_frame -= 12;
				DxLib::DrawBox(0, 0, 1024, 768, GetColor(27, 8, 5), TRUE);
			}
			// 選択肢が始まるとき
			else if(selection_back_trans_frame < 102){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, selection_back_trans_frame);
				if(selection_back_trans_frame < 102) selection_back_trans_frame += 12;
				DxLib::DrawBox(0, 0, 1024, 768, GetColor(27, 8, 5), TRUE);
			}
			// 選択肢通常表示中
			else{
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, 102);
				DxLib::DrawBox(0, 0, 1024, 768, GetColor(27, 8, 5), TRUE);
			}
			DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);



			int idx = 0;
			for (auto it = message->getSelects().begin(); it != message->getSelects().end(); it++){


				if (Common::cursorIn(
					TENG_GRAPHIC_SELECT_TOP_X,
					TENG_GRAPHIC_SELECT_TOP_X + TENG_GRAPHIC_SELECT_WIDTH,
					TENG_GRAPHIC_SELECT_TOP_Y + TENG_GRAPHIC_SELECT_DISTANCE * idx,
					TENG_GRAPHIC_SELECT_TOP_Y + TENG_GRAPHIC_SELECT_DISTANCE * idx + TENG_GRAPHIC_SELECT_HEIGHT) || message->getSelectClickWaiting()){

					if (clicked_select_num == idx){
						DxLib::DrawGraph(
							TENG_GRAPHIC_SELECT_TOP_X,
							TENG_GRAPHIC_SELECT_TOP_Y + TENG_GRAPHIC_SELECT_DISTANCE * idx,
							g_manager->system_graphics[Teng::SystemGraphic::arrnum_select_focus].getHandle(),
							TRUE);
					}
					else{
						DxLib::DrawGraph(
							TENG_GRAPHIC_SELECT_TOP_X,
							TENG_GRAPHIC_SELECT_TOP_Y + TENG_GRAPHIC_SELECT_DISTANCE * idx,
							g_manager->system_graphics[Teng::SystemGraphic::arrnum_select_normal].getHandle(),
							TRUE);
					}
					//一度のみ再生
					if (!select_focused[idx] && !message->getSelectClickWaiting()){
						select_focused[idx] = true;
						double vol_focus =
							(double)(config->getSetting().master_volume) *
							(double)(config->getSetting().sound_volume) / 65025 * TENG_SOUND_SE_SELECT_FOCUS_VOLUME;
						int s_handle_focus = s_manager->system_sound[Teng::SystemSound::arrnum_se_select_focus].getHandle();
						DxLib::ChangeVolumeSoundMem((int)vol_focus, s_handle_focus);
						DxLib::PlaySoundMem(s_handle_focus, DX_PLAYTYPE_BACK, TRUE);
					}
					//選択肢クリック
					if (input->getKeepMouseLeft() && !message->getSelectClickWaiting()){
						message->setSelectClickWaiting(true);
						select_focused[idx] = false;
						clicked_select_num = idx;

						//選択肢の選択情報を書き込み
						already_read_flag[current_line - 2] = calcSelectedCount(already_read_flag[current_line - 2], idx);
						FILE *already_fp;
						char already_read_file_full_path[TENG_FILE_NAME_MAX];
						sprintf_s(already_read_file_full_path, TENG_FILE_NAME_MAX, "%s%s%s", Config::savedata_dir, scenario_file, TENG_FILE_ALREADY_READ_FILE_EXTENSION);
						if (fopen_s(&already_fp, already_read_file_full_path, "wb") != 0){
							DEBUG printf("[ERROR] 既読情報ファイル(選択肢)の書き込みに失敗しました：%s%s\n", scenario_file, TENG_FILE_ALREADY_READ_FILE_EXTENSION);
							throw ENGINE_SAVE_SELECTED_FLAG_WRITE_FAILED;
						}
						else{
							fwrite(&already_read_flag, sizeof(short)*TENG_SCRIPT_MAX_LINE, 1, already_fp);
							fclose(already_fp);
						}


						double vol_clicked =
							(double)(config->getSetting().master_volume) *
							(double)(config->getSetting().sound_volume) / 65025 * TENG_SOUND_SE_SELECT_CLICKED_VOLUME;
						int s_handle_clicked = s_manager->system_sound[Teng::SystemSound::arrnum_se_select_clicked].getHandle();
						DxLib::ChangeVolumeSoundMem((int)vol_clicked, s_handle_clicked);
						//DxLib::PlaySoundMem(s_handle_clicked, DX_PLAYTYPE_BACK, TRUE);
						//message->setDrawSelect(false);
						gotoScenario(it->getLabel().c_str());
					}
				}
				//選択肢が選択済みの場合
				else if (isSelectionSelected(already_read_flag[current_line - 2], idx)){
					DxLib::DrawGraph(
						TENG_GRAPHIC_SELECT_TOP_X,
						TENG_GRAPHIC_SELECT_TOP_Y + TENG_GRAPHIC_SELECT_DISTANCE * idx,
						g_manager->system_graphics[Teng::SystemGraphic::arrnum_select_selected].getHandle(),
						TRUE);
					select_focused[idx] = false;
				}
				//未選択の選択肢
				else{
					DxLib::DrawGraph(
						TENG_GRAPHIC_SELECT_TOP_X,
						TENG_GRAPHIC_SELECT_TOP_Y + TENG_GRAPHIC_SELECT_DISTANCE * idx,
						g_manager->system_graphics[Teng::SystemGraphic::arrnum_select_normal].getHandle(),
						TRUE);
					//選択肢からフォーカスが外れたら色を元に戻す
					select_focused[idx] = false;
				}
				//選択肢内のテキスト表示
				int selection_font = message->getFontHandle(CmdMessage::MsgType::SELECTION);
				int strwidth = DxLib::GetDrawStringWidthToHandle(it->getMsg().c_str(), it->getMsg().size(), selection_font);
				DxLib::DrawStringToHandle(
					//TENG_GRAPHIC_SELECT_TOP_X + 50,
					TENG_GRAPHIC_SELECT_TOP_X + (TENG_GRAPHIC_SELECT_WIDTH - strwidth) / 2,
					TENG_GRAPHIC_SELECT_TOP_Y + TENG_GRAPHIC_SELECT_DISTANCE * idx + 17,
					it->getMsg().c_str(),
					GetColor(213, 202, 180),
					selection_font
					);
				idx++;
			}
		}
		if (!message->getSelectClickWaiting()) clicked_select_num = -1;

		//名前入力
		if(message->isDrawNameEntry()){
			static const char* NAME_DEFAULT = "イゾルデ";
			static char input_name[TENG_MESSAGE_CHARACTER_NAME_MAXLENGTH] = "";
			static bool is_key_pushed_never = true;
			if (is_key_pushed_never) DxLib::SetKeyInputString(NAME_DEFAULT, message->input_handle);
			// 制限7文字
			DxLib::DrawGraph(
				btn_name_entry_base.x,
				btn_name_entry_base.y,
				btn_name_entry_base.normal,
				TRUE);
			DxLib::DrawStringToHandle(
				327, 307,
				"名前を入力してください(最大5文字)",
				DxLib::GetColor(236, 226, 200),
				message->getFontHandle(CmdMessage::MsgType::NAME_ENTRY_LABEL)
				);
			// 名前初期化ボタン
			if (Common::cursorIn(
				btn_name_entry_init.x,
				btn_name_entry_init.x + btn_name_entry_init.width,
				btn_name_entry_init.y,
				btn_name_entry_init.y + btn_name_entry_init.height))
			{
				if (input->getMouseLeft() != 0){
					btn_name_entry_init.now = btn_name_entry_init.click;
					DxLib::SetKeyInputString(NAME_DEFAULT, message->input_handle);
				}

			}
			// 名前決定ボタン
			if (Common::cursorIn(
				btn_name_entry_decision.x,
				btn_name_entry_decision.x + btn_name_entry_decision.width,
				btn_name_entry_decision.y,
				btn_name_entry_decision.y + btn_name_entry_decision.height))
			{
				if (input->getMouseLeft() != 0){
					btn_name_entry_decision.now = btn_name_entry_decision.click;
				}
				if(input->getKeepMouseLeft()){
					if(input_name[0] != NC && is_name_valid){
						//キャラ名を確定
						message->setCharacterName(string(input_name));
						//入力ボックスを消す
						message->setDrawNameEntry(false);
						//入力ハンドルを削除
						DxLib::DeleteKeyInput(message->input_handle);

						message->setSelectClickWaiting(5);
					}
				}

			}
			DxLib::DrawGraph(btn_name_entry_init.x, btn_name_entry_init.y, btn_name_entry_init.now, TRUE);
			DxLib::DrawGraph(btn_name_entry_decision.x, btn_name_entry_decision.y, btn_name_entry_decision.now, TRUE);
			DxLib::SetActiveKeyInput(message->input_handle);

			//キー入力のたびに入力制限チェックをする
			if (DxLib::CheckHitKeyAll() != 0){
				is_key_pushed_never = false;
				//入力文字列をinput_nameに格納する
				DxLib::GetKeyInputString(input_name, message->input_handle);
				is_name_valid = message->checkInputNameLength(string(input_name));
			}
			// 確定した文字
			DxLib::DrawStringToHandle(
				337, 350,
				input_name,
				DxLib::GetColor(54, 25, 6),
				message->getFontHandle(CmdMessage::MsgType::NAME_ENTRY_INPUT));

			// 日本語入力中の文字
			int decided_input_str_width =
				GetDrawStringWidthToHandle(
					input_name,
					TENG_MESSAGE_CHARACTER_NAME_MAXLENGTH,
					message->getFontHandle(CmdMessage::MsgType::NAME_ENTRY_INPUT));
			DxLib::ChangeFontType(DX_FONTTYPE_ANTIALIASING);
			DxLib::DrawIMEInputString(337 + decided_input_str_width, 350, message->input_handle);

			const IMEINPUTDATA* ImeData;
			ImeData = DxLib::GetIMEInputData();
			if (!ImeData){
				// -5は、IMEオン時のパイプとの差分を埋めるため
				DrawString(337 + decided_input_str_width - 5, 350, "|", GetColor(54, 25, 6));
			}


		} // end of nameset

		if (!is_name_valid){
			//DrawString(650, 215, "文字数オーバー", GetColor(255, 255, 255));
		}

		//選択肢クリック後、一定時間待機
		static unsigned int select_clicked_wait_frame = 0;
		if(message->getSelectClickWaiting()){
			if(select_clicked_wait_frame < TENG_SELECT_AFTER_CLICK_WAIT_TIME){
				select_clicked_wait_frame++;
			}else{
				select_clicked_wait_frame = 0;
				message->setDrawSelect(false);
				message->setSelectClickWaiting(false);
				message->setForceClickFlg(true);
				message->isClickAfterSelectWait(true);
				message->clearSelects();
			}
		}

	}//unless dialog drawing




















	DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

	frame++;

	if (is_qload_clicked){
		frame += 10;
		int f = 255 - frame;
		SetDrawBright(f, f, f);
		if (f <= 0){
			frame = 0;
			//scene->setNowScene(eScene_scena)
			Teng::SaveData *savedata = new Teng::SaveData;
			// シナリオからのロード
			savedata->load(65, this, message, g_manager, s_manager, window, config, scene);
			delete savedata;
			frame = 0;
			is_qload_clicked = false;
			scene->setSceneChanging(true);
			//scene->setClickedButton(eScene)
			scene->setBeforeScene(6);
		}
	}

	// シナリオ画面でコンフィグを押した
	if (is_config_clicked){
		cursor_on_button = true;
		scene->cursorOnButton(true);
		if (frame < 255){
			frame += 7;
			DxLib::SetDrawBright(255 - frame, 255 - frame, 255 - frame);
			scene->setBeforeScene(Scene::eScene_scenario);
		}
		//シナリオ→コンフィグ フェードアウト完了直後
		else{
			scene->setBeforeScene(Scene::eScene_scenario);
			scene->setNowScene(Scene::eScene_scenario_config);
			frame = 0;
			DxLib::SetDrawBright(0, 0, 0);
			scene->setFadeType(Scene::FadeType::fadein);
			scene->resetFrame();
		}
	}



}

//選択肢をクリックした後、既読ファイルに書き込む値を計算する
int Teng::Scenario::calcSelectedCount(short current_line_read_flag, int idx){
	bool sel_1 = false, sel_2 = false, sel_3 = false, sel_4 = false;
	//idx 0-3がクリックされたとき
	//↓クリックされる前の値
	switch (current_line_read_flag)	{
	case 1:
		//既読で未選択
		break;
	case 2:
		//１だけ選択している
		sel_1 = true;
		break;
	case 3:
		sel_2 = true;
		break;
	case 5:
		sel_3 = true;
		break;
	case 9:
		sel_4 = true;
		break;
	case 4:
		sel_1 = true;
		sel_2 = true;
		break;
	case 6:
		sel_1 = true;
		sel_3 = true;
		break;
	case 10:
		sel_1 = true;
		sel_4 = true;
		break;
	case 7:
		sel_2 = true;
		sel_3 = true;
		break;
	case 11:
		sel_2 = true;
		sel_4 = true;
		break;
	case 13:
		sel_3 = true;
		sel_4 = true;
		break;
	case 8:
		sel_1 = true;
		sel_2 = true;
		sel_3 = true;
		break;
	case 12:
		sel_1 = true;
		sel_2 = true;
		sel_4 = true;
		break;
	case 14:
		sel_1 = true;
		sel_3 = true;
		sel_4 = true;
		break;
	case 15:
		sel_2 = true;
		sel_3 = true;
		sel_4 = true;
		break;
	case 16:
		sel_1 = sel_2 = sel_3 = sel_4 = true;
		break;
	default:
		DEBUG printf("[ERROR] @select 選択肢の既読フラグが異常です。\n");
		throw ENGINE_SAVE_SELECTED_FLAG_LOAD_FAILED;
		break;
	}
	short after_click_num = current_line_read_flag;
	switch (idx){
	case 0:
		//選択肢１を選択した
		if (!sel_1) after_click_num += 1;
		break;
	case 1:
		//選択肢２を選択した
		if (!sel_2) after_click_num += 2;
		break;
	case 2:
		//選択肢３を選択した
		if (!sel_3) after_click_num += 4;
		break;
	case 3:
		//選択肢４を選択した
		if (!sel_4) after_click_num += 8;
		break;
	default:
		DEBUG printf("[ERROR] @select 異常な選択肢を選択しました。\n");
		throw ENGINE_ERROR_DEFAULT;
		break;
	}

	return after_click_num;
}

//既読ファイルを読み込み、idx番目の選択肢は選択済みかどうかを返す
bool Teng::Scenario::isSelectionSelected(short current_line_read_flag, int idx){
	//idx 0
	//idx 1
	//idx 2
	//idx 3
	switch (current_line_read_flag)	{
	case 0:
		return false;
	case 1:
		//既読で未選択
		return false;
	case 2:
		//１だけ選択している
		if (idx == 0) return true;
		return false;
	case 3:
		if (idx == 1) return true;
		return false;
	case 5:
		if (idx == 2) return true;
		return false;
	case 9:
		if (idx == 3) return true;
		return false;
	case 4:
		if (idx == 0 || idx == 1) return true;
		return false;
	case 6:
		if (idx == 0 || idx == 2) return true;
		return false;
	case 10:
		if (idx == 0 || idx == 3) return true;
		return false;
	case 7:
		if (idx == 1 || idx == 2) return true;
		return false;
	case 11:
		if (idx == 1 || idx == 3) return true;
		return false;
	case 13:
		if (idx == 2 || idx == 3) return true;
		return false;
	case 8:
		if (idx == 0 || idx == 1 || idx == 2) return true;
		return false;
	case 12:
		if (idx == 0 || idx == 1 || idx == 3) return true;
		return false;
	case 14:
		if (idx == 0 || idx == 2 || idx == 3) return true;
		return false;
	case 15:
		if (idx == 1 || idx == 2 || idx == 4) return true;
		return false;
	case 16:
		return true;
	default:
		DEBUG printf("[ERROR] @select 選択肢の既読フラグが異常です。\n");
		throw ENGINE_SAVE_SELECTED_FLAG_LOAD_FAILED;
		break;
	}
	return false;
}

// 
bool Teng::Scenario::checkForceSkip(Input* input, Config* config, Teng::CmdMessage* msg){

	if((input->getKeyControl() == TRUE || config->getSkipModeFlag()) &&
		config->getSetting().force_skip == TRUE
		//&& !msg->getRenderingFlg()
		)
	{
		msg->setForceSkipFlag(true);
		msg->removeAllPlus();



		//msg->drawSpeedUp();	
		return true;
	}else if(
		(input->getKeyControl() == TRUE || config->getSkipModeFlag()) &&
		config->getSetting().force_skip == FALSE
		//&& !msg->getRenderingFlg()
		)
	{
		if(already_read_flag[current_line] == 1){
			msg->setForceSkipFlag(true);
			return true;
		}

	}
	msg->setForceSkipFlag(false);
	msg->removeAllPlus();

	return false;
}

void Teng::Scenario::reloadHyperSkip(Scenario* scenario, CmdMessage* message, GraphicManager* g_manager, SoundManager* s_manager, Window* window, Config* config, Scene* scene){

	DEBUG printf("[INFO] ハイパースキップ停止\n");
	scenario->isHyperSkipButtonPushed(false);
	//ロード用？
	is_hyper_skip = true;
	DxLib::SetUseASyncLoadFlag(FALSE);
	SaveData::makeScreenShotShumbnail();
	Teng::SaveData *savedata1 = new Teng::SaveData;
	DEBUG printf("[INFO] ハイパースキップ：一時シナリオデータの保存開始\n");
	savedata1->save(99, this, g_manager, s_manager, window, message, scene);
	DEBUG printf("[INFO] ハイパースキップ：一時シナリオデータの保存完了\n");
	delete savedata1;
	is_hyper_skip = false;
	Teng::SaveData *savedata2 = new Teng::SaveData;
	DEBUG printf("[INFO] ハイパースキップ：一時シナリオデータのロード開始\n");
	savedata2->load(99, this, message, g_manager, s_manager, window, config, scene);
	DEBUG printf("[INFO] ハイパースキップ：一時シナリオデータのロード完了\n");
	delete savedata2;
	DxLib::SetUseASyncLoadFlag(TRUE);

}

bool Teng::Scenario::isAlreadyReadLine(const int line){
	return (already_read_flag[line] > 0);
}

char* Teng::Scenario::getCommandZero(){
	if (command[0][0] == '@'){
		return command[0];
	}
	else{
		// ＠から始まらないタグの場合
		// msgの場合
		// 0: テキスト
		// sayの場合
		// 0: 名前、1: テキスト
		if (command[1][0] == NC){
			// msgと判定
			return "@msg";
		}
		else{
			// say と判定
			return "@say";
		}
	}

}

char* Teng::Scenario::getCommandOne(){
	if (command[0][0] == '@'){
		return command[1];
	}
	else{
		return command[0];
	}
}
char* Teng::Scenario::getCommandTwo(){
	if (command[0][0] == '@'){
		return command[2];
	}
	else{
		return command[1];
	}
}

void Teng::Scenario::nameEntryInitialize(GraphicManager* g_manager){
	
	// 名前入力ベース
	btn_name_entry_base.normal = btn_name_entry_base.now = 
		g_manager->system_graphics[SystemGraphic::arrnum_name_entry_box].getHandle();
	btn_name_entry_base.x = 300;
	btn_name_entry_base.y = 262;
	DxLib::GetGraphSize(btn_name_entry_base.normal, &btn_name_entry_base.width, &btn_name_entry_base.height);
	// 名前入力初期化ボタン
	btn_name_entry_init.normal = btn_name_entry_init.now = 
		g_manager->system_graphics[SystemGraphic::arrnum_name_entry_init_normal].getHandle();
	btn_name_entry_init.click =
		g_manager->system_graphics[SystemGraphic::arrnum_name_entry_init_click].getHandle();
	btn_name_entry_init.x = 400;
	btn_name_entry_init.y = 406;
	DxLib::GetGraphSize(btn_name_entry_init.normal, &btn_name_entry_init.width, &btn_name_entry_init.height);
	// 名前入力決定ボタン
	btn_name_entry_decision.normal = btn_name_entry_decision.now = 
		g_manager->system_graphics[SystemGraphic::arrnum_name_entry_decision_normal].getHandle();
	btn_name_entry_decision.click =
		g_manager->system_graphics[SystemGraphic::arrnum_name_entry_decision_click].getHandle();
	btn_name_entry_decision.x = 540;
	btn_name_entry_decision.y = 406;
	DxLib::GetGraphSize(btn_name_entry_decision.normal, &btn_name_entry_decision.width, &btn_name_entry_decision.height);
}

void Teng::Scenario::msgBoxInitialize(GraphicManager* g_manager){

	btn_textbox_base.normal = btn_textbox_base.now =
		g_manager->system_graphics[SystemGraphic::arrnum_textbox_base].getHandle();
	btn_textbox_config.normal = btn_textbox_config.now =
		g_manager->system_graphics[SystemGraphic::arrnum_textbox_config].getHandle();
	btn_textbox_hskip.normal = btn_textbox_hskip.now =
		g_manager->system_graphics[SystemGraphic::arrnum_textbox_hskip].getHandle();
	btn_textbox_qload.normal = btn_textbox_qload.now =
		g_manager->system_graphics[SystemGraphic::arrnum_textbox_qload].getHandle();
	btn_textbox_qsave.normal = btn_textbox_qsave.now =
		g_manager->system_graphics[SystemGraphic::arrnum_textbox_qsave].getHandle();
	btn_textbox_skip.normal = btn_textbox_skip.now =
		g_manager->system_graphics[SystemGraphic::arrnum_textbox_skip].getHandle();
	btn_textbox_x.normal = btn_textbox_x.now =
		g_manager->system_graphics[SystemGraphic::arrnum_textbox_x].getHandle();
	btn_textbox_auto.normal = btn_textbox_auto.now =
		g_manager->system_graphics[SystemGraphic::arrnum_textbox_auto].getHandle();

	btn_textbox_config.click =
		g_manager->system_graphics[SystemGraphic::arrnum_textbox_config_on].getHandle();
	btn_textbox_hskip.click =
		g_manager->system_graphics[SystemGraphic::arrnum_textbox_hskip_on].getHandle();
	btn_textbox_qload.click =
		g_manager->system_graphics[SystemGraphic::arrnum_textbox_qload_on].getHandle();
	btn_textbox_qsave.click =
		g_manager->system_graphics[SystemGraphic::arrnum_textbox_qsave_on].getHandle();
	btn_textbox_skip.click =
		g_manager->system_graphics[SystemGraphic::arrnum_textbox_skip_on].getHandle();
	btn_textbox_x.click =
		g_manager->system_graphics[SystemGraphic::arrnum_textbox_x_on].getHandle();
	btn_textbox_auto.click =
		g_manager->system_graphics[SystemGraphic::arrnum_textbox_auto_on].getHandle();

	DxLib::GetGraphSize(btn_textbox_config.normal, &btn_textbox_config.width, &btn_textbox_config.height);
	DxLib::GetGraphSize(btn_textbox_hskip.normal, &btn_textbox_hskip.width, &btn_textbox_hskip.height);
	DxLib::GetGraphSize(btn_textbox_qload.normal, &btn_textbox_qload.width, &btn_textbox_qload.height);
	DxLib::GetGraphSize(btn_textbox_qsave.normal, &btn_textbox_qsave.width, &btn_textbox_qsave.height);
	DxLib::GetGraphSize(btn_textbox_skip.normal, &btn_textbox_skip.width, &btn_textbox_skip.height);
	DxLib::GetGraphSize(btn_textbox_x.normal, &btn_textbox_x.width, &btn_textbox_x.height);
	DxLib::GetGraphSize(btn_textbox_auto.normal, &btn_textbox_auto.width, &btn_textbox_auto.height);

	btn_textbox_base.x = 27;
	btn_textbox_base.y = 540;
	btn_textbox_qsave.x = 502;
	btn_textbox_qsave.y = 563;
	btn_textbox_qload.x = 595;
	btn_textbox_qload.y = 563;
	btn_textbox_config.x = 689;
	btn_textbox_config.y = 563;
	btn_textbox_skip.x = 771;
	btn_textbox_skip.y = 563;
	btn_textbox_hskip.x = 854;
	btn_textbox_hskip.y = 563;
	btn_textbox_x.x = 937;
	btn_textbox_x.y = 563;
	btn_textbox_auto.x = 420;
	btn_textbox_auto.y = 563;
	
}

bool Teng::Scenario::inButton(Btn& btn){
	if (cursor_x >= btn.x &&
		cursor_x < btn.x + btn.width &&
		cursor_y >= btn.y &&
		cursor_y < btn.y + btn.height){
		return true;
	}
	return false;
}