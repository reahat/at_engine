﻿#include "../graphic.h"
#include "../define.h"
#include "../global.h"
#include "../error_code_define.h"
#include "sound.h"

Teng::Sound::Sound(){
	filename[0] = NC;
	volume = 255;
	handle = 0;
	playing = false;
};

Teng::Sound::~Sound(){};

void Teng::Sound::setFileName(const char *arg_filename){
	strcpy_s(filename, TENG_FILE_NAME_MAX, arg_filename);
}

char* Teng::Sound::getFileName(){
	return filename;
}

void Teng::Sound::setVolume(const short volume){
	this->volume = volume;
}

short Teng::Sound::getVolume(){
	return volume;
}

void Teng::Sound::setHandle(const int handle){
	this->handle = handle;
}

int Teng::Sound::getHandle(){
	return handle;
}

void Teng::Sound::setPlaying(const bool playing){
	this->playing = playing;
}

bool Teng::Sound::getPlaying(){
	return playing;
}