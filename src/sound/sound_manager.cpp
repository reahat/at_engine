﻿#include "../define.h"
#include "../global.h"
#include "../error_code_define.h"
#include "sound_manager.h"
#include <Shlwapi.h>
#pragma comment(lib, "shlwapi.lib")
extern char common_dir[256];
using namespace Teng;

SoundManager::SoundManager(){
	for(int i = 0; i < TENG_SOUND_SYSTEM_MAX_COUNT; i++){
		system_sound[i].setHandle(0);
	}
	is_bgm_fading = false;
	bgm_fade_power = 0;
	bgm_stop_complete = false;
};

SoundManager::~SoundManager(){};

arrnum SoundManager::setSystemSound(
	SystemSound::SystemSoundType type,
	bool is_bgm,
	const char *filename,
	int volume)
{
	char fullpath[256];
	sprintf_s(fullpath, TENG_FILE_NAME_MAX, "%s%s%s", common_dir, TENG_DIR_SYSTEM_SOUND, filename);

	for(int i = 0; i < TENG_SOUND_SYSTEM_MAX_COUNT; i++){
		if(this->system_sound[i].getHandle() == 0){
			this->system_sound[i].setHandle(DxLib::LoadSoundMem(fullpath));
			this->system_sound[i].setFileName(filename);
			this->system_sound[i].setPlaying(false);
			this->system_sound[i].setVolume(volume);
			this->system_sound[i].setSystemSoundType(type, is_bgm);
			if(this->system_sound[i].getHandle() < 0 || PathFileExists(fullpath) == FALSE){
				//DEBUG printf("[ERROR] システム音声の読み込みに失敗しました: %s\n", filename);
				//throw ENGINE_SOUND_SYSTEM_LOAD_FAILED;
			}
			return i;
		}
	}
	DEBUG printf("[ERROR] システム音声の読込制限数に達しました: %s\n", filename);
	throw ENGINE_SOUND_SYSTEM_LOAD_LIMIT_EXCEEDED;
	return -2;
}