﻿#include "system_sound.h"
#include "../define.h"
#include "../global.h"
#include "../error_code_define.h"
#include "sound.h"

using namespace Teng;

arrnum SystemSound::arrnum_bgm_title;
arrnum SystemSound::arrnum_se_config_demo;
arrnum SystemSound::arrnum_se_select_focus;
arrnum SystemSound::arrnum_se_select_clicked;
arrnum SystemSound::arrnum_se_button_title_clicked;
arrnum SystemSound::arrnum_se_button_title_focus;


SystemSound::SystemSound(){
	system_sound_type = eSysSoundType_none;
	sound_div = true;
}

SystemSound::~SystemSound(){}