﻿#pragma once
#include "../define.h"
//#include "sound_manager.h"

namespace Teng{
	class Sound{
	public:
		Sound();
		virtual ~Sound();

		void  setFileName(const char *filename);
		char* getFileName();
		void  setVolume(const short volume);
		short getVolume();
		void  setHandle(int handle);
		int   getHandle();
		void  setPlaying(const bool playing);
		bool  getPlaying();
		
	private:
		char  filename[TENG_FILE_NAME_MAX];
		short volume;
		int   handle;
		bool  playing;
	};

}