﻿#include "../graphic.h"
#include "../define.h"
#include "../global.h"
#include "../error_code_define.h"
#include "bgm.h"
#include "sound_manager.h"
#include <Shlwapi.h>
#pragma comment(lib, "shlwapi.lib")
extern char common_dir[256];
extern bool is_hyper_skip;

Teng::BackGroundMusic::BackGroundMusic(){
	loop_point = 0;
}

Teng::BackGroundMusic::~BackGroundMusic(){

}

void Teng::SoundManager::loadBgm(const char* filename, const int loop_point){
	if(filename[0] == NC){
		DEBUG printf("[ERROR] @bgm.load ファイル名を指定して下さい\n");
		throw ENGINE_SOUND_BGM_LOAD_ARGUMENT_NOT_EXIST;
	}
	//TODO
	//////開発用
	char fullpath[256];
	strcpy_s(fullpath, 256, common_dir);
	strcat_s(fullpath, 256, TENG_DIR_BGM);
	strcat_s(fullpath, TENG_FILE_NAME_MAX, filename);
	DxLib::SetCreateSoundDataType(DX_SOUNDDATATYPE_MEMPRESS);
	int tmp_handle;
	if(!is_hyper_skip){
		tmp_handle= DxLib::LoadSoundMem(fullpath);
		if(tmp_handle < 0 || PathFileExists(fullpath) == FALSE){
			//DEBUG printf("[ERROR] @bgm.load BGMの読み込みに失敗しました %s\n", filename);
			//throw ENGINE_SOUND_BGM_FILE_NOT_FOUND;
		}
	}else{
		tmp_handle = -2;
	}
	for(int i = 0; i < TENG_SOUND_BGM_MAX_COUNT; i++){
		if(bgm[i].getHandle() == 0){
			bgm[i].setFileName(filename);
			bgm[i].setHandle(tmp_handle);
			if (loop_point > 0){
				DEBUG printf("[INFO] @bgm.load ループ位置：%dms\n", loop_point);
				bgm[i].setLoopPoint(loop_point);
			}
			else if (loop_point < 0){
				DEBUG printf("[INFO] @bgm.load ループ位置不正\n");
				bgm[i].setLoopPoint(0);
			}
			return;
		}
	}

	DEBUG printf("BGMのロード数上限に達しました。 %s\n", filename);
	throw ENGINE_SOUND_BGM_EXCEED_LIMIT;
}

void Teng::SoundManager::playBgm(Config* config, char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER+1][TENG_SCRIPT_COMMAND_MAX_LENGTH]){
	// @bgm.play filename, volume
	int volume;

	if(command[1][0] == NC){
		DEBUG printf("[ERROR] (@bgm.play) ファイル名を指定して下さい\n");
		throw ENGINE_SOUND_BGM_PLAY_ARGUMENT_NOT_EXIST;
	}
	//volumeセット
	if(command[2][0] == NC || atoi(command[2]) < 0 || atoi(command[2]) > 255){
		volume = TENG_SOUND_BGM_VOLUME_DEFAULT;
		DEBUG printf("[INFO] (@bgm.play) volume値が未指定のためデフォルト値を使用します。\n");
	}else{
		volume = atoi(command[2]);
	}

	//再生中のBGMがあれば停止する
	for(int i = 0; i < TENG_SOUND_BGM_MAX_COUNT; i++){
		if(bgm[i].getPlaying()){
			bgm[i].setPlaying(false);
			DxLib::StopSoundMem(bgm[i].getHandle());
		}
	}

	//再生
	for(int i = 0; i < TENG_SOUND_BGM_MAX_COUNT; i++){
		if(strncmp(bgm[i].getFileName(), command[1], TENG_FILE_NAME_MAX) == 0){
			bgm[i].setVolume(volume);

			double vol =
				(double)(config->getSetting().master_volume) *
				(double)(config->getSetting().bgm_volume) / 65025 * volume;

			int loop_cnt = 0;
			while(!is_hyper_skip && DxLib::CheckHandleASyncLoad(bgm[i].getHandle()) != FALSE && loop_cnt < 500){
				DxLib::WaitTimer(10);
				DEBUG printf("[INFO] 非同期ロード待機：%s\n", bgm[i].getFileName());
				loop_cnt++;
			}

			DxLib::SetLoopPosSoundMem(bgm[i].getLoopPoint(), bgm[i].getHandle());
			int play_result = 0;
			if(!is_hyper_skip){
				DxLib::ChangeVolumeSoundMem((int)vol, bgm[i].getHandle());
				play_result = DxLib::PlaySoundMem(bgm[i].getHandle(), DX_PLAYTYPE_LOOP, TRUE);
			}
			if (play_result == -1){
				DEBUG printf("[ERROR] @bgm.play BGMの再生に失敗しました %s\n", command[1]);
				throw ENGINE_SOUND_BGM_FILE_NOT_FOUND;
			}

			bgm[i].setPlaying(true);
			return;
		}
	}

	DEBUG printf("[ERROR] (@bgm.play) %sは未ロードです。\n", command[1]);
	throw ENGINE_SOUND_BGM_PLAY_FILE_YET_LOADED;
}

bool Teng::SoundManager::stopBgm(const char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER+1][TENG_SCRIPT_COMMAND_MAX_LENGTH]){
//	for each(auto music in bgm){

//			while (!is_hyper_skip && DxLib::CheckHandleASyncLoad(music.getHandle()) == TRUE){
//				DxLib::ProcessMessage();
//				DEBUG printf("[INFO] 非同期ロード完了まで停止を待機：%s\n", music.getFileName());
//				Sleep(100);
//			}

//	}
	bool now_stop = false;
	if (command[1][0] == NC || is_hyper_skip){
		DEBUG printf("[INFO] (@bgm.stop) BGMを停止します。\n");

		now_stop = true;

		//停止
		for (int i = 0; i < TENG_SOUND_BGM_MAX_COUNT; i++){
			DxLib::StopSoundMem(bgm[i].getHandle());
			bgm[i].setPlaying(false);
		}
	}

	// 停止させるBGMを指定する必要はない気がする
	//}else{
	//	DEBUG printf("[INFO] (@bgm.stop) %sを停止します。\n", command);
	//	//停止
	//	for(int i = 0; i < TENG_SOUND_BGM_MAX_COUNT; i++){
	//		if(strncmp(bgm[i].getFileName(), command, TENG_FILE_NAME_MAX) == 0 &&
	//			bgm[i].getPlaying())
	//		{
	//			DxLib::StopSoundMem(bgm[i].getHandle());
	//			bgm[i].setPlaying(false);
	//			return;
	//		}
	//	}
	//	DEBUG printf("[ERROR] (@bgm.stop) %sは再生中ではありません。\n", command);
	//}

	//フェードの強さをセット
	if (command[1][0] != NC && !is_hyper_skip){
		bgm_fade_power = (double)atoi(command[1]);
		is_bgm_fading = true;
	}
	return now_stop;
}

void Teng::SoundManager::fadeBgm(Config* config, Scene* scene, Input* input){
	if (is_bgm_fading){
		int playing_num = -1;
		//再生中のBGM番号を探す
		for (size_t i = 0; i < TENG_SOUND_BGM_MAX_COUNT; i++){
			if (bgm[i].getPlaying()){
				playing_num = (int)i;
				break;
			}
		}
		
		int vol_down_pow = 0;
		// １～
		if (bgm_fade_power >= 1){
			vol_down_pow = (int)bgm_fade_power;
		}
		// 0.75 ~ 1
		else if (bgm_fade_power >= 0.75){
			vol_down_pow = scene->getFrame() % 4 == 0 ? 0 : 1;
		}
		// 0.66 ~ 0.75
		else if (bgm_fade_power >= 0.66){
			vol_down_pow = scene->getFrame() % 3 == 0 ? 0 : 1;
		}
		// 0.5 ~ 0.75
		else if (bgm_fade_power >= 0.5){
			vol_down_pow = scene->getFrame() % 2 == 0 ? 1 : 0;
		}
		// 0.33 ~ 0.5
		else if (bgm_fade_power >= 0.33){
			vol_down_pow = scene->getFrame() % 3 == 0 ? 1 : 0;
		}
		// 0.25 ~ 0.33
		else if (bgm_fade_power >= 0.25){
			vol_down_pow = scene->getFrame() % 4 == 0 ? 1 : 0;
		}
		else{
			vol_down_pow = 1;
		}

		short volume = bgm[playing_num].getVolume();
		volume -= (short)vol_down_pow;
		bgm[playing_num].setVolume(volume);
		double vol =
			(double)(config->getSetting().master_volume) *
			(double)(config->getSetting().bgm_volume) / 65025 * volume;
		DxLib::ChangeVolumeSoundMem((int)vol, bgm[playing_num].getHandle());

		if (volume <= 0){
			is_bgm_fading = false;
			bgm[playing_num].setVolume(0);
			bgm_stop_complete = true;
			DxLib::StopSoundMem(bgm[playing_num].getHandle());
			bgm[playing_num].setPlaying(false);
		}

		// Ctrlスキップ中は即停止
		if (input->getKeyControl() != 0 || config->getSkipModeFlag()){
			is_bgm_fading = false;
			bgm[playing_num].setVolume(0);
			bgm_stop_complete = true;
			DxLib::StopSoundMem(bgm[playing_num].getHandle());
			bgm[playing_num].setPlaying(false);
		}

	}
}


void Teng::SoundManager::freeBgm(const char *command){
	if(command[0] == NC){
		DEBUG printf("[ERROR] (@bgm.free) 解放対象を指定してください。\n");
		return;
	}
	for(int i = 0; i < TENG_SOUND_BGM_MAX_COUNT; i++){
		if(strncmp(bgm[i].getFileName(), command, TENG_FILE_NAME_MAX) == 0){
			if(bgm[i].getPlaying()){
				DxLib::StopSoundMem(bgm[i].getHandle());
			}
			DxLib::DeleteSoundMem(bgm[i].getHandle());
			bgm[i].setPlaying(false);
			bgm[i].setFileName("");
			bgm[i].setHandle(0);
			return;
		}
	}
	DEBUG printf("[ERROR] (@bgm.free) %sは未ロードです。\n", command);
}

void Teng::SoundManager::freeBgmAll(){
	DEBUG printf("[INFO] 全てのBGMを解放します\n");
	for(int i = 0; i < TENG_SOUND_BGM_MAX_COUNT; i++){
		if(bgm[i].getPlaying()){
			DxLib::StopSoundMem(bgm[i].getHandle());
		}
		DxLib::DeleteSoundMem(bgm[i].getHandle());
		bgm[i].setPlaying(false);
		bgm[i].setFileName("");
		bgm[i].setHandle(0);
	}
}