﻿#pragma once
#include "../define.h"
#include "sound.h"


namespace Teng{
	class BackGroundMusic : public Sound{
	public:
		BackGroundMusic();
		~BackGroundMusic();
		void setLoopPoint(const int point_ms){ loop_point = point_ms; }
		int  getLoopPoint(){ return loop_point; }
	private:
		int loop_point;
	};
}