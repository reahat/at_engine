﻿#pragma once
#include "../define.h"
#include "bgm.h"
#include "se.h"
#include "../config.h"
#include "system_sound.h"

namespace Teng{
	class Scene;
	class Input;
	class SoundManager{
	public:
		SoundManager();
		virtual ~SoundManager();

		void loadBgm(const char *filename, const int loop_point);
		void playBgm(Config* config, char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER+1][TENG_SCRIPT_COMMAND_MAX_LENGTH]);
		bool stopBgm(const char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER+1][TENG_SCRIPT_COMMAND_MAX_LENGTH]);
		void freeBgm(const char *filename);
		void freeBgmAll();

		void loadSe(const char *filename, const int loop_point);
		void playSe(Config* config, char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER+1][TENG_SCRIPT_COMMAND_MAX_LENGTH]);
		void stopSe(const char *filename);
		void freeSe(const char *filename);
		void freeSeAll();

		bool isBgmFading(){ return is_bgm_fading; }
		void isBgmFading(const bool now_fade){ is_bgm_fading = now_fade; }
		void fadeBgm(Config* config, Scene* scene, Input* input);
		bool isBgmStopComplete(){ return bgm_stop_complete; }
		void isBgmStopComplete(const bool is_stopped){ bgm_stop_complete = is_stopped; }
		void setBgmFadePower(double power){ bgm_fade_power = power; }

		//システム音声をロードする
		arrnum setSystemSound(SystemSound::SystemSoundType type, bool is_bgm, const char *filename, int volume);

		Teng::BackGroundMusic bgm[TENG_SOUND_BGM_MAX_COUNT];
		Teng::SoundEffect se[TENG_SOUND_SE_MAX_COUNT];
		Teng::SystemSound system_sound[TENG_SOUND_SYSTEM_MAX_COUNT];
	private:
		double bgm_fade_power;
		bool is_bgm_fading;
		bool bgm_stop_complete; //完全に停止した瞬間のみtrueになる
	};
}
