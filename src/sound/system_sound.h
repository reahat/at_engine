﻿#pragma once
#include "../define.h"
#include "sound.h"

namespace Teng{
	class SystemSound : public Sound{
	public:
		enum SystemSoundType{
			eSysSoundType_none,
			eSysSoundType_bgm_title,
			eSysSoundType_se_config_demo,
			eSysSoundType_se_select_clicked,
			eSysSoundType_se_select_focus,
			eSysSoundType_se_button_title_clicked,
			eSysSoundType_se_button_title_focus
		};

		SystemSound();
		~SystemSound();

		static arrnum arrnum_bgm_title;
		static arrnum arrnum_se_config_demo;
		static arrnum arrnum_se_select_clicked;
		static arrnum arrnum_se_select_focus;
		static arrnum arrnum_se_button_title_clicked;
		static arrnum arrnum_se_button_title_focus;

		void setSystemSoundType(SystemSoundType type, bool is_bgm){ system_sound_type = type;sound_div = is_bgm;}

	private:
		int system_sound_type;
		bool sound_div; // true:bgm, false:se
	};

}