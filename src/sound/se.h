﻿#pragma once
#include "../define.h"
#include "sound.h"

namespace Teng{
	class SoundEffect : public Sound{
	public:
		SoundEffect();
		~SoundEffect();
		void setLoopPoint(const int point_ms){ loop_point = point_ms; }
		int  getLoopPoint(){ return loop_point; }
	private:
		int loop_point;
	};
}