﻿#include "../graphic.h"
#include "../define.h"
#include "../global.h"
#include "../error_code_define.h"
#include "se.h"
#include "sound_manager.h"
#include <Shlwapi.h>
#pragma comment(lib, "shlwapi.lib")
extern char common_dir[256];
extern bool is_hyper_skip;

Teng::SoundEffect::SoundEffect(){

}

Teng::SoundEffect::~SoundEffect(){

}

void Teng::SoundManager::loadSe(const char *filename, const int loop_point){
	if(filename[0] == NC){
		DEBUG printf("@se.load ファイル名を指定して下さい\n");
		throw ENGINE_SOUND_SE_LOAD_ARGUMENT_NOT_EXIST;
	}
	//TODO
	//////開発用
	char fullpath[256];
	strcpy_s(fullpath, 256, common_dir);
	strcat_s(fullpath, 256, TENG_DIR_SE);
	strcat_s(fullpath, TENG_FILE_NAME_MAX, filename);
	DxLib::SetCreateSoundDataType(DX_SOUNDDATATYPE_FILE);
	int tmp_handle;
	if(!is_hyper_skip){
		tmp_handle = DxLib::LoadSoundMem(fullpath);
		if(tmp_handle < 0 || PathFileExists(fullpath) == FALSE){
			//DEBUG printf("[ERROR] @se.load 効果音の読み込みに失敗しました %s\n", filename);
			//throw ENGINE_SOUND_SE_FILE_NOT_FOUND;
		}
	}else{
		tmp_handle = -2;
	}

	for(int i = 0; i < TENG_SOUND_SE_MAX_COUNT; i++){
		if(se[i].getHandle() == 0){
			se[i].setFileName(filename);
			se[i].setHandle(tmp_handle);
			if (loop_point > 0){
				DEBUG printf("[INFO] @se.load ループ位置：%dms\n", loop_point);
				se[i].setLoopPoint(loop_point);
			}
			else if (loop_point < 0){
				DEBUG printf("[INFO] @se.load ループ位置不正\n");
				se[i].setLoopPoint(0);
			}
			return;
		}
	}

	DEBUG printf("SEのロード数上限に達しました。 %s\n", filename);
	throw ENGINE_SOUND_SE_EXCEED_LIMIT;
}

void Teng::SoundManager::playSe(Config* config, char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER+1][TENG_SCRIPT_COMMAND_MAX_LENGTH]){
	// @se.play filename, volume
	int volume;

	if(command[1][0] == NC){
		DEBUG printf("[ERROR] (@se.play) ファイル名を指定して下さい\n");
		throw ENGINE_SOUND_SE_PLAY_ARGUMENT_NOT_EXIST;
	}
	//volumeセット
	if(command[2][0] == NC || atoi(command[2]) < 0 || atoi(command[2]) > 255){
		volume = TENG_SOUND_SE_VOLUME_DEFAULT;
		DEBUG printf("[INFO] (@se.play) volume値が未指定のためデフォルト値を使用します。\n");
	}else{
		volume = atoi(command[2]);
	}

	//SEをループ再生するかどうか
	int loop;
	if (command[3][0] == NC){
		loop = DX_PLAYTYPE_BACK;
		DEBUG printf("[INFO] (@se.play) ループ無しで再生します。\n");
	}
	else if(strcmp(command[3], "loop") == 0){
		loop = DX_PLAYTYPE_LOOP;
		DEBUG printf("[INFO] (@se.play) ループ有りで再生します。\n");
	}
	else{
		loop = DX_PLAYTYPE_BACK;
		DEBUG printf("[INFO] (@se.play) 不正なループ指定です。\n");
		DEBUG printf("[INFO] (@se.play) ループ無しで再生します。\n");
	}

	//再生中のSEがあっても停止はしない
	//for(int i = 0; i < TENG_SOUND_BGM_MAX_COUNT; i++){
	//	if(bgm[i].getPlaying()){
	//		bgm[i].setPlaying(false);
	//		DxLib::StopSoundMem(bgm[i].getHandle());
	//	}
	//}

	//再生
	for(int i = 0; i < TENG_SOUND_SE_MAX_COUNT; i++){
		if(strncmp(se[i].getFileName(), command[1], TENG_FILE_NAME_MAX) == 0){
			//se[i].setVolume(volume);
			double vol =
				(double)(config->getSetting().master_volume) *
				(double)(config->getSetting().sound_volume) / 65025 * volume;

			int loop_cnt = 0;
			while(!is_hyper_skip && DxLib::CheckHandleASyncLoad(se[i].getHandle()) != FALSE && loop_cnt < 500){
				DxLib::ProcessMessage();
				DEBUG printf("[INFO] 非同期ロード待機：%s\n", se[i].getFileName());
				Sleep(10);
				loop_cnt++;
			}
			if (loop == DX_PLAYTYPE_LOOP){
				DxLib::SetLoopPosSoundMem(se[i].getLoopPoint(), se[i].getHandle());
			}
			int play_result = 0;
			if(!is_hyper_skip){
				DxLib::ChangeVolumeSoundMem((int)vol, se[i].getHandle());
				play_result = DxLib::PlaySoundMem(se[i].getHandle(), loop, TRUE);
				se[i].setPlaying(true);
			}
			if (play_result == -1){
				DEBUG printf("[ERROR] @se.play SEの再生に失敗しました %s\n", command[1]);
				throw ENGINE_SOUND_SE_FILE_NOT_FOUND;
			}
			return;
		}
	}

	DEBUG printf("[ERROR] (@se.play) %sは未ロードです。\n", command[1]);
	throw ENGINE_SOUND_SE_PLAY_FILE_YET_LOADED;
}

void Teng::SoundManager::stopSe(const char *command){
	if(command[0] == NC){
		DEBUG printf("[INFO] (@se.stop) 全てのSEを停止します。\n");
		//停止
		for(int i = 0; i < TENG_SOUND_BGM_MAX_COUNT; i++){
			DxLib::StopSoundMem(se[i].getHandle());
			//se[i].setPlaying(false);
		}
	}else{
		DEBUG printf("[INFO] (@se.stop) %sを停止します。\n", command);
		//停止
		for(int i = 0; i < TENG_SOUND_SE_MAX_COUNT; i++){
			if(strncmp(se[i].getFileName(), command, TENG_FILE_NAME_MAX) == 0
				&& se[i].getPlaying()
					)
			{
				DxLib::StopSoundMem(se[i].getHandle());
				se[i].setPlaying(false);
				return;
			}
		}
		DEBUG printf("[ERROR] (@se.stop) %sは再生中ではありません。\n", command);
	}
}

void Teng::SoundManager::freeSe(const char *command){
	if(command[0] == NC){
		DEBUG printf("[ERROR] (@se.free) 解放対象を指定してください。\n");
		return;
	}
	for(int i = 0; i < TENG_SOUND_SE_MAX_COUNT; i++){
		if(strncmp(se[i].getFileName(), command, TENG_FILE_NAME_MAX) == 0){
			//if(se[i].getPlaying()){
			DxLib::StopSoundMem(se[i].getHandle());
			//}
			DxLib::DeleteSoundMem(se[i].getHandle());
			se[i].setPlaying(false);
			se[i].setFileName("");
			se[i].setHandle(0);
			return;
		}
	}
	DEBUG printf("[ERROR] (@se.free) %sは未ロードです。\n", command);
}

void Teng::SoundManager::freeSeAll(){
	DEBUG printf("[INFO] 全てのSEを解放します\n");
	for(int i = 0; i < TENG_SOUND_SE_MAX_COUNT; i++){
		//if(se[i].getPlaying()){
		DxLib::StopSoundMem(se[i].getHandle());
		//}
		DxLib::DeleteSoundMem(se[i].getHandle());
		se[i].setPlaying(false);
		se[i].setFileName("");
		se[i].setHandle(0);
	}
}