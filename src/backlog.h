﻿//バックログ管理クラス
#pragma once

#include <string>
#include <list>
using std::string;
using std::list;

namespace Teng{
	class BacklogMessage : public list<string>{
	public:
		// ログ１行を追加
		void   add(string message);
		// 話者を追加
		void   setSpeaker(string speaker){this->speaker = speaker;}
		string getSpeaker(){return speaker;}
		list<string> getLine(){return log_line;}
	private:
		list<string> log_line;
		string speaker;
	};

	// ログ全体の管理クラス
	class Backlog{
	public:
		Backlog();
		virtual ~Backlog();

		// ログ１回分を追加
		void push(BacklogMessage& log);
		//ログをすべて削除
		void clear();
		list<BacklogMessage> logs(){return backlogs;}
	private:
		list<BacklogMessage> backlogs;
		
	};




}