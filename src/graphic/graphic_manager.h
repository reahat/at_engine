﻿#pragma once
#include "../define.h"
#include "system_graphic.h"
#include "character_graphic.h"
#include "background_graphic.h"
#include "rule_graphic.h"
#include "face_graphic.h"
#include "../config.h"
#include "../click_wait.h"

namespace Teng{
	//class ClickWait;
	class Input;
	class GraphicManager : public SystemGraphic, CharacterGraphic{
	public:
		GraphicManager();

		void resetClickWait();
		void drawClickWaitFall();
		void drawClickWaitFade(const int x, const int y);

		//システム画像をロードする
		arrnum setSystemGraphic(int type, const char *filename, int x, int y, int parent = 0, int child = 0);
		SystemGraphic getSystemGraphic(int num){ return system_graphics[num]; }
		SystemGraphic* getSystemGraphics(){ return system_graphics; }
		//SystemGraphicはdeleteはしない

		//立ち絵のロード
		void loadCharacterGraphic(char *arg_filename);

		//立ち絵の表示セット
		bool setCharacterGraphic(char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER+1][TENG_SCRIPT_COMMAND_MAX_LENGTH]);
		//立ち絵の表示切り替え
		void changeCharacterGraphic(char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER+1][TENG_SCRIPT_COMMAND_MAX_LENGTH]);

		//システム画像の描画
		void drawSystemGraphic(int scene);

		void drawBackGroundGraphic(Input* input);
		void hideBackGroundGraphic(const int exclude_arrnum);

		//キャラクター画像の描画
		void drawCharacterGraphic(Input* input);

		void hideCharacterGraphic(char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER+1][TENG_SCRIPT_COMMAND_MAX_LENGTH]);
		void freeCharacterImage(char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER+1][TENG_SCRIPT_COMMAND_MAX_LENGTH]);
		void freeCharacterImageAll();

		
		Teng::SystemGraphic system_graphics[TENG_GRAPHIC_SYSTEM_MAX];
		Teng::CharacterGraphic character_graphics[TENG_GRAPHIC_CHARACTER_MAX];
		Teng::BackGroundGraphic background_graphics[TENG_GRAPHIC_BACKGROUND_MAX];
		Teng::RuleGraphic rule_graphics[TENG_GRAPHIC_RULE_MAX];
		Teng::FaceGraphic face_graphics[TENG_GRAPHIC_FACE_MAX];

		///////////// 背景 ////////////////////////

		//背景ロード
		void loadBackGroundGraphic(char *arg_filename);
		//背景ロード（シナリオ以外からロードする用） 入れたarrnumを返す
		arrnum loadBackGroundGraphicSystem(char *arg_filename, int type);

		//背景画像の表示セット
		void setBackGroundGraphic(char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER+1][TENG_SCRIPT_COMMAND_MAX_LENGTH], Config* config);

		//背景画像の解放
		void freeBackGroundGraphic(const char *filename);
		void freeBackGroundGraphicAll();

		//ルール画像のロード
		void loadRuleGraphic(char *rule_file);

		//ルール画像の解放
		void freeRuleGraphic(const char *rule_file);
		void freeRuleGraphicAll();

		//フェイス画像
		void loadFaceGraphic(const char *filename);
		void setFaceGraphic(const char *filename);
		void drawFaceGraphic();
		void hideFaceGraphic();
		void freeFaceGraphic(const char *filename);
		void freeFaceGraphicAll();

		//トランジション切替状態の取得
		bool getTransitionDoing(){ return transition_doing; }
		void setTransitionDoing(bool is_doing){ transition_doing = is_doing; }

		bool getCharacterFadeCompletedNow(){ return character_fade_completed_now; }
		void setCharacterFadeCompletedNow(bool completed_now){ character_fade_completed_now = completed_now; }
	private:
		bool transition_doing;
		bool character_fade_completed_now;
		bool is_character_crossfading;

		ClickWait click_wait;
	};


}