﻿#pragma once
#include "../define.h"
#include "../graphic.h"

namespace Teng{
	class SystemGraphic : public Graphic{
	public:
		enum SystemType{
			eGraphicSys_none,
			eGraphicSys_dialog_base,
			eGraphicSys_dialog_yes_normal,
			eGraphicSys_dialog_yes_click,
			eGraphicSys_dialog_no_normal,
			eGraphicSys_dialog_no_click,
			eGraphicSys_textbox_base,
			eGraphicSys_textbox_config,
			eGraphicSys_textbox_config_on,
			eGraphicSys_textbox_hskip,
			eGraphicSys_textbox_hskip_on,
			eGraphicSys_textbox_qsave,
			eGraphicSys_textbox_qsave_on,
			eGraphicSys_textbox_qload,
			eGraphicSys_textbox_qload_on,
			eGraphicSys_textbox_skip,
			eGraphicSys_textbox_skip_on,
			eGraphicSys_textbox_x,
			eGraphicSys_textbox_x_on,
			eGraphicSys_textbox_auto,
			eGraphicSys_textbox_auto_on,
			eGraphicSys_btn_save,
			eGraphicSys_btn_load,
			eGraphicSys_close_dialog,
			eGraphicSys_bg_save_1,
			eGraphicSys_bg_load_1,
			eGraphicSys_bg_title,
			eGraphicSys_bg_logo_1,
			eGraphicSys_bg_white,
			eGraphicSys_bg_blank,
			eGraphicSys_bg_config,
			eGraphicSys_select_normal,
			eGraphicSys_select_focus,
			eGraphicSys_select_selected,
			eGraphicSys_name_entry_box,
			eGraphicSys_message_arrow,
			eGraphicSys_bg_gallery,
			eGraphicSys_gallery_pic_small,
			eGraphicSys_gallery_pic_large,
			eGraphicSys_gallery_rule_slide,
			eGraphicSys_button_title_start_normal,
			eGraphicSys_button_title_start_focus,
			eGraphicSys_button_title_start_click,
			eGraphicSys_button_title_load_normal,
			eGraphicSys_button_title_load_focus,
			eGraphicSys_button_title_load_click,
			eGraphicSys_button_title_config_normal,
			eGraphicSys_button_title_config_focus,
			eGraphicSys_button_title_config_click,
			eGraphicSys_button_title_gallery_normal,
			eGraphicSys_button_title_gallery_focus,
			eGraphicSys_button_title_gallery_click,
			eGraphicSys_button_title_exit_normal,
			eGraphicSys_button_title_exit_focus,
			eGraphicSys_button_title_exit_click,
			eGraphicSys_button_title_kago_normal,
			eGraphicSys_button_title_kago_focus,
			eGraphicSys_button_save_page1_normal,
			eGraphicSys_button_save_page1_focus,
			eGraphicSys_button_save_page1_click,
			eGraphicSys_button_save_page2_normal,
			eGraphicSys_button_save_page2_focus,
			eGraphicSys_button_save_page2_click,
			eGraphicSys_button_save_page3_normal,
			eGraphicSys_button_save_page3_focus,
			eGraphicSys_button_save_page3_click,
			eGraphicSys_button_save_page4_normal,
			eGraphicSys_button_save_page4_focus,
			eGraphicSys_button_save_page4_click,
			eGraphicSys_button_save_page5_normal,
			eGraphicSys_button_save_page5_focus,
			eGraphicSys_button_save_page5_click,
			eGraphicSys_button_save_back_normal,
			eGraphicSys_button_save_back_focus,
			eGraphicSys_button_save_back_click,
			eGraphicSys_button_save_icon_bara,
			eGraphicSys_button_save_icon_arrow,
			eGraphicSys_button_save_thumbnail_arrow,
			eGraphicSys_rule_save_confirm_dialog,
			eGraphicSys_rule_save_confirm_dialog_reverse,
			eGraphicSys_config_button_all_normal,
			eGraphicSys_config_button_all_focus,
			eGraphicSys_config_button_all_click,
			eGraphicSys_config_button_already_normal,
			eGraphicSys_config_button_already_focus,
			eGraphicSys_config_button_already_click,
			eGraphicSys_config_button_minus_normal,
			eGraphicSys_config_button_minus_focus,
			eGraphicSys_config_button_minus_click,
			eGraphicSys_config_button_plus_normal,
			eGraphicSys_config_button_plus_focus,
			eGraphicSys_config_button_plus_click,
			eGraphicSys_config_button_bar_normal,
			eGraphicSys_config_button_bar_focus,
			eGraphicSys_config_button_bar_click,
			eGraphicSys_config_button_back_normal,
			eGraphicSys_config_button_back_focus,
			eGraphicSys_config_button_back_click,
			eGraphicSys_config_bar_base,
			eGraphicsys_menu_save_normal,
			eGraphicsys_menu_load_normal,
			eGraphicsys_menu_hskip_normal,
			eGraphicsys_menu_config_normal,
			eGraphicsys_menu_backlog_normal,
			eGraphicsys_menu_title_normal,
			eGraphicsys_menu_exit_normal,
			eGraphicsys_menu_back_normal,
			eGraphicsys_menu_save_focus,
			eGraphicsys_menu_load_focus,
			eGraphicsys_menu_hskip_focus,
			eGraphicsys_menu_config_focus,
			eGraphicsys_menu_backlog_focus,
			eGraphicsys_menu_title_focus,
			eGraphicsys_menu_exit_focus,
			eGraphicsys_menu_back_focus,
			eGraphicsys_menu_save_click,
			eGraphicsys_menu_load_click,
			eGraphicsys_menu_hskip_click,
			eGraphicsys_menu_config_click,
			eGraphicsys_menu_backlog_click,
			eGraphicsys_menu_title_click,
			eGraphicsys_menu_exit_click,
			eGraphicsys_menu_back_click,
			eGraphicSys_menu_base,
			eGraphicSys_name_entry_init_normal,
			eGraphicSys_name_entry_init_click,
			eGraphicSys_name_entry_decision_normal,
			eGraphicSys_name_entry_decision_click,
			eGraphicSys_backlog_base,
			eGraphicSys_backlog_base_bar,
			eGraphicSys_backlog_bar_normal,
			eGraphicSys_backlog_bar_focus,
			eGraphicSys_backlog_bar_click,
			eGraphicSys_backlog_down_normal,
			eGraphicSys_backlog_down_focus,
			eGraphicSys_backlog_down_click,
			eGraphicSys_backlog_up_normal,
			eGraphicSys_backlog_up_focus,
			eGraphicSys_backlog_up_click,
			eGraphicSys_backlog_back_normal,
			eGraphicSys_backlog_back_focus,
			eGraphicSys_backlog_back_click,
			eGraphicSys_wait_1,
			eGraphicSys_wait_2,
			eGraphicSys_wait_3,
			eGraphicSys_wait_4,
			eGraphicSys_wait_5,
			eGraphicSys_wait_6,
			eGraphicSys_wait_7,
			eGraphicSys_wait_8,
			eGraphicSys_wait_9,
			eGraphicSys_wait_10,
			eGraphicSys_wait_11,
			eGraphicSys_wait_12,
			eGraphicSys_wait_off
		};
		void setSystemType(int type);
		SystemGraphic();

		static arrnum arrnum_bg_load_1;
		static arrnum arrnum_bg_save_1;
		static arrnum arrnum_bg_logo_1;
		static arrnum arrnum_bg_blank;
		static arrnum arrnum_bg_white;
		static arrnum arrnum_bg_title;
		static arrnum arrnum_bg_config;
		//メッセージボックス

		static arrnum arrnum_textbox_base;
		static arrnum arrnum_textbox_config;
		static arrnum arrnum_textbox_config_on;
		static arrnum arrnum_textbox_hskip;
		static arrnum arrnum_textbox_hskip_on;
		static arrnum arrnum_textbox_qsave;
		static arrnum arrnum_textbox_qsave_on;
		static arrnum arrnum_textbox_qload;
		static arrnum arrnum_textbox_qload_on;
		static arrnum arrnum_textbox_skip;
		static arrnum arrnum_textbox_skip_on;
		static arrnum arrnum_textbox_x;
		static arrnum arrnum_textbox_x_on;
		static arrnum arrnum_textbox_auto;
		static arrnum arrnum_textbox_auto_on;
		// 選択肢	
		static arrnum arrnum_select_normal;
		static arrnum arrnum_select_focus;
		static arrnum arrnum_select_selected;
		static arrnum arrnum_name_entry_box;
		static arrnum arrnum_message_arrow;
		static arrnum arrnum_bg_gallery;
		static arrnum gallery_pic_small[TENG_GALLERY_MAX_NUMBER];
		static arrnum gallery_pic_large[TENG_GALLERY_MAX_NUMBER];
		static arrnum arrnum_gallery_rule_slide;
		static arrnum arrnum_dialog_base;
		static arrnum arrnum_dialog_yes_normal;
		static arrnum arrnum_dialog_yes_click;
		static arrnum arrnum_dialog_no_normal;
		static arrnum arrnum_dialog_no_click;
		//タイトル画面ボタン類
		static arrnum arrnum_button_title_start_normal;
		static arrnum arrnum_button_title_start_focus;
		static arrnum arrnum_button_title_start_click;
		static arrnum arrnum_button_title_load_normal;
		static arrnum arrnum_button_title_load_focus;
		static arrnum arrnum_button_title_load_click;
		static arrnum arrnum_button_title_config_normal;
		static arrnum arrnum_button_title_config_focus;
		static arrnum arrnum_button_title_config_click;
		static arrnum arrnum_button_title_gallery_normal;
		static arrnum arrnum_button_title_gallery_focus;
		static arrnum arrnum_button_title_gallery_click;
		static arrnum arrnum_button_title_exit_normal;
		static arrnum arrnum_button_title_exit_focus;
		static arrnum arrnum_button_title_exit_click;
		static arrnum arrnum_button_title_kago_normal;
		static arrnum arrnum_button_title_kago_focus;
		//セーブロード画面ボタン類
		static arrnum arrnum_button_save_page1_normal;
		static arrnum arrnum_button_save_page1_focus;
		static arrnum arrnum_button_save_page1_click;
		static arrnum arrnum_button_save_page2_normal;
		static arrnum arrnum_button_save_page2_focus;
		static arrnum arrnum_button_save_page2_click;
		static arrnum arrnum_button_save_page3_normal;
		static arrnum arrnum_button_save_page3_focus;
		static arrnum arrnum_button_save_page3_click;
		static arrnum arrnum_button_save_page4_normal;
		static arrnum arrnum_button_save_page4_focus;
		static arrnum arrnum_button_save_page4_click;
		static arrnum arrnum_button_save_page5_normal;
		static arrnum arrnum_button_save_page5_focus;
		static arrnum arrnum_button_save_page5_click;
		static arrnum arrnum_button_save_back_normal;
		static arrnum arrnum_button_save_back_focus;
		static arrnum arrnum_button_save_back_click;
		static arrnum arrnum_button_save_icon_bara;
		static arrnum arrnum_button_save_icon_arrow;
		static arrnum arrnum_rule_save_confirm_dialog;
		static arrnum arrnum_button_save_thumbnail_arrow;
		static arrnum arrnum_rule_save_confirm_dialog_reverse;

		// コンフィグ画面
		static arrnum arrnum_config_button_all_normal;
		static arrnum arrnum_config_button_all_focus;
		static arrnum arrnum_config_button_all_click;
		static arrnum arrnum_config_button_already_normal;
		static arrnum arrnum_config_button_already_focus;
		static arrnum arrnum_config_button_already_click;
		static arrnum arrnum_config_button_minus_normal;
		static arrnum arrnum_config_button_minus_focus;
		static arrnum arrnum_config_button_minus_click;
		static arrnum arrnum_config_button_plus_normal;
		static arrnum arrnum_config_button_plus_focus;
		static arrnum arrnum_config_button_plus_click;
		static arrnum arrnum_config_button_bar_normal;
		static arrnum arrnum_config_button_bar_focus;
		static arrnum arrnum_config_button_bar_click;
		static arrnum arrnum_config_button_back_normal;
		static arrnum arrnum_config_button_back_focus;
		static arrnum arrnum_config_button_back_click;
		static arrnum arrnum_config_bar_base;

		// 右クリックメニュー
		static arrnum arrnum_menu_save_normal;
		static arrnum arrnum_menu_load_normal;
		static arrnum arrnum_menu_hskip_normal;
		static arrnum arrnum_menu_config_normal;
		static arrnum arrnum_menu_backlog_normal;
		static arrnum arrnum_menu_title_normal;
		static arrnum arrnum_menu_exit_normal;
		static arrnum arrnum_menu_back_normal;
		static arrnum arrnum_menu_save_focus;
		static arrnum arrnum_menu_load_focus;
		static arrnum arrnum_menu_hskip_focus;
		static arrnum arrnum_menu_config_focus;
		static arrnum arrnum_menu_backlog_focus;
		static arrnum arrnum_menu_title_focus;
		static arrnum arrnum_menu_exit_focus;
		static arrnum arrnum_menu_back_focus;
		static arrnum arrnum_menu_save_click;
		static arrnum arrnum_menu_load_click;
		static arrnum arrnum_menu_hskip_click;
		static arrnum arrnum_menu_config_click;
		static arrnum arrnum_menu_backlog_click;
		static arrnum arrnum_menu_title_click;
		static arrnum arrnum_menu_exit_click;
		static arrnum arrnum_menu_back_click;
		static arrnum arrnum_menu_base;

		// 名前入力
		static arrnum arrnum_name_entry_init_normal;
		static arrnum arrnum_name_entry_init_click;
		static arrnum arrnum_name_entry_decision_normal;
		static arrnum arrnum_name_entry_decision_click;

		// バックログ
		static arrnum arrnum_backlog_base;
		static arrnum arrnum_backlog_base_bar;
		static arrnum arrnum_backlog_bar_normal;
		static arrnum arrnum_backlog_bar_focus;
		static arrnum arrnum_backlog_bar_click;
		static arrnum arrnum_backlog_down_normal;
		static arrnum arrnum_backlog_down_focus;
		static arrnum arrnum_backlog_down_click;
		static arrnum arrnum_backlog_up_normal;
		static arrnum arrnum_backlog_up_focus;
		static arrnum arrnum_backlog_up_click;
		static arrnum arrnum_backlog_back_normal;
		static arrnum arrnum_backlog_back_focus;
		static arrnum arrnum_backlog_back_click;
		// クリック待ち

		static arrnum arrnum_wait_1;
		static arrnum arrnum_wait_2;
		static arrnum arrnum_wait_3;
		static arrnum arrnum_wait_4;
		static arrnum arrnum_wait_5;
		static arrnum arrnum_wait_6;
		static arrnum arrnum_wait_7;
		static arrnum arrnum_wait_8;
		static arrnum arrnum_wait_9;
		static arrnum arrnum_wait_10;
		static arrnum arrnum_wait_11;
		static arrnum arrnum_wait_12;
		static arrnum arrnum_wait_off;

		void setBranchParent(int num){ branch_parent = num; }
		int  getBranchParent(){return branch_parent;}
		void setBranchChild(int num){ branch_child = num; }
		int  getBranchChild(){ return branch_child; }
	private:
		int system_type;//システムのどの画像か
		int branch_parent;//差分画像の枝番
		int branch_child;
	};

}