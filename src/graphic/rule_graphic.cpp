﻿#include "../graphic.h"
#include "../define.h"
#include "../global.h"
#include "../error_code_define.h"
#include "rule_graphic.h"
#include <Shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

//globals
extern char common_dir[256];
int Teng::RuleGraphic::using_arrnum;
extern bool is_hyper_skip;


Teng::RuleGraphic::RuleGraphic(){
	this->setHandle(0);
	this->setImageType(eGraphicType_rule);
}

Teng::RuleGraphic::~RuleGraphic(){};

void Teng::GraphicManager::loadRuleGraphic(char *rule_file){
	if(rule_file[0] == '\0'){
		DEBUG printf("[ERROR] @rule.load 引数が不足しています\n");
		throw ENGINE_GRAPHIC_RULE_NO_SUCH_ARGUMENT;
	}
	//TODO
	//////開発用
	char fullpath[256];
	strcpy_s(fullpath, 256, common_dir);
	strcat_s(fullpath, 256, TENG_DIR_RULE_IMAGE);
	//	strcat_s(fullpath, 256, rule_file);
	for(int i = 0; i < TENG_GRAPHIC_RULE_MAX; i++){
		if(this->rule_graphics[i].getHandle() == 0){

			//char file_fullname[TENG_FILE_NAME_MAX] = TENG_DIR_RULE_IMAGE;
			strcat_s(fullpath, TENG_FILE_NAME_MAX, rule_file);

			this->rule_graphics[i].setFileName(rule_file);
			this->rule_graphics[i].setX(0);
			this->rule_graphics[i].setY(0);

			if(!is_hyper_skip) this->rule_graphics[i].setHandle(DxLib::LoadBlendGraph(fullpath));
			if(is_hyper_skip) this->rule_graphics[i].setHandle(-2);
			if(this->rule_graphics[i].getHandle() == -1 || PathFileExists(fullpath) == FALSE){
				//DEBUG printf("[ERROR] @rule.load ルール画像の読み込みに失敗しました: %s\n", rule_file);
				//throw ENGINE_GRAPHIC_RULE_GRAPHIC_LOADGRAPH_FAILED;
			}
			break;
		}

	}
}

//ルール画像の解放
void Teng::GraphicManager::freeRuleGraphic(const char *rule_file){
	for(int i = 0; i < TENG_GRAPHIC_RULE_MAX; i++){
		if(strcmp(rule_file, this->rule_graphics[i].getFileName()) == 0){
			DxLib::DeleteGraph(this->rule_graphics[i].getHandle());
			rule_graphics[i].setFileName("");
			rule_graphics[i].setHandle(0);
			return;
		}
	}
	DEBUG printf("[ERROR] @rule.free %sの解放に失敗しました。\n", rule_file);
	throw ENGINE_GRAPHIC_RULE_FREE_FAILURE;
}

void Teng::GraphicManager::freeRuleGraphicAll(){
	DEBUG printf("[INFO] 全ての背景画像を解放します\n");
	for(int i = 0; i < TENG_GRAPHIC_RULE_MAX; i++){
		DxLib::DeleteGraph(this->rule_graphics[i].getHandle());
		rule_graphics[i].setFileName("");
		rule_graphics[i].setHandle(0);

	}
}

void Teng::RuleGraphic::setFileName(char *arg_filename)
{
	strcpy_s(filename, TENG_FILE_NAME_MAX, arg_filename);
}

char* Teng::RuleGraphic::getFileName(){
	return this->filename;
}