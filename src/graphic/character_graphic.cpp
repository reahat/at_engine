﻿#include "../graphic.h"
#include "../define.h"
#include "../global.h"
#include "../error_code_define.h"
#include "character_graphic.h"
#include <memory>
#include <Shlwapi.h>
#pragma comment(lib, "shlwapi.lib")
extern char common_dir[256];
extern bool is_hyper_skip;
extern std::unique_ptr<Teng::Config> config;

Teng::CharacterGraphic::CharacterGraphic(){
	transition_speed = TENG_GRAPHIC_TRANSITION_SPEED_DEFAULT;
	transition_type = TENG_GRAPHIC_TRANSITION_NOEFFECT;
	filename[0] = '\0';
	is_visible = false;
	is_changing = false;
	z = 1;
	trans_power = 0;
	this->setImageType(eGraphicType_character);
}


void Teng::GraphicManager::loadCharacterGraphic(char *filename)
{

	//TODO
	//////開発用
	char fullpath[256];
	strcpy_s(fullpath, 256, common_dir);
	strcat_s(fullpath, 256, TENG_DIR_CHARACTER_IMAGE);
	//strcat_s(fullpath, 256, filename);

	//TODO 引数がNULLの場合のエラー処理
	for(int i = 0; i < TENG_GRAPHIC_CHARACTER_MAX; i++){
		if(this->character_graphics[i].getHandle() == 0){

			//char file_fullname[TENG_FILE_NAME_MAX] = TENG_DIR_CHARACTER_IMAGE;
			strcat_s(fullpath, TENG_FILE_NAME_MAX, filename);

			this->character_graphics[i].setFileName(filename);
			this->character_graphics[i].setX(0);
			this->character_graphics[i].setY(0);
			this->character_graphics[i].setVisible(false);
			this->character_graphics[i].setChanging(false);
			if(!is_hyper_skip) this->character_graphics[i].setHandle(DxLib::LoadGraph(fullpath));
			if(is_hyper_skip) this->character_graphics[i].setHandle(-2);
			if(this->character_graphics[i].getHandle() == -1 || !PathFileExists(fullpath)){
				//DEBUG printf("[ERROR] @char.load 画像の読み込みに失敗しました: %s\n", filename);
				//throw ENGINE_GRAPHIC_CHARACTER_GRAPHIC_LOADGRAPH_FAILED;
			}
			break;
		}
		if( i == TENG_GRAPHIC_CHARACTER_MAX-1 ){
			DEBUG printf("[ERROR] @char.load 画像の読み込み制限数を超えています: %s\n", filename);
			throw ENGINE_GRAPHIC_CHARACTER_GRAPHIC_TOO_MUCH;
		}
	}

}

void Teng::GraphicManager::changeCharacterGraphic(char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER + 1][TENG_SCRIPT_COMMAND_MAX_LENGTH]){

	int i;
	bool exist_flg = false;
	string draw_position;
	int x;
	int tmp_x, tmp_y, y;
	int handle;

	// @char.change left-left, char.png
	if (command[1][0] == NC){
		DEBUG printf("[ERROR] @char.change 表示位置が未指定です。\n");
		throw ENGINE_GRAPHIC_CHARACTER_GRAPHIC_LOCATION_NULL;
	}
	else if (command[2][0] == NC){
		DEBUG printf("[ERROR] @char.change ファイル名が未指定です。\n");
		throw ENGINE_GRAPHIC_CHARACTER_GRAPHIC_FILENAME_NULL;
	}

	// ファイル名
	char filename[TENG_FILE_NAME_MAX];
	strcpy_s(filename, TENG_FILE_NAME_MAX, command[2]);
	// ファイル名に対応するオブジェクトを検索
	for(i = 0; i < TENG_GRAPHIC_CHARACTER_MAX; i++){
		if(character_graphics[i].getHandle() != 0){
			if(character_graphics[i].getFileName() != NULL &&
				strncmp(filename, character_graphics[i].getFileName(), TENG_FILE_NAME_MAX) == 0)
			{
				// 見つかったi番目のものを以後処理
				exist_flg = true;
				handle = character_graphics[i].getHandle();
				DxLib::GetGraphSize(handle, &tmp_x, &tmp_y);
				y = 768 - tmp_y;
				break;
			}
		}
	}
	// 発見できなかった場合
	if(!is_hyper_skip && exist_flg == false){
		DEBUG printf("ERROR 未ロードの画像ファイルを描画しようとしています：%s\n", command[2]);
		throw ENGINE_GRAPHIC_CHARACTER_GRAPHIC_DRAWFILE_YET_LOAD;
	}
	draw_position = string(command[1]);

	if (draw_position == "left-left"){
		character_graphics[i].setX(TENG_GRAPHIC_LOCATION_LEFT_LEFT);
		character_graphics[i].setY(y);
	}
	else if (draw_position == "left"){
		character_graphics[i].setX(TENG_GRAPHIC_LOCATION_LEFT_MIDDLE);
		character_graphics[i].setY(y);
	}
	else if (draw_position == "left-right"){
		character_graphics[i].setX(TENG_GRAPHIC_LOCATION_LEFT_RIGHT);
		character_graphics[i].setY(y);
	}
	else if (draw_position == "center-left"){
		character_graphics[i].setX(TENG_GRAPHIC_LOCATION_CENTER_LEFT);
		character_graphics[i].setY(y);
	}
	else if (draw_position == "center"){
		character_graphics[i].setX(TENG_GRAPHIC_LOCATION_CENTER_MIDDLE);
		character_graphics[i].setY(y);
	}
	else if (draw_position == "center-right"){
		character_graphics[i].setX(TENG_GRAPHIC_LOCATION_CENTER_RIGHT);
		character_graphics[i].setY(y);
	}
	else if (draw_position == "right-left"){
		character_graphics[i].setX(TENG_GRAPHIC_LOCATION_RIGHT_LEFT);
		character_graphics[i].setY(y);
	}
	else if (draw_position == "right"){
		character_graphics[i].setX(TENG_GRAPHIC_LOCATION_RIGHT_MIDDLE);
		character_graphics[i].setY(y);
	}
	else if (draw_position == "right-right"){
		character_graphics[i].setX(TENG_GRAPHIC_LOCATION_RIGHT_RIGHT);
		character_graphics[i].setY(y);
	}
	else{
		DEBUG printf("[ERROR] @char.change 表示位置の指定が間違っています： %s\n", command[1]);
		throw ENGINE_GRAPHIC_CHARACTER_GRAPHIC_INVALID_LOCATION;
	}
	x = character_graphics[i].getX();

	//消すファイルを検索
	int j;
	bool before_exist = false;
	for(j = 0; j < TENG_GRAPHIC_CHARACTER_MAX; j++){
		if (character_graphics[j].getHandle() != 0 &&
			character_graphics[j].getVisible() &&
			character_graphics[j].getFileName() != NULL &&
			character_graphics[j].getX() == x && // 書き換え前後の横位置が同じものを探す
			i != j) // 書き換え前と後は当然ながら別の番号
		{
			// 見つかったj番目のものを以後処理
			before_exist = true;
			break;
		}
	}

	if (!before_exist){
		DEBUG printf("[ERROR] @char.change %sに表示中の画像がない、または同じ画像を再度表示しようとしています\n", draw_position.c_str());
		throw ENGINE_GRAPHIC_CHANGECHAR_LOCATION_NO_DRAWING;
	}

	// Z座標の処理
	character_graphics[i].setZ(character_graphics[j].getZ());
	// トランジション効果の有無
	character_graphics[i].setTransitionType(TENG_GRAPHIC_TRANSITION_FADEIN);
	character_graphics[j].setTransitionType(TENG_GRAPHIC_TRANSITION_FADEOUT);
	character_graphics[i].setTransPower(0);
	character_graphics[j].setTransPower(255);
	transition_doing = true;
	// トランジション速度
	character_graphics[i].setTransitionSpeed(10);
	character_graphics[j].setTransitionSpeed(4);
	// 表示フラグ
	character_graphics[i].setVisible(true);

	is_character_crossfading = true;
	if (is_hyper_skip){
		is_character_crossfading = false;
		character_graphics[i].setChanging(false);
		character_graphics[i].setTransitionType(TENG_GRAPHIC_TRANSITION_NOEFFECT);
		character_graphics[i].setVisible(true);
		character_graphics[j].setChanging(false);
		character_graphics[j].setTransitionType(TENG_GRAPHIC_TRANSITION_NOEFFECT);
		character_graphics[j].setVisible(false);
	}
}





bool Teng::GraphicManager::setCharacterGraphic(char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER+1][TENG_SCRIPT_COMMAND_MAX_LENGTH])
{
	//            1          2     3         4                 5
	// @drawimage image.png, left, 1(Z座標), true(transition), speed
	char filename[TENG_FILE_NAME_MAX];
	int x, y, z;
	int transition;
	int speed;
	int i;
	bool exist_flg = false;
	int handle = -1;

	if(command[1][0] == NULL){
		DEBUG printf("[ERROR] @char.draw ファイル名が指定されていません\n");
		throw ENGINE_GRAPHIC_CHARACTER_GRAPHIC_FILENAME_NULL;
	}
	
	// [1] ファイル名
	strcpy_s(filename, TENG_FILE_NAME_MAX, command[1]);
	// ファイル名に対応するオブジェクトを検索
	for(i = 0; i < TENG_GRAPHIC_CHARACTER_MAX; i++){
		if(this->character_graphics[i].getHandle() != 0){
			if(this->character_graphics[i].getFileName() != NULL &&
				strncmp(filename, this->character_graphics[i].getFileName(), TENG_FILE_NAME_MAX) == 0)
			{
				// 見つかったi番目のものを以後処理
				exist_flg = true;
				handle = character_graphics[i].getHandle();
				DxLib::GetGraphSize(handle, &x, &y);
				y = 768 - y;
				break;
			}
		}
	}
	// 発見できなかった場合
	if(!is_hyper_skip && exist_flg == false){
		DEBUG printf("ERROR 未ロードの画像ファイルを描画しようとしています：%s\n", command[1]);
		throw ENGINE_GRAPHIC_CHARACTER_GRAPHIC_DRAWFILE_YET_LOAD;
	}
	// [2] 表示位置
	// 画像位置が未設定の場合
	if(command[2] == NULL){
		DEBUG printf("ERROR 画像の表示位置を指定してください：@drawchar %s\n", command[1]);
		throw ENGINE_GRAPHIC_CHARACTER_GRAPHIC_LOCATION_NULL;
	}
	//Y座標は、もともと TENG_GRAPHIC_LOCATION_Yだった
	if(strncmp(command[2], "left-left", TENG_SCRIPT_COMMAND_MAX_LENGTH) == 0){
		this->character_graphics[i].setX(TENG_GRAPHIC_LOCATION_LEFT_LEFT);
		this->character_graphics[i].setY(y);
	}else if(strncmp(command[2], "left", TENG_SCRIPT_COMMAND_MAX_LENGTH) == 0){
		this->character_graphics[i].setX(TENG_GRAPHIC_LOCATION_LEFT_MIDDLE);
		this->character_graphics[i].setY(y);
	}else if(strncmp(command[2], "left-right", TENG_SCRIPT_COMMAND_MAX_LENGTH) == 0){
		this->character_graphics[i].setX(TENG_GRAPHIC_LOCATION_LEFT_RIGHT);
		this->character_graphics[i].setY(y);
	}else if(strncmp(command[2], "center-left", TENG_SCRIPT_COMMAND_MAX_LENGTH) == 0){
		this->character_graphics[i].setX(TENG_GRAPHIC_LOCATION_CENTER_LEFT);
		this->character_graphics[i].setY(y);
	}else if(strncmp(command[2], "center", TENG_SCRIPT_COMMAND_MAX_LENGTH) == 0){
		this->character_graphics[i].setX(TENG_GRAPHIC_LOCATION_CENTER_MIDDLE);
		this->character_graphics[i].setY(y);
	}else if(strncmp(command[2], "center-right", TENG_SCRIPT_COMMAND_MAX_LENGTH) == 0){
		this->character_graphics[i].setX(TENG_GRAPHIC_LOCATION_CENTER_RIGHT);
		this->character_graphics[i].setY(y);
	}else if(strncmp(command[2], "right-left", TENG_SCRIPT_COMMAND_MAX_LENGTH) == 0){
		this->character_graphics[i].setX(TENG_GRAPHIC_LOCATION_RIGHT_LEFT);
		this->character_graphics[i].setY(y);
	}else if(strncmp(command[2], "right", TENG_SCRIPT_COMMAND_MAX_LENGTH) == 0){
		this->character_graphics[i].setX(TENG_GRAPHIC_LOCATION_RIGHT_MIDDLE);
		this->character_graphics[i].setY(y);
	}else if(strncmp(command[2], "right-right", TENG_SCRIPT_COMMAND_MAX_LENGTH) == 0){
		this->character_graphics[i].setX(TENG_GRAPHIC_LOCATION_RIGHT_RIGHT);
		this->character_graphics[i].setY(y);
	}else{
		DEBUG printf("ERROR 表示位置の指定が間違っています：@drawchar %s\n", command[1]);
		throw ENGINE_GRAPHIC_CHARACTER_GRAPHIC_INVALID_LOCATION;
	}

	// [3] Z座標の処理
	if(command[3] != NULL){
		this->character_graphics[i].setZ(atoi(command[3]));
	}else{
		this->character_graphics[i].setZ(1);
	}
	// [4] トランジション効果の有無
	if(command[4] != NULL){
		if(strncmp(command[4], "none", TENG_SCRIPT_COMMAND_MAX_LENGTH) == 0){
			this->character_graphics[i].setTransitionType(TENG_GRAPHIC_TRANSITION_NOEFFECT);
			this->character_graphics[i].setTransPower(255);
		}else if(strncmp(command[4], "fadein", TENG_SCRIPT_COMMAND_MAX_LENGTH) == 0){
			this->character_graphics[i].setTransitionType(TENG_GRAPHIC_TRANSITION_FADEIN);
			this->character_graphics[i].setTransPower(0);
			transition_doing = true;
		}else{
			this->character_graphics[i].setTransitionType(TENG_GRAPHIC_TRANSITION_FADEIN);
			this->character_graphics[i].setTransPower(0);
			transition_doing = true;
		}
	}else{
		// デフォルトはフェード
		this->character_graphics[i].setTransitionType(TENG_GRAPHIC_TRANSITION_FADEIN);
		this->character_graphics[i].setTransPower(0);
		transition_doing = true;
	}
	// [5] トランジション速度
	if(command[5] != NULL){
		this->character_graphics[i].setTransitionSpeed(atoi(command[5]));
		if(this->character_graphics[i].getTransitionSpeed() == 0){
			this->character_graphics[i].setTransitionSpeed(TENG_GRAPHIC_TRANSITION_SPEED_DEFAULT);
		}
	}else{
		this->character_graphics[i].setTransitionSpeed(TENG_GRAPHIC_TRANSITION_SPEED_DEFAULT);
	}
	// 表示フラグ
	this->character_graphics[i].setVisible(true);

	if (is_hyper_skip){
		character_graphics[i].setChanging(false);
		character_graphics[i].setHandle(-2);
		character_graphics[i].setTransitionType(TENG_GRAPHIC_TRANSITION_NOEFFECT);
	}

	// エフェクトnoneで次のタグが自動実行しない問題対策
	return !transition_doing;
}


//キャラクターグラフィックの描画
void Teng::GraphicManager::drawCharacterGraphic(Teng::Input* input){
	if(!is_hyper_skip){

		//頭から詰めていく。handleに0が見つかった時点で終わり
		for(int i = 0; i < TENG_GRAPHIC_CHARACTER_MAX; i++){
			if(this->character_graphics[i].getHandle() != NULL
				&& this->character_graphics[i].getHandle() > 0)
			{
				if(this->character_graphics[i].getVisible()){

					// フェードイン
					if (this->character_graphics[i].getTransitionType() == TENG_GRAPHIC_TRANSITION_FADEIN){
						// Ctrlスキップ中は即表示
						if (input->getKeyControl() != 0 || config->getSkipModeFlag()){
							transition_doing = false;
							this->character_graphics[i].setTransPower(255);
							character_fade_completed_now = true;
							is_character_crossfading = false;
						}

						// トラン中のとき（power == 255 のとき完全に表示）
						if (this->character_graphics[i].getTransPower() < 255){
							// トラン中にクリックしたら完全表示
							if (input->getKeepMouseLeft() != 0 && this->character_graphics[i].getTransPower() > 0){
								transition_doing = false;
								this->character_graphics[i].setTransPower(255);
								character_fade_completed_now = false;
								DxLib::DrawGraph(
									this->character_graphics[i].getX(),
									this->character_graphics[i].getY(),
									this->character_graphics[i].getHandle(),
									TRUE);
								continue;
							}
							this->character_graphics[i].setTransPower(this->character_graphics[i].getTransPower() + this->character_graphics[i].getTransitionSpeed());
							DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, this->character_graphics[i].getTransPower());
							if (character_graphics[i].getTransPower() >= 255){
								// @char.changeの場合は消去より表示完了の方が早い
								// フェードイン（表示）側では完了時に自動クリックせず、フェードアウト完了時のみクリックさせる

								bool auto_next = true;
								//表示完了の時点で、フェードアウト中の画像があればクリックはしない
								for (int c = 0; c < TENG_GRAPHIC_CHARACTER_MAX; c++){
									if (character_graphics[c].getHandle() > 0 &&
										character_graphics[c].getVisible() &&
										c != i &&
										character_graphics[c].getTransPower() > 0 &&
										character_graphics[c].getTransPower() < 255){
										auto_next = false;
									}
								}
								if (auto_next){
									character_fade_completed_now = true;
								}
							}
						}
						else{
							//if(!is_character_crossfading) 
								transition_doing = false;
							DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
						}
					}

					// フェードアウト
					if (this->character_graphics[i].getTransitionType() == TENG_GRAPHIC_TRANSITION_FADEOUT){
						// Ctrlスキップ中は即消去
						if (input->getKeyControl() != 0 || config->getSkipModeFlag()){
							transition_doing = false;
							this->character_graphics[i].setTransPower(0);
							this->character_graphics[i].setVisible(false);
							character_fade_completed_now = true;
						}

						// トラン中のとき（power == 255 のとき完全に表示）
						if (this->character_graphics[i].getTransPower() > 0){
							// トラン中にクリックしたら完全消去
							if (input->getKeepMouseLeft() != 0 && this->character_graphics[i].getTransPower() < 255){
								transition_doing = false;
								character_fade_completed_now = false;
								this->character_graphics[i].setTransPower(0);
								this->character_graphics[i].setVisible(false);
								
								DxLib::DrawGraph(
									this->character_graphics[i].getX(),
									this->character_graphics[i].getY(),
									this->character_graphics[i].getHandle(),
									TRUE);
								continue;
							}
							this->character_graphics[i].setTransPower(this->character_graphics[i].getTransPower() - this->character_graphics[i].getTransitionSpeed());
							DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, this->character_graphics[i].getTransPower());
							if (character_graphics[i].getTransPower() <= 0){
								character_fade_completed_now = true;
								// @char.changeの場合は消去より表示完了の方が早い
								// フェードイン（表示）側では完了時に自動クリックせず、フェードアウト完了時のみ自動で進める
								is_character_crossfading = false;
							}
						}
						// フェードアウト完了時
						else{
							this->character_graphics[i].setVisible(false);
							transition_doing = false;
							//character_fade_completed_now = true;
							DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
						}
					}
					if (this->character_graphics[i].getVisible()){
						DxLib::DrawGraph(
							this->character_graphics[i].getX(),
							this->character_graphics[i].getY(),
							this->character_graphics[i].getHandle(),
							TRUE);
					}
					DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
				}
			}
		}
	}
}




bool Teng::CharacterGraphic::getVisible(){
	return is_visible;
}

void Teng::CharacterGraphic::setVisible(bool visible){
	this->is_visible = visible;
}

bool Teng::CharacterGraphic::getChanging(){
	return is_changing;
}

void Teng::CharacterGraphic::setChanging(bool changing){
	this->is_changing = changing;
}

void Teng::CharacterGraphic::setFileName(char *arg_filename)
{
	strcpy_s(filename, TENG_FILE_NAME_MAX, arg_filename);
}

char* Teng::CharacterGraphic::getFileName(){
	return this->filename;
}

void Teng::CharacterGraphic::setTransitionType(int type){
	this->transition_type = type;
}
int Teng::CharacterGraphic::getTransitionType(){
	return transition_type;
}
void Teng::CharacterGraphic::setTransitionSpeed(int speed){
	this->transition_speed = speed;
}
int Teng::CharacterGraphic::getTransitionSpeed(){
	return transition_speed;
}
void Teng::CharacterGraphic::setZ(int z){
	this->z = z;
}
int Teng::CharacterGraphic::getZ(){
	return z;
}