﻿#pragma once
#include "../define.h"
#include "../graphic.h"

namespace Teng{
	class RuleGraphic : public Graphic{
	public:

		RuleGraphic();
		~RuleGraphic();
		void setFileName(char *filename);
		char* getFileName();

		static int using_arrnum;
	private:
		char filename[TENG_FILE_NAME_MAX];//画像ファイル名

	};

}