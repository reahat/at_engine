﻿#pragma once
#include "../define.h"
#include "../graphic.h"
//#include "../scene.h"

namespace Teng{

	class BackGroundGraphic : public Graphic{
	public:
		enum BgGraphicType{
			eBgGraphic_before,//トラン用 切り替え前の画像
			eBgGraphic_after  //トラン用 切り替え後の画像
		};
		BackGroundGraphic();
		void setFileName(char *filename);
		void setVisible(bool visible);
		bool getVisible();
		void setChanging(bool changing);
		bool getChanging();
		char* getFileName();
		void setTransitionType(int type);
		int  getTransitionType();
		void setTransitionSpeed(int speed);
		int  getTransitionSpeed();
		void setUseRule(bool rule);
		bool getUseRule();
		void setRuleFile(char *rule);
		char* getRuleFile();
		void setTransitionThreshold(int threshold);
		int  getTransitionThreshold();
		void setIsAfterTrans(bool after);
		bool getIsAfterTrans();
		void setRuleHandle(int handle);
		int  getRuleHandle();
		void setFrame(short frame);
		short getFrame();

		//描画時のループ省略のために使用
		static bool changed;

		//トラン用
		static int before_trans_arrnum;
		static int after_trans_arrnum;
		static short frame;

	private:
		char filename[TENG_FILE_NAME_MAX];//画像ファイル名
		bool is_visible;//使用中かどうか
		bool is_changing;//画像切替中かどうか
		int transition_type;//トランジションエフェクトの書類
		int transition_speed;//トランジション速度

		//ルール関係
		bool use_rule;//ルール使用する場合。トラン完了後はfalseに戻す
		int blend_power;//ルール画像との合成率（ループで0-255まで変化）
		int blend_threshold;//ルール合成のしきい値
		char rule_file[TENG_FILE_NAME_MAX];//ルールファイル名
		bool is_after_trans;
		int rule_handle;//使用するルール画像のハンドル

	};

}