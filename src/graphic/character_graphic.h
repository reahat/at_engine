﻿#pragma once
#include "../define.h"
#include "../graphic.h"

namespace Teng{

	class CharacterGraphic : public Graphic{
	public:
		CharacterGraphic();
		void setFileName(char *filename);
		void setVisible(bool visible);
		bool getVisible();
		void setChanging(bool changing);
		bool getChanging();
		char* getFileName();
		void setTransitionType(int type);
		int  getTransitionType();
		void setTransitionSpeed(int speed);
		int  getTransitionSpeed();
		void setZ(int z);
		int  getZ();
		void setTransPower(int alpha){ trans_power = alpha; }
		int  getTransPower(){ return trans_power; }
	private:
		char filename[TENG_FILE_NAME_MAX];//画像ファイル名
		bool is_visible;//使用中かどうか
		bool is_changing;//画像切替中かどうか
		int  transition_type;//トランジションエフェクトの書類
		int  transition_speed;//トランジション速度
		int  z;//z座標
		int  trans_power;//トラン合成のアルファ値
	};

}