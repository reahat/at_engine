﻿#include "../graphic.h"
#include "../define.h"
#include "../global.h"
#include "../error_code_define.h"
#include "system_graphic.h"


arrnum Teng::SystemGraphic::arrnum_bg_load_1;
arrnum Teng::SystemGraphic::arrnum_bg_save_1;
arrnum Teng::SystemGraphic::arrnum_bg_logo_1;
arrnum Teng::SystemGraphic::arrnum_bg_white;
arrnum Teng::SystemGraphic::arrnum_bg_blank;
arrnum Teng::SystemGraphic::arrnum_bg_title;
arrnum Teng::SystemGraphic::arrnum_bg_config;
arrnum Teng::SystemGraphic::arrnum_dialog_base;
arrnum Teng::SystemGraphic::arrnum_dialog_yes_normal;
arrnum Teng::SystemGraphic::arrnum_dialog_yes_click;
arrnum Teng::SystemGraphic::arrnum_dialog_no_normal;
arrnum Teng::SystemGraphic::arrnum_dialog_no_click;
// メッセージボックス
arrnum Teng::SystemGraphic::arrnum_textbox_base;
arrnum Teng::SystemGraphic::arrnum_textbox_config;
arrnum Teng::SystemGraphic::arrnum_textbox_config_on;
arrnum Teng::SystemGraphic::arrnum_textbox_hskip;
arrnum Teng::SystemGraphic::arrnum_textbox_hskip_on;
arrnum Teng::SystemGraphic::arrnum_textbox_qsave;
arrnum Teng::SystemGraphic::arrnum_textbox_qsave_on;
arrnum Teng::SystemGraphic::arrnum_textbox_qload;
arrnum Teng::SystemGraphic::arrnum_textbox_qload_on;
arrnum Teng::SystemGraphic::arrnum_textbox_skip;
arrnum Teng::SystemGraphic::arrnum_textbox_skip_on;
arrnum Teng::SystemGraphic::arrnum_textbox_x;
arrnum Teng::SystemGraphic::arrnum_textbox_x_on;
arrnum Teng::SystemGraphic::arrnum_textbox_auto;
arrnum Teng::SystemGraphic::arrnum_textbox_auto_on;

// バックログ
arrnum Teng::SystemGraphic::arrnum_backlog_base;
arrnum Teng::SystemGraphic::arrnum_backlog_base_bar;
arrnum Teng::SystemGraphic::arrnum_backlog_bar_normal;
arrnum Teng::SystemGraphic::arrnum_backlog_bar_focus;
arrnum Teng::SystemGraphic::arrnum_backlog_bar_click;
arrnum Teng::SystemGraphic::arrnum_backlog_down_normal;
arrnum Teng::SystemGraphic::arrnum_backlog_down_focus;
arrnum Teng::SystemGraphic::arrnum_backlog_down_click;
arrnum Teng::SystemGraphic::arrnum_backlog_up_normal;
arrnum Teng::SystemGraphic::arrnum_backlog_up_focus;
arrnum Teng::SystemGraphic::arrnum_backlog_up_click;
arrnum Teng::SystemGraphic::arrnum_backlog_back_normal;
arrnum Teng::SystemGraphic::arrnum_backlog_back_focus;
arrnum Teng::SystemGraphic::arrnum_backlog_back_click;
// クリック待ち
arrnum Teng::SystemGraphic::arrnum_wait_1;
arrnum Teng::SystemGraphic::arrnum_wait_2;
arrnum Teng::SystemGraphic::arrnum_wait_3;
arrnum Teng::SystemGraphic::arrnum_wait_4;
arrnum Teng::SystemGraphic::arrnum_wait_5;
arrnum Teng::SystemGraphic::arrnum_wait_6;
arrnum Teng::SystemGraphic::arrnum_wait_7;
arrnum Teng::SystemGraphic::arrnum_wait_8;
arrnum Teng::SystemGraphic::arrnum_wait_9;
arrnum Teng::SystemGraphic::arrnum_wait_10;
arrnum Teng::SystemGraphic::arrnum_wait_11;
arrnum Teng::SystemGraphic::arrnum_wait_12;
arrnum Teng::SystemGraphic::arrnum_wait_off;


arrnum Teng::SystemGraphic::arrnum_select_normal;
arrnum Teng::SystemGraphic::arrnum_select_focus;
arrnum Teng::SystemGraphic::arrnum_select_selected;
arrnum Teng::SystemGraphic::arrnum_message_arrow;
arrnum Teng::SystemGraphic::arrnum_bg_gallery;
arrnum Teng::SystemGraphic::gallery_pic_small[TENG_GALLERY_MAX_NUMBER];
arrnum Teng::SystemGraphic::gallery_pic_large[TENG_GALLERY_MAX_NUMBER];
arrnum Teng::SystemGraphic::arrnum_gallery_rule_slide;
//タイトル画面
arrnum Teng::SystemGraphic::arrnum_button_title_start_normal;
arrnum Teng::SystemGraphic::arrnum_button_title_start_focus;
arrnum Teng::SystemGraphic::arrnum_button_title_start_click;
arrnum Teng::SystemGraphic::arrnum_button_title_load_normal;
arrnum Teng::SystemGraphic::arrnum_button_title_load_focus;
arrnum Teng::SystemGraphic::arrnum_button_title_load_click;
arrnum Teng::SystemGraphic::arrnum_button_title_config_normal;
arrnum Teng::SystemGraphic::arrnum_button_title_config_focus;
arrnum Teng::SystemGraphic::arrnum_button_title_config_click;
arrnum Teng::SystemGraphic::arrnum_button_title_gallery_normal;
arrnum Teng::SystemGraphic::arrnum_button_title_gallery_focus;
arrnum Teng::SystemGraphic::arrnum_button_title_gallery_click;
arrnum Teng::SystemGraphic::arrnum_button_title_exit_normal;
arrnum Teng::SystemGraphic::arrnum_button_title_exit_focus;
arrnum Teng::SystemGraphic::arrnum_button_title_exit_click;
arrnum Teng::SystemGraphic::arrnum_button_title_kago_normal;
arrnum Teng::SystemGraphic::arrnum_button_title_kago_focus;
//セーブロード画面
arrnum Teng::SystemGraphic::arrnum_button_save_page1_normal;
arrnum Teng::SystemGraphic::arrnum_button_save_page1_focus;
arrnum Teng::SystemGraphic::arrnum_button_save_page1_click;
arrnum Teng::SystemGraphic::arrnum_button_save_page2_normal;
arrnum Teng::SystemGraphic::arrnum_button_save_page2_focus;
arrnum Teng::SystemGraphic::arrnum_button_save_page2_click;
arrnum Teng::SystemGraphic::arrnum_button_save_page3_normal;
arrnum Teng::SystemGraphic::arrnum_button_save_page3_focus;
arrnum Teng::SystemGraphic::arrnum_button_save_page3_click;
arrnum Teng::SystemGraphic::arrnum_button_save_page4_normal;
arrnum Teng::SystemGraphic::arrnum_button_save_page4_focus;
arrnum Teng::SystemGraphic::arrnum_button_save_page4_click;
arrnum Teng::SystemGraphic::arrnum_button_save_page5_normal;
arrnum Teng::SystemGraphic::arrnum_button_save_page5_focus;
arrnum Teng::SystemGraphic::arrnum_button_save_page5_click;
arrnum Teng::SystemGraphic::arrnum_button_save_back_normal;
arrnum Teng::SystemGraphic::arrnum_button_save_back_focus;
arrnum Teng::SystemGraphic::arrnum_button_save_back_click;
arrnum Teng::SystemGraphic::arrnum_button_save_icon_bara;
arrnum Teng::SystemGraphic::arrnum_button_save_icon_arrow;
arrnum Teng::SystemGraphic::arrnum_button_save_thumbnail_arrow;
arrnum Teng::SystemGraphic::arrnum_rule_save_confirm_dialog;
arrnum Teng::SystemGraphic::arrnum_rule_save_confirm_dialog_reverse;

// コンフィグ画面
arrnum Teng::SystemGraphic::arrnum_config_button_all_normal;
arrnum Teng::SystemGraphic::arrnum_config_button_all_focus;
arrnum Teng::SystemGraphic::arrnum_config_button_all_click;
arrnum Teng::SystemGraphic::arrnum_config_button_already_normal;
arrnum Teng::SystemGraphic::arrnum_config_button_already_focus;
arrnum Teng::SystemGraphic::arrnum_config_button_already_click;
arrnum Teng::SystemGraphic::arrnum_config_button_minus_normal;
arrnum Teng::SystemGraphic::arrnum_config_button_minus_focus;
arrnum Teng::SystemGraphic::arrnum_config_button_minus_click;
arrnum Teng::SystemGraphic::arrnum_config_button_plus_normal;
arrnum Teng::SystemGraphic::arrnum_config_button_plus_focus;
arrnum Teng::SystemGraphic::arrnum_config_button_plus_click;
arrnum Teng::SystemGraphic::arrnum_config_button_bar_normal;
arrnum Teng::SystemGraphic::arrnum_config_button_bar_focus;
arrnum Teng::SystemGraphic::arrnum_config_button_bar_click;
arrnum Teng::SystemGraphic::arrnum_config_button_back_normal;
arrnum Teng::SystemGraphic::arrnum_config_button_back_focus;
arrnum Teng::SystemGraphic::arrnum_config_button_back_click;
arrnum Teng::SystemGraphic::arrnum_config_bar_base;

// 右クリックメニュー
arrnum Teng::SystemGraphic::arrnum_menu_save_normal;
arrnum Teng::SystemGraphic::arrnum_menu_load_normal;
arrnum Teng::SystemGraphic::arrnum_menu_hskip_normal;
arrnum Teng::SystemGraphic::arrnum_menu_config_normal;
arrnum Teng::SystemGraphic::arrnum_menu_backlog_normal;
arrnum Teng::SystemGraphic::arrnum_menu_title_normal;
arrnum Teng::SystemGraphic::arrnum_menu_exit_normal;
arrnum Teng::SystemGraphic::arrnum_menu_back_normal;
arrnum Teng::SystemGraphic::arrnum_menu_save_focus;
arrnum Teng::SystemGraphic::arrnum_menu_load_focus;
arrnum Teng::SystemGraphic::arrnum_menu_hskip_focus;
arrnum Teng::SystemGraphic::arrnum_menu_config_focus;
arrnum Teng::SystemGraphic::arrnum_menu_backlog_focus;
arrnum Teng::SystemGraphic::arrnum_menu_title_focus;
arrnum Teng::SystemGraphic::arrnum_menu_exit_focus;
arrnum Teng::SystemGraphic::arrnum_menu_back_focus;
arrnum Teng::SystemGraphic::arrnum_menu_save_click;
arrnum Teng::SystemGraphic::arrnum_menu_load_click;
arrnum Teng::SystemGraphic::arrnum_menu_hskip_click;
arrnum Teng::SystemGraphic::arrnum_menu_config_click;
arrnum Teng::SystemGraphic::arrnum_menu_backlog_click;
arrnum Teng::SystemGraphic::arrnum_menu_title_click;
arrnum Teng::SystemGraphic::arrnum_menu_exit_click;
arrnum Teng::SystemGraphic::arrnum_menu_back_click;
arrnum Teng::SystemGraphic::arrnum_menu_base;

// 名前入力
arrnum Teng::SystemGraphic::arrnum_name_entry_box;
arrnum Teng::SystemGraphic::arrnum_name_entry_init_normal;
arrnum Teng::SystemGraphic::arrnum_name_entry_init_click;
arrnum Teng::SystemGraphic::arrnum_name_entry_decision_normal;
arrnum Teng::SystemGraphic::arrnum_name_entry_decision_click;

Teng::SystemGraphic::SystemGraphic(){
	system_type = eGraphicSys_none;
	branch_parent = 0;
	branch_child = 0;
}

void Teng::SystemGraphic::setSystemType(int type){
	system_type = type;
}
