﻿#include "../graphic.h"
#include "../define.h"
#include "../global.h"
#include "../error_code_define.h"
#include "background_graphic.h"
#include "../scene.h"
#include <Shlwapi.h>
#pragma comment(lib, "shlwapi.lib")
#include <memory>
extern char common_dir[256];
extern bool is_hyper_skip;
extern std::unique_ptr<Teng::Config> config;

//static変数定義
bool Teng::BackGroundGraphic::changed = false;//描画時のループ省略のために使用
//TODO ↑いらない気がする
int Teng::BackGroundGraphic::before_trans_arrnum;
int Teng::BackGroundGraphic::after_trans_arrnum;
short Teng::BackGroundGraphic::frame;//0-255


Teng::BackGroundGraphic::BackGroundGraphic(){
	transition_speed = 1;
	transition_type = TENG_GRAPHIC_TRANSITION_NOEFFECT;
	filename[0] = NC;
	rule_file[0] = NC;
	is_visible = false;
	is_changing = false;
	changed = false;
	blend_power = 0;
	use_rule = false;
	this->setImageType(eGraphicType_background);
	blend_threshold = 128;
	is_after_trans = false;
	rule_handle = 0;
	frame = 0;
}


void Teng::GraphicManager::loadBackGroundGraphic(char *filename)
{

	//TODO
	//////開発用
	char fullpath[256];
	strcpy_s(fullpath, 256, common_dir);
	strcat_s(fullpath, 256, TENG_DIR_BACKGROUND_IMAGE);
	//strcat_s(fullpath, 256, filename);

	for(int i = 0; i < TENG_GRAPHIC_BACKGROUND_MAX; i++){
		if(this->background_graphics[i].getHandle() == 0){

			//char file_fullname[TENG_FILE_NAME_MAX] = TENG_DIR_BACKGROUND_IMAGE;
			strcat_s(fullpath, TENG_FILE_NAME_MAX, filename);

			this->background_graphics[i].setFileName(filename);
			this->background_graphics[i].setX(0);
			this->background_graphics[i].setY(0);
			this->background_graphics[i].setVisible(false);
			this->background_graphics[i].setChanging(false);
			if (!is_hyper_skip){
				this->background_graphics[i].setHandle(DxLib::LoadGraph(fullpath));
			}
			if(is_hyper_skip) this->background_graphics[i].setHandle(-2);
			if((!is_hyper_skip && this->background_graphics[i].getHandle() == -1) || !PathFileExists(fullpath)){
				//DEBUG printf("[ERROR] @bg.load 背景画像の読み込みに失敗しました: %s\n", filename);
				//throw ENGINE_GRAPHIC_BACKGROUND_LOADGRAPH_FAILED;
			}
			break;
		}
		if( i == TENG_GRAPHIC_BACKGROUND_MAX-1 ){
			DEBUG printf("[ERROR] @bg.load 背景画像の読み込み制限数を超えています: %s\n", filename);
			throw ENGINE_GRAPHIC_BACKGROUND_TOO_MUCH;
		}
	}

}

arrnum Teng::GraphicManager::loadBackGroundGraphicSystem(char *filename, int type)
{

	//TODO
	//////開発用
	char fullpath[256];
	strcpy_s(fullpath, 256, common_dir);
	strcat_s(fullpath, 256, TENG_DIR_BACKGROUND_IMAGE);
	//strcat_s(fullpath, 256, filename);

	for(int i = 0; i < TENG_GRAPHIC_BACKGROUND_MAX; i++){
		if(this->background_graphics[i].getHandle() == 0){

			//char file_fullname[TENG_FILE_NAME_MAX] = TENG_DIR_BACKGROUND_IMAGE;
			strcat_s(fullpath, TENG_FILE_NAME_MAX, filename);

			this->background_graphics[i].setFileName(filename);
			this->background_graphics[i].setX(0);
			this->background_graphics[i].setY(0);
			this->background_graphics[i].setVisible(false);
			this->background_graphics[i].setChanging(false);
			// ↓　ここがloadBackGroundGraphicと違う
			this->background_graphics[i].setImageType(Teng::Scene::eScene_title);
			if (!is_hyper_skip){
				this->background_graphics[i].setHandle(DxLib::LoadGraph(fullpath));
			}
			if(is_hyper_skip) this->background_graphics[i].setHandle(-2);
			if(this->background_graphics[i].getHandle() == -1){
				DEBUG printf("[ERROR] 背景画像の読み込みに失敗しました: %s\n", filename);
				throw ENGINE_GRAPHIC_BACKGROUND_LOADGRAPH_FAILED;
			}
			return i;
		}
		if( i == TENG_GRAPHIC_BACKGROUND_MAX-1 ){
			DEBUG printf("[ERROR] 背景画像の読み込み制限数を超えています: %s\n", filename);
			throw ENGINE_GRAPHIC_BACKGROUND_TOO_MUCH;
		}
	}

}

void Teng::GraphicManager::setBackGroundGraphic(char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER+1][TENG_SCRIPT_COMMAND_MAX_LENGTH], Config* config)
{
	// @bg.draw image.png, type, speed, rule.jpg, threshold
	char bgfile[TENG_FILE_NAME_MAX];
	char rulefile[TENG_FILE_NAME_MAX];

	int transition;
	int speed;
	int i;
	bool exist_flg = false;

	if(command[1][0] == NC){
		DEBUG printf("ERROR ファイル名が指定されていません(@bg.draw)\n");
		throw ENGINE_GRAPHIC_BACKGROUND_FILENAME_NULL;
	}

	//まず最初に、after画像番号をbefore画像番号にする
	Teng::BackGroundGraphic::before_trans_arrnum = Teng::BackGroundGraphic::after_trans_arrnum;

	// [1] ファイル名
	strcpy_s(bgfile, TENG_FILE_NAME_MAX, command[1]);
	// ファイル名に対応するオブジェクトを検索
	for(i = 0; i < TENG_GRAPHIC_BACKGROUND_MAX; i++){
		if(is_hyper_skip || this->background_graphics[i].getHandle() != 0){
			if(this->background_graphics[i].getFileName() != NULL &&
				strncmp(bgfile, this->background_graphics[i].getFileName(), TENG_FILE_NAME_MAX) == 0)
			{
				// 見つかったi番目のものを以後処理
				exist_flg = true;
				Teng::BackGroundGraphic::changed = true;
				Teng::BackGroundGraphic::after_trans_arrnum = i;
				break;
			}
		}
	}
	// 発見できなかった場合
	if(!is_hyper_skip && exist_flg == false){
		DEBUG printf("ERROR 未ロードの画像ファイルを描画しようとしています：%s\n", command[1]);
		throw ENGINE_GRAPHIC_BACKGROUND_DRAWFILE_YET_LOAD;
	}

	this->background_graphics[i].setX(0);
	this->background_graphics[i].setY(0);


	// [2] トランジションの種類
	if(command[2] != NULL){
		if(strncmp(command[2], "crossfade", TENG_SCRIPT_COMMAND_MAX_LENGTH) == 0){
			this->background_graphics[i].setUseRule(false);
			this->background_graphics[i].setTransitionType(TENG_GRAPHIC_TRANSITION_CROSSFADE);
			//トラン対象にする
			this->background_graphics[i].setChanging(true);
			transition_doing = true;
			DEBUG printf("@bg.draw set crossfade\n");
		}else if(strncmp(command[2], "rule", TENG_SCRIPT_COMMAND_MAX_LENGTH) == 0){
			this->background_graphics[i].setTransitionType(TENG_GRAPHIC_TRANSITION_RULE);
			//トラン対象にする
			this->background_graphics[i].setChanging(true);
			transition_doing = true;
			DEBUG printf("@bg.draw set use-rule\n");
		}else{
			this->background_graphics[i].setTransitionType(TENG_GRAPHIC_TRANSITION_NOEFFECT);
			DEBUG printf("@bg.draw set noeffect\n");
		}
	}

	// [3] トランジション速度
	if(command[3][0] != NC){
		this->background_graphics[i].setTransitionSpeed(atoi(command[3]));
		if(this->background_graphics[i].getTransitionSpeed() <= 0){
			DEBUG printf("@bg.draw トランジション速度に0以下の値は指定できません。デフォルト値を使用します。\n");
			this->background_graphics[i].setTransitionSpeed(TENG_GRAPHIC_TRANSITION_SPEED_DEFAULT);
		}
	}else{
		this->background_graphics[i].setTransitionSpeed(TENG_GRAPHIC_TRANSITION_SPEED_DEFAULT);
	}

	// [4] ルール画像
	if(command[4][0] != NC){
		this->background_graphics[i].setUseRule(true);
		this->background_graphics[i].setRuleFile(command[4]);


		//引数のルール画像名に合致するルール画像を検索
		bool flg = false;
		for(int r = 0; r < TENG_GRAPHIC_RULE_MAX; r++){
			if(strcmp(this->rule_graphics[r].getFileName(), command[4]) == NULL){
				//TODO いるのかな…？
				this->background_graphics[i].setRuleHandle(this->rule_graphics[r].getHandle());
				Teng::RuleGraphic::using_arrnum = r;
				flg = true;
				break;
			}
		}
		//存在しないルール画像を使用しようとした
		if(!is_hyper_skip && !flg){
			DEBUG printf("ロードしていないルール画像を使用しようとしました。\n");
			throw ENGINE_GRAPHIC_BACKGROUND_NO_SUCH_RULE_FILE;
		}


		// 引数不足
		//if(command[4] == NULL) throw ENGINE_GRAPHIC_BACKGROUND_NO_SUCH_ARGUMENT;

		//既に表示中の画像をtrans-before画像にする
		//今読み込んだ画像をtrans-after画像にする
		//this->background_graphics[i].setIsAfterTrans(true);
		//トランが完了したらtrans-beforeにする必要がある
	}

	//ルール使用でファイル名を指定しなかった場合
	if(this->background_graphics[i].getTransitionType() == TENG_GRAPHIC_TRANSITION_RULE){
		if(command[4][0] == NC){
			DEBUG printf("@bg.draw ルール画像が指定されていません。\n");
			throw ENGINE_GRAPHIC_BACKGROUND_NO_SUCH_ARGUMENT;
		}
	}

	// [5] ルール合成のしきい値
	if(command[5] != NULL){
		this->background_graphics[i].setTransitionThreshold(atoi(command[5]));
	}else{
		DEBUG printf("@bg.draw 合成境界のしきい値が未指定のためデフォルト値を使用します。\n");
		this->background_graphics[i].setTransitionThreshold(TENG_GRAPHIC_TRANSITON_THRESHOLD_DEFAULT);
	}

	//ギャラリー対象の背景なら、
	int gallery_num = Scene::isGalleryPicture(string(command[1]));
	if (gallery_num >= 0){
		config->setCollectedGraphic(gallery_num);
		config->outputConfig();
	}

	// 表示フラグ
	this->background_graphics[i].setVisible(true);

	if(is_hyper_skip) hideBackGroundGraphic(i);
	//after_trans_handle = this->background_graphics[i].getHandle();
}

//背景画像の描画
void Teng::GraphicManager::drawBackGroundGraphic(Teng::Input* input){
	int aft = Teng::BackGroundGraphic::after_trans_arrnum;
	int bef = Teng::BackGroundGraphic::before_trans_arrnum;

	//visibleかどうかの判定はしない（あってもループが大変）
	if(!is_hyper_skip){
		//ロード済みの画像がある場合
		if(this->background_graphics[aft].getHandle() != NULL
			&& this->background_graphics[aft].getHandle() > 0)
		{
			//ルール画像を使わない場合
			if(!this->background_graphics[aft].getUseRule() &&
				this->background_graphics[aft].getTransitionType() == TENG_GRAPHIC_TRANSITION_NOEFFECT){
					DxLib::DrawGraph(
						this->background_graphics[aft].getX(),
						this->background_graphics[aft].getY(),
						this->background_graphics[aft].getHandle(),
						TRUE);
					transition_doing = false;
					//hideBackGroundGraphic(aft);
			}
			//ルール画像を使う場合
			else if(this->background_graphics[aft].getUseRule() &&
				this->background_graphics[aft].getTransitionType() == TENG_GRAPHIC_TRANSITION_RULE){

					//最初にafter画像を描画
					DxLib::DrawGraph(
						this->background_graphics[aft].getX(),
						this->background_graphics[aft].getY(),
						this->background_graphics[aft].getHandle(),
						FALSE);
					//before画像とルール画像を合成して描画
					DxLib::DrawBlendGraph(
						0,
						0,
						this->background_graphics[bef].getHandle(),
						FALSE,
						this->background_graphics[aft].getRuleHandle(),
						this->background_graphics[aft].getFrame(),
						this->background_graphics[aft].getTransitionThreshold());

					if(this->background_graphics[aft].getChanging()){
						//フレーム加算
						if(this->background_graphics[aft].getFrame() < 255){
							this->background_graphics[aft].setFrame(
								this->background_graphics[aft].getFrame() +
								this->background_graphics[aft].getTransitionSpeed());

							// Ctrlスキップ中は即表示
							if (input->getKeyControl() != 0 || config->getSkipModeFlag()){
								transition_doing = false;
								this->background_graphics[aft].setFrame(255);
								hideBackGroundGraphic(aft);
							}
						}
						//トランジションが完了したら
						else{
							this->background_graphics[aft].setVisible(false);
							this->background_graphics[aft].setIsAfterTrans(false);
							this->background_graphics[aft].setChanging(false);
							this->background_graphics[aft].setFrame(0);
							this->background_graphics[aft].setUseRule(false);
							this->background_graphics[aft].setTransitionType(TENG_GRAPHIC_TRANSITION_NOEFFECT);
							transition_doing = false;
							hideBackGroundGraphic(aft);
						}
					}

			}
			//クロスフェードの場合
			else if(!this->background_graphics[aft].getUseRule() &&
				this->background_graphics[aft].getTransitionType() == TENG_GRAPHIC_TRANSITION_CROSSFADE){
					DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255 - this->background_graphics[aft].getFrame());
					DxLib::DrawGraph(0, 0, this->background_graphics[bef].getHandle(), FALSE);
					DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, this->background_graphics[aft].getFrame());
					DxLib::DrawGraph(0, 0, this->background_graphics[aft].getHandle(), FALSE);
					DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

					if(this->background_graphics[aft].getChanging()){
						//フレーム加算
						if(this->background_graphics[aft].getFrame() < 255){
							this->background_graphics[aft].setFrame(
								this->background_graphics[aft].getFrame() +
								this->background_graphics[aft].getTransitionSpeed());

							// Ctrlスキップ中は即表示
							if (input->getKeyControl() != 0 || config->getSkipModeFlag()){
								transition_doing = false;
								this->background_graphics[aft].setFrame(255);
								hideBackGroundGraphic(aft);
							}
						}
						//トランジションが完了したら
						else{
							this->background_graphics[aft].setVisible(false);
							this->background_graphics[aft].setIsAfterTrans(false);
							this->background_graphics[aft].setChanging(false);
							this->background_graphics[aft].setFrame(0);
							this->background_graphics[aft].setUseRule(false);
							this->background_graphics[aft].setTransitionType(TENG_GRAPHIC_TRANSITION_NOEFFECT);
							transition_doing = false;
							hideBackGroundGraphic(aft);
						}
					}
			}
		}
	}else{
		// ハイパースキップ中
		// * * 考えてみれば、ここは実行されない！ * * 

		//this->background_graphics[aft].setVisible();
		this->background_graphics[aft].setIsAfterTrans(false);
		this->background_graphics[aft].setChanging(false);
		this->background_graphics[aft].setFrame(0);
		this->background_graphics[aft].setUseRule(false);
		this->background_graphics[aft].setTransitionType(TENG_GRAPHIC_TRANSITION_NOEFFECT);
		this->background_graphics[bef].setIsAfterTrans(false);
		this->background_graphics[bef].setChanging(false);
		this->background_graphics[bef].setFrame(0);
		this->background_graphics[bef].setUseRule(false);
		this->background_graphics[bef].setTransitionType(TENG_GRAPHIC_TRANSITION_NOEFFECT);
		this->background_graphics[bef].setVisible(false);
		hideBackGroundGraphic(aft);
	}

}

//指定したarrnumの背景以外をvisible=falseにする
void Teng::GraphicManager::hideBackGroundGraphic(const int exclude_arrnum){
	for (int i = 0; i < TENG_GRAPHIC_BACKGROUND_MAX; i++){
		if (i == exclude_arrnum) continue;
		background_graphics[i].setIsAfterTrans(false);
		background_graphics[i].setChanging(false);
		//background_graphics[i].setFrame(0);
		background_graphics[i].setUseRule(false);
		background_graphics[i].setTransitionType(TENG_GRAPHIC_TRANSITION_NOEFFECT);
		background_graphics[i].setVisible(false);
	}
}

//背景画像の解放
void Teng::GraphicManager::freeBackGroundGraphic(const char *filename){
	for(int i = 0; i < TENG_GRAPHIC_BACKGROUND_MAX; i++){
		if(strcmp(filename, this->background_graphics[i].getFileName()) == 0){
			DxLib::DeleteGraph(this->background_graphics[i].getHandle());
			this->background_graphics[i].setChanging(false);
			this->background_graphics[i].setFileName("");
			this->background_graphics[i].setFrame(0);
			this->background_graphics[i].setHandle(0);
			this->background_graphics[i].setImageType(eGraphicType_background);
			this->background_graphics[i].setIsAfterTrans(true);
			this->background_graphics[i].setRuleFile("");
			this->background_graphics[i].setRuleHandle(0);
			this->background_graphics[i].setVisible(false);
			this->background_graphics[i].setUseRule(false);
			return;
		}
	}
	DEBUG printf("@bg.free 無効なファイルです。%s\n", filename);
	throw ENGINE_GRAPHIC_BACKGROUND_FREE_FAILURE;
}

void Teng::GraphicManager::freeBackGroundGraphicAll(){
	DEBUG printf("[INFO] 全ての背景画像を解放します\n");

	//エラーが出ても無視して全て解放する
	for(int i = 0; i < TENG_GRAPHIC_BACKGROUND_MAX; i++){
		DxLib::DeleteGraph(this->background_graphics[i].getHandle());
		this->background_graphics[i].setChanging(false);
		this->background_graphics[i].setFileName("");
		this->background_graphics[i].setFrame(0);
		this->background_graphics[i].setHandle(0);
		this->background_graphics[i].setImageType(eGraphicType_background);
		this->background_graphics[i].setIsAfterTrans(true);
		this->background_graphics[i].setRuleFile("");
		this->background_graphics[i].setRuleHandle(0);
		this->background_graphics[i].setVisible(false);
		this->background_graphics[i].setUseRule(false);
	}

}


bool Teng::BackGroundGraphic::getVisible(){
	return is_visible;
}

void Teng::BackGroundGraphic::setVisible(bool visible){
	this->is_visible = visible;
}

bool Teng::BackGroundGraphic::getChanging(){
	return is_changing;
}

void Teng::BackGroundGraphic::setChanging(bool changing){
	this->is_changing = changing;
}

void Teng::BackGroundGraphic::setFileName(char *arg_filename)
{
	strcpy_s(filename, TENG_FILE_NAME_MAX, arg_filename);
}

char* Teng::BackGroundGraphic::getFileName(){
	return this->filename;
}

void Teng::BackGroundGraphic::setTransitionType(int type){
	this->transition_type = type;
}
int Teng::BackGroundGraphic::getTransitionType(){
	return transition_type;
}
void Teng::BackGroundGraphic::setTransitionSpeed(int speed){
	this->transition_speed = speed;
}
int Teng::BackGroundGraphic::getTransitionSpeed(){
	return transition_speed;
}
void Teng::BackGroundGraphic::setUseRule(bool use){
	this->use_rule = use;
}
bool Teng::BackGroundGraphic::getUseRule(){
	return use_rule;
}
char* Teng::BackGroundGraphic::getRuleFile(){
	return rule_file;
}
void Teng::BackGroundGraphic::setRuleFile(char *rule){
	strcpy_s(rule_file, TENG_FILE_NAME_MAX, rule);
}
void Teng::BackGroundGraphic::setTransitionThreshold(int threshold){
	this->blend_threshold = threshold;
};
int Teng::BackGroundGraphic::getTransitionThreshold(){
	return blend_threshold;
}
void Teng::BackGroundGraphic::setIsAfterTrans(bool after){
	this->is_after_trans = after;
}
bool Teng::BackGroundGraphic::getIsAfterTrans(){
	return is_after_trans;
}
void Teng::BackGroundGraphic::setRuleHandle(int handle){
	this->rule_handle = handle;
}
int Teng::BackGroundGraphic::getRuleHandle(){
	return rule_handle;
}
void Teng::BackGroundGraphic::setFrame(short frame){
	this->frame = frame;
}
short Teng::BackGroundGraphic::getFrame(){
	return frame;
}