﻿#include "../graphic.h"
#include "../define.h"
#include "../global.h"
#include "../error_code_define.h"
#include "graphic_manager.h"
#include <Shlwapi.h>
#pragma comment(lib, "shlwapi.lib")
extern char common_dir[256];

//SystemGraphicをロードする
//type:どの場所の画像か、filename：ディレクトリ含めたパス
arrnum Teng::GraphicManager::setSystemGraphic(int type, const char *filename, int x, int y, int parent, int child){

	//TODO
	//////開発用
	char fullpath[256];
	strcpy_s(fullpath, 256, common_dir);
	strcat_s(fullpath, 256, TENG_DIR_SYSTEM_IMAGE);
	strcat_s(fullpath, 256, filename);

	if (type == eGraphicSys_gallery_pic_small){
		sprintf_s(fullpath, 256, "%s%s%s", common_dir, TENG_DIR_GALLERY, filename);
	}
	else if (type == eGraphicSys_gallery_pic_large){
		sprintf_s(fullpath, 256, "%s%s%s", common_dir, TENG_DIR_BACKGROUND_IMAGE, filename);
	}
	else if (type == eGraphicSys_gallery_rule_slide){
		sprintf_s(fullpath, 256, "%s%s%s", common_dir, TENG_DIR_RULE_IMAGE, filename);
	}


	for(int i = 0; i < TENG_GRAPHIC_SYSTEM_MAX; i++){
		if(this->system_graphics[i].getHandle() == 0){
			this->system_graphics[i].setSystemType(type);
			this->system_graphics[i].setX(x);
			this->system_graphics[i].setY(y);
			if (type == eGraphicSys_gallery_rule_slide || type == eGraphicSys_rule_save_confirm_dialog){
				this->system_graphics[i].setHandle(DxLib::LoadBlendGraph(fullpath));
			}
			else{
				this->system_graphics[i].setHandle(DxLib::LoadGraph(fullpath));
			}
			this->system_graphics[i].setImageType(eGraphicType_system);
			this->system_graphics[i].setBranchParent(parent);
			this->system_graphics[i].setBranchChild(child);
			if(this->system_graphics[i].getHandle() == -1
				//|| PathFileExists(fullpath) == FALSE
				){
				DEBUG printf("[ERROR] システム画像の読み込みに失敗しました: %s\n", filename);
				throw ENGINE_GRAPHIC_SYSTEM_GRAPHIC_LOADGRAPH_FAILED;
			}
			return i;
		}
		if( i == TENG_GRAPHIC_SYSTEM_MAX ){
			DEBUG printf("[ERROR] システム画像の読み込み制限数を超えています: %s\n", filename);
			throw ENGINE_GRAPHIC_SYSTEM_GRAPHIC_TOO_MUCH;
		}
	}
	return -1;
}




Teng::GraphicManager::GraphicManager() : click_wait(this){
	for(int i = 0; i < TENG_GRAPHIC_SYSTEM_MAX; i++){
		system_graphics[i].setHandle(0);
	}
	transition_doing = false;
	character_fade_completed_now = false;
	is_character_crossfading = false;
}

// クリック待ちをロード完了後に再設定する（読み込み前の不完全な状態なので）
void Teng::GraphicManager::resetClickWait(){
	click_wait = ClickWait(this);
}
void Teng::GraphicManager::drawClickWaitFall(){
	click_wait.drawFall();
}
void Teng::GraphicManager::drawClickWaitFade(const int x, const int y){
	click_wait.drawFade(x, y);
}


void Teng::GraphicManager::drawSystemGraphic(int scene){

	if(scene == Teng::Scene::eScene_scenario){
	// 	//メッセージウィンドウ
	// 	DxLib::DrawGraph(
	// 		this->system_graphics[Teng::SystemGraphic::arrnum_msgwindow].getX(),
	// 		this->system_graphics[Teng::SystemGraphic::arrnum_msgwindow].getY(),
	// 		this->system_graphics[Teng::SystemGraphic::arrnum_msgwindow].getHandle(),
	// 		TRUE);
	}

	else if(scene == Teng::Scene::eScene_logo){
		//static int logo_frame = 0;
		//static int wait_frame = 0;
		//static string logo_status = "white_to_logo";
		
		//if (logo_status == "white_to_logo"){
		//	DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		//	DxLib::DrawGraph(0, 0, system_graphics[SystemGraphic::arrnum_bg_white].getHandle(), FALSE);
		//	SetDrawBlendMode(DX_BLENDMODE_ALPHA, logo_frame);
		//	DxLib::DrawGraph(0, 0, system_graphics[SystemGraphic::arrnum_bg_logo_1].getHandle(), FALSE);
		//	DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		//}
		//else if (logo_status == "logo_to_blank"){
		//	DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		//	DxLib::DrawGraph(0, 0, system_graphics[SystemGraphic::arrnum_bg_logo_1].getHandle(), FALSE);
		//	SetDrawBlendMode(DX_BLENDMODE_ALPHA, logo_frame);
		//	DxLib::DrawGraph(0, 0, system_graphics[SystemGraphic::arrnum_bg_blank].getHandle(), FALSE);
		//	DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		//}


		//if (logo_frame >= 255){

		//	if (logo_status == "white_to_logo" && wait_frame > 30){
		//		logo_status = "logo_to_blank";
		//		wait_frame = 0;
		//		logo_frame = 0;
		//	}
		//	else if (logo_status == "logo_to_blank" && wait_frame > 30){
		//		wait_frame = 0;
		//		logo_frame = 0;
		//		
		//		before_scene = eScene_logo;
		//		now_scene = eScene_title;
		//	}
		//	wait_frame += 1;
		//}
		//logo_frame += 3;
	}

	else if(scene == Teng::Scene::eScene_title){
	}

	else if(scene == Teng::Scene::eScene_scenario_load || scene == Teng::Scene::eScene_title_load){
		//ロード画面背景
		DxLib::DrawGraph(
			this->system_graphics[Teng::SystemGraphic::arrnum_bg_load_1].getX(),
			this->system_graphics[Teng::SystemGraphic::arrnum_bg_load_1].getY(),
			this->system_graphics[Teng::SystemGraphic::arrnum_bg_load_1].getHandle(),
			TRUE);
	}

	else if(scene == Teng::Scene::eScene_scenario_save){
		//セーブ画面背景
		DxLib::DrawGraph(
			this->system_graphics[Teng::SystemGraphic::arrnum_bg_save_1].getX(),
			this->system_graphics[Teng::SystemGraphic::arrnum_bg_save_1].getY(),
			this->system_graphics[Teng::SystemGraphic::arrnum_bg_save_1].getHandle(),
			TRUE);
	}

	else if(scene == Teng::Scene::eScene_title_config || scene == Teng::Scene::eScene_scenario_config){
		//ロード画面背景
		DxLib::DrawGraph(
			this->system_graphics[Teng::SystemGraphic::arrnum_bg_config].getX(),
			this->system_graphics[Teng::SystemGraphic::arrnum_bg_config].getY(),
			this->system_graphics[Teng::SystemGraphic::arrnum_bg_config].getHandle(),
			TRUE);
	}
	else if (scene == Teng::Scene::eScene_gallery){
		//回想画面背景
		DxLib::DrawGraph(
			this->system_graphics[Teng::SystemGraphic::arrnum_bg_gallery].getX(),
			this->system_graphics[Teng::SystemGraphic::arrnum_bg_gallery].getY(),
			this->system_graphics[Teng::SystemGraphic::arrnum_bg_gallery].getHandle(),
			TRUE);
	}

}







