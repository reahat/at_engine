﻿#pragma once
#include "../define.h"
#include "../graphic.h"

namespace Teng{

	class FaceGraphic : public Graphic{
	public:
		FaceGraphic();
		~FaceGraphic();
		void setFileName(const char *filename);
		char* getFileName();

		//描画しないときは-1をセットする。それ以外の時は描画
		static int visible_arrnum;
	private:
		char filename[TENG_FILE_NAME_MAX];//画像ファイル名
	};

}