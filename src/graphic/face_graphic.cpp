﻿#include "../graphic.h"
#include "../define.h"
#include "../global.h"
#include "../error_code_define.h"
#include "face_graphic.h"
#include <Shlwapi.h>
#pragma comment(lib, "shlwapi.lib")
extern char common_dir[256];
extern bool is_hyper_skip;

//static変数定義
int Teng::FaceGraphic::visible_arrnum = -1;

Teng::FaceGraphic::FaceGraphic(){
	this->setImageType(eGraphicType_face);
	this->setFileName("");
	this->setHandle(0);
	this->setX(TENG_GRAPHIC_FACE_X);
	this->setY(TENG_GRAPHIC_FACE_Y);
}

Teng::FaceGraphic::~FaceGraphic(){};

void Teng::GraphicManager::loadFaceGraphic(const char *filename){
	//TODO
	//////開発用
	char fullpath[256];
	strcpy_s(fullpath, 256, common_dir);
	strcat_s(fullpath, 256, TENG_DIR_FACE_IMAGE);

	for(int i = 0; i < TENG_GRAPHIC_FACE_MAX; i++){
		if(this->face_graphics[i].getHandle() == 0){

			strcat_s(fullpath, TENG_FILE_NAME_MAX, filename);

			this->face_graphics[i].setFileName(filename);
			if(!is_hyper_skip) this->face_graphics[i].setHandle(DxLib::LoadGraph(fullpath));
			if(is_hyper_skip) this->face_graphics[i].setHandle(-2);
			if(this->face_graphics[i].getHandle() == -1 || PathFileExists(fullpath) == FALSE){
				//DEBUG printf("[ERROR] @face.load フェイス画像の読み込みに失敗しました: %s\n", filename);
				//throw ENGINE_GRAPHIC_FACE_LOAD_FAILED;
			}
			break;
		}
		if( i == TENG_GRAPHIC_FACE_MAX-1 ){
			DEBUG printf("[ERROR] @face.load フェイス画像の読み込み制限数を超えています: %s\n", filename);
			throw ENGINE_GRAPHIC_FACE_EXCEED_LIMIT;
		}
	}
}

void Teng::GraphicManager::setFaceGraphic(const char *filename){
	for(int i = 0; i < TENG_GRAPHIC_FACE_MAX; i++){
		if(this->face_graphics[i].getHandle() != 0){
			if(this->face_graphics[i].getFileName() != NULL &&
				strncmp(filename, this->face_graphics[i].getFileName(), TENG_FILE_NAME_MAX) == 0)
			{
				Teng::FaceGraphic::visible_arrnum = i;
				return;
			}
		}
	}
	DEBUG printf("[ERROR] @face.draw %sは未ロードです。\n", filename);
	throw ENGINE_GRAPHIC_FACE_DRAW_YET_LOADED;
}

void Teng::GraphicManager::drawFaceGraphic(){
	if(!is_hyper_skip){
		if(Teng::FaceGraphic::visible_arrnum >= 0){
			DxLib::DrawGraph(
				TENG_GRAPHIC_FACE_X,
				TENG_GRAPHIC_FACE_Y,
				this->face_graphics[Teng::FaceGraphic::visible_arrnum].getHandle(),
				TRUE);
		}
	}
}

void Teng::GraphicManager::freeFaceGraphic(const char *filename){
	for(int i = 0; i < TENG_GRAPHIC_FACE_MAX; i++){
		if(strcmp(filename, this->face_graphics[i].getFileName()) == 0){
			DxLib::DeleteGraph(this->face_graphics[i].getHandle());
			this->face_graphics[i].setImageType(eGraphicType_face);
			this->face_graphics[i].setFileName("");
			this->face_graphics[i].setHandle(0);
			Teng::FaceGraphic::visible_arrnum = -1;
			return;
		}
	}
	DEBUG printf("@face.free 未ロードの画像を解放しようとしました。%s\n", filename);
	throw ENGINE_GRAPHIC_FACE_FREE_YET_LOADED;
}

void Teng::GraphicManager::freeFaceGraphicAll(){
	DEBUG printf("[INFO] 全てのフェイス画像を解放します\n");

	//エラーが出ても無視して全て解放する
	for(int i = 0; i < TENG_GRAPHIC_FACE_MAX; i++){
		DxLib::DeleteGraph(this->face_graphics[i].getHandle());
		this->face_graphics[i].setHandle(0);
		//this->face_graphics[i].setX(0);
		//this->face_graphics[i].setY(0);
		this->face_graphics[i].setImageType(eGraphicType_face);
		Teng::FaceGraphic::visible_arrnum = -1;
		this->face_graphics[i].setFileName("");
	}

}

//非表示にする場合は-1をセット
void Teng::GraphicManager::hideFaceGraphic(){
	Teng::FaceGraphic::visible_arrnum = -1;
}

void Teng::FaceGraphic::setFileName(const char *arg_filename){
	strcpy_s(filename, TENG_FILE_NAME_MAX, arg_filename);
}

char* Teng::FaceGraphic::getFileName(){
	return this->filename;
}