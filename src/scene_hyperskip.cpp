
#include "scene.h"
#include "scenario.h"
#include "DxLib.h"
#include "global.h"
#include "common.h"
#include "savedata.h"
#include "error_code_define.h"
#include <fstream>
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")
#include <list>

#include <typeinfo>
#include <iostream>


extern char common_dir[256];
extern bool is_hyper_skip;
using namespace Teng;

void Scene::doHyperskip(
	CmdMessage* message,
	Window* window,
	Config* config,
	Input* input,
	GraphicManager* g_manager,
	SoundManager* s_manager,
	Teng::Scenario* scenario,
	Teng::Scenario* subscenario
	)
{
	scenario->incrementCurrentLine();
	int nexttag;
	while (true){
		if (!scenario->getIsImportScenario()){
			scenario->setNextTag(nexttag = scenario->decodeScript(input, this, config, message));
			if (isStopTargetTagForHyperskip(nexttag) || !scenario->isAlreadyReadLine(scenario->getCurrentLine())){

				if (nexttag == TENG_TAG_SELECT){
					scenario->executeTag(input, this, message, g_manager, subscenario, s_manager, config);
					message->setDrawSelect(true);
				}
				break;
			}
			if (isExecuteTargetTagForHyperskip(nexttag)){
				scenario->executeTag(input, this, message, g_manager, subscenario, s_manager, config);
			}
			else{
				//通常はexecuteTag内でcurrent_line++されている
				scenario->incrementCurrentLine();
			}
		}
		// 外部ファイル実行時（@returnで通常側に戻る）
		else{
			subscenario->setNextTag(nexttag = subscenario->decodeScript(input, this, config, message));
			if (isStopTargetTagForHyperskip(nexttag) || !scenario->isAlreadyReadLine(scenario->getCurrentLine())){
				if (nexttag == TENG_TAG_SELECT){
					scenario->executeTag(input, this, message, g_manager, subscenario, s_manager, config);
					message->setDrawSelect(true);
				}
				break;
			}
			if (isExecuteTargetTagForHyperskip(nexttag)){
				subscenario->executeTag(input, this, message, g_manager, scenario, s_manager, config);
			}
			else{
				scenario->incrementCurrentLine();
			}
		}

		//scenario->incrementCurrentLine();
	}


	reloadHyperSkip(scenario, message, g_manager, s_manager, window, config, this);
}

bool Scene::isExecuteTargetTagForHyperskip(const int tag){
	switch (tag){
	case TENG_TAG_COMMENT:
		return false;
	case TENG_TAG_EXMESSAGE:
		return false;
	case TENG_TAG_MESSAGE:
		return false;
	case TENG_TAG_SAY:
		return false;
	case TENG_TAG_LABEL:
		return false;
	case TENG_TAG_WAIT:
		return false;
	case TENG_TAG_PLAYMOVIE:
		return false;
	case TENG_TAG_HIDEMSG:
		return false;
	case TENG_TAG_DISPMSG:
		return false;
	default:
		return true;
	}
}

bool Scene::isStopTargetTagForHyperskip(const int tag){

	switch (tag){
	case TENG_TAG_SELECT:
		DEBUG printf("[INFO] @selectタグ出現のためハイパースキップ停止\n");
		return true;
	case TENG_TAG_NAMESET:
		DEBUG printf("[INFO] @namesetタグ出現のためハイパースキップ停止\n");
		return true;
	default:
		return false;
	}
}



void Scene::reloadHyperSkip(Scenario* scenario, CmdMessage* message, GraphicManager* g_manager, SoundManager* s_manager, Window* window, Config* config, Scene* scene){
	DEBUG printf("[INFO] ハイパースキップ停止\n");
	scenario->isHyperSkipButtonPushed(false);
	//ロード用？
	is_hyper_skip = true;
	DxLib::SetUseASyncLoadFlag(FALSE);
	SaveData::makeScreenShotShumbnail();
	Teng::SaveData *savedata1 = new Teng::SaveData;
	DEBUG printf("[INFO] ハイパースキップ：一時シナリオデータの保存開始\n");
	savedata1->save(99, scenario, g_manager, s_manager, window, message, scene);
	DEBUG printf("[INFO] ハイパースキップ：一時シナリオデータの保存完了\n");
	delete savedata1;
	is_hyper_skip = false;
	Teng::SaveData *savedata2 = new Teng::SaveData;
	DEBUG printf("[INFO] ハイパースキップ：一時シナリオデータのロード開始\n");
	savedata2->load(99, scenario, message, g_manager, s_manager, window, config, scene);
	DEBUG printf("[INFO] ハイパースキップ：一時シナリオデータのロード完了\n");
	delete savedata2;
	DxLib::SetUseASyncLoadFlag(TRUE);
}
