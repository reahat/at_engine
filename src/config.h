﻿//コンフィグファイル処理クラス
#include "define.h"
#pragma once
namespace Teng{
	class Scene;
	struct ConfigData{
		int master_volume;
		int sound_volume;
		int bgm_volume;
		int window_mode; //ウィンドウorフルスクリーン
		int window_size; //800*600 or 1024*768
		int force_skip; //未読文章もスキップ
		int message_wait;
		int auto_message_wait;
		bool draw_gallery_menu;
		bool cg_collected[TENG_GALLERY_MAX_NUMBER];
	};

	enum ConfigType{
		eConfigType_master_volume,
		eConfigType_sound_volume,
		eConfigType_bgm_volume,
		eConfigType_force_skip,
		eConfigType_message_wait,
		eConfigType_auto_message_wait,
		eConfigType_common_cg_recollection
	};

	class Config
	{
	public:
		Config();
		~Config();
		int loadConfig(const char *file); //設定ファイルの読み込み
		int makeConfig(const char *file); //設定ファイルの作成
		ConfigData getSetting();
		void setConfig(ConfigType type, int value);
		void backupConfig();
		void restoreConfig();
		void outputConfig();
		void setAutoMsgFlag(bool auto_flag){auto_msg_flag = auto_flag;}
		bool getAutoMsgFlag(){return auto_msg_flag;}
		void setSkipModeFlag(bool skip_mode_flag){this->skip_mode_flag = skip_mode_flag;}
		bool getSkipModeFlag(){return skip_mode_flag;}
		//void setAutoMode(bool is_auto){auto_mode = is_auto;}
		//bool getAutoMode(){return auto_mode;}
		void setDisplayGalleryMenuFlag();
		int  calcPlayVolume(const int& master_vol, const int& type_vol, const int& file_vol);

		void setCollectedGraphic(const int gallery_num);
		static char savedata_dir[1024];

	private:
		//コンフィグ設定
		ConfigData setting;
		ConfigData old_setting;
		int id;
		//オートモード状態
		bool auto_msg_flag;
		//通常スキップモード状態
		bool skip_mode_flag;

		//オートモードで自動クリックされる瞬間にtrue
		//bool do_auto_click_flag;
		//bool auto_mode;
	};
}