﻿//キー入力処理クラス
#pragma once

#include "scene.h"

namespace Teng{
	class Input{
	public:
		Input();
		virtual ~Input();

		//キーの入力状態を取得する
		void checkKeyInput();

		//左クリックの押しっぱなし状態を取得する
		//※必ず毎フレーム実行すること
		int checkKeepMouseLeft(Teng::Scene* scn);
		int checkKeepMouseRight(Teng::Scene* scn);
		int checkKeepKeyReturn(Teng::Scene* scn);
		int checkKeepKeyF5(Teng::Scene* scn);
		int checkKeepKeyF9(Teng::Scene* scn);
		int checkKeepWheelUp(Teng::Scene* scn);
		int checkKeepWheelDown(Teng::Scene* scn);

		void setKeepMouseLeft(int other_input){ keep_mouse_left = other_input; }

		//すべてのキー入力を1フレームだけ無効にする
		void disableAllKeys();

		void disableKeyControl(){ key_control = 0; }
		void disableMouseRight(){ mouse_right = 0; keep_mouse_right = 0; }
		void disableMouseLeft(){ mouse_left = 0; keep_mouse_left = 0; }

		//getter
		int getKeySpace();
		int getKeyReturn();
		int getKeyControl();
		int getKeyEscape();
		int getKeyAlt();
		int getKeyF4();
		int getKeyF5();
		int getKeyF9();
		int getMouseLeft();
		int getMouseRight();
		int getMouseWheelUp();
		int getMouseWheelDown();

		int getKeepMouseLeft();
		int getKeepMouseRight();
		int getKeepKeyReturn();
		int getKeepKeyF5();
		int getKeepKeyF9();
		int getKeepWheelUp();
		int getKeepWheelDown();

		int cursorX();
		int cursorY();
		bool cursorIn(const int left, const int right, const int top, const int bottom);

	private:
		int key_space;
		int key_return;
		int key_control;
		int key_escape;
		int key_alt;
		int key_f4;
		int key_f5;
		int key_f9;
		int mouse_left;
		int mouse_right;
		int mouse_wheel_up;
		int mouse_wheel_down;

		int keep_mouse_left;
		int keep_mouse_right;
		int keep_key_return;
		int keep_key_f5;
		int keep_key_f9;
		int keep_wheel_up;
		int keep_wheel_down;

		int cursor_x;
		int cursor_y;
	};
}