#include "scene.h"
#include "scenario.h"
#include "DxLib.h"
#include "global.h"
#include "common.h"
#include "savedata.h"
#include "error_code_define.h"
#include <fstream>
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")
#include <list>

#include <typeinfo>
#include <iostream>


extern char common_dir[256];
extern bool is_hyper_skip;
using namespace Teng;


void Teng::Scene::doLogo(
	Window* window,
	Config* config,
	Input* input,
	GraphicManager* g_manager,
	SoundManager* s_manager)
{
	//起動時に一度だけ実行
	if (!loaded){
		//セーブデータ存在情報を作成する
		char savefile[TENG_FILE_NAME_MAX];
		for (int i = 0; i < TENG_SAVE_SAVEDATA_MAX; i++){
			sprintf_s(savefile, TENG_FILE_NAME_MAX, TENG_SAVE_FILENAME, i);
			if (PathFileExists(savefile) == TRUE){
				SaveData::setSavedataExist(i, true);
			}
			else{
				SaveData::setSavedataExist(i, false);
			}
		}
		//起動時にシステム画像ロード
		Teng::SystemGraphic::arrnum_bg_title = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_bg_title,
			TENG_GRAPHIC_BACKGROUND_TITLE, 0, 0);

		Teng::SystemGraphic::arrnum_bg_load_1 = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_bg_load_1,
			TENG_GRAPHIC_BACKGROUND_LOAD_1, 0, 0);

		Teng::SystemGraphic::arrnum_bg_save_1 = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_bg_save_1,
			TENG_GRAPHIC_BACKGROUND_SAVE_1, 0, 0);

		Teng::SystemGraphic::arrnum_bg_config = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_bg_config,
			TENG_GRAPHIC_BACKGROUND_CONFIG, 0, 0);

		Teng::SystemGraphic::arrnum_bg_gallery = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_bg_gallery,
			TENG_GRAPHIC_BACKGROUND_GALLERY, 0, 0);

		// メッセージボックス
		Teng::SystemGraphic::arrnum_textbox_base = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_textbox_base,
			TENG_GRAPHIC_TEXTBOX_BASE, 0, 0);
		Teng::SystemGraphic::arrnum_textbox_config = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_textbox_config,
			TENG_GRAPHIC_TEXTBOX_CONFIG, 0, 0);
		Teng::SystemGraphic::arrnum_textbox_config_on = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_textbox_config_on,
			TENG_GRAPHIC_TEXTBOX_CONFIG_ON, 0, 0);
		Teng::SystemGraphic::arrnum_textbox_hskip = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_textbox_hskip,
			TENG_GRAPHIC_TEXTBOX_HSKIP, 0, 0);
		Teng::SystemGraphic::arrnum_textbox_hskip_on = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_textbox_hskip_on,
			TENG_GRAPHIC_TEXTBOX_HSKIP_ON, 0, 0);
		Teng::SystemGraphic::arrnum_textbox_hskip = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_textbox_hskip,
			TENG_GRAPHIC_TEXTBOX_HSKIP, 0, 0);
		Teng::SystemGraphic::arrnum_textbox_hskip_on = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_textbox_hskip_on,
			TENG_GRAPHIC_TEXTBOX_HSKIP_ON, 0, 0);
		Teng::SystemGraphic::arrnum_textbox_qsave = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_textbox_qsave,
			TENG_GRAPHIC_TEXTBOX_QSAVE, 0, 0);
		Teng::SystemGraphic::arrnum_textbox_qsave_on = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_textbox_qsave_on,
			TENG_GRAPHIC_TEXTBOX_QSAVE_ON, 0, 0);
		Teng::SystemGraphic::arrnum_textbox_qload = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_textbox_qload,
			TENG_GRAPHIC_TEXTBOX_QLOAD, 0, 0);
		Teng::SystemGraphic::arrnum_textbox_qload_on = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_textbox_qload_on,
			TENG_GRAPHIC_TEXTBOX_QLOAD_ON, 0, 0);
		Teng::SystemGraphic::arrnum_textbox_skip = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_textbox_skip,
			TENG_GRAPHIC_TEXTBOX_SKIP, 0, 0);
		Teng::SystemGraphic::arrnum_textbox_skip_on = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_textbox_skip_on,
			TENG_GRAPHIC_TEXTBOX_SKIP_ON, 0, 0);
		Teng::SystemGraphic::arrnum_textbox_x = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_textbox_x,
			TENG_GRAPHIC_TEXTBOX_X, 0, 0);
		Teng::SystemGraphic::arrnum_textbox_x_on = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_textbox_x_on,
			TENG_GRAPHIC_TEXTBOX_X_ON, 0, 0);
		Teng::SystemGraphic::arrnum_textbox_auto = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_textbox_auto,
			TENG_GRAPHIC_TEXTBOX_AUTO, 0, 0);
		Teng::SystemGraphic::arrnum_textbox_auto_on = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_textbox_auto_on,
			TENG_GRAPHIC_TEXTBOX_AUTO_ON, 0, 0);

		//選択肢ボックスの画像ロード
		Teng::SystemGraphic::arrnum_select_normal = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_select_normal,
			TENG_GRAPHIC_SYSTEM_SELECT_NORMAL, 0, 0);

		Teng::SystemGraphic::arrnum_select_focus = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_select_focus,
			TENG_GRAPHIC_SYSTEM_SELECT_FOCUS, 0, 0);

		//選択肢ボックス（既に選択したもの）
		Teng::SystemGraphic::arrnum_select_selected = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_select_selected,
			TENG_GRAPHIC_SYSTEM_SELECT_SELECTED, 0, 0);

		//ネームエントリー画像ロード
		Teng::SystemGraphic::arrnum_name_entry_box = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_name_entry_box,
			TENG_GRAPHIC_SYSTEM_NAME_ENTRY_BOX, 0, 0);
		Teng::SystemGraphic::arrnum_name_entry_init_normal = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_name_entry_init_normal,
			TENG_GRAPHIC_SYSTEM_NAME_ENTRY_INIT_NORMAL, 0, 0);
		Teng::SystemGraphic::arrnum_name_entry_init_click = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_name_entry_init_click,
			TENG_GRAPHIC_SYSTEM_NAME_ENTRY_INIT_CLICK, 0, 0);
		Teng::SystemGraphic::arrnum_name_entry_decision_normal = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_name_entry_decision_normal,
			TENG_GRAPHIC_SYSTEM_NAME_ENTRY_DECISION_NORMAL, 0, 0);
		Teng::SystemGraphic::arrnum_name_entry_decision_click = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_name_entry_decision_click,
			TENG_GRAPHIC_SYSTEM_NAME_ENTRY_DECISION_CLICK, 0, 0);

		//メッセージ待機の矢印
		Teng::SystemGraphic::arrnum_message_arrow = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_message_arrow,
			TENG_GRAPHIC_SYSTEM_MESSAGE_ARROW, 0, 0);


		//ギャラリー内部用のトラン画像
		Teng::SystemGraphic::arrnum_gallery_rule_slide = g_manager->setSystemGraphic(
			Teng::SystemGraphic::eGraphicSys_gallery_rule_slide,
			TENG_GRAPHIC_SYSTEM_GALLERY_RULE_SLIDE, 0, 0);

		DxLib::SetCreateSoundDataType(DX_SOUNDDATATYPE_FILE);
		//起動時にシステム音声ロード
		Teng::SystemSound::arrnum_se_config_demo = s_manager->setSystemSound(
			Teng::SystemSound::eSysSoundType_se_config_demo,
			false,
			TENG_SOUND_SE_CONFIG_DEMO,
			255);
		Teng::SystemSound::arrnum_se_select_clicked = s_manager->setSystemSound(
			Teng::SystemSound::eSysSoundType_se_select_clicked,
			false,
			TENG_SOUND_SE_SELECT_CLICKED,
			255);
		Teng::SystemSound::arrnum_se_select_focus = s_manager->setSystemSound(
			Teng::SystemSound::eSysSoundType_se_select_focus,
			false,
			TENG_SOUND_SE_SELECT_FOCUS,
			255);
		//タイトル用SE
		Teng::SystemSound::arrnum_se_button_title_clicked = s_manager->setSystemSound(
			Teng::SystemSound::eSysSoundType_se_button_title_clicked,
			false,
			TENG_SOUND_SYSTEM_BUTTON_TITLE_CLICKED,
			255);
		Teng::SystemSound::arrnum_se_button_title_focus = s_manager->setSystemSound(
			Teng::SystemSound::eSysSoundType_se_button_title_focus,
			false,
			TENG_SOUND_SYSTEM_BUTTON_TITLE_FOCUS,
			255);

		//タイトル画面BGM
		DxLib::SetCreateSoundDataType(DX_SOUNDDATATYPE_MEMPRESS);
		Teng::SystemSound::arrnum_bgm_title = s_manager->setSystemSound(
			Teng::SystemSound::eSysSoundType_bgm_title,
			true,
			TENG_SOUND_BGM_TITLE,
			255);
		//タイトル画面ボタン類
		//スタートボタン
		SystemGraphic::arrnum_button_title_start_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_title_start_normal,
			TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_START_NORMAL, 0, 0);
		SystemGraphic::arrnum_button_title_start_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_title_start_focus,
			TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_START_FOCUS, 0, 0);
		SystemGraphic::arrnum_button_title_start_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_title_start_click,
			TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_START_CLICK, 0, 0);
		//ロードボタン
		SystemGraphic::arrnum_button_title_load_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_title_load_normal,
			TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_LOAD_NORMAL, 0, 0);
		SystemGraphic::arrnum_button_title_load_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_title_load_focus,
			TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_LOAD_FOCUS, 0, 0);
		SystemGraphic::arrnum_button_title_load_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_title_load_click,
			TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_LOAD_CLICK, 0, 0);
		//コンフィグボタン
		SystemGraphic::arrnum_button_title_config_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_title_config_normal,
			TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_CONFIG_NORMAL, 0, 0);
		SystemGraphic::arrnum_button_title_config_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_title_config_focus,
			TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_CONFIG_FOCUS, 0, 0);
		SystemGraphic::arrnum_button_title_config_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_title_config_click,
			TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_CONFIG_CLICK, 0, 0);
		//ギャラリーボタン
		SystemGraphic::arrnum_button_title_gallery_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_title_gallery_normal,
			TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_GALLERY_NORMAL, 0, 0);
		SystemGraphic::arrnum_button_title_gallery_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_title_gallery_focus,
			TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_GALLERY_FOCUS, 0, 0);
		SystemGraphic::arrnum_button_title_gallery_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_title_gallery_click,
			TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_GALLERY_CLICK, 0, 0);
		//終了ボタン
		SystemGraphic::arrnum_button_title_exit_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_title_exit_normal,
			TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_EXIT_NORMAL, 0, 0);
		SystemGraphic::arrnum_button_title_exit_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_title_exit_focus,
			TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_EXIT_FOCUS, 0, 0);
		SystemGraphic::arrnum_button_title_exit_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_title_exit_click,
			TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_EXIT_CLICK, 0, 0);
		// タイトル鳥かご
		SystemGraphic::arrnum_button_title_kago_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_title_kago_normal,
			TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_KAGO_NORMAL, 0, 0);
		SystemGraphic::arrnum_button_title_kago_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_title_kago_focus,
			TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_KAGO_FOCUS, 0, 0);


		//セーブロード画面のボタン類
		const int SAVE_PAGE_BUTTON_X   = 75;
		const int SAVE_PAGE_BUTTON_Y_1 = 255;
		const int SAVE_PAGE_BUTTON_Y_2 = 306;
		const int SAVE_PAGE_BUTTON_Y_3 = 362;
		const int SAVE_PAGE_BUTTON_Y_4 = 418;
		const int SAVE_PAGE_BUTTON_Y_5 = 468;
		const int SAVE_BACK_BUTTON_X   = 25;
		const int SAVE_BACK_BUTTON_Y   = 700;
		
		// SystemGraphicは登録した順にarrnumに格納される
		// 番号アイコンは、番号毎にノーマル→フォーカス→クリックの順でセットすること
		SystemGraphic::arrnum_button_save_page1_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_save_page1_normal,
			TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_1_NORMAL, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_1);
		SystemGraphic::arrnum_button_save_page1_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_save_page1_focus,
			TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_1_FOCUS, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_1);
		SystemGraphic::arrnum_button_save_page1_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_save_page1_click,
			TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_1_CLICK, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_1);

		SystemGraphic::arrnum_button_save_page2_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_save_page2_normal,
			TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_2_NORMAL, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_2);
		SystemGraphic::arrnum_button_save_page2_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_save_page2_focus,
			TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_2_FOCUS, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_2);
		SystemGraphic::arrnum_button_save_page2_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_save_page2_click,
			TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_2_CLICK, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_2);

		SystemGraphic::arrnum_button_save_page3_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_save_page3_normal,
			TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_3_NORMAL, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_3);
		SystemGraphic::arrnum_button_save_page3_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_save_page3_focus,
			TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_3_FOCUS, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_3);
		SystemGraphic::arrnum_button_save_page3_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_save_page3_click,
			TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_3_CLICK, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_3);

		SystemGraphic::arrnum_button_save_page4_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_save_page4_normal,
			TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_4_NORMAL, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_4);
		SystemGraphic::arrnum_button_save_page4_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_save_page4_focus,
			TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_4_FOCUS, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_4);
		SystemGraphic::arrnum_button_save_page4_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_save_page4_click,
			TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_4_CLICK, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_4);

		SystemGraphic::arrnum_button_save_page5_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_save_page5_normal,
			TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_5_NORMAL, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_5);
		SystemGraphic::arrnum_button_save_page5_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_save_page5_focus,
			TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_5_FOCUS, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_5);
		SystemGraphic::arrnum_button_save_page5_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_save_page5_click,
			TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_5_CLICK, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_5);

		SystemGraphic::arrnum_button_save_back_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_save_back_normal,
			TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_BACK_NORMAL, SAVE_BACK_BUTTON_X, SAVE_BACK_BUTTON_Y);
		SystemGraphic::arrnum_button_save_back_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_save_back_focus,
			TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_BACK_FOCUS, SAVE_BACK_BUTTON_X, SAVE_BACK_BUTTON_Y);
		SystemGraphic::arrnum_button_save_back_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_save_back_click,
			TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_BACK_CLICK, SAVE_BACK_BUTTON_X, SAVE_BACK_BUTTON_Y);

		SystemGraphic::arrnum_button_save_icon_bara = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_save_icon_bara,
			TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_ICON_BARA, 0, 0);
		SystemGraphic::arrnum_button_save_icon_arrow = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_save_icon_arrow,
			TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_ICON_ARROW, 0, 0);
		SystemGraphic::arrnum_button_save_thumbnail_arrow = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_button_save_thumbnail_arrow,
			TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_THUMBNAIL_ARROW, 0, 0);


		// セーブロードの確認ダイアログ表示用ルール
		SystemGraphic::arrnum_rule_save_confirm_dialog = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_rule_save_confirm_dialog,
			TENG_GRAPHIC_SYSTEM_RULE_SAVE_CONFIRM_DIALOG, 0, 0);
		SystemGraphic::arrnum_rule_save_confirm_dialog_reverse = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_rule_save_confirm_dialog,
			TENG_GRAPHIC_SYSTEM_RULE_SAVE_CONFIRM_DIALOG_REVERSE, 0, 0);

		// コンフィグ画面
		SystemGraphic::arrnum_config_button_all_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_config_button_all_normal,
			SYSTEM_BUTTON_CONFIG_SKIP_ALL_NORMAL, 0, 0);
		SystemGraphic::arrnum_config_button_all_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_config_button_all_focus,
			SYSTEM_BUTTON_CONFIG_SKIP_ALL_FOCUS, 0, 0);
		SystemGraphic::arrnum_config_button_all_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_config_button_all_click,
			SYSTEM_BUTTON_CONFIG_SKIP_ALL_CLICK, 0, 0);
		SystemGraphic::arrnum_config_button_already_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_config_button_already_normal,
			SYSTEM_BUTTON_CONFIG_SKIP_ALREADY_NORMAL, 0, 0);
		SystemGraphic::arrnum_config_button_already_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_config_button_already_focus,
			SYSTEM_BUTTON_CONFIG_SKIP_ALREADY_FOCUS, 0, 0);
		SystemGraphic::arrnum_config_button_already_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_config_button_already_click,
			SYSTEM_BUTTON_CONFIG_SKIP_ALREADY_CLICK, 0, 0);
		SystemGraphic::arrnum_config_button_minus_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_config_button_minus_normal,
			SYSTEM_BUTTON_CONFIG_MINUS_NORMAL, 0, 0);
		SystemGraphic::arrnum_config_button_minus_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_config_button_minus_focus,
			SYSTEM_BUTTON_CONFIG_MINUS_FOCUS, 0, 0);
		SystemGraphic::arrnum_config_button_minus_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_config_button_minus_click,
			SYSTEM_BUTTON_CONFIG_MINUS_CLICK, 0, 0);
		SystemGraphic::arrnum_config_button_plus_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_config_button_plus_normal,
			SYSTEM_BUTTON_CONFIG_PLUS_NORMAL, 0, 0);
		SystemGraphic::arrnum_config_button_plus_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_config_button_plus_focus,
			SYSTEM_BUTTON_CONFIG_PLUS_FOCUS, 0, 0);
		SystemGraphic::arrnum_config_button_plus_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_config_button_plus_click,
			SYSTEM_BUTTON_CONFIG_PLUS_CLICK, 0, 0);
		SystemGraphic::arrnum_config_button_bar_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_config_button_bar_normal,
			SYSTEM_BUTTON_CONFIG_BAR_NORMAL, 0, 0);
		SystemGraphic::arrnum_config_button_bar_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_config_button_bar_focus,
			SYSTEM_BUTTON_CONFIG_BAR_FOCUS, 0, 0);
		SystemGraphic::arrnum_config_button_bar_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_config_button_bar_click,
			SYSTEM_BUTTON_CONFIG_BAR_CLICK, 0, 0);
		SystemGraphic::arrnum_config_button_back_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_config_button_back_normal,
			SYSTEM_BUTTON_CONFIG_BACK_NORMAL, 0, 0);
		SystemGraphic::arrnum_config_button_back_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_config_button_back_focus,
			SYSTEM_BUTTON_CONFIG_BACK_FOCUS, 0, 0);
		SystemGraphic::arrnum_config_button_back_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_config_button_back_click,
			SYSTEM_BUTTON_CONFIG_BACK_CLICK, 0, 0);
		SystemGraphic::arrnum_config_bar_base = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_config_bar_base,
			SYSTEM_BUTTON_CONFIG_BAR_BASE, 0, 0);

		//右クリックメニュー
		SystemGraphic::arrnum_menu_save_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicsys_menu_save_normal,
			SYSTEM_MENU_BUTTON_SAVE_NORMAL, 0, 0);
		SystemGraphic::arrnum_menu_load_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicsys_menu_load_normal,
			SYSTEM_MENU_BUTTON_LOAD_NORMAL, 0, 0);
		SystemGraphic::arrnum_menu_hskip_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicsys_menu_hskip_normal,
			SYSTEM_MENU_BUTTON_HSKIP_NORMAL, 0, 0);
		SystemGraphic::arrnum_menu_config_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicsys_menu_config_normal,
			SYSTEM_MENU_BUTTON_CONFIG_NORMAL, 0, 0);
		SystemGraphic::arrnum_menu_backlog_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicsys_menu_backlog_normal,
			SYSTEM_MENU_BUTTON_BACKLOG_NORMAL, 0, 0);
		SystemGraphic::arrnum_menu_title_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicsys_menu_title_normal,
			SYSTEM_MENU_BUTTON_TITLE_NORMAL, 0, 0);
		SystemGraphic::arrnum_menu_exit_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicsys_menu_exit_normal,
			SYSTEM_MENU_BUTTON_EXIT_NORMAL, 0, 0);
		SystemGraphic::arrnum_menu_back_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicsys_menu_back_normal,
			SYSTEM_MENU_BUTTON_BACK_NORMAL, 0, 0);

		SystemGraphic::arrnum_menu_save_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicsys_menu_save_focus,
			SYSTEM_MENU_BUTTON_SAVE_FOCUS, 0, 0);
		SystemGraphic::arrnum_menu_load_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicsys_menu_load_focus,
			SYSTEM_MENU_BUTTON_LOAD_FOCUS, 0, 0);
		SystemGraphic::arrnum_menu_hskip_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicsys_menu_hskip_focus,
			SYSTEM_MENU_BUTTON_HSKIP_FOCUS, 0, 0);
		SystemGraphic::arrnum_menu_config_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicsys_menu_config_focus,
			SYSTEM_MENU_BUTTON_CONFIG_FOCUS, 0, 0);
		SystemGraphic::arrnum_menu_backlog_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicsys_menu_backlog_focus,
			SYSTEM_MENU_BUTTON_BACKLOG_FOCUS, 0, 0);
		SystemGraphic::arrnum_menu_title_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicsys_menu_title_focus,
			SYSTEM_MENU_BUTTON_TITLE_FOCUS, 0, 0);
		SystemGraphic::arrnum_menu_exit_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicsys_menu_exit_focus,
			SYSTEM_MENU_BUTTON_EXIT_FOCUS, 0, 0);
		SystemGraphic::arrnum_menu_back_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicsys_menu_back_focus,
			SYSTEM_MENU_BUTTON_BACK_FOCUS, 0, 0);

		SystemGraphic::arrnum_menu_save_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicsys_menu_save_click,
			SYSTEM_MENU_BUTTON_SAVE_CLICK, 0, 0);
		SystemGraphic::arrnum_menu_load_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicsys_menu_load_click,
			SYSTEM_MENU_BUTTON_LOAD_CLICK, 0, 0);
		SystemGraphic::arrnum_menu_hskip_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicsys_menu_hskip_click,
			SYSTEM_MENU_BUTTON_HSKIP_CLICK, 0, 0);
		SystemGraphic::arrnum_menu_config_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicsys_menu_config_click,
			SYSTEM_MENU_BUTTON_CONFIG_CLICK, 0, 0);
		SystemGraphic::arrnum_menu_backlog_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicsys_menu_backlog_click,
			SYSTEM_MENU_BUTTON_BACKLOG_CLICK, 0, 0);
		SystemGraphic::arrnum_menu_title_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicsys_menu_title_click,
			SYSTEM_MENU_BUTTON_TITLE_CLICK, 0, 0);
		SystemGraphic::arrnum_menu_exit_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicsys_menu_exit_click,
			SYSTEM_MENU_BUTTON_EXIT_CLICK, 0, 0);
		SystemGraphic::arrnum_menu_back_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicsys_menu_back_click,
			SYSTEM_MENU_BUTTON_BACK_CLICK, 0, 0);
		SystemGraphic::arrnum_menu_base = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_menu_base,
			SYSTEM_MENU_BASE, 0, 0);

		//起動時にロード ダイアログボックス
		SystemGraphic::arrnum_dialog_base = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_dialog_base,
			TENG_GRAPHIC_SYSTEM_DIALOG_BASE, 0, 0);
		SystemGraphic::arrnum_dialog_yes_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_dialog_yes_normal,
			TENG_GRAPHIC_SYSTEM_DIALOG_YES_NORMAL, 0, 0);
		SystemGraphic::arrnum_dialog_no_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_dialog_no_normal,
			TENG_GRAPHIC_SYSTEM_DIALOG_NO_NORMAL, 0, 0);
		SystemGraphic::arrnum_dialog_yes_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_dialog_yes_click,
			TENG_GRAPHIC_SYSTEM_DIALOG_YES_CLICK, 0, 0);
		SystemGraphic::arrnum_dialog_no_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_dialog_no_click,
			TENG_GRAPHIC_SYSTEM_DIALOG_NO_CLICK, 0, 0);

		// バックログ
		SystemGraphic::arrnum_backlog_base = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_backlog_base,
			TENG_GRAPHIC_BACKLOG_BASE, 0, 0);
		SystemGraphic::arrnum_backlog_base_bar = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_backlog_base_bar,
			TENG_GRAPHIC_BACKLOG_BASE_BAR, 0, 0);
		SystemGraphic::arrnum_backlog_bar_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_backlog_bar_normal,
			TENG_GRAPHIC_BACKLOG_BAR_NORMAL, 0, 0);
		SystemGraphic::arrnum_backlog_bar_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_backlog_bar_focus,
			TENG_GRAPHIC_BACKLOG_BAR_FOCUS, 0, 0);
		SystemGraphic::arrnum_backlog_bar_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_backlog_bar_click,
			TENG_GRAPHIC_BACKLOG_BAR_CLICK, 0, 0);
		SystemGraphic::arrnum_backlog_down_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_backlog_down_normal,
			TENG_GRAPHIC_BACKLOG_DOWN_NORMAL, 0, 0);
		SystemGraphic::arrnum_backlog_down_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_backlog_down_focus,
			TENG_GRAPHIC_BACKLOG_DOWN_FOCUS, 0, 0);
		SystemGraphic::arrnum_backlog_down_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_backlog_down_click,
			TENG_GRAPHIC_BACKLOG_DOWN_CLICK, 0, 0);
		SystemGraphic::arrnum_backlog_up_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_backlog_up_normal,
			TENG_GRAPHIC_BACKLOG_UP_NORMAL, 0, 0);
		SystemGraphic::arrnum_backlog_up_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_backlog_up_focus,
			TENG_GRAPHIC_BACKLOG_UP_FOCUS, 0, 0);
		SystemGraphic::arrnum_backlog_up_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_backlog_up_click,
			TENG_GRAPHIC_BACKLOG_UP_CLICK, 0, 0);
		SystemGraphic::arrnum_backlog_back_normal = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_backlog_back_normal,
			TENG_GRAPHIC_BACKLOG_BACK_NORMAL, 0, 0);
		SystemGraphic::arrnum_backlog_back_focus = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_backlog_back_focus,
			TENG_GRAPHIC_BACKLOG_BACK_FOCUS, 0, 0);
		SystemGraphic::arrnum_backlog_back_click = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_backlog_back_click,
			TENG_GRAPHIC_BACKLOG_BACK_CLICK, 0, 0);

		// クリック待ち
		SystemGraphic::arrnum_wait_1 = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_wait_1,
			TENG_GRAPHIC_WAIT_1, 0, 0);
		SystemGraphic::arrnum_wait_2 = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_wait_2,
			TENG_GRAPHIC_WAIT_2, 0, 0);
		SystemGraphic::arrnum_wait_3 = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_wait_3,
			TENG_GRAPHIC_WAIT_3, 0, 0);
		SystemGraphic::arrnum_wait_4 = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_wait_4,
			TENG_GRAPHIC_WAIT_4, 0, 0);
		SystemGraphic::arrnum_wait_5 = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_wait_5,
			TENG_GRAPHIC_WAIT_5, 0, 0);
		SystemGraphic::arrnum_wait_6 = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_wait_6,
			TENG_GRAPHIC_WAIT_6, 0, 0);
		SystemGraphic::arrnum_wait_7 = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_wait_7,
			TENG_GRAPHIC_WAIT_7, 0, 0);
		SystemGraphic::arrnum_wait_8 = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_wait_8,
			TENG_GRAPHIC_WAIT_8, 0, 0);
		SystemGraphic::arrnum_wait_9 = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_wait_9,
			TENG_GRAPHIC_WAIT_9, 0, 0);
		SystemGraphic::arrnum_wait_10 = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_wait_10,
			TENG_GRAPHIC_WAIT_10, 0, 0);
		SystemGraphic::arrnum_wait_11 = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_wait_11,
			TENG_GRAPHIC_WAIT_11, 0, 0);
		SystemGraphic::arrnum_wait_12 = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_wait_12,
			TENG_GRAPHIC_WAIT_12, 0, 0);
		SystemGraphic::arrnum_wait_off = g_manager->setSystemGraphic(
			SystemGraphic::eGraphicSys_wait_off,
			TENG_GRAPHIC_WAIT_OFF, 0, 0);



		loaded = true;
		printf("ロード完了\n");
		resetTitleButtonFlag();
	}

	//ロード完了したらタイトル画面に移動
	//if (DxLib::GetASyncLoadNum() == 0 && frame > 60){
	//	//Teng::BackGroundGraphic::after_trans_arrnum = tmp_title_arrnum;
	//	before_scene = eScene_logo;
	//	now_scene = eScene_title;
	//}
	static int logo_frame = 0;
	static int wait_frame = 0;
	static string logo_status = "white_to_logo";

	if (logo_status == "white_to_logo"){
		DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		DxLib::DrawGraph(0, 0, g_manager->system_graphics[SystemGraphic::arrnum_bg_white].getHandle(), FALSE);
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, logo_frame);
		DxLib::DrawGraph(0, 0, g_manager->system_graphics[SystemGraphic::arrnum_bg_logo_1].getHandle(), FALSE);
		DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	}
	else if (logo_status == "logo_to_blank"){
		DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		DxLib::DrawGraph(0, 0, g_manager->system_graphics[SystemGraphic::arrnum_bg_logo_1].getHandle(), FALSE);
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, logo_frame);
		DxLib::DrawGraph(0, 0, g_manager->system_graphics[SystemGraphic::arrnum_bg_blank].getHandle(), FALSE);
		DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	}


	if (logo_frame >= 255){

		if (logo_status == "white_to_logo" && wait_frame > 180){
			logo_status = "logo_to_blank";
			wait_frame = 0;
			logo_frame = 0;
		}
		else if (logo_status == "logo_to_blank" && wait_frame > 30){
			wait_frame = 0;
			logo_frame = 0;

			before_scene = eScene_logo;
			now_scene = eScene_title;
			scene_changing = true;
			frame = 0;
			DxLib::SetBackgroundColor(0, 0, 0);
			g_manager->resetClickWait();
		}
		wait_frame += 1;
	}
	logo_frame += 3;

	// ロゴスキップ
	if (input->getKeyEscape() != 0){
			wait_frame = 0;
			logo_frame = 0;

			before_scene = eScene_logo;
			now_scene = eScene_title;
			scene_changing = true;
			frame = 0;
			DxLib::SetBackgroundColor(0, 0, 0);
			g_manager->resetClickWait();

	}
}