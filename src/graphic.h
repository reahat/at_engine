﻿//画像管理クラス
#pragma once
#include "define.h"
#include <vector>

namespace Teng{
	class Graphic{
	public:
		enum GraphicType{
			eGraphicType_null,
			eGraphicType_system,
			eGraphicType_background,
			eGraphicType_rule,
			eGraphicType_character,
			eGraphicType_face,
			eGraphicType_savedata,
			eGraphicType_effect
		};
		Graphic();
		virtual ~Graphic();
		int getHandle();
		void setHandle(int handle);
		void setImageType(int type);
		void setX(int x);
		void setY(int y);
		int getX();
		int getY();

	private:
		int handle;//グラフィックハンドル
		int image_type;//画像の種類
		int x;
		int y;

	};


	class SaveDataGraphic : public Graphic{

	};

	class EffectGraphic : public Graphic{

	};


}