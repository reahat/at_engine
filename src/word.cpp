﻿#include "define.h"
#include "DxLib.h"
#include "error_code_define.h"
#include "word.h"

using namespace Teng;

Word::Word(){
	clear();
}

Word::~Word(){

}

void Word::addBaseText(const char character){
	char ch = character;
	static unsigned char before_char = NC;
	// スペース文字 _ が出現したら、スペースに置き換える（半角SPは自動でカンマに置換されてしまうため）
	if (ch == TENG_MESSAGE_SPACE_CHARACTER &&
		before_char != 0x83 &&
		before_char != 0x89 &&
		before_char != 0x8A &&
		before_char != 0x8C &&
		before_char != 0x8D &&
		before_char != 0x8E &&
		before_char != 0x8F &&
		before_char != 0x90 &&
		before_char != 0x91 &&
		before_char != 0x92 &&
		before_char != 0x93 &&
		before_char != 0x94 &&
		before_char != 0x95 &&
		before_char != 0x96 &&
		before_char != 0x97 &&
		before_char != 0x98)
	{
		ch = ' ';
	}
	base_text += ch;
	before_char = character;
}

void Word::addRubiText(const char character){
	char ch = character;
	// スペース文字 _ が出現したら、スペースに置き換える（半角SPは自動でカンマに置換されてしまうため）
	if(ch == TENG_MESSAGE_SPACE_CHARACTER) ch = ' ';
	rubi += ch;
	rubi_exists = true;
}

void Word::clear(){
	base_text      = "";
	rubi           = "";
	witdh          = 0;
	x_pos          = 0;
	rubi_x_pos     = 0;
	space          = 0;
	use_rubi_width = false;
	rubi_exists    = false;
	speaker_name   = "";
	on_stop        = false;
}

bool Word::isBlank(){
	return base_text.empty();
}

bool Word::isPresent(){
	return !isBlank();
}

bool Word::isRubiGtBaseWidth(){
	return rubi.size() > (base_text.size() * 2);
	//DxLib::GetDrawStringWidthToHandle(base_text.c_str(), strnlen_s(base_text.c_str(), TENG_SCRIPT_MAX_STRING_LENGTH), font_handle);
}

void Word::useRubiWidth(){
	use_rubi_width = true;
}

void Word::calc(const int font_handle){
	// まずはルビ用のフォントを作成すること
	// isRubiGtBaseWidth() の結果に応じてルビの幅を使うのかどうか
	// widthの計算、rubi_x_posの計算を行う
	// rubi_x_posはベースのx_posからの相対値がよい？spaceはルビの文字間隔
	if(isRubiGtBaseWidth()){
		useRubiWidth();
	}else{
		
	}
}

void Word::makeNameWord(string name){
	base_text = name;
}