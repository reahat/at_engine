﻿//例外処理クラス

#include <string>
#include <map>
namespace Teng{
	class Exception
	{
	public:
		Exception(); //コンストラクタ：エラーメッセージ一覧のmapを作成する
		~Exception();
		void putError(int errnum); //メッセージボックスを出力
		void systemExit(); //例外発生時の終了処理

	private:
		std::map<int, std::string> error_map;
	};
}