#include "scene.h"
#include "scenario.h"
#include "DxLib.h"
#include "global.h"
#include "common.h"
#include "savedata.h"
#include "error_code_define.h"
#include <fstream>
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")
#include <list>

#include <typeinfo>
#include <iostream>


extern char common_dir[256];
extern bool is_hyper_skip;
using namespace Teng;

void Teng::Scene::doScenario(
	CmdMessage* message,
	Window* window,
	Config* config,
	Input* input,
	GraphicManager* g_manager,
	SoundManager* s_manager,
	Teng::Scenario* scenario,
	Teng::Scenario* subscenario
	)
{
	// エンターキーの入力をクリックに充てる
	int input_return = input->getKeepKeyReturn();
	if (input_return != 0){
		input->setKeepMouseLeft(input_return);
	}

	if (before_scene != eScene_scenario_menu && before_scene != eScene_scenario){
	//if (before_scene == eScene_title){
		//画面開始時
		if (fade_type == FadeType::fadein){
			int f = static_cast<int>(frame)* 10;
			if (f < 255){
				DxLib::SetDrawBright(f, f, f);
			}
			else{
				if (fade_type == FadeType::fadein){
					fade_type = FadeType::no_fade;
					DxLib::SetDrawBright(255, 255, 255);
				}
			}
		}
	}

	static bool is_initialized = false;
	if (!is_initialized){
		// 名前入力初期化
		scenario->nameEntryInitialize(g_manager);
	}




	if (message->isDrawSelect()){
		input->disableKeyControl();
	}

	static bool is_before_frame_hyper = false;

	bool force_do = false;
	if (message->plus_stopping && (input->getKeyControl() || config->getSkipModeFlag())){
		force_do = true;
	}
	//wait時、残り時間が0の場合強制的に次のコマンドを実行
	static unsigned long before_wait_frame = 0;
	if (before_wait_frame == 1){
		force_do = true;
		message->setForceClickFlg(true);
	}
	before_wait_frame = calcWait();
	if (is_hyper_skip) resetWait();
	if ((input->getKeyControl() != 0 || config->getSkipModeFlag()) && !message->isDrawSelect()){
		if (wait_frame > 1){
			resetWait();
			setWaitFrame(2);
		}
	}
	//wait中・BGMフェード中はキー操作無効
	if (before_wait_frame > 0 || s_manager->isBgmFading()){
		force_do = false;
		input->disableAllKeys();
	}



	if (s_manager->isBgmStopComplete() && !movie_playing){
		force_do = true;
		message->setForceClickFlg(true);
		s_manager->isBgmStopComplete(false);
	}
	if (message->getMsgWindowStatus() == CmdMessage::eMsgWindow::MSG_WINDOW_TRANS_COMPLETED){
		if (!scenario->isXClicked()){
			force_do = true;
			message->setForceClickFlg(true);
		}
		message->setMsgWindowStatus(CmdMessage::eMsgWindow::MSG_WINDOW_TRANS_TYPE_DISPLAY);
	}
	//はじめからのとき、最初の一回だけ自動で実行
	if (scenario->startedFromTitle()){
		force_do = true;
		message->setForceClickFlg(true);
		scenario->startedFromTitle(false);
	}

	//キャラクターのフェードが完了したら次に進める
	if (g_manager->getCharacterFadeCompletedNow() && before_wait_frame == 0){
		force_do = true;
		message->setForceClickFlg(true);
		g_manager->setCharacterFadeCompletedNow(false);
	}

	if (message->getSelectClickWaiting()){
		input->disableMouseLeft();
		input->disableMouseRight();
	}

	if (message->getForceClickFlg()){
		force_do = true;
	}

	if (force_do || (
		!is_backlog_enable &&
		!message->isDrawNameEntry() &&
		!message->isDrawSelect() &&
		!message->getRenderingFlg() &&
		(!cursor_on_button || message->isClickAfterSelectWait()) &&
		!Teng::Window::getDialogDrawing() &&
		!g_manager->getTransitionDoing() &&
		!message->getMsgWindowHideFlg() &&
		wait_frame == 0 &&
		!s_manager->isBgmFading())){

		message->isClickAfterSelectWait(false);
		//タグ実行
		do{
			// 通常時
			if (!scenario->getIsImportScenario()){
				scenario->setNextTag(scenario->decodeScript(input, this, config, message));
				loop_flg = scenario->executeTag(input, this, message, g_manager, subscenario, s_manager, config);
			}
			// 外部ファイル実行時（@returnで通常側に戻る）
			else{
				subscenario->setNextTag(subscenario->decodeScript(input, this, config, message));
				loop_flg = subscenario->executeTag(input, this, message, g_manager, scenario, s_manager, config);
			}
		} while (loop_flg);
	}

	//メッセージ描画中にクリックしたら全文表示
	if (message->getRenderingFlg() &&
		((input->getKeepMouseLeft() != 0 && !cursor_on_button) || (input->getKeyControl() != 0 || config->getSkipModeFlag())) &&
		!Teng::Window::getDialogDrawing() &&
		!message->plus_stopping)
	{
		message->drawSpeedUp();
	}
	//＋記号でストップしたあと、クリックしたら描画速度を元に戻す
	if (message->plus_stopping && ((!cursor_on_button && input->getKeepMouseLeft() != 0) || input->getKeyControl() != 0)){
		message->removeFirstPlus();
		message->plus_stopping = false;
		message->resetDrawSpeed(config);
	}
	else if (message->plus_stopping && checkAutoMsg(config, input)){
		message->removeFirstPlus();
		message->plus_stopping = false;
		message->resetDrawSpeed(config);
	}

	if (input->getKeyControl() != 0){
		//message->removeFirstPlus();
		//message->removeFirstPlus();
		//message->removeFirstPlus();
		//message->removeFirstPlus();
		//message->removeFirstPlus();
	}

	// ハイパースキップが停止した
	//if (is_before_frame_hyper && !is_hyper_skip){
	//if (scenario->isHyperSkipButtonPushed() && !is_hyper_skip){
	//	DEBUG printf("[INFO] ハイパースキップ停止\n");
	//	scenario->isHyperSkipButtonPushed(false);
	//	//ロード用？
	//	is_hyper_skip = true;
	//	DxLib::SetUseASyncLoadFlag(FALSE);
	//	SaveData::makeScreenShotShumbnail();
	//	Teng::SaveData *savedata1 = new Teng::SaveData;
	//	DEBUG printf("[INFO] ハイパースキップ：一時シナリオデータの保存開始\n");
	//	savedata1->save(99, scenario, g_manager, s_manager, window, message, this);
	//	DEBUG printf("[INFO] ハイパースキップ：一時シナリオデータの保存完了\n");
	//	delete savedata1;
	//	is_hyper_skip = false;
	//	Teng::SaveData *savedata2 = new Teng::SaveData;
	//	DEBUG printf("[INFO] ハイパースキップ：一時シナリオデータのロード開始\n");
	//	savedata2->load(99, scenario, message, g_manager, s_manager, window, config, this);
	//	DEBUG printf("[INFO] ハイパースキップ：一時シナリオデータのロード完了\n");
	//	delete savedata2;
	//	before_wait_frame = 0;
	//	DxLib::SetUseASyncLoadFlag(TRUE);

	//}

	//BGMのフェード
	s_manager->fadeBgm(config, this, input);

	//メッセージウィンドウのフェード
	message->windowFade();

	//動画用フェードチェック
	checkFadeForMovie(message, s_manager);

	//ホイール上でバックログ表示
	if (input->getKeepWheelUp() != 0){
		//now_scene = eScene_scenario_backlog;
		//before_scene = eScene_scenario;
		is_backlog_enable = true;
	}

	drawScreen(g_manager, scenario, message, config, input, s_manager, window);

	////右クリックでメニューへ
	//if (input->getKeepMouseRight() != 0 && !window->getDialogDrawing() && !is_backlog_enable && message->isMsgDrawCompleted()){
	//	//画面遷移前にスクリーンショットを取得しておく
	//	DxLib::SetUseASyncLoadFlag(FALSE);
	//	SaveData::makeScreenShotShumbnail();



	// ↓ scene_backlog側で実装した

	//	//DxLib::SetUseASyncLoadFlag(FALSE);
	//	//スクリーンショットを作成して、メニュー画面の背景にする
	//	screenshot_handle = DxLib::MakeGraph(1024, 768);
	//	DxLib::GetDrawScreenGraph(0, 0, 1024, 768, screenshot_handle);
	//	now_scene = eScene_scenario_menu;
	//	before_scene = eScene_scenario;
	//	DxLib::SetUseASyncLoadFlag(TRUE);
	//	sceneMenuInitialize(window, config, input, g_manager, s_manager);
	//	resetButtons();
	//	//シナリオに戻った時の復元用
	//	//resources->tmpData()->tmpScriptFileName(command_queue->getScriptFilename());
	//	//resources->tmpData()->tmpScenarioExist(true);

	//	//for (auto it = cmd_list.begin(); it != cmd_list.end(); it++){
	//	//	if (!(*it)->isExecuted()){
	//	//		it--;
	//	//		(*it)->isExecuted(false);
	//	//		break;
	//	//	}
	//	//}


	//}



	if (scenario->getChangeTitleFlag()){
		fade_completed = true;
		now_scene = eScene_title;
		before_scene = eScene_scenario_menu;
		frame = 0;
		resetTitleButtonFlag();
		scene_changing = true;

		// ここでデータ開放
		g_manager->freeCharacterImageAll();
		g_manager->freeFaceGraphicAll();
		g_manager->freeBackGroundGraphicAll();
		g_manager->freeRuleGraphicAll();
		s_manager->freeBgmAll();
		s_manager->freeSeAll();

		message->clear();

		delete scenario;
		scenario = new Teng::Scenario("k001");
		scenario->setIsImportScenario(false);
	}



	is_before_frame_hyper = is_hyper_skip;
}

