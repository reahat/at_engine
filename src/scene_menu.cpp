#include "scene.h"
#include "scenario.h"
#include "DxLib.h"
#include "global.h"
#include "common.h"
#include "savedata.h"
#include "error_code_define.h"
#include <fstream>
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")
#include <list>

#include <typeinfo>
#include <iostream>


extern char common_dir[256];
extern bool is_hyper_skip;
using namespace Teng;

void Teng::Scene::doScenarioMenu(
	Window* window,
	Config* config,
	Input* input,
	GraphicManager* g_manager,
	SoundManager* s_manager,
	Scenario* scenario,
	CmdMessage* message)
{
	input->checkKeyInput();

	static CmdMessage* gomi = new CmdMessage();
	drawScreen(g_manager, scenario, gomi, config, input, s_manager, window);

	if (fade_type != FadeType::no_fade){
		input->disableAllKeys();
	}
	mouse_left_clicked = (input->getKeepMouseLeft() != 0);
	mouse_right_clicked = (input->getKeepMouseRight() != 0);

	//シナリオ画面のスクリーンショット
	DxLib::DrawGraph(0, 0, screenshot_handle, FALSE);
	//暗くする
	DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, back_alpha);
	DxLib::DrawBox(0, 0, 1024, 768, back_color, TRUE);
	//DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	//メニューベース
	if (fade_type == FadeType::fadein && frame <= 15){
		int pow = 255 / 20 * (frame - 1);
		DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, pow);
		// 移動後の位置がx=20なので、15フレームかけて画面外から20まで移動する必要がある
		// frame * n - d = 20 になるようにすること
		DxLib::DrawGraph(frame * 6 - 70, 0, menu_base, TRUE);
		//DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	}
	else if (fade_type == FadeType::fadeout && tmp_next_scene == SceneType::scenario){
		int pow = frame * 12;
		int alpha = 255 - frame * 12;
		DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha);
		DxLib::DrawGraph(20 - pow, 0, menu_base, TRUE);
		//DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	}
	else{
		DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		DxLib::DrawGraph(20, 0, menu_base, TRUE);
	}

	//ボタン
	// xの最終地点は54
	if (fade_type == FadeType::fadein){
		if (frame > 8){
			if (btn_save.x < 54){
				btn_save.x += 8;
			}
			btn_save.power += 12;
			if (btn_save.power < 100){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, btn_save.power * 3);
				DxLib::DrawGraph(btn_save.x, btn_save.y, btn_save.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, btn_save.power * 3);
				DxLib::DrawGraph(btn_save.x, btn_save.y, btn_save.normal, TRUE);
			}
			else if (btn_save.power < 200){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_save.power);
				DxLib::DrawGraph(btn_save.x, btn_save.y, btn_save.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 255);
				DxLib::DrawGraph(btn_save.x, btn_save.y, btn_save.normal, TRUE);
			}
			else if (btn_save.power < 255){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_save.power - 10);
				DxLib::DrawGraph(btn_save.x, btn_save.y, btn_save.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 240);
				DxLib::DrawGraph(btn_save.x, btn_save.y, btn_save.normal, TRUE);
			}
			else{
				//DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
				DxLib::DrawGraph(btn_save.x, btn_save.y, btn_save.normal, TRUE);
			}
		}
		if (frame > 11){
			if (btn_load.x < 54){
				btn_load.x += 8;
			}
			btn_load.power += 12;
			if (btn_load.power < 100){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, btn_load.power * 3);
				DxLib::DrawGraph(btn_load.x, btn_load.y, btn_load.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, btn_load.power * 3);
				DxLib::DrawGraph(btn_load.x, btn_load.y, btn_load.normal, TRUE);
			}
			else if (btn_load.power < 200){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_load.power);
				DxLib::DrawGraph(btn_load.x, btn_load.y, btn_load.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 255);
				DxLib::DrawGraph(btn_load.x, btn_load.y, btn_load.normal, TRUE);
			}
			else if (btn_load.power < 255){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_load.power - 10);
				DxLib::DrawGraph(btn_load.x, btn_load.y, btn_load.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 240);
				DxLib::DrawGraph(btn_load.x, btn_load.y, btn_load.normal, TRUE);
			}
			else{
				//DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
				DxLib::DrawGraph(btn_load.x, btn_load.y, btn_load.normal, TRUE);
			}
		}
		if (frame > 14){
			if (btn_hskip.x < 54){
				btn_hskip.x += 8;
			}
			btn_hskip.power += 12;
			if (btn_hskip.power < 100){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, btn_hskip.power * 3);
				DxLib::DrawGraph(btn_hskip.x, btn_hskip.y, btn_hskip.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, btn_hskip.power * 3);
				DxLib::DrawGraph(btn_hskip.x, btn_hskip.y, btn_hskip.normal, TRUE);
			}
			else if (btn_hskip.power < 200){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_hskip.power);
				DxLib::DrawGraph(btn_hskip.x, btn_hskip.y, btn_hskip.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 255);
				DxLib::DrawGraph(btn_hskip.x, btn_hskip.y, btn_hskip.normal, TRUE);
			}
			else if (btn_hskip.power < 255){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_hskip.power - 10);
				DxLib::DrawGraph(btn_hskip.x, btn_hskip.y, btn_hskip.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 240);
				DxLib::DrawGraph(btn_hskip.x, btn_hskip.y, btn_hskip.normal, TRUE);
			}
			else{
				//DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
				DxLib::DrawGraph(btn_hskip.x, btn_hskip.y, btn_hskip.normal, TRUE);
			}
		}
		if (frame > 17){
			if (btn_config.x < 54){
				btn_config.x += 8;
			}
			btn_config.power += 12;
			if (btn_config.power < 100){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, btn_config.power * 3);
				DxLib::DrawGraph(btn_config.x, btn_config.y, btn_config.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, btn_config.power * 3);
				DxLib::DrawGraph(btn_config.x, btn_config.y, btn_config.normal, TRUE);
			}
			else if (btn_config.power < 200){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_config.power);
				DxLib::DrawGraph(btn_config.x, btn_config.y, btn_config.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 255);
				DxLib::DrawGraph(btn_config.x, btn_config.y, btn_config.normal, TRUE);
			}
			else if (btn_config.power < 255){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_config.power - 10);
				DxLib::DrawGraph(btn_config.x, btn_config.y, btn_config.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 240);
				DxLib::DrawGraph(btn_config.x, btn_config.y, btn_config.normal, TRUE);
			}
			else{
				//DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
				DxLib::DrawGraph(btn_config.x, btn_config.y, btn_config.normal, TRUE);
			}
		}
		if (frame > 20){
			if (btn_backlog.x < 54){
				btn_backlog.x += 8;
			}
			btn_backlog.power += 12;
			if (btn_backlog.power < 100){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, btn_backlog.power * 3);
				DxLib::DrawGraph(btn_backlog.x, btn_backlog.y, btn_backlog.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, btn_backlog.power * 3);
				DxLib::DrawGraph(btn_backlog.x, btn_backlog.y, btn_backlog.normal, TRUE);
			}
			else if (btn_backlog.power < 200){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_backlog.power);
				DxLib::DrawGraph(btn_backlog.x, btn_backlog.y, btn_backlog.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 255);
				DxLib::DrawGraph(btn_backlog.x, btn_backlog.y, btn_backlog.normal, TRUE);
			}
			else if (btn_backlog.power < 255){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_backlog.power - 10);
				DxLib::DrawGraph(btn_backlog.x, btn_backlog.y, btn_backlog.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 240);
				DxLib::DrawGraph(btn_backlog.x, btn_backlog.y, btn_backlog.normal, TRUE);
			}
			else{
				//DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
				DxLib::DrawGraph(btn_backlog.x, btn_backlog.y, btn_backlog.normal, TRUE);
			}
		}
		if (frame > 23){
			if (btn_title.x < 54){
				btn_title.x += 8;
			}
			btn_title.power += 12;
			if (btn_title.power < 100){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, btn_title.power * 3);
				DxLib::DrawGraph(btn_title.x, btn_title.y, btn_title.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, btn_title.power * 3);
				DxLib::DrawGraph(btn_title.x, btn_title.y, btn_title.normal, TRUE);
			}
			else if (btn_title.power < 200){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_title.power);
				DxLib::DrawGraph(btn_title.x, btn_title.y, btn_title.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 255);
				DxLib::DrawGraph(btn_title.x, btn_title.y, btn_title.normal, TRUE);
			}
			else if (btn_title.power < 255){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_title.power - 10);
				DxLib::DrawGraph(btn_title.x, btn_title.y, btn_title.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 240);
				DxLib::DrawGraph(btn_title.x, btn_title.y, btn_title.normal, TRUE);
			}
			else{
				//DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
				DxLib::DrawGraph(btn_title.x, btn_title.y, btn_title.normal, TRUE);
			}
		}
		if (frame > 26){
			if (btn_exit.x < 54){
				btn_exit.x += 8;
			}
			btn_exit.power += 12;
			if (btn_exit.power < 100){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, btn_exit.power * 3);
				DxLib::DrawGraph(btn_exit.x, btn_exit.y, btn_exit.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, btn_exit.power * 3);
				DxLib::DrawGraph(btn_exit.x, btn_exit.y, btn_exit.normal, TRUE);
			}
			else if (btn_exit.power < 200){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_exit.power);
				DxLib::DrawGraph(btn_exit.x, btn_exit.y, btn_exit.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 255);
				DxLib::DrawGraph(btn_exit.x, btn_exit.y, btn_exit.normal, TRUE);
			}
			else if (btn_exit.power < 255){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_exit.power - 10);
				DxLib::DrawGraph(btn_exit.x, btn_exit.y, btn_exit.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 240);
				DxLib::DrawGraph(btn_exit.x, btn_exit.y, btn_exit.normal, TRUE);
			}
			else{
				//DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
				DxLib::DrawGraph(btn_exit.x, btn_exit.y, btn_exit.normal, TRUE);
			}
		}
		if (frame > 29){
			if (btn_back.x < 54){
				btn_back.x += 8;
			}
			btn_back.power += 12;
			if (btn_back.power < 100){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, btn_back.power * 3);
				DxLib::DrawGraph(btn_back.x, btn_back.y, btn_back.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, btn_back.power * 3);
				DxLib::DrawGraph(btn_back.x, btn_back.y, btn_back.normal, TRUE);
			}
			else if (btn_back.power < 200){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_back.power);
				DxLib::DrawGraph(btn_back.x, btn_back.y, btn_back.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 255);
				DxLib::DrawGraph(btn_back.x, btn_back.y, btn_back.normal, TRUE);
			}
			else if (btn_back.power < 255){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_back.power - 10);
				DxLib::DrawGraph(btn_back.x, btn_back.y, btn_back.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 230);
				DxLib::DrawGraph(btn_back.x, btn_back.y, btn_back.normal, TRUE);
			}
			else{
				//DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
				DxLib::DrawGraph(btn_back.x, btn_back.y, btn_back.normal, TRUE);
				fade_type = FadeType::no_fade;
			}
		}
		DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	}

	if (fade_type == FadeType::no_fade){
		if (!button_clicked) resetButtons();

		if (!window->getDialogDrawing() && !is_backlog_enable){
			if (input->cursorIn(btn_save.x, btn_save.x + btn_save.width, btn_save.y, btn_save.y + btn_save.height) && !button_clicked){
				btn_save.now = btn_save.focus;
				if (!cursor_on_button){
					cursor_on_button = true;
					DxLib::PlaySoundMem(s_manager->system_sound[SystemSound::arrnum_se_button_title_focus].getHandle(), DX_PLAYTYPE_BACK);
				}
				if (mouse_left_clicked){
					btn_save.now = btn_save.focus;
					if (mouse_left_clicked){
						button_clicked = true;
						btn_save.now = btn_save.click;
						tmp_next_scene = SceneType::save;
						fade_type = FadeType::fadeout;
						frame = 0;
					}
				}
			}
			else if (input->cursorIn(btn_load.x, btn_load.x + btn_load.width, btn_load.y, btn_load.y + btn_load.height) && !button_clicked){
				btn_load.now = btn_load.focus;
				if (!cursor_on_button){
					cursor_on_button = true;
					DxLib::PlaySoundMem(s_manager->system_sound[SystemSound::arrnum_se_button_title_focus].getHandle(), DX_PLAYTYPE_BACK);
				}
				if (mouse_left_clicked){
					btn_load.now = btn_load.focus;
					if (mouse_left_clicked){
						button_clicked = true;
						btn_load.now = btn_load.click;
						tmp_next_scene = SceneType::scenario_load;
						fade_type = FadeType::fadeout;
						frame = 0;
					}
				}
			}
			else if (input->cursorIn(btn_hskip.x, btn_hskip.x + btn_hskip.width, btn_hskip.y, btn_hskip.y + btn_hskip.height) && !button_clicked){
				//btn_hskip.now = btn_hskip.focus;
				//if (input->mouseLeftClicked()){
				//	button_clicked = true;
				//	btn_hskip.now = btn_hskip.click;
				//}
			}
			else if (input->cursorIn(btn_config.x, btn_config.x + btn_config.width, btn_config.y, btn_config.y + btn_config.height) && !button_clicked){
				btn_config.now = btn_config.focus;
				if (!cursor_on_button){
					cursor_on_button = true;
					DxLib::PlaySoundMem(s_manager->system_sound[SystemSound::arrnum_se_button_title_focus].getHandle(), DX_PLAYTYPE_BACK);
				}
				if (mouse_left_clicked){
					button_clicked = true;
					btn_config.now = btn_config.click;
					tmp_next_scene = SceneType::scenario_config;
					fade_type = FadeType::fadeout;
					frame = 0;
					//sceneConfigInitialize(window, config, input, g_manager, s_manager);
				}
			}
			else if (input->cursorIn(btn_backlog.x, btn_backlog.x + btn_backlog.width, btn_backlog.y, btn_backlog.y + btn_backlog.height) && !button_clicked){
				btn_backlog.now = btn_backlog.focus;
				if (!cursor_on_button){
					cursor_on_button = true;
					DxLib::PlaySoundMem(s_manager->system_sound[SystemSound::arrnum_se_button_title_focus].getHandle(), DX_PLAYTYPE_BACK);
				}
				if (mouse_left_clicked){
					//button_clicked = true;
					//btn_backlog.now = btn_backlog.click;
					is_backlog_enable = true;
				}
			}
			else if (input->cursorIn(btn_title.x, btn_title.x + btn_title.width, btn_title.y, btn_title.y + btn_title.height) && !button_clicked){
				btn_title.now = btn_title.focus;
				if (!cursor_on_button){
					cursor_on_button = true;
					DxLib::PlaySoundMem(s_manager->system_sound[SystemSound::arrnum_se_button_title_focus].getHandle(), DX_PLAYTYPE_BACK);
				}
				if (mouse_left_clicked){
					button_clicked = true;
					btn_title.now = btn_title.click;
					tmp_next_scene = SceneType::title;
					fade_type = FadeType::fadeout;
					frame = 0;
				}
			}
			else if (input->cursorIn(btn_back.x, btn_back.x + btn_back.width, btn_back.y, btn_back.y + btn_back.height) && !button_clicked){
				btn_back.now = btn_back.focus;
				if (!cursor_on_button){
					cursor_on_button = true;
					DxLib::PlaySoundMem(s_manager->system_sound[SystemSound::arrnum_se_button_title_focus].getHandle(), DX_PLAYTYPE_BACK);
				}
				if (mouse_left_clicked){
					button_clicked = true;
					btn_back.now = btn_back.click;
					fade_type = FadeType::fadeout;
					tmp_next_scene = SceneType::scenario;
					frame = 0;
				}
			}
			else if (input->cursorIn(btn_exit.x, btn_exit.x + btn_exit.width, btn_exit.y, btn_exit.y + btn_exit.height) && !button_clicked){
				btn_exit.now = btn_exit.focus;
				if (!cursor_on_button){
					cursor_on_button = true;
					DxLib::PlaySoundMem(s_manager->system_sound[SystemSound::arrnum_se_button_title_focus].getHandle(), DX_PLAYTYPE_BACK);
				}
				if (mouse_left_clicked){
					//button_clicked = true;
					//btn_exit.now = btn_exit.click;
					//DEBUG printf("システムを終了します。");
					//throw ENGINE_SYSTEM_STANDARD_EXIT;
					window->setDialogDrawing(true);
				}
			}
		}

		DxLib::DrawGraph(btn_save.x, btn_save.y, btn_save.now, TRUE);
		DxLib::DrawGraph(btn_load.x, btn_load.y, btn_load.now, TRUE);
		DxLib::DrawGraph(btn_hskip.x, btn_hskip.y, btn_hskip.now, TRUE);
		DxLib::DrawGraph(btn_config.x, btn_config.y, btn_config.now, TRUE);
		DxLib::DrawGraph(btn_backlog.x, btn_backlog.y, btn_backlog.now, TRUE);
		DxLib::DrawGraph(btn_title.x, btn_title.y, btn_title.now, TRUE);
		DxLib::DrawGraph(btn_exit.x, btn_exit.y, btn_exit.now, TRUE);
		DxLib::DrawGraph(btn_back.x, btn_back.y, btn_back.now, TRUE);
	}

	static const auto calc = [](int& x){return x -= 12; };
	if (fade_type == FadeType::fadeout && tmp_next_scene == SceneType::scenario){
		int alpha = 255 - frame * 14;
		DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha);
		DxLib::DrawGraph(calc(btn_save.x), btn_save.y, btn_save.now, TRUE);
		DxLib::DrawGraph(calc(btn_load.x), btn_load.y, btn_load.now, TRUE);
		DxLib::DrawGraph(calc(btn_hskip.x), btn_hskip.y, btn_hskip.now, TRUE);
		DxLib::DrawGraph(calc(btn_config.x), btn_config.y, btn_config.now, TRUE);
		DxLib::DrawGraph(calc(btn_backlog.x), btn_backlog.y, btn_backlog.now, TRUE);
		DxLib::DrawGraph(calc(btn_title.x), btn_title.y, btn_title.now, TRUE);
		DxLib::DrawGraph(calc(btn_exit.x), btn_exit.y, btn_exit.now, TRUE);
		DxLib::DrawGraph(calc(btn_back.x), btn_back.y, btn_back.now, TRUE);
		DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	}
	else if (fade_type == FadeType::fadeout && tmp_next_scene == SceneType::scenario_config){
		DxLib::DrawGraph(btn_save.x, btn_save.y, btn_save.now, TRUE);
		DxLib::DrawGraph(btn_load.x, btn_load.y, btn_load.now, TRUE);
		DxLib::DrawGraph(btn_hskip.x, btn_hskip.y, btn_hskip.now, TRUE);
		DxLib::DrawGraph(btn_config.x, btn_config.y, btn_config.now, TRUE);
		DxLib::DrawGraph(btn_backlog.x, btn_backlog.y, btn_backlog.now, TRUE);
		DxLib::DrawGraph(btn_title.x, btn_title.y, btn_title.now, TRUE);
		DxLib::DrawGraph(btn_exit.x, btn_exit.y, btn_exit.now, TRUE);
		DxLib::DrawGraph(btn_back.x, btn_back.y, btn_back.now, TRUE);
	}
	else if (fade_type == FadeType::fadeout && (
		tmp_next_scene == SceneType::save ||
		tmp_next_scene == SceneType::scenario_load))
	{
		DxLib::DrawGraph(btn_save.x, btn_save.y, btn_save.now, TRUE);
		DxLib::DrawGraph(btn_load.x, btn_load.y, btn_load.now, TRUE);
		DxLib::DrawGraph(btn_hskip.x, btn_hskip.y, btn_hskip.now, TRUE);
		DxLib::DrawGraph(btn_config.x, btn_config.y, btn_config.now, TRUE);
		DxLib::DrawGraph(btn_backlog.x, btn_backlog.y, btn_backlog.now, TRUE);
		DxLib::DrawGraph(btn_title.x, btn_title.y, btn_title.now, TRUE);
		DxLib::DrawGraph(btn_exit.x, btn_exit.y, btn_exit.now, TRUE);
		DxLib::DrawGraph(btn_back.x, btn_back.y, btn_back.now, TRUE);
	}


	if (btn_save.now == btn_save.normal &&
		btn_load.now == btn_load.normal &&
		btn_hskip.now == btn_hskip.normal &&
		btn_config.now == btn_config.normal &&
		btn_backlog.now == btn_backlog.normal &&
		btn_title.now == btn_title.normal &&
		btn_exit.now == btn_exit.normal &&
		btn_back.now == btn_back.normal)
	{
		cursor_on_button = false;
	}




	/****************** exec fade ***********************/

	////画面開始時
	if (fade_type == FadeType::fadein){

		int f = static_cast<int>(frame)* 15; // 15はフェード速度
		// 255 * 60% = 153
		if (f < 153){
			back_alpha = f;
		}
		else{
			if (fade_type == FadeType::fadein){
				back_alpha = 153;
			}
		}
		if (frame >= 60){
			//fade_type = FadeType::no_fade;
		}
	}

	// シナリオに戻る
	if (fade_type == FadeType::fadeout && tmp_next_scene == SceneType::scenario){
		int f = static_cast<int>(frame)* 2;
		back_alpha -= f;
		if (back_alpha <= 0){
			//next_scene = SceneType::scenario;
			now_scene = eScene_scenario;
			before_scene = eScene_scenario_menu;
			////resources->tmpData()->tmpBeforeScene(SceneType::menu);
		}
	}
	// ロード画面へ
	else if (fade_type == FadeType::fadeout && tmp_next_scene == SceneType::scenario_load){
		int f = 255 - static_cast<int>(frame)* 10;
		if (f > 0){
			DxLib::SetDrawBright(f, f, f);
		}
		else{
			if (fade_type == FadeType::fadeout){
				fade_completed = true;
				now_scene = eScene_scenario;
				before_scene = eScene_scenario;
				resetTitleButtonFlag();
				scene_changing = true;
				process_status = 1;
				decodeSavedataThumbnail1(window, config, input, g_manager, s_manager);
				clicked_button = eScene_scenario_load;
			}
		}
	}
	// セーブ画面へ
	else if (fade_type == FadeType::fadeout && tmp_next_scene == SceneType::save){
		int f = 255 - static_cast<int>(frame)* 10;
		if (f > 0){
			DxLib::SetDrawBright(f, f, f);
		}
		else{
			if (fade_type == FadeType::fadeout){
				fade_completed = true;
				now_scene = eScene_scenario;
				before_scene = eScene_scenario;
				resetTitleButtonFlag();
				scene_changing = true;
				process_status = 1;
				decodeSavedataThumbnail1(window, config, input, g_manager, s_manager);
				clicked_button = eScene_scenario_save;
			}
		}
	}
	// コンフィグに遷移
	else if (fade_type == FadeType::fadeout && tmp_next_scene == SceneType::scenario_config){
		int f = 255 - static_cast<int>(frame)* 10;
		if (f > 0){
			DxLib::SetDrawBright(f, f, f);
		}
		else{
			if (fade_type == FadeType::fadeout){
				fade_completed = true;
				now_scene = SceneType::scenario_config;
				////resources->tmpData()->tmpBeforeScene(SceneType::scenario);
				//fade_type == FadeType::fadein;
				sceneConfigInitialize(window, config, input, g_manager, s_manager);
			}
		}
	}
	// タイトルへ
	else if (fade_type == FadeType::fadeout && tmp_next_scene == SceneType::title){
		int f = 255 - static_cast<int>(frame)* 10;
		if (f > 0){
			DxLib::SetDrawBright(f, f, f);
		}
		else{
			if (fade_type == FadeType::fadeout){
				fade_completed = true;
				now_scene = eScene_title;
				before_scene = eScene_scenario_menu;
				frame = 0;
				resetTitleButtonFlag();
				scene_changing = true;

				// ここでデータ開放
				g_manager->freeCharacterImageAll();
				g_manager->freeFaceGraphicAll();
				g_manager->freeBackGroundGraphicAll();
				g_manager->freeRuleGraphicAll();
				s_manager->freeBgmAll();
				s_manager->freeSeAll();

				//scenario->setCurrentLine(0);
				//scenario->setFlagResetFlg(true);
				//scenario->setIsImportScenario(false);
				//scenario->setScenarioFile("scenario1");
				//scenario->setNextTag(-1);

				//scenario->clearLove();
				message->clear();
				
				delete scenario;
				//scenario = new Teng::Scenario("myscenario");
				scenario = new Teng::Scenario("k001");
				scenario->setIsImportScenario(false);
				
			}
		}
	}




















	//右クリックで前画面に戻る
	if (input->getKeepMouseRight() != 0 && !is_backlog_enable){
		fade_type = FadeType::fadeout;
		frame = 0;
		tmp_next_scene = SceneType::scenario;
		//now_scene = eScene_scenario;
		//before_scene = eScene_scenario_menu;
		before_scene = eScene_scenario;
	}

}

void Scene::sceneMenuInitialize(
	Window* window,
	Config* config,
	Input* input,
	GraphicManager* g_manager,
	SoundManager* s_manager)
{
	frame = 0;
	fade_type = FadeType::fadein;
	next_scene = SceneType::no_change;
	back_alpha = 0;
	button_clicked = false;
	tmp_next_scene = SceneType::no_change;
	fade_completed = false;

	// 背景色 #1b0805 透過60%
	back_color = DxLib::GetColor(27, 8, 5);



	btn_save = Btn();
	btn_load = Btn();
	btn_hskip = Btn();
	btn_config = Btn();
	btn_backlog = Btn();
	btn_title = Btn();
	btn_exit = Btn();
	btn_back = Btn();



	btn_save.now = btn_save.normal = g_manager->system_graphics[SystemGraphic::arrnum_menu_save_normal].getHandle();
	DxLib::GetGraphSize(btn_save.normal, &btn_save.width, &btn_save.height);
	btn_save.y = 168;
	btn_load.now = btn_load.normal = g_manager->system_graphics[SystemGraphic::arrnum_menu_load_normal].getHandle();
	DxLib::GetGraphSize(btn_load.normal, &btn_load.width, &btn_load.height);
	btn_load.y = 218;
	btn_hskip.now = btn_hskip.normal = g_manager->system_graphics[SystemGraphic::arrnum_menu_hskip_normal].getHandle();
	DxLib::GetGraphSize(btn_hskip.normal, &btn_hskip.width, &btn_hskip.height);
	btn_hskip.y = 266;
	btn_config.now = btn_config.normal = g_manager->system_graphics[SystemGraphic::arrnum_menu_config_normal].getHandle();
	DxLib::GetGraphSize(btn_config.normal, &btn_config.width, &btn_config.height);
	btn_config.y = 315;
	btn_backlog.now = btn_backlog.normal = g_manager->system_graphics[SystemGraphic::arrnum_menu_backlog_normal].getHandle();
	DxLib::GetGraphSize(btn_backlog.normal, &btn_backlog.width, &btn_backlog.height);
	btn_backlog.y = 365;
	btn_title.now = btn_title.normal = g_manager->system_graphics[SystemGraphic::arrnum_menu_title_normal].getHandle();
	DxLib::GetGraphSize(btn_title.normal, &btn_title.width, &btn_title.height);
	btn_title.y = 420;
	btn_exit.now = btn_exit.normal = g_manager->system_graphics[SystemGraphic::arrnum_menu_exit_normal].getHandle();
	DxLib::GetGraphSize(btn_exit.normal, &btn_exit.width, &btn_exit.height);
	btn_exit.y = 470;
	btn_back.now = btn_back.normal = g_manager->system_graphics[SystemGraphic::arrnum_menu_back_normal].getHandle();
	DxLib::GetGraphSize(btn_back.normal, &btn_back.width, &btn_back.height);
	btn_back.y = 520;

	btn_save.focus = g_manager->system_graphics[SystemGraphic::arrnum_menu_save_focus].getHandle();
	btn_load.focus = g_manager->system_graphics[SystemGraphic::arrnum_menu_load_focus].getHandle();
	btn_hskip.focus = g_manager->system_graphics[SystemGraphic::arrnum_menu_hskip_focus].getHandle();
	btn_config.focus = g_manager->system_graphics[SystemGraphic::arrnum_menu_config_focus].getHandle();
	btn_backlog.focus = g_manager->system_graphics[SystemGraphic::arrnum_menu_backlog_focus].getHandle();
	btn_title.focus = g_manager->system_graphics[SystemGraphic::arrnum_menu_title_focus].getHandle();
	btn_exit.focus = g_manager->system_graphics[SystemGraphic::arrnum_menu_exit_focus].getHandle();
	btn_back.focus = g_manager->system_graphics[SystemGraphic::arrnum_menu_back_focus].getHandle();
	btn_save.click = g_manager->system_graphics[SystemGraphic::arrnum_menu_save_click].getHandle();
	btn_load.click = g_manager->system_graphics[SystemGraphic::arrnum_menu_load_click].getHandle();
	btn_hskip.click = g_manager->system_graphics[SystemGraphic::arrnum_menu_hskip_click].getHandle();
	btn_config.click = g_manager->system_graphics[SystemGraphic::arrnum_menu_config_click].getHandle();
	btn_backlog.click = g_manager->system_graphics[SystemGraphic::arrnum_menu_backlog_click].getHandle();
	btn_title.click = g_manager->system_graphics[SystemGraphic::arrnum_menu_title_click].getHandle();
	btn_exit.click = g_manager->system_graphics[SystemGraphic::arrnum_menu_exit_click].getHandle();
	btn_back.click = g_manager->system_graphics[SystemGraphic::arrnum_menu_back_click].getHandle();
	menu_base = g_manager->system_graphics[SystemGraphic::arrnum_menu_base].getHandle();

}


void Scene::resetButtons(){
	btn_save.now = btn_save.normal;
	btn_load.now = btn_load.normal;
	btn_hskip.now = btn_hskip.normal;
	btn_config.now = btn_config.normal;
	btn_backlog.now = btn_backlog.normal;
	btn_title.now = btn_title.normal;
	btn_exit.now = btn_exit.normal;
	btn_back.now = btn_back.normal;
}