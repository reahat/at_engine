﻿#ifndef CLASS_HEADER_INCLUDED
#define CLASS_HEADER_INCLUDED

#include "DxLib.h"
#include "define.h"
#include "window.h"
#include "config.h"
#include "input.h"
#include "graphic.h"
#include "scene.h"
#include "scenario.h"
#include "sound\sound_manager.h"
#include "tag\cmd_message.h"

#endif //CLASS_HEADER_INCLUDED