﻿#include "graphic.h"
#include "define.h"
#include "global.h"
#include "error_code_define.h"
#include <Windows.h>
#include <iostream>
#include <time.h>
#include <sstream>

#include "savedata.h"

using namespace Teng;

int SaveData::thumbnail_handle[TENG_SAVE_SAVEDATA_MAX];
bool SaveData::savedata_exist[TENG_SAVE_SAVEDATA_MAX];
//int SaveData::keep_load_number = -1;

SaveData::SaveData(bool initialize){
	if (initialize){
		for (int i = 0; i < TENG_SAVE_SAVEDATA_MAX; i++){
			thumbnail_handle[i] = -1;
		}
	}
};
SaveData::~SaveData(){};


int SaveData::save(int id, Scenario* scenario, GraphicManager* graphic_manager, SoundManager* sound_manager, Window* wnd, CmdMessage* message, Scene* scene){
	save_struct.id = id;
	//Scenario系データ
	save_struct.is_import_scenario = scenario->getIsImportScenario();
	strcpy_s(save_struct.scenario_file, TENG_FILE_NAME_MAX, scenario->getScenarioFile());
	save_struct.current_line = scenario->getCurrentLine();
	if (message->isDrawSelect() || message->isDrawNameEntry()){
		save_struct.current_line += 1;
		save_struct.is_selection_drawing = true;

		vector<Select> selects = message->getSelects();
		int i = 0;
		for (auto iter = selects.begin(); iter != selects.end(); iter++){
			strcpy_s(save_struct.select_msg[i], 100, selects[i].getMsg().c_str());
			strcpy_s(save_struct.select_label[i], 100, selects[i].getLabel().c_str());
			i++;
		}
		while (i < 5){
			save_struct.select_msg[i][0] = NC;
			save_struct.select_label[i][0] = NC;
			i++;
		}
	}
	else{
		save_struct.is_selection_drawing = false;
	}
	//好感度保存
	memcpy(save_struct.character_loves, scenario->backupLove(), sizeof(save_struct.character_loves));

	//キャラ名保存
	strcpy_s(save_struct.character_name, message->getCharacterName().c_str());

	//GraphicManagerデータ
	//オブジェクトを無理矢理書き出す
	save_struct.g_manager = *graphic_manager;

	save_struct.s_manager = *sound_manager;

	//フェイス画像のvisible_arrnumに対応するファイル名をセーブデータに保存
	if (Teng::FaceGraphic::visible_arrnum >= 0){
		strcpy_s(
			save_struct.visible_facename,
			TENG_FILE_NAME_MAX,
			graphic_manager->face_graphics[Teng::FaceGraphic::visible_arrnum].getFileName()
			);
	}
	else{
		strcpy_s(
			save_struct.visible_facename,
			TENG_FILE_NAME_MAX,
			""
			);
	}


	//背景のトラン用配列番号
	save_struct.bg_after_trans_arrnum = Teng::BackGroundGraphic::after_trans_arrnum;
	save_struct.bg_before_trans_arrnum = Teng::BackGroundGraphic::before_trans_arrnum;
	//ルール使用番号
	save_struct.rule_using_arrnum = Teng::RuleGraphic::using_arrnum;

	//ウィンドウタイトル
	strcpy_s(save_struct.window_title, 128, scene->getWindowTitle());

	//表示済の最終メッセージ表示
	list<BacklogMessage> logs = message->getLog().logs();
	auto it = logs.end();
	it--;
	list<string> msgs = *it;
	auto it_str = msgs.begin();
	int i = 0;
	for (int j = 0; j < 3; j++){
		save_struct.messages[j][0] = 0;
	}
	for (auto it_str = msgs.begin(); it_str != msgs.end(); it_str++){
		strcpy_s(save_struct.messages[i++], it_str->c_str());
	}

	// 時刻保存
	time_t timer;
	struct tm t_st;
	(void)time(&timer);
	localtime_s(&t_st, &timer);
	char *wday[] = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };
	int y = t_st.tm_year;
	sprintf_s(save_struct.date, "%d.%d.%d %s. %d:%02d",
		t_st.tm_year + 1900,
		t_st.tm_mon + 1,
		t_st.tm_mday,
		wday[t_st.tm_wday],
		t_st.tm_hour,
		t_st.tm_min);

	//スクリーンショット縮小に失敗するので、ここだけ非同期をOFFにする
	DxLib::SetUseASyncLoadFlag(FALSE);


	//スクリーンショット出力
	//makeScreenShotShumbnail();

	FILE *bmp;
	string tmp_path = string(Config::savedata_dir) + "tmp";
	//if(fopen_s(&bmp, "tmp", "rb") != 0){
	if(fopen_s(&bmp, tmp_path.c_str(), "rb") != 0){
		//TODO 例外出力
		DEBUG printf("[ERROR] スクリーンショット一時ファイルの読込に失敗しました。\n");
	}else{
		unsigned char *bmp_data = (unsigned char *)malloc(sizeof(char) * 100000);
		fread(bmp_data, sizeof(unsigned char), TENG_SAVE_SCREENSHOT_WIDTH*TENG_SAVE_SCREENSHOT_HEIGHT*3+10000, bmp);
		memcpy(save_struct.thumbnail, bmp_data, TENG_SAVE_SCREENSHOT_WIDTH*TENG_SAVE_SCREENSHOT_HEIGHT*3+10000);
		fclose(bmp);
		//remove("tmp");


		create(save_struct, scene);
		//free(bmp_data);
	}
	DxLib::SetUseASyncLoadFlag(TRUE);
	return 0;
}

errno_t SaveData::create(SaveDataStruct& strct, Scene* scene){

	FILE *fout;
	errno_t error;
	char filename[TENG_FILE_NAME_MAX];
	sprintf_s(filename, TENG_FILE_NAME_MAX, TENG_SAVE_FILENAME, save_struct.id);
	//ファイルの存在確認
	if((error = fopen_s(&fout, filename, "wb")) != 0){
		//エラー処理
		//ENOENT
		DEBUG printf("[ERROR] (SAVE) No such file or directory\n");
		throw ENGINE_SAVE_NO_SUCH_FILE_OR_DIRECTORY;

	}else{
		char* c_save = reinterpret_cast<char*>(&strct);
		for (int i = 0; i < sizeof(SaveDataStruct); i++){
			fprintf_s(fout, "%c", c_save[i]);// ^ TENG_SAVEDATA_PASSWORD);
		}
		fprintf_s(fout, TENG_VERSION);
		
		//fwrite(&strct, sizeof(SaveDataStruct), 1, fout);
		fclose(fout);
	}

	DEBUG printf("[INFO] (SAVE) セーブデータを出力しました。\n");

	return error;
}

errno_t SaveData::load(int num, Scenario* scenario, CmdMessage* msg, GraphicManager* graphic_manager, SoundManager* sound_manager, Window* wnd, Config* config, Scene* scene){
	FILE *fin;
	errno_t error;
	char filename[TENG_FILE_NAME_MAX];
	sprintf_s(filename, TENG_FILE_NAME_MAX, TENG_SAVE_FILENAME, num);

	//セーブデータを構造体に移す
	if((error = fopen_s(&fin, filename, "rb")) != 0){
		DEBUG printf("[ERROR] (LOAD) No such file or directory\n");//たぶんENOENT
		throw ENGINE_SAVE_NO_SUCH_FILE_OR_DIRECTORY;

	}else{
		fread_s(&save_struct, sizeof(SaveDataStruct), sizeof(SaveDataStruct), 1, fin);
		fclose(fin);


		char* c_save = reinterpret_cast<char*>(&save_struct);
		

		for (int i = 0; i < sizeof(SaveDataStruct); i++){
			//c_save[i] = c_save[i] ^ TENG_SAVEDATA_PASSWORD;
		}
		memcpy(&save_struct, c_save, sizeof(SaveDataStruct));

	}
	//構造体をメンバ変数に移す
	id = save_struct.id;
	strcpy_s(scenario_file, TENG_FILE_NAME_MAX, save_struct.scenario_file);
	is_import_scenario = save_struct.is_import_scenario;
	current_line = save_struct.current_line;

	//セーブデータのシナリオファイル名を元に、Scenarioオブジェクトを作り直す
	Scenario *load_scenario = new Scenario(scenario_file);
	*scenario = *load_scenario;
	delete load_scenario;

	//Scenarioオブジェクトにロードした情報をコピー
	scenario->setScenarioFile(scenario_file);
	scenario->setIsImportScenario(is_import_scenario);
	scenario->setCurrentLine(current_line-0);

	//表示メッセージを再度ロードする
	msg->clear();
	if(save_struct.is_selection_drawing) current_line--;
	scenario->splitString(current_line-1);
	if(string(scenario->getCommandZero()) == "@say"){
		msg->setDrawMessage(scenario->getCommandTwo(), true, string(scenario->getCommandOne()));
	}else{
		//msg->setDrawMessage(scenario->getCommandOne(), false);
		msg->setExMessage(scenario->getCommandOne());
	}

	
	DEBUG printf("[INFO] (LOAD) シナリオの復元が完了しました。\n");

	//CharacterGraphic
	graphic_manager->freeCharacterImageAll();
	for(int i = 0; i < TENG_GRAPHIC_CHARACTER_MAX; i++){
		if(save_struct.g_manager.character_graphics[i].getHandle() != 0){
			graphic_manager->loadCharacterGraphic(save_struct.g_manager.character_graphics[i].getFileName());
			//ファイル名とハンドル以外は手動で復元する
			int x = save_struct.g_manager.character_graphics[i].getX();
			int y = save_struct.g_manager.character_graphics[i].getY();
			int z = save_struct.g_manager.character_graphics[i].getZ();
			graphic_manager->character_graphics[i].setX(x);
			graphic_manager->character_graphics[i].setY(y);
			graphic_manager->character_graphics[i].setZ(z);
			bool visible = save_struct.g_manager.character_graphics[i].getVisible();
			graphic_manager->character_graphics[i].setVisible(visible);
			bool changing = save_struct.g_manager.character_graphics[i].getChanging();
			graphic_manager->character_graphics[i].setChanging(changing);
			int trans_type = save_struct.g_manager.character_graphics[i].getTransitionType();
			graphic_manager->character_graphics[i].setTransitionType(trans_type);
			int trans_speed = save_struct.g_manager.character_graphics[i].getTransitionSpeed();
			graphic_manager->character_graphics[i].setTransitionSpeed(trans_speed);
		}
	}
	DEBUG printf("[INFO] (LOAD) キャラクター画像の復元が完了しました。\n");

	//フェイス画像
	graphic_manager->freeFaceGraphicAll();
	for(int i = 0; i < TENG_GRAPHIC_FACE_MAX; i++){
		if(save_struct.g_manager.face_graphics[i].getHandle() != 0){
			graphic_manager->loadFaceGraphic(save_struct.g_manager.face_graphics[i].getFileName());
			graphic_manager->face_graphics[i].setImageType(Teng::Graphic::eGraphicType_face);
			graphic_manager->face_graphics[i].setX(TENG_GRAPHIC_FACE_X);
			graphic_manager->face_graphics[i].setY(TENG_GRAPHIC_FACE_Y);
			if(strcmp(graphic_manager->face_graphics[i].getFileName(), save_struct.visible_facename) == 0){
				Teng::FaceGraphic::visible_arrnum = i;
			}
		}
	}
	DEBUG printf("[INFO] (LOAD) フェイス画像の復元が完了しました。\n");

	//背景画像
	graphic_manager->freeBackGroundGraphicAll();
	Teng::BackGroundGraphic::before_trans_arrnum = save_struct.bg_before_trans_arrnum;
	Teng::BackGroundGraphic::after_trans_arrnum = save_struct.bg_after_trans_arrnum;

	for(int i = 0; i < TENG_GRAPHIC_BACKGROUND_MAX; i++){
		if(save_struct.g_manager.background_graphics[i].getHandle() != 0){
			graphic_manager->loadBackGroundGraphic(save_struct.g_manager.background_graphics[i].getFileName());
			graphic_manager->background_graphics[i].setImageType(Teng::Graphic::eGraphicType_background);
			bool after_tr = save_struct.g_manager.background_graphics[i].getIsAfterTrans();
			graphic_manager->background_graphics[i].setIsAfterTrans(after_tr);
			char* rulefile = save_struct.g_manager.background_graphics[i].getRuleFile();
			graphic_manager->background_graphics[i].setRuleFile(rulefile);
			int rulehandle = save_struct.g_manager.background_graphics[i].getRuleHandle();
			graphic_manager->background_graphics[i].setRuleHandle(rulehandle);
			bool visible = save_struct.g_manager.background_graphics[i].getVisible();
			graphic_manager->background_graphics[i].setVisible(visible);
			bool userule = save_struct.g_manager.background_graphics[i].getUseRule();
			graphic_manager->background_graphics[i].setUseRule(userule);
		}
	}
	DEBUG printf("[INFO] (LOAD) 背景画像の復元が完了しました。\n");

	//ルール画像
	graphic_manager->freeRuleGraphicAll();
	Teng::RuleGraphic::using_arrnum = save_struct.rule_using_arrnum;
	for(int i = 0; i < TENG_GRAPHIC_RULE_MAX; i++){
		if(save_struct.g_manager.rule_graphics[i].getHandle() != 0){
			char* rulefile = save_struct.g_manager.rule_graphics[i].getFileName();
			graphic_manager->loadRuleGraphic(rulefile);
			graphic_manager->rule_graphics[i].setImageType(Teng::Graphic::eGraphicType_rule);
		}
	}
	DEBUG printf("[INFO] (LOAD) ルール画像の復元が完了しました。\n");

	//BGM
	DxLib::SetUseASyncLoadFlag(FALSE);
	sound_manager->freeBgmAll();
	for(int i = 0; i < TENG_SOUND_BGM_MAX_COUNT; i++){

		if(save_struct.s_manager.bgm[i].getHandle() != 0){
			char* file = save_struct.s_manager.bgm[i].getFileName();
			sound_manager->loadBgm(file, save_struct.s_manager.bgm[i].getLoopPoint());
			short vol = save_struct.s_manager.bgm[i].getVolume();
			sound_manager->bgm[i].setVolume(vol);
			bool play = save_struct.s_manager.bgm[i].getPlaying();
			sound_manager->bgm[i].setPlaying(play);
			if(play){
				double volume =
					(double)config->getSetting().master_volume *
					(double)config->getSetting().bgm_volume / 65025.0 * (double)vol;
				DxLib::ChangeVolumeSoundMem((int)volume, sound_manager->bgm[i].getHandle());
				int st = DxLib::PlaySoundMem(sound_manager->bgm[i].getHandle(), DX_PLAYTYPE_LOOP, TRUE);
				sound_manager->bgm[i].setPlaying(true);
			}
		}
	}
	DEBUG printf("[INFO] (LOAD) BGMの復元が完了しました。\n");

	//SE
	DxLib::SetUseASyncLoadFlag(FALSE);
	sound_manager->freeSeAll();
	for(int i = 0; i < TENG_SOUND_BGM_MAX_COUNT; i++){

		if(save_struct.s_manager.se[i].getHandle() != 0){
			char* file = save_struct.s_manager.se[i].getFileName();
			sound_manager->loadSe(file, save_struct.s_manager.se[i].getLoopPoint());
			short vol = save_struct.s_manager.se[i].getVolume();
			sound_manager->se[i].setVolume(vol);
			bool play = save_struct.s_manager.se[i].getPlaying();
			sound_manager->se[i].setPlaying(play);
			if(play){
				double volume =
					(double)config->getSetting().master_volume *
					(double)config->getSetting().bgm_volume / 65025.0 * (double)vol;
				DxLib::ChangeVolumeSoundMem((int)volume, sound_manager->se[i].getHandle());
				//DxLib::PlaySoundMem(sound_manager->se[i].getHandle(), DX_PLAYTYPE_LOOP, TRUE);
				sound_manager->se[i].setPlaying(true);
			}
		}
	}
	DEBUG printf("[INFO] (LOAD) SEの復元が完了しました。\n");
	DxLib::SetUseASyncLoadFlag(TRUE);

	char title[TENG_SCRIPT_ARGUMENT_MAX_NUMBER + 1][TENG_SCRIPT_COMMAND_MAX_LENGTH];
	strcpy_s(title[1], TENG_SCRIPT_MAX_STRING_LENGTH, save_struct.window_title);
	title[2][0] = NC;
	scene->setTitle(title);
	DEBUG printf("[INFO] (LOAD) ウィンドウタイトルを復元しました。\n");

	DEBUG printf("[INFO] (LOAD) フラグのリセット処理。\n");
	msg->setDrawSelect(save_struct.is_selection_drawing);
	msg->clearSelects();
	if(save_struct.is_selection_drawing){
		for(int i = 0; i < 5; i++){
			if(save_struct.select_msg[i][0] != NC){
				Select sel;
				sel.setMsg(save_struct.select_msg[i]);
				sel.setLabal(save_struct.select_label[i]);
				msg->addSelect(sel);
			}
		}
	}
	scenario->startedFromTitle(false);//これをセットしないと自動クリックされる

	int loves[9];
	memcpy(loves, save_struct.character_loves, sizeof(loves));
	scenario->restoreLove(loves);
	DEBUG printf("[INFO] (LOAD) 好感度を復元しました。\n");

	msg->setCharacterName(string(save_struct.character_name));
	DEBUG printf("[INFO] (LOAD) 主人公の名前を復元しました。\n");


	DEBUG printf("[INFO] (LOAD) セーブデータのロードが完了しました。\n");

	return error;
}

void Teng::SaveData::makeScreenShotShumbnail(){
	int tmp_graph = DxLib::MakeGraph(TENG_SCREEN_WIDTH_LARGE, TENG_SCREEN_HEIGHT_LARGE);
	DxLib::GetDrawScreenGraph(0, 0, TENG_SCREEN_WIDTH_LARGE, TENG_SCREEN_HEIGHT_LARGE, tmp_graph);
	DxLib::SetDrawMode(DX_DRAWMODE_BILINEAR);
	int tmp_screen = DxLib::MakeScreen(TENG_SAVE_SCREENSHOT_WIDTH, TENG_SAVE_SCREENSHOT_HEIGHT, FALSE);
	DxLib::SetDrawScreen(tmp_screen);
	DxLib::DrawExtendGraph(0, 0, TENG_SAVE_SCREENSHOT_WIDTH, TENG_SAVE_SCREENSHOT_HEIGHT, tmp_graph, FALSE);
	string tmp_path = string(Config::savedata_dir) + "tmp";
	//int tst = DxLib::SaveDrawScreen(0, 0, TENG_SAVE_SCREENSHOT_WIDTH, TENG_SAVE_SCREENSHOT_HEIGHT, "tmp");
	int tst = DxLib::SaveDrawScreen(0, 0, TENG_SAVE_SCREENSHOT_WIDTH, TENG_SAVE_SCREENSHOT_HEIGHT, tmp_path.c_str());
	DxLib::DeleteGraph(tmp_graph);
	DxLib::SetDrawScreen(DX_SCREEN_BACK);
	DxLib::DeleteGraph(tmp_screen);
}

const SaveDataStruct SaveData::get(int id){
	FILE *fin;
	errno_t error;
	char filename[TENG_FILE_NAME_MAX];
	sprintf_s(filename, TENG_FILE_NAME_MAX, TENG_SAVE_FILENAME, id);
	SaveDataStruct savedata;
	//セーブデータを構造体に移す
	if((error = fopen_s(&fin, filename, "rb")) != 0){
		DEBUG printf("[ERROR] (LOAD) No such file or directory\n");//たぶんENOENT
		throw ENGINE_SAVE_NO_SUCH_FILE_OR_DIRECTORY;
	}else{
		fread_s(&savedata, sizeof(SaveDataStruct), sizeof(SaveDataStruct), 1, fin);
		fclose(fin);


		char* c_save = reinterpret_cast<char*>(&savedata);
		

		for (int i = 0; i < sizeof(SaveDataStruct); i++){
			//c_save[i] = c_save[i] ^ TENG_SAVEDATA_PASSWORD;
		}
		memcpy(&savedata, c_save, sizeof(SaveDataStruct));

		fclose(fin);
	}
	return savedata;
}