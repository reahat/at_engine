﻿
#include "../define.h"
#include "../global.h"
#include "../error_code_define.h"
#include "../scene.h"

void Teng::Scene::setTitle(char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER+1][TENG_SCRIPT_COMMAND_MAX_LENGTH]){
	const char *software_version = __DATE__;
	char window_fullname[128] = TENG_DEFAULT_WINDOW_TITLE;

	int i = 1;
	string text = "";
	while (command[i][0] != NC){
		if (i > 1) text += " ";
		text += string(command[i]);
		i++;
	}


	sprintf_s(window_fullname, "%s | %s", window_fullname, text.c_str());
	//DEBUG sprintf_s(window_fullname, "%s | DEBUG MODE - %s", window_fullname, software_version);
	DxLib::SetMainWindowText(window_fullname);
	strcpy_s(window_title, 128, text.c_str());
}


void Teng::Scene::resetTitle(){
	const char *software_version = __DATE__;
	char window_fullname[128] = TENG_DEFAULT_WINDOW_TITLE;
	//DEBUG sprintf_s(window_fullname, "%s | DEBUG MODE - %s", window_fullname, software_version);
	DxLib::SetMainWindowText(window_fullname);
	window_title[0] = NC;
}