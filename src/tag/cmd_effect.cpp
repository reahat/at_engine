﻿#include "../define.h"
#include "../global.h"
#include "../error_code_define.h"
#include "../scene.h"

void Teng::Scene::setEffectFlash(const char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER+1][TENG_SCRIPT_COMMAND_MAX_LENGTH]){
	// 1: fadein-time
	// 2: fadeout-time
	if (command[1][0] == NC || command[2][0] == NC){
		DEBUG printf("[ERROR] 引数の数が不正です：[TAG] @effect.flash\n");
		throw ENGINE_EFFECT_FLASH_INVALID_ARGUMENT;
	}
	effect_flash_fadein_power = atoi(command[1]);
	effect_flash_fadeout_power = atoi(command[2]);
	effect_flash_frame = 0;
	is_effect_do = true;
	effect_type = "flash";
	effect_status = "fadein";
}

void Teng::Scene::setEffectSepia(const char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER+1][TENG_SCRIPT_COMMAND_MAX_LENGTH]){
	// 1: start / end
	if (command[1][0] == NC){
		DEBUG printf("[ERROR] 引数の数が不正です：[TAG] @effect.sepia\n");
		throw ENGINE_EFFECT_SEPIA_INVALID_ARGUMENT;
	}
	effect_flash_frame = 0;
	if (string(command[1]) == "start"){
		is_effect_do = true;
		effect_status = "fadein";
		effect_type = "sepia";
	}
	else if (string(command[1]) == "end"){
		is_effect_do = true;
		effect_status = "fadeout";
		effect_type = "sepia";
	}
}

bool Teng::Scene::doBackEffect(GraphicManager* g_manager, Input* input){
	DxLib::SetUseASyncLoadFlag(FALSE);
	if (is_effect_do){
		if (effect_type == "sepia"){
			if (effect_status == "fadein"){
				if (effect_flash_frame < 255){

					// 元の背景を別スクリーンに移してセピア化する
					DxLib::GetDrawScreenGraph(0, 0, 1024, 768, tmp_screen);
					GraphFilter(tmp_screen, DX_GRAPH_FILTER_MONO, -60, 7);
					DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, effect_flash_frame);
					DrawGraph(0, 0, tmp_screen, TRUE);
					DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

					effect_flash_frame += 10;
				}
				else{
					effect_status = "continue";
					// 元の背景を別スクリーンに移してセピア化する
					DxLib::GetDrawScreenGraph(0, 0, 1024, 768, tmp_screen);
					DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
					GraphFilter(tmp_screen, DX_GRAPH_FILTER_MONO, -60, 7);
					DrawGraph(0, 0, tmp_screen, TRUE);
					//is_effect_do = false;
					return true;
				}
			}
			else if (effect_status == "continue"){
					// 元の背景を別スクリーンに移してセピア化する
					DxLib::GetDrawScreenGraph(0, 0, 1024, 768, tmp_screen);
					DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
					GraphFilter(tmp_screen, DX_GRAPH_FILTER_MONO, -60, 7);
					DrawGraph(0, 0, tmp_screen, TRUE);
			}
			else if (effect_status == "fadeout"){
				if (effect_flash_frame < 255){
					// 元の背景を別スクリーンに移してセピア化する
					DxLib::GetDrawScreenGraph(0, 0, 1024, 768, tmp_screen);
					GraphFilter(tmp_screen, DX_GRAPH_FILTER_MONO, -60, 7);
					DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255 - effect_flash_frame);
					DrawGraph(0, 0, tmp_screen, TRUE);
					DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
					effect_flash_frame += 10;
				}
				else{
					effect_status = "stop";
					effect_flash_frame = 0;
					DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
					effect_type = "none";
					is_effect_do = false;
					return true;
				}

			}

		}
		else if (effect_type == "flash"){
			if (effect_status == "fadein"){

				if (effect_flash_frame < 255){
					DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, effect_flash_frame);
					DxLib::DrawGraph(0, 0, g_manager->system_graphics[Teng::SystemGraphic::arrnum_bg_white].getHandle(), TRUE);
					DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
					effect_flash_frame += effect_flash_fadein_power;
				}
				else{
					effect_status = "fadeout";
					effect_flash_frame = 255;
					DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
					DxLib::DrawGraph(0, 0, g_manager->system_graphics[Teng::SystemGraphic::arrnum_bg_white].getHandle(), TRUE);
				}

			}
			else if (effect_status == "fadeout"){
				if (effect_flash_frame > 0){
					DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, effect_flash_frame);
					DxLib::DrawGraph(0, 0, g_manager->system_graphics[Teng::SystemGraphic::arrnum_bg_white].getHandle(), TRUE);
					DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
					effect_flash_frame -= effect_flash_fadeout_power;
				}
				else{
					effect_status = "stop";
					effect_flash_frame = 0;
					DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
					effect_type = "none";
					is_effect_do = false;
					return true;
				}
			}
			// クリックでエフェクト中断
			// ただしエフェクト開始時のクリックと同時にストップしないよう制御
			if (input->getKeepMouseLeft() != 0 && effect_flash_frame > effect_flash_fadein_power && effect_flash_frame < 255 - effect_flash_fadeout_power){
					effect_status = "stop";
					effect_flash_frame = 0;
					DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
					effect_type = "none";
					effect_status = "stop";
					is_effect_do = false;
					return false;
			}
		}
	}

	return false;
}