﻿
#include "../define.h"
#include "../global.h"
#include "../error_code_define.h"


void Teng::Scenario::cmdJump(
	Teng::Input* input,
	Teng::Scene* scene,
	Teng::CmdMessage* msg,
	Teng::GraphicManager* graphic,
	char *importfile,
	Teng::Scenario* this_scenario)
{
	//char importfile_fullname[TENG_FILE_NAME_MAX] = TENG_DIR_SCENARIO;
	//strcat_s(importfile_fullname, TENG_FILE_NAME_MAX, importfile);
	Teng::Scenario *import_scenario = new Teng::Scenario(importfile);

	*this = *import_scenario;

	started_from_title = false;

	this->setIsImportScenario(false);
	DEBUG printf("[INFO] @jump スクリプト %s に移動します\n", this_scenario->scenario_file);
}
