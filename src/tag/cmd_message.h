﻿//メッセージ処理クラス
#pragma once
#include "../config.h"
#include <list>
#include <vector>
#include <sstream>
#include "../word.h"
#include "../backlog.h"
#include "../define.h"
#include "../select.h"
#include "../common.h"
#include "../scenario.h"
#include "../scene.h"

using std::list;
using std::string;
using std::vector;
namespace Teng{

	class Scene;
	class Word;
	class Select;
	class GraphicManager;


	class CmdMessage
	{
	public:
		CmdMessage();
		~CmdMessage();
		enum MsgType
		{
			MSG,
			RUBI,
			SAVE_MSG,
			SAVE_CONFIRM,
			SAVE_DATE,
			SAVE_YESNO,
			SAVE_DISABLED,
			SAVE_ENABLED,
			SAVE_FOCUS,
			NAME_ENTRY_LABEL,
			NAME_ENTRY_INPUT,
			DIALOG_TEXT,
			SELECTION,
			BACKLOG
		};
		//ShiftJISマルチバイト文字の1バイト目かどうか
		static bool isLeadShiftJIS(unsigned char c);

		//禁則文字テーブルの検索
		static bool isProhibitLineBreakA(const char *char_table[2], char mb[]);

		//行頭禁則文字かどうか
		static bool isProhibitLineBreakBeforeA(char mb[]);

		//行末禁則文字かどうか
		static bool isProhibitLineBreakAfterA(char mb[]);

		//分断禁止文字かどうか
		static bool isProhibitSplitChars(char* mb);

		void clear();
		void update(unsigned long& frame, Config* config, Scenario* scenario, Scene* scene);
		void render(int width, int lineheight, GraphicManager* g_graphic, Scenario* scenario);
		//void render(int width, int lineheight, GraphicManager* g_manager);
		void calcMsgSpeed(Config* config);

		void setSelect(char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER+1][TENG_SCRIPT_COMMAND_MAX_LENGTH]);

		//描画中にクリックで高速表示
		void drawSpeedUp();

		//コマ送りで表示するメッセージをセット
		void setDrawMessage(const char *msg, bool is_say, string name = "");
		//メッセージの簡略記述用
		void setExMessage(const char *msg);
		//特殊文字（プラス、改行など）の判定
		bool isExCharacter(const char& character)const;
		
		//getter
		bool getRenderingFlg();
		void setRenderingFlg(bool is_render){rendering_flg = is_render;}
		void setForceSkipFlag(bool is_forceskip){ force_skip_flg = is_forceskip;}
		bool getForceSkipFlag(){return force_skip_flg;}
		Backlog getLog(){return log;}
		int  msgFontHandle(){return msg_font_handle;}
		bool isDrawSelect(){return draw_select;}
		void setDrawSelect(bool is_draw){draw_select = is_draw;}
		vector<Select> &getSelects(){return selects;}
		void setSelectClickWaiting(bool set_wait){select_click_waiting = set_wait;}
		bool getSelectClickWaiting(){return select_click_waiting;}
		void setForceClickFlg(bool is_force_click){force_click_flg = is_force_click;}
		bool getForceClickFlg(){return force_click_flg;}
		void clearSelects();
		void addSelect(Select sel);
		bool isDrawNameEntry(){return draw_name_entry;}
		void setDrawNameEntry(bool to_draw);
		int  input_handle;
		char input_string[TENG_MESSAGE_CHARACTER_NAME_MAXLENGTH];
		void setCharacterName(string name);
		//名前入力の上限チェック
		const bool checkInputNameLength(const string& name);
		string getCharacterName(){return character_name;}
		void cmdNameSet();
		bool plus_stopping;
		void removeFirstPlus();
		void resetDrawSpeed(Config* config);
		int removeAllPlus();
		void removeAllSpace();
		bool getMsgWindowHideFlg(){ return msg_window_hide_flg; }
		void setMsgWindowHideFlg(bool to_hide){ msg_window_hide_flg = to_hide; }
		typedef enum{
			MSG_WINDOW_TRANS_TYPE_DISPLAY,
			MSG_WINDOW_TRANS_TYPE_HIDE,
			MSG_WINDOW_TRANS_TYME_FADEIN,
			MSG_WINDOW_TRANS_TYPE_FADEOUT,
			MSG_WINDOW_TRANS_COMPLETED
		} eMsgWindow;
		void windowFade();
		int  getMsgWindowTransFrame(){ return msg_window_trans_frame; }
		void setMsgWindowTransFrame(int frame){ msg_window_trans_frame = frame; }
		int  getMsgWindowStatus(){ return msg_window_status; }
		void setMsgWindowStatus(int status){ msg_window_status = status; }
		bool isMsgWindowTransDoing(){ return msg_window_trans_frame != 255; }
		const int getFontHandle(const int& type);
		bool isMsgDrawCompleted(){ return is_msg_draw_completed; }
		bool isClickAfterSelectWait(){ return is_click_after_select_wait; }
		void isClickAfterSelectWait(bool _is){ is_click_after_select_wait = _is; }
	private:
		bool is_click_after_select_wait;
		bool is_msg_draw_completed;
		int loadFontFile();
		const char *pStream;
		static const int BUF_SIZE = 4*1024;
		static const int LINE_NUM = 64;
		char buf[BUF_SIZE];
		int line_top[LINE_NUM];
		int line_len[LINE_NUM];
		int line_num;
		int hold_len; // 送り出し保留分文字列長
		int hold_cnt; // 送り出し保留分確定猶予文字数カウンタ
		int curr_idx; // 逐次描画対象行のインデックス
		int curr_len; // 逐次描画対象行の描画済み文字列長さ

		bool rendering_flg; //文字送りで表示中の際にtrue
	//	int msg_draw_speed;
		bool highspeed_flg; //高速表示フラグ

		int msg_font_handle; //メッセージ用フォントハンドル
		int rubi_font_handle; //ルビ用フォントハンドル
		int font_handle_save_date;
		int font_handle_save_msg_and_confirm;
		int font_handle_save_yesno;
		int font_handle_saveload_select;
		int font_handle_saveload_msg;
		int font_handle_19px;
		int font_handle_23px;
		int font_handle_27px;
		int font_handle_26px;
		int font_handle_25px;
		const static int FONT_SIZE_SAVE_DATE = 34;
		const static int FONT_SIZE_SAVE_MSG_AND_CONFIRM = 26;
		const static int FONT_SIZE_SAVE_YESNO = 35;
		const static int FONT_SIZE_SAVELOAD_SELECT = 30;
		const static int FONT_SIZE_SAVELOAD_MSG = 21;
		const static int FONT_SIZE_23 = 23;
		const static int FONT_SIZE_27 = 27;
		const static int FONT_SIZE_25 = 25;

		/* ↓の２つを組み合わせて表示速度の変化を実現する */
		int msg_draw_interval;//何フレームごとに描画するか
		int msg_draw_characters_num;//一度の描画で何バイト表示するか

		bool force_skip_flg;
		list<Word> message; // メッセージ１回分

		int msg_color;
		list<Word>::iterator it_word;

		Backlog log;
		vector<Select> selects;
		bool draw_select;
		bool select_click_waiting;
		bool force_click_flg;
		bool draw_name_entry;
		string character_name;
		bool msg_window_hide_flg;
		int  msg_window_trans_frame;
		int  msg_window_status;

	};

}
