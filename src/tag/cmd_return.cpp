﻿
#include "../define.h"
#include "../global.h"
#include "../error_code_define.h"


void Teng::Scenario::returnScenario(Teng::Scenario* main_scenario)
{
	main_scenario->setIsImportScenario(false);
	this->setIsImportScenario(false);
	DEBUG printf("[INFO] スクリプト %s に戻ります\n", main_scenario->scenario_file);
}
