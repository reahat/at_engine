﻿#include "../define.h"
#include "../global.h"
#include "../error_code_define.h"
#include "../scenario.h"
#include <regex>

//@if条件分岐
void Teng::Scenario::cmdIf(char (*command)[TENG_SCRIPT_COMMAND_MAX_LENGTH]){

	if(command[1][0] == NC){
		DEBUG printf("[ERROR] @if 引数を指定してください\n");
		throw ENGINE_SCRIPT_IF_ARGS_NULL;
	}

	string arg1_a, arg1_b;
	splitCommandAndCharName(string(command[1]), arg1_a, arg1_b);
	
	bool compare_result = false;
	if(arg1_a == "love"){
		// @if love.topのとき
		if(arg1_b == "top"){
			if(command[2][0] == NC){
				DEBUG printf("[ERROR] @if 引数が不足しています。\n");
				throw ENGINE_SCRIPT_IF_MISSING_ARGUMENT;
			}

			string oper = string(command[2]);
			if(oper != "=" && oper != "==" && oper != "!=" && oper != "<>"){
				//不正な記号エラー
				DEBUG printf("[ERROR] @if 不正な演算子です。\n");
				throw ENGINE_SCRIPT_IF_INVALID_OPERATOR;
			}

			if(command[3][0] == NC){
				DEBUG printf("[ERROR] @if 引数が不足しています。\n");
				throw ENGINE_SCRIPT_IF_MISSING_ARGUMENT;
			}
			string name = string(command[3]);
			if(!checkCharName(name)){
				//不正キャラ名エラー
				DEBUG printf("[ERROR] @if 不正なキャラクターを指定しました。\n");
				throw ENGINE_SCRIPT_IF_NO_SUCH_CHARACTER;
			}
			string top_char = getTopLoveCharName();
			if(oper == "=" || oper == "=="){
				// @if char.top = tristan then...
				compare_result = (name == top_char);
			}else{
				// @if char.top != tristan then...
				compare_result = (name != top_char);
			}
		}
		// @if love.tristanのとき
		else{
			int love_point;
			string name = arg1_b;

			if(!checkCharName(name)){
				//不正キャラ名エラー
				DEBUG printf("[ERROR] @if 不正なキャラクターを指定しました。\n");
				throw ENGINE_SCRIPT_IF_NO_SUCH_CHARACTER;
			}
			if(command[2][0] == NC || command[3][0] == NC){
				DEBUG printf("[ERROR] @if 引数が不足しています。\n");
				throw ENGINE_SCRIPT_IF_MISSING_ARGUMENT;
			}

			love_point   = char_status[name];
			string oper  = string(command[2]);
			string rhs   = string(command[3]);
			bool arg_is_number = checkLoveArg(rhs);

			int i_num = INT_MIN;
			// @if love.tristan > 10 記法のとき
			if(arg_is_number){
				string s_num = rhs;
				i_num = stoi(s_num);
			}
			// @if love.tristan > love.hanako 記法のとき
			else{
				string arg_name = rhs;
				string a_cmd, a_name;
				splitCommandAndCharName(arg_name, a_cmd, a_name);

				if(!checkCharName(a_name)){
					DEBUG printf("[ERROR] @if 不正なキャラクターを指定しました。\n");
					throw ENGINE_SCRIPT_IF_NO_SUCH_CHARACTER;
				}
				i_num = char_status[a_name];
			}

			if(oper == "<"){
				compare_result = (love_point < i_num);
			}else if(oper == "<=" || oper == "=<"){
				compare_result = (love_point <= i_num);
			}else if(oper == "="  || oper == "=="){
				compare_result = (love_point == i_num);
			}else if(oper == ">=" || oper == "=>"){
				compare_result = (love_point >= i_num);
			}else if(oper == ">"){
				compare_result = (love_point > i_num);
			}else{
				//不正な記号エラー
				DEBUG printf("[ERROR] @if 不正な演算子です。\n");
				throw ENGINE_SCRIPT_IF_INVALID_OPERATOR;
			}
		}
	} // if judge "LOVE"

	// if判定がtrueのとき
	if(compare_result){
		if(command[4][0] == NC || command[5][0] == NC){
			DEBUG printf("[ERROR] @if 引数が不足しています。\n");
			throw ENGINE_SCRIPT_IF_MISSING_ARGUMENT;
		}
		string second_command = string(command[4]);
		if(second_command == "@goto"){
			string goto_label = string(command[5]);
			gotoScenario(command[5]);
		}else{
			//使用できないコマンドエラー
			DEBUG("[ERROR] @if @ifでは使用できないコマンドです\n");
			throw ENGINE_SCRIPT_IF_INVALID_COMMAND;
		}
	}



}
