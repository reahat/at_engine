﻿#include "cmd_message.h"
#include "../define.h"
#include "../error_code_define.h"
#include <DxLib.h>
#include "../scene.h"
#include <memory>
extern char common_dir[256];
extern std::unique_ptr<Teng::Input> input;

Teng::CmdMessage::CmdMessage(){
	is_click_after_select_wait = false;
	is_msg_draw_completed = false;
	pStream = "";
	msg_draw_characters_num = 1;
	highspeed_flg = false;
	//msg_font_handle = CreateFontToHandle(TENG_MESSAGE_FONT_NAME, TENG_MESSAGE_FONT_SIZE, -1, TENG_MESSAGE_FONT_ANTIALIAS_TYPE);
	loadFontFile();
	list<Word> message;
	msg_color = GetColor(32,32,32);
	Backlog log;
	//buf[0] = NULL;
	memset(buf, NC, BUF_SIZE);
	vector<Select> selects;
	draw_select = false;
	select_click_waiting = false;
	force_click_flg = false;
	draw_name_entry = false;
	plus_stopping = false;
	msg_window_hide_flg = false;
	msg_window_trans_frame = 255;
	msg_window_status = eMsgWindow::MSG_WINDOW_TRANS_TYPE_DISPLAY;


	DxLib::SetKeyInputStringColor(
		GetColor(54, 25, 6), GetColor(54, 25, 6),
		GetColor(155, 148, 135), GetColor(54, 25, 6),
		GetColor(54, 25, 6), GetColor(54, 25, 6),
		GetColor(255, 255, 255), GetColor(54, 25, 6),
		GetColor(54, 25, 6), GetColor(64, 64, 64),
		GetColor(54, 25, 6), GetColor(255, 255, 255),
		GetColor(128, 128, 128), GetColor(54, 25, 6),
		GetColor(64, 64, 64), GetColor(54, 25, 6),
		GetColor(64, 64, 64));
	DxLib::SetKeyInputStringColor2(DX_KEYINPSTRCOLOR_NORMAL_STR, GetColor(255,255,255));
	DxLib::SetKeyInputStringColor2(DX_KEYINPSTRCOLOR_IME_STR, GetColor(54, 25, 6));
	DxLib::SetKeyInputStringColor2(DX_KEYINPSTRCOLOR_SELECT_STR, GetColor(255,255,255));
	DxLib::SetKeyInputStringColor2(DX_KEYINPSTRCOLOR_IME_CONV_WIN_SELECT_STR_BACK, GetColor(54,25,6));
	DxLib::SetKeyInputStringColor2(DX_KEYINPSTRCOLOR_IME_CONV_WIN_SELECT_STR, GetColor(255,255,255));
	DxLib::SetKeyInputStringColor2(DX_KEYINPSTRCOLOR_IME_SELECT_STR, GetColor(255,255,255));

	character_name = "";

}

Teng::CmdMessage::~CmdMessage(){

}

int Teng::CmdMessage::loadFontFile(){
	DxLib::SetUseASyncLoadFlag(FALSE);
	char font_path[512];
	sprintf_s(font_path, "%s%s%s", common_dir, TENG_DIR_FONT, TENG_MESSAGE_FONT_FILE_NAME);
	// ファイルのサイズを得る
	int fontFileSize = DxLib::FileRead_size(font_path);
	// フォントファイルを開く
	int fontFileHandle = DxLib::FileRead_open(font_path);
	// フォントデータ格納用のメモリ領域を確保
	void *buffer = malloc(fontFileSize);
	// フォントファイルを丸ごとメモリに読み込む
	DxLib::FileRead_read(buffer, fontFileSize, fontFileHandle);

	// AddFontMemResourceEx引数用
	DWORD font_num = 0;

	if (AddFontMemResourceEx(buffer, fontFileSize, NULL, &font_num) > 0) {
	}else{
		// フォント読込エラー処理
		RemoveFontMemResourceEx(buffer);
		free(buffer);
		throw ENGINE_RESOURCE_FONT_FILE_LOAD_FAILURE;
	}
	DxLib::SetUseASyncLoadFlag(TRUE);

	msg_font_handle = CreateFontToHandle(TENG_MESSAGE_FONT_NAME, TENG_MESSAGE_FONT_SIZE, -1, TENG_MESSAGE_FONT_ANTIALIAS_TYPE);
	rubi_font_handle = CreateFontToHandle(TENG_RUBI_FONT_NAME, TENG_RUBI_FONT_SIZE, -1, TENG_RUBI_FONT_ANTIALIAS_TYPE);
	DxLib::SetFontCacheUsePremulAlphaFlag(TRUE);
	font_handle_save_date = CreateFontToHandle(TENG_MESSAGE_FONT_NAME, FONT_SIZE_SAVE_DATE, -1, DX_FONTTYPE_ANTIALIASING);
	font_handle_save_msg_and_confirm = CreateFontToHandle(TENG_MESSAGE_FONT_NAME, FONT_SIZE_SAVE_MSG_AND_CONFIRM, -1, DX_FONTTYPE_ANTIALIASING);
	font_handle_save_yesno = CreateFontToHandle(TENG_MESSAGE_FONT_NAME, FONT_SIZE_SAVE_YESNO, -1, DX_FONTTYPE_ANTIALIASING);
	font_handle_saveload_msg = CreateFontToHandle(TENG_MESSAGE_FONT_NAME, FONT_SIZE_SAVELOAD_MSG, -1, DX_FONTTYPE_ANTIALIASING);
	DxLib::SetFontCacheUsePremulAlphaFlag(FALSE);
	// 乗算済みアルファ指定を切らないとエッジが付いてしまう
	font_handle_saveload_select = CreateFontToHandle(TENG_MESSAGE_FONT_NAME, FONT_SIZE_SAVELOAD_SELECT, -1, DX_FONTTYPE_ANTIALIASING);
	font_handle_23px = CreateFontToHandle(TENG_MESSAGE_FONT_NAME, FONT_SIZE_23, -1, DX_FONTTYPE_ANTIALIASING);
	font_handle_27px = CreateFontToHandle(TENG_MESSAGE_FONT_NAME, FONT_SIZE_27, -1, DX_FONTTYPE_ANTIALIASING);
	font_handle_19px = CreateFontToHandle("KozMinPro-Heavy", 18, -1, DX_FONTTYPE_ANTIALIASING_4X4);
	//font_handle_25px = CreateFontToHandle(TENG_MESSAGE_FONT_NAME, FONT_SIZE_25, -1, DX_FONTTYPE_ANTIALIASING);
	font_handle_26px = CreateFontToHandle("KozMinPro-Heavy", 26, -1, DX_FONTTYPE_ANTIALIASING);
	return 0;
}

void Teng::CmdMessage::clear(void)
{
	line_num = 0;
	line_top[0] = 0;
	line_len[0] = 0;
	hold_len = 0;
	hold_cnt = 0;
	curr_idx = 0;
	curr_len = 0;
	list<Word>::iterator iter = message.begin();
	if (message.size() > 0){
		iter->setSpeakerName("");
	}
}

void Teng::CmdMessage::clearSelects(void)
{
	selects.clear();
}

void Teng::CmdMessage::update(unsigned long& frame, Config* config, Scenario* scenario, Scene* scene)
{
	//if(force_skip_flg){
	//	removeAllPlus();
	//	removeAllSpace();

	//	for (int cnt = 0; cnt < line_num; cnt++){
	//		string tmp = &buf[line_top[cnt]];
	//		//プラスストップに注意
	//		if (tmp.size() >= line_len[cnt]){
	//			buf[line_len[cnt]] = NC;
	//		}
	//	}
	//}
	static bool log_set = false;
	char mb;
	//line_num メッセージ全体の行数
	//line_len[] 何行目に何文字あるかを格納
	//line_top 先頭はなん文字目か
	//curr_idx 現在何行目か
	//curr_len 現在その行の何文字目か
	if(!highspeed_flg) calcMsgSpeed(config);
	if(line_num > 0){
		//curr_idx = line_num-1;
		//curr_len = line_len[line_num-1];
	}


	// 現在の行より前に何文字あるかを計算
	int before_now_line_char_num = 0;
	for (int n = 0; n < curr_idx; n++){
		before_now_line_char_num += line_len[n];
	}
	// plus_loc(プラスの位置は全体の何文字目か）を、プラスの位置は現在の行の何文字目か、に変換する
	int plus_loc = 9999;
	for(int i = 0; buf[i] != NC; i++){
		if(buf[i] == '+'){
			plus_loc = i;
			plus_loc -= before_now_line_char_num;
			break;
		}
	}

	if(frame % msg_draw_interval == 0){
		if (line_len[curr_idx] > curr_len) {
			log_set = false;
			rendering_flg = true;
			/* 逐次描画対象行に文字列が残っているとき */
			// 一文字ずつ描画対象を伸ばしていく
			mb = buf[line_top[curr_idx] + curr_len];
			if(curr_len + msg_draw_characters_num < plus_loc){
				//一度に多数の文字を表示する場合、文字バッファをオーバーしないようにする
				if(line_len[curr_idx] < curr_len + msg_draw_characters_num){
					msg_draw_characters_num = line_len[curr_idx] - curr_len - 1;
					//↓試しに書いた
					if(msg_draw_characters_num == 0) msg_draw_characters_num = 1;
					curr_len += msg_draw_characters_num;

				}else{
					//通常処理。msg_draw_characters_numの分だけ一度に文字送りする
					curr_len += msg_draw_characters_num;
				}
				if (isLeadShiftJIS(mb)) {
					++curr_len;
				}
			}else{

				if(!plus_stopping){

					msg_draw_characters_num = plus_loc - curr_len;
					//下手に消すとプラスが表示されるので注意
					if(msg_draw_characters_num != 0){
						if(msg_draw_characters_num == 0) msg_draw_characters_num = 1;
						curr_len += msg_draw_characters_num;
					}
					plus_stopping = true;
					scene->resetAutoFrame();
					//rendering_flg = true;
				}

			}
		}
		else {
			/* 逐次描画対象行に文字列が残っていないとき */
			if (curr_idx < line_num - 1) {
				// 次の行に逐次描画対象を移す
				++curr_idx;
				curr_len = 0;
				if(highspeed_flg) msg_draw_characters_num = TENG_MESSAGE_RENDER_SPEED_SUPER;
			}
			//最終行まで表示が終わったら{{
			else{
				rendering_flg = false;
				msg_draw_characters_num = 1;
				highspeed_flg = false;
				if(!log_set && buf[0] != NC){
					log_set = true;
					BacklogMessage backlog_message;
					for(int cnt = 0; cnt < line_num; cnt++){
						string tmp = &buf[line_top[cnt]];
						//プラスストップに注意
						if(tmp.size() >= line_len[cnt]){
							tmp[line_len[cnt]] = NC;
						}
						backlog_message.add(tmp);
					}
					list<Word>::iterator iter = message.begin();
					if(!iter->getSpeakerName().empty()){
						backlog_message.setSpeaker(iter->getSpeakerName());
					}
					log.push(backlog_message);
				}

			}
		}

	}
	if (curr_len == 0){
		rendering_flg = true;
	}
	// ↑Ctrlスキップが効かなくなる原因？現在はctrlスキップからrendering_flgの判定を外している

	// ctrlスキップ時のログ格納はrender()で行うこと
	// update()で行うと、bufに格納されていない状態でログを追加しようとする
}

void Teng::CmdMessage::drawSpeedUp()
{
	//msg_draw_characters_num = TENG_MESSAGE_RENDER_SPEED_SUPER;//必ず奇数にすること
	msg_draw_interval = 1;
	msg_draw_characters_num = 7;

	highspeed_flg = true;
}

void Teng::CmdMessage::resetDrawSpeed(Teng::Config* config){
	highspeed_flg = false;
	calcMsgSpeed(config);
}


void Teng::CmdMessage::render(int width, int lineheight, GraphicManager* g_manager, Scenario* scenario)
{
	DxLib::SetFontSpaceToHandle(-2, msg_font_handle);
	char mb[2];
	int mb_len;
	// カギ括弧の位置対策
	int width_with_bracket = width - TENG_MESSAGE_FONT_SIZE;
	int width_for_draw = width;
	static int before_rendered_xpos = 0;

	// message: ベース＋ルビを@msg1行分結合したもの
	while(!message.empty() && it_word != message.end()){
		// 初回はルビの表示位置を左端にリセット
		if(it_word == message.begin()){
			before_rendered_xpos = 0;
		}

		string base_text = it_word->baseText();
		// 文字単位でループ
		for(int i = 0; i < (int)base_text.size(); i++){
			mb[0] = base_text[i];
			if (mb[0] == '|') {
				/* 改行コードで強制改行 */
				int n = line_num;
				line_len[n] += hold_len;
				hold_len = 0;
				line_num++;
				line_top[line_num] = line_top[n] + line_len[n];
				line_len[line_num] = 0;
				before_rendered_xpos = 0;
				it_word->setRubiBaseLineCharNum(0);//必要？
				continue;
			}
			if (mb[0] == '+'){
				//if(line_num > 0)line_top[line_num]--;
				//line_len[line_num]-=2;
				//line_len[line_num] -= 1;
				//line_top[line_num+1] -= 1;
				//int n = line_num;
				//line_len[n] += hold_len;
				//hold_len = 0;
				//line_num++;
				//line_top[line_num] = line_top[n] + line_len[n];
				//line_len[line_num] = 0;
				//before_rendered_xpos = 0;
				//it_word->setRubiBaseLineCharNum(0);//必要？
			}

			mb_len = 1;
			if (isLeadShiftJIS(mb[0])) {
				//mb[1] = *(pStream++);
				mb[1] = base_text[++i];
				mb_len = 2;
			}

			int n = line_num;
			for (int j=0; j<mb_len; ++j) {
				int loc = line_top[n]+line_len[n]+hold_len+j;
				buf[line_top[n]+line_len[n]+hold_len+j] = mb[j];
			}
			if (mb[0] == '+'){

			}
			else if (isProhibitLineBreakBeforeA(mb)) {
				/* 行頭禁則文字のとき */
				// 確定猶予カウントをリセット
				//hold_cnt = 0;
				int w2 = GetDrawStringWidthToHandle(&buf[line_top[n]], line_len[n] + mb_len + it_word->baseText().size(), msg_font_handle);

				if (w2 > width){
					width += 30;
				}
			}
			else if (isProhibitLineBreakAfterA(mb)) {
				/* 行末禁則文字のとき */
				if (hold_cnt == 0) {
					// 猶予なしなら保留分を確定
					line_len[n] += hold_len;
					hold_len = 0;
				}
				// 続く1文字を確定猶予
				hold_cnt = 1;
			}
			// 分断禁止文字のとき
			else if (isProhibitSplitChars(mb)){
				// 行末のとき？

				//if (buf[line_top[n]])
				int w2 = GetDrawStringWidthToHandle(&buf[line_top[n]], line_len[n] + mb_len + it_word->baseText().size(), msg_font_handle);
				if (w2 > width){



					if (hold_cnt == 0) {
						if (i + 3 <= (int)base_text.size()){
							//char tmp_mb[2];
							//tmp_mb[0] = base_text[i + 1];
							//tmp_mb[1] = base_text[i + 2];

							if (isProhibitSplitChars(mb)){
								width += 30;
							}
						}
					}
				}

			}
			else {
				/* 禁則文字以外のとき */
				if (hold_cnt > 0) {
					// 猶予カウンタ分は保留
					hold_cnt--;
				}
				else {
					// 猶予なしなら保留分を確定
					line_len[n] += hold_len;
					hold_len = 0;
					if (force_skip_flg){
						//removeAllPlus();
					}
				}
			}






			//int w = GetDrawStringWidth(&buf[line_top[n]], line_len[n]+hold_len+mb_len);
			int w = GetDrawStringWidthToHandle(&buf[line_top[n]], line_len[n]+hold_len+mb_len, msg_font_handle);


			bool flg = false;
			it_word++;
			if(!message.empty() && it_word != message.end()){
				if(it_word->rubiExist() && base_text[i] != '|'){
					int wid = GetDrawStringWidthToHandle(&buf[line_top[n]], line_len[n]+mb_len+it_word->baseText().size(), msg_font_handle);
					width_for_draw = (line_num == 0 || !it_word->withBracket()) ? width : width_with_bracket;
					if(wid > width_for_draw){
						if (line_len[n] == 0) {
							// 1行すべて保留分のとき確定する
							line_len[n] += hold_len;
							hold_len = 0;
						}
						line_num++;
						line_top[line_num] = line_top[n] + line_len[n];
						line_len[line_num] = 0;
						flg = true;
					}
				}
			}
			it_word--;


			width_for_draw = (line_num == 0 || !it_word->withBracket()) ? width : width_with_bracket;
			if (!flg && w > width_for_draw) {
				if (line_len[n] == 0) {
					// 1行すべて保留分のとき確定する
					line_len[n] += hold_len;
					hold_len = 0;
				}
				line_num++;
				line_top[line_num] = line_top[n] + line_len[n];
				line_len[line_num] = 0;
			}
			hold_len += mb_len;

			// wordの最後の文字のとき、次のルビ表示位置を知るために最後の表示位置を記憶する
			if(i == ((int)base_text.size() - 1) || (int)base_text.size() <= 2){
				string buf_for_calc_xpos(buf);
				//ルビの位置は+を除去してから計算する
				size_t p = 0;
				size_t plus_cnt = 0;
				while (true){
					p = buf_for_calc_xpos.find('+');
					if (p == string::npos) break;
					buf_for_calc_xpos.erase(buf_for_calc_xpos.begin() + p);
					plus_cnt++;
				}
				char tmp_buf[4096];
				strcpy_s(tmp_buf, 4096, buf_for_calc_xpos.c_str());

				before_rendered_xpos = GetDrawStringWidthToHandle(&tmp_buf[line_top[n]], line_len[n]+hold_len-plus_cnt, msg_font_handle);
				//before_rendered_xpos = GetDrawStringWidthToHandle(&buf[line_top[n]], line_len[n]+hold_len, msg_font_handle);
				// ルビ対象のベース文字が1文字のみのとき
				if((int)base_text.size() == 2){
					before_rendered_xpos = GetDrawStringWidthToHandle(&tmp_buf[line_top[n]], line_len[n]+hold_len-2-plus_cnt, msg_font_handle);
					//before_rendered_xpos = GetDrawStringWidthToHandle(&buf[line_top[n]], line_len[n]+hold_len-2, msg_font_handle);
				}

				if((int)base_text.size() == 1){
					before_rendered_xpos = GetDrawStringWidthToHandle(&tmp_buf[line_top[n]], line_len[n]+hold_len-1-plus_cnt, msg_font_handle);
					//before_rendered_xpos = GetDrawStringWidthToHandle(&buf[line_top[n]], line_len[n]+hold_len-1, msg_font_handle);
				}
			}

			if(it_word->rubiExist() && i != ((int)base_text.size() - 1)){
				it_word->setRubiXPos(before_rendered_xpos);
				it_word->setLineNum(line_num);
				it_word->setRubiBaseLineCharNum(line_len[n]);
			}
			if(it_word->rubiExist() && i == ((int)base_text.size())){
				it_word->setRubiXPos(before_rendered_xpos);
				it_word->setLineNum(line_num);
				it_word->setRubiBaseLineCharNum(line_len[n]);
			}

			// ベース１文字かつルビ１文字のとき、自動的にルビを文字の中央に寄せる
			if(it_word->rubiExist() && (int)base_text.size() == 2){
				it_word->setRubiXPos(before_rendered_xpos);
				before_rendered_xpos = GetDrawStringWidthToHandle(&buf[line_top[n]], line_len[n]+hold_len, msg_font_handle);
				it_word->setLineNum(line_num);
				it_word->setRubiBaseLineCharNum(line_len[n]);
				if(((int)it_word->rubiText().size()/2)%2==1){
					it_word->setRubiXPos(it_word->rubiXPos()+TENG_MESSAGE_FONT_SIZE/4);
				}
			}
			if(it_word->rubiExist() && (int)base_text.size() == 1){
				it_word->setRubiXPos(before_rendered_xpos);
				before_rendered_xpos = GetDrawStringWidthToHandle(&buf[line_top[n]], line_len[n]+hold_len, msg_font_handle);
				it_word->setLineNum(line_num);
				it_word->setRubiBaseLineCharNum(line_len[n]);
				if(((int)it_word->rubiText().size()/2)%2==1){
					it_word->setRubiXPos(it_word->rubiXPos()+TENG_MESSAGE_FONT_SIZE/8);
				}
			}

		} // character loop
		it_word++;
	} // list loop





	if (hold_len > 0) {
		// 保留分が残っていたら確定する
		line_len[line_num] += hold_len;
		hold_len = 0;
		hold_cnt = 0;
	}
	if (line_len[line_num] > 0) {
		// 最終行の〆
		int n = line_num;
		line_num++;
		line_top[line_num] = line_top[n] + line_len[n];
		line_len[line_num] = 0;
	}

	/*
	*  描画
	*/
	// ctrlスキップ中
	if(force_skip_flg){
		curr_idx = line_num-1;
		curr_len = line_len[line_num-1];


	}



	// ctrlスキップ中のログ格納処理
	if (force_skip_flg && !isDrawNameEntry() && !isDrawSelect()){
		static int n = 0;
		// if (line_len[curr_idx] <= curr_len && curr_idx >= line_num - 1){
		static int before_set_line = -1;
		if (before_set_line != scenario->getCurrentLine() && (scenario->getNextTag() == TENG_TAG_MESSAGE || scenario->getNextTag() == TENG_TAG_SAY || scenario->getNextTag() == TENG_TAG_EXMESSAGE)){
			if (buf[0] != NC){
				removeAllPlus();
				removeAllSpace();
				BacklogMessage backlog_message;
				for (int cnt = 0; cnt < line_num; cnt++){
					string tmp = string(&buf[line_top[cnt]]);
					//プラスストップに注意
					if (tmp.size() >= line_len[cnt]){
						tmp[line_len[cnt]] = NC;
					}
					backlog_message.add(tmp);
				}
				list<Word>::iterator iter = message.begin();
				if (!iter->getSpeakerName().empty()){
					backlog_message.setSpeaker(iter->getSpeakerName());
				}
				log.push(backlog_message);
			}
			before_set_line = scenario->getCurrentLine();
		}
		// }
	}

	//プラスの位置全て見つける
	//int pos_arr[64] = {};
	//int arr_n = 0;
	//for (size_t pos = 0; buf[pos] != NC; pos++){
	//	if (buf[pos] == '+'){
	//		pos_arr[arr_n] = pos;
	//		arr_n++;
	//	}
	//}

	

	for (int i=0; i<=curr_idx; ++i) { // 逐次描画対象行まで
		// 逐次描画対象より前の行なら全体を描画
		int len = i < curr_idx ? line_len[i] : curr_len;
		char c = buf[line_top[i] + len];

		//プラスの位置を計算して、そこを過ぎたら経過したプラスの分を足す必要がある
		int p_cnt = 0;
		//for (int arr = 0; arr < 64 && pos_arr[arr] != 0; arr++){
		//	if (len >= pos_arr[arr]){
		//		p_cnt++;
		//		//line_len[0]++;
		//		//line_top[0]++;
		//	}
		//	else{
		//		break;
		//	}
		//}


		//buf[line_top[i] + len + p_cnt] = '\0';
		buf[line_top[i] + len] = '\0';
		list<Word>::iterator iter2 = message.begin();

		int bracket_slide = 0;

		if (i > 0 && iter2->withBracket()){
			bracket_slide = TENG_MESSAGE_FONT_SIZE;
			// 先頭にカギ括弧がある場合、2行目以降を一文字後ろに下げる
		}
		





		DrawStringToHandle(
			TENG_MESSAGE_POINT_X + bracket_slide,
			lineheight*i + TENG_MESSAGE_POINT_Y + 10 * (i + 1),
			&buf[line_top[i] ],
			msg_color,
			msg_font_handle);

		buf[line_top[i] + len] = c;

		if(plus_stopping && i == curr_idx){
				// 現在の行より前に何文字あるかを計算
			int before_now_line_char_num = 0;
			for (int n = 0; n < curr_idx; n++){
				before_now_line_char_num += (line_len[n]);
			}
			int width = DxLib::GetDrawStringWidthToHandle(&buf[line_top[i]], line_top[i]+len-before_now_line_char_num, msg_font_handle);
			//DrawGraph(TENG_MESSAGE_POINT_X+width+bracket_slide, lineheight*i+TENG_MESSAGE_POINT_Y + 10*(i+1), 
			//	g_manager->system_graphics[Teng::SystemGraphic::arrnum_message_arrow].getHandle(),
			//	TRUE);
			g_manager->drawClickWaitFade(
				TENG_MESSAGE_POINT_X + width + bracket_slide,
				lineheight * i + TENG_MESSAGE_POINT_Y + 10 * (i + 1));

		}


		list<Word>::iterator iter = message.begin();
		//キャラクター名を表示
		bool name_draw = false;
		if (message.size() > 0){
			if (!iter->getSpeakerName().empty() && i == 0){
				DrawStringToHandle(TENG_MESSAGE_SPEAKER_NAME_X, TENG_MESSAGE_SPEAKER_NAME_Y, iter->getSpeakerName().c_str(), msg_color, msg_font_handle);
				name_draw = true;
			}
		}
		while (!message.empty() && iter != message.end()){
			//単語ブロックの２番目以降にキャラ名が入っている場合の対策
			if (!iter->getSpeakerName().empty() && i == 0 && !name_draw){
				DrawStringToHandle(TENG_MESSAGE_SPEAKER_NAME_X, TENG_MESSAGE_SPEAKER_NAME_Y, iter->getSpeakerName().c_str(), msg_color, msg_font_handle);
				name_draw = true;
			}
			if (iter->getLineNum() == i){
				// && (i < curr_idx || line_num-1 == curr_idx)
				if ((len) > iter->getRubiBaseLineCharNum()){

					string rubi_text = iter->rubiText();

					DrawStringToHandle(
						TENG_MESSAGE_POINT_X + iter->rubiXPos() + bracket_slide,
						lineheight*i + TENG_MESSAGE_POINT_Y + 10 * i,
						rubi_text.c_str(),
						msg_color,
						rubi_font_handle);
				}
			}
			iter++;
		}
	}



	// 最終行のとき
	// かつ最後の文字までいったら
	if (curr_idx == line_num - 1 && curr_len >= line_len[line_num - 1]){
		g_manager->drawClickWaitFall();
		is_msg_draw_completed = true;
	}
	else{
		is_msg_draw_completed = false;
	}


	//line_num メッセージ全体の行数
	//line_len[] 何行目に何文字あるかを格納
	//curr_idx 現在何行目か
	//curr_len 現在その行の何文字目か
}

//ShiftJISマルチバイト文字の1バイト目かどうか
bool Teng::CmdMessage::isLeadShiftJIS(unsigned char c)
{
	return (c >= 0x81 && c <= 0x9F) || (c >= 0xE0 && c <= 0xFC);
}

//禁則文字テーブルの検索
bool Teng::CmdMessage::isProhibitLineBreakA(const char *char_table[2], char mb[])
{
	if ((unsigned char)mb[0] < 0x81) return false;
	if (isLeadShiftJIS(mb[0])) {
		const char *p = char_table[0];
		while (*p != '\0') {
			if (p[0] == mb[0] && p[1] == mb[1]) {
				return true;
			}
			p+=2;
		}
	}
	else {
		const char *p = char_table[1];
		while (*p != '\0') {
			if (p[0] == mb[0]) {
				return true;
			}
			p++;
		}
	}
	return false;
}

//行頭禁則文字かどうか
bool Teng::CmdMessage::isProhibitLineBreakBeforeA(char mb[])
{
	static const char *char_table[2] = {
		"、。，．・：；？！゛゜"
		"’”）〕］｝〉》」』】"
		"ーぁぃぅぇぉっゃゅょゎァィゥェォッャュョヮヵヶヽヾゝゞ〃々"
		"—‐〜−＝"
		"°′″℃¢％"
		,
		"、。,.・:;?!゛゜"
		")]}」"
		"ーァィゥェォッャュョ"
		"-="
		"%"
	};
	return isProhibitLineBreakA(char_table, mb);
}

// 分断禁止文字かどうか
bool Teng::CmdMessage::isProhibitSplitChars(char* mb){
	static const char *char_table[2] = {
		"―…"
	};
	return isProhibitLineBreakA(char_table, mb);
}

//行末禁則文字かどうか
bool Teng::CmdMessage::isProhibitLineBreakAfterA(char mb[])
{
	static const char *char_table[2] = {
		"‘“（〔［｛〈《「『【¥＄",
		"([{「\\$"
	};
	return isProhibitLineBreakA(char_table, mb);
}

void Teng::CmdMessage::setExMessage(const char *msg){
	string buffer_1;
	string buffer_2;
	int point = 0;
	bool is_first_buffer = true;
	bool add_bracket = false;
	while (msg[point] != NC){
		if ((UCHAR)msg[point] == 0x81 && (UCHAR)msg[point + 1] == 0x46){
			buffer_1 += NC;
			point += 2;
			is_first_buffer = false;
		}
		else if ((UCHAR)msg[point] == 0x81 && (UCHAR)msg[point + 1] == 0x47){
			buffer_1 += NC;
			point += 2;
			is_first_buffer = false;
			add_bracket = true;
		}
		if (is_first_buffer){
			buffer_1 += msg[point];
		}
		else{
			buffer_2 += msg[point];
		}
		point++;
	}
	if (add_bracket){
		buffer_2 = "「" + buffer_2 + "」";
	}
	if(!is_first_buffer) buffer_2 += NC;


	if (input->getKeyControl() != 0){
		for (size_t c = buffer_1.find_first_of("+"); c != string::npos; c = c = buffer_1.find_first_of("+")){
			buffer_1.erase(c, 1);
		}
		for (size_t c = buffer_2.find_first_of("+"); c != string::npos; c = c = buffer_2.find_first_of("+")){
			buffer_2.erase(c, 1);
		}
	}




	// キャラクター名がないとき
	if (buffer_2.empty()){
		setDrawMessage(buffer_1.c_str(), false);
	}
	// キャラ名有りのときはsayに振り分ける
	else{
		setDrawMessage(buffer_2.c_str(), true, buffer_1);
	}
}

//コマ送りで表示するメッセージをセット
void Teng::CmdMessage::setDrawMessage(const char *msg, bool is_say, string name)
{
	bool with_bracket = false;
	// カギ括弧の2行目以降対策
	if (!name.empty()){
		string msg_body(msg);
		if (msg_body.compare(0, 2, "「") == 0 || msg_body.compare(0, 2, "（") == 0){
			with_bracket = true;
		}
	}
	//pStream = msg;
	curr_len = 0;
	// list<Word> message が定義済み
	message.clear();
	// こんにちは[漢字]{かんじ}さん
	char mb[2];
	pStream = msg;
	bool rubi;
	Word word;

	//メッセージは全て２バイト文字とする
	bool is_first_byte = true;
	word.withBracket(with_bracket);
	// どこかでbufを初期化しないとあぶないので
	memset(buf, NC, BUF_SIZE);

	if(is_say && msg[0]==NC){
		DEBUG printf("[ERROR] @say キャラクター名またはメッセージが不足しています\n");
		throw ENGINE_SCRIPT_SAY_NO_SPEAKER_NAME;
	}

	if((UCHAR)name.c_str()[0] == '$' && (UCHAR)name.c_str()[1] == NC){
		name = character_name;
	}

	while ((mb[0] = *pStream) != '\0'){
		int cnt = 0;
		rubi = false;
		pStream++;
		static unsigned char before_mb = 0x00;
		//char next = *pStream;

		//if((mb[0] != '[' && mb[0] != '$') || (mb[0] == '[' && (before_mb >= 0x81 && before_mb <= 0x98))){
		if ((is_first_byte && isLeadShiftJIS(mb[0])) || !is_first_byte || (is_first_byte && isExCharacter(mb[0]))){
			word.addBaseText(mb[0]);
			is_first_byte = is_first_byte ? false : true; //反転
			if (!is_first_byte && isExCharacter(mb[0])) is_first_byte = true; //プラス判定の際、上の行でフラグが逆転している
		}

		else if(mb[0] == '$' && is_first_byte){
			if(word.isPresent()){
				if(name != "") word.setSpeakerName(name);
				message.push_back(word);
			}
			Word myname;
			myname.clear();
			myname.makeNameWord(character_name);
			if(name != "") word.setSpeakerName(name);
			myname.withBracket(false);
			message.push_back(myname);
			word.clear();
			word.withBracket(with_bracket);
		//}else if(mb[0] == '+' && is_first_byte){
		//	word.setOnStop(true);
		//	message.push_back(word);
		}

		// ルビ対象開始文字 [ が出現
		else if(mb[0] == '[' && is_first_byte){

			try{
				if((unsigned char)word.baseText().at(word.baseText().size()-1) == 0x81){
					word.addBaseText(mb[0]);
					continue;
				}
			}catch(std::out_of_range ex){

			}

			rubi = true;
			// ルビのない文字列をmessageにセット
			if(word.isPresent()){
				if(name != "") word.setSpeakerName(name);
				message.push_back(word);
			}
			word.clear();
			word.withBracket(with_bracket);
			Word word_with_rubi;
			word_with_rubi.withBracket(with_bracket);

			mb[0] = *pStream;
			pStream++;
			//is_first_byte = is_first_byte ? false : true; //反転
			char b = 0;
			while(!(mb[0] == ']' && is_first_byte)){
				b = mb[0];
				word_with_rubi.addBaseText(mb[0]);
				mb[0] = *pStream;
				pStream++;
				
				//一つ前が特殊文字でなかったら
				if (!(isExCharacter(b) && is_first_byte)){
					
					is_first_byte = is_first_byte ? false : true; //反転
				}
			}
			mb[0] = *pStream;
			b = 0;
			if (mb[0] == '{' && is_first_byte){
				pStream++;
				mb[0] = *pStream;
				//while(mb[0] != '}'){
				while (!(mb[0] == '}' && is_first_byte)){
					word_with_rubi.addRubiText(mb[0]);
					pStream++;
					mb[0] = *pStream;

					if (!isExCharacter(b)){
						is_first_byte = is_first_byte ? false : true; //反転
					}
					b = mb[0];
				}
				pStream++;
			}
			word_with_rubi.calc(msg_font_handle);
			if (name != "") {
				word_with_rubi.setSpeakerName(name);
			}
			message.push_back(word_with_rubi);
		}
		cnt++;
		before_mb = mb[0];
	}
	if(!rubi){
		if(name != "") word.setSpeakerName(name);
		message.push_back(word);
	}
	it_word = message.begin();

	//if (input->getKeyControl() != 0){
	//	removeAllPlus();
	//}
}

//描画中フラグを返す
bool Teng::CmdMessage::getRenderingFlg(){
	return rendering_flg;
}

//コンフィグ値をもとに、実際の描画速度を決定する
void Teng::CmdMessage::calcMsgSpeed(Teng::Config* config){
	int msg_wait = config->getSetting().message_wait;

	switch (msg_wait){
	case 0:
		msg_draw_interval = 8;
		msg_draw_characters_num = 1;
		break;
	case 1:
		msg_draw_interval = 7;
		msg_draw_characters_num = 1;
		break;
	case 2:
		msg_draw_interval = 6;
		msg_draw_characters_num = 1;
		break;
	case 3:
		msg_draw_interval = 5;
		msg_draw_characters_num = 1;
		break;
	case 4:
		msg_draw_interval = 4;
		msg_draw_characters_num = 1;
		break;
	case 5:
		msg_draw_interval = 3;
		msg_draw_characters_num = 1;
		break;
	case 6:
		msg_draw_interval = 2;
		msg_draw_characters_num = 1;
		break;
	case 7:
		msg_draw_interval = 1;
		msg_draw_characters_num = 1;
		break;
	case 8:
		msg_draw_interval = 1;
		msg_draw_characters_num = 3;
		break;
	case 9:
		msg_draw_interval = 1;
		msg_draw_characters_num = 5;
		break;
	case 10:
		msg_draw_interval = 1;
		msg_draw_characters_num = 7;
		break;
	default:
		msg_draw_interval = 1;
		msg_draw_characters_num = 3;
		break;
	}

}

// @selectタグのメッセージ・ラベルをvector<Select> selects に格納する
void Teng::CmdMessage::setSelect(char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER+1][TENG_SCRIPT_COMMAND_MAX_LENGTH]){

	if(command[1][0] == NC){
		DEBUG printf("[ERROR] @select 引数を指定してください\n");
		throw ENGINE_SCRIPT_SELECT_ARGS_BLANK;
	}
	if (command[5][0] != NC){
		DEBUG printf("[ERROR] @select 選択肢は４つ以下にしてください。");
		throw ENGINE_SCRIPT_SELECT_ARGS_TOO_MUCH;
	}

	draw_select = true;
	for(int n = 1; command[n][0] != NC; n++){
		std::istringstream iss(command[n]);
		string splitted_cmd;
		Select select;
		while(getline(iss, splitted_cmd, '.')){
			if(select.getMsg() == ""){
				select.setMsg(splitted_cmd);
			}else{
				select.setLabal(splitted_cmd);
			}
		}
		if(select.getLabel() == ""){
			DEBUG printf("[ERROR] @select ラベルが指定されていません\n");
			throw ENGINE_SCRIPT_SELECT_ARGS_LABEL_NULL;
		}
		selects.push_back(select);
	}




}

void Teng::CmdMessage::addSelect(Select sel){
	selects.push_back(sel);

}

void Teng::CmdMessage::setDrawNameEntry(bool to_draw){
	draw_name_entry = to_draw;
	int sss = DxLib::SetActiveKeyInput(input_handle);
}

void Teng::CmdMessage::removeFirstPlus(){
	int i;
	bool plus_skip = false;

	int size = 0;
	for(auto it = message.begin(); it != message.end(); it++){
		size += it->baseText().size();
	}

	for(i = 0; buf[i] != NC && i < size && i < BUF_SIZE; i++){
		if(buf[i] == '+' && !plus_skip){
			plus_skip = true;
		}else{
			if(plus_skip){
				buf[i-1] = buf[i];
			}
		}
	}
	buf[i-1] = NC;
	//このタイミングでline_topとline_lenを調整
	//プラスを除去したら、除去した分の長さと位置をずらす必要がある
	line_len[curr_idx]--;
	line_top[curr_idx + 1]--;
	line_top[curr_idx + 2]--;

}

void Teng::CmdMessage::removeAllSpace(){
	int i;

	int plus_cnt = 0;
	int size = 0;
	for(auto it = message.begin(); it != message.end(); it++){
		size += it->baseText().size();
	}

	for(i = 0; buf[i] != NC && i < size && i < BUF_SIZE; i++){
		if(buf[i] == ' '){
			plus_cnt++;
		}else{
			buf[i - plus_cnt] = buf[i];
		}
	}
	buf[i-plus_cnt] = NC;
	//line_len[i] -= plus_cnt;
	//line_len[i+1] -= plus_cnt;
	//line_len[i+2] -= plus_cnt;
}
int Teng::CmdMessage::removeAllPlus(){
	int i;

	int plus_cnt = 0;
	int size = 0;
	for(auto it = message.begin(); it != message.end(); it++){
		size += it->baseText().size();
	}

	for(i = 0; buf[i] != NC && i < size && i < BUF_SIZE; i++){
		if(buf[i] == '+'){
			plus_cnt++;
		}else{
			buf[i - plus_cnt] = buf[i];
		}
	}
	buf[i-plus_cnt] = NC;
	return plus_cnt;
}

void Teng::CmdMessage::windowFade(){
	switch (msg_window_status){
	case eMsgWindow::MSG_WINDOW_TRANS_TYPE_FADEOUT:
		if (msg_window_trans_frame > 0){
			msg_window_trans_frame -= TENG_MESSAGE_WINDOW_TRANS_SPEED;
		}
		else{
			msg_window_trans_frame = 0;
			msg_window_hide_flg = true;
			msg_window_status = eMsgWindow::MSG_WINDOW_TRANS_TYPE_HIDE;
		}
		break;
	case eMsgWindow::MSG_WINDOW_TRANS_TYME_FADEIN:
		if (msg_window_trans_frame < 255){
			msg_window_hide_flg = false;
			msg_window_trans_frame += TENG_MESSAGE_WINDOW_TRANS_SPEED;
		}
		else{
			static int wait_frame = TENG_MESSAGE_WINDOW_TRANS_WAIT_FRAME;
			if (wait_frame > 0){
				wait_frame--;
			}else{
				msg_window_hide_flg = false;
				msg_window_status = eMsgWindow::MSG_WINDOW_TRANS_COMPLETED;
				wait_frame = TENG_MESSAGE_WINDOW_TRANS_WAIT_FRAME;
				msg_window_trans_frame = 255;
			}
		}
		break;
	default:
		break;
	}
}


bool Teng::CmdMessage::isExCharacter(const char& character)const{
	char* ex_char = "+|_";
	for (int pos = 0; ex_char[pos] != NC; pos++){
		if (character == ex_char[pos]) return true;
	}
	return false;
}

const int Teng::CmdMessage::getFontHandle(const int& type){
	switch (type){
	case MsgType::MSG:
		return msg_font_handle;
	case MsgType::RUBI:
		return rubi_font_handle;
	case MsgType::SAVE_DATE:
		return font_handle_save_date;
	case MsgType::SAVE_MSG:
		return font_handle_saveload_msg;
	case MsgType::SAVE_CONFIRM:
		return font_handle_save_msg_and_confirm;
	case MsgType::SAVE_YESNO:
		return font_handle_save_yesno;
	case MsgType::SAVE_DISABLED:
		return font_handle_saveload_select;
	case MsgType::SAVE_ENABLED:
		return font_handle_saveload_select;
	case MsgType::SAVE_FOCUS:
		return font_handle_saveload_select;
	case MsgType::NAME_ENTRY_LABEL:
		return font_handle_23px;
	case MsgType::NAME_ENTRY_INPUT:
		return font_handle_save_msg_and_confirm;
	case MsgType::DIALOG_TEXT:
		return font_handle_27px;
	case MsgType::SELECTION:
		return font_handle_26px;
	case MsgType::BACKLOG:
		return font_handle_19px;
	default:
		return -1;
	}
	return -1;
}

void Teng::CmdMessage::setCharacterName(string name){
	const char* namestring = name.c_str();
	char new_name[20];
	LCMapString(
		GetSystemDefaultLCID(),
		LCMAP_FULLWIDTH,
		namestring,
		20,
		new_name,
		sizeof(new_name));

	character_name = string(new_name);
}


const bool Teng::CmdMessage::checkInputNameLength(const string& name){
	const char* namestring = name.c_str();
	char new_name[20];
	LCMapString(
		GetSystemDefaultLCID(),
		LCMAP_FULLWIDTH,
		namestring,
		20,
		new_name,
		sizeof(new_name));
	string str = string(new_name);
	size_t name_size = str.length();
	printf("size:%d\n", (int)name_size);
	return name_size <= TENG_MESSAGE_CHARACTER_NAME_MAXLENGTH - 2;
}
