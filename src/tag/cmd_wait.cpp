﻿
#include "../define.h"
#include "../global.h"
#include "../error_code_define.h"
#include "../scene.h"

void Teng::Scene::wait(CmdMessage* msg, char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER + 1][TENG_SCRIPT_COMMAND_MAX_LENGTH]){
	int frame = atoi(command[1]);
	if (frame < 0){
		DEBUG printf("[ERROR] @wait[%d] 負の値が入力されました。\n", frame);
		throw ENGINE_SCRIPT_WAIT_NEGATIVE_VALUE;
	}
	if (string(command[2]) == "clear"){
		msg->clear();
	}
	wait_frame = frame;
}

int Teng::Scene::calcWait(){
	if (wait_frame > 0){
		wait_frame--;
	}
	return wait_frame;
}