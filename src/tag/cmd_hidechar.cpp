﻿#include "../graphic.h"
#include "../define.h"
#include "../global.h"
#include "../error_code_define.h"
#include "../graphic/graphic_manager.h"

extern bool is_hyper_skip;
void Teng::GraphicManager::hideCharacterGraphic(char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER+1][TENG_SCRIPT_COMMAND_MAX_LENGTH]){

	if(command[1][0] == '\0'){
		DEBUG printf("@char.hide 引数が不足しています\n");
		throw ENGINE_GRAPHIC_HIDECHAR_NO_SUCH_ARGUMENT;
	}
	std::string trans_type;
	int transition_speed;
	if (command[2][0] != NC){
		// トランタイプ
		
		if (strncmp(command[2], "none", TENG_SCRIPT_COMMAND_MAX_LENGTH) == 0){
			trans_type = "none";
		} else if (strncmp(command[2], "fadeout", TENG_SCRIPT_COMMAND_MAX_LENGTH) == 0){
			trans_type = "fadeout";
			
		}else{
			// デフォルトはフェードアウト
			trans_type = "fadeout";
		}
	}
	else{
		// デフォルトはフェードアウト
		trans_type = "fadeout";
	}

	if (command[3][0] != NC){
		// トラン速度
		transition_speed = atoi(command[3]);
	}
	else{
		transition_speed = TENG_GRAPHIC_TRANSITION_SPEED_DEFAULT;
	}

	bool discover_flg = false;
	int i;
	for(i = 0; i < TENG_GRAPHIC_CHARACTER_MAX; i++){
		if(this->character_graphics[i].getHandle() != 0
			&& this->character_graphics[i].getFileName() != NULL
			&& strncmp(command[1], this->character_graphics[i].getFileName(), TENG_FILE_NAME_MAX) == 0
			&& this->character_graphics[i].getVisible())
		{
			// トラン none の場合は即時消去
			if (trans_type == "none"){
				this->character_graphics[i].setVisible(false);
			}
			// fadeoutの場合はdrawCharacterGraphic内で消去を行う
			else if (trans_type == "fadeout"){
				transition_doing = true;
				this->character_graphics[i].setTransitionType(TENG_GRAPHIC_TRANSITION_FADEOUT);
				this->character_graphics[i].setTransitionSpeed(transition_speed);
				this->character_graphics[i].setTransPower(255);
			}
			discover_flg = true;
			break;
		}

	}
	if(!discover_flg){
		DEBUG printf("@char.hide 消去対象の画像は表示されていません：%s\n", command[1]);
		throw ENGINE_GRAPHIC_HIDECHAR_NOT_DISPLAYED;
	}
	if (is_hyper_skip){
		character_graphics[i].setTransitionType(TENG_GRAPHIC_TRANSITION_NOEFFECT);
		character_graphics[i].setVisible(false);
	}

}
