﻿#include "../graphic.h"
#include "../define.h"
#include "../global.h"
#include "../error_code_define.h"
#include "../graphic/graphic_manager.h"

void Teng::GraphicManager::freeCharacterImage(char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER+1][TENG_SCRIPT_COMMAND_MAX_LENGTH]){

	if(command[1][0] == '\0'){
		DEBUG printf("@freechar 引数が不足しています\n");
		throw ENGINE_GRAPHIC_FREECHAR_NO_SUCH_ARGUMENT;
	}
	bool discover_flg = false;
	int i;
	for(i = 0; i < TENG_GRAPHIC_CHARACTER_MAX; i++){
		if(this->character_graphics[i].getHandle() != 0
			&& this->character_graphics[i].getFileName() != NULL
			&& strncmp(command[1], this->character_graphics[i].getFileName(), TENG_FILE_NAME_MAX) == 0)
		{
			DxLib::DeleteGraph(this->character_graphics[i].getHandle());
			this->character_graphics[i].setVisible(false);
			this->character_graphics[i].setHandle(0);
			this->character_graphics[i].setX(0);
			this->character_graphics[i].setY(0);
			this->character_graphics[i].setZ(1);
			this->character_graphics[i].setImageType(eGraphicType_null);
			this->character_graphics[i].setFileName("");
			this->character_graphics[i].setChanging(false);

			discover_flg = true;
			break;
		}

	}
	if(!discover_flg){
		DEBUG printf("@freechar 解放対象の画像はロードされていません：%s\n", command[1]);
		throw ENGINE_GRAPHIC_FREECHAR_YET_LOADED;
	}
}

void Teng::GraphicManager::freeCharacterImageAll(){
	DEBUG printf("[INFO] 全てのキャラクター画像を解放します\n");

	//エラーが出ても無視して全て解放する
	for(int i = 0; i < TENG_GRAPHIC_CHARACTER_MAX; i++){
		DxLib::DeleteGraph(this->character_graphics[i].getHandle());
		this->character_graphics[i].setVisible(false);
		this->character_graphics[i].setHandle(0);
		this->character_graphics[i].setX(0);
		this->character_graphics[i].setY(0);
		this->character_graphics[i].setZ(1);
		this->character_graphics[i].setImageType(eGraphicType_character);
		this->character_graphics[i].setFileName("");
		this->character_graphics[i].setChanging(false);

	}

}