﻿#define _CRT_SECURE_NO_WARNINGS
#include "../define.h"
#include "../global.h"
#include "../error_code_define.h"


void Teng::Scenario::gotoScenario(const char *cmdlabel)
{
	int dest_line = searchLabel(cmdlabel);
	DEBUG printf("[GOTO] Jump to line #%d, label: %s\n", dest_line, cmdlabel);
	this->current_line = dest_line;
}

int Teng::Scenario::searchLabel(const char *cmdlabel)
{
	int line;

	//めんどくさいのでsplitStringみたいなものをここで書く

	//分割したスクリプト
	char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER+1][TENG_SCRIPT_COMMAND_MAX_LENGTH];
	int i;
	char* cp;
	char* copySrc;
	char* dst;

	const char* command_prefix = TENG_SCRIPT_COMMAND_PREFIX;
	const char* delim = TENG_SCRIPT_DELIMITER;

	//検索開始位置は現在行か先頭行か
	//for(line = this->current_line; line < this->max_line; line++){
	for(line = 0; line < this->max_line; line++){

		//元の文章をコピーする
		copySrc = (char*)malloc( sizeof(int) * TENG_SCRIPT_MAX_STRING_LENGTH + 1);

		strncpy_s(copySrc, TENG_SCRIPT_MAX_STRING_LENGTH, this->script[line], TENG_SCRIPT_MAX_STRING_LENGTH );
		cp = copySrc;

		//strtokを使って copySrc をdelim区切りで分割する
		for( i = 0; i < TENG_SCRIPT_ARGUMENT_MAX_NUMBER+1 ; i++ ) {
			//分割対象文字列が無くなるまで分割
			if( (dst = strtok(cp, TENG_SCRIPT_DELIMITER)) == NULL ) {
				break;
			}
			strcpy(command[i], dst);
			//2回目にstrtokを呼び出す時は，cpをNULLにする
			cp = NULL;
		}
		//分割された文字列の最後の要素はNULLとしておく
		command[i][0] = NULL;

		free(copySrc);

		//指定したラベルを探す
		if( strncmp(command[0], "@label", TENG_SCRIPT_COMMAND_MAX_LENGTH) == 0 ) {
			if( strncmp(command[1], cmdlabel, TENG_SCRIPT_COMMAND_MAX_LENGTH) == 0 ) {
				//指定したラベルが見つかった時
				return line;
			}
		}
	}
	DEBUG printf("[ERROR] @goto ラベル %s が見つかりませんでした。\n", cmdlabel);
	throw ENGINE_SCRIPT_LABEL_NOT_FOUND;
	return -1; //ここは通らないので意味は無い
}