﻿
#include "../define.h"
#include "../global.h"
#include "../error_code_define.h"


void Teng::Scenario::importScenario(
	Teng::Input* input,
	Teng::Scene* scene,
	Teng::CmdMessage* msg,
	Teng::GraphicManager* graphic,
	char *importfile,
	Teng::Scenario* subscenario)
{
	//char importfile_fullname[TENG_FILE_NAME_MAX] = TENG_DIR_SCENARIO;
	//strcat_s(importfile_fullname, TENG_FILE_NAME_MAX, importfile);
	Teng::Scenario *import_scenario = new Teng::Scenario(importfile);

	*subscenario = *import_scenario;

	this->setIsImportScenario(true);
	DEBUG printf("[INFO] スクリプト %s に移動します\n", subscenario->scenario_file);
}
