﻿#include "../define.h"
#include "../global.h"
#include "../error_code_define.h"
#include "../scenario.h"
#include <regex>

//キャラクターの好感度を初期化する
void Teng::Scenario::resetLove(){
	char_status.insert(make_pair(TENG_CHARACTER_NAME_TAG_1, 0));
	char_status.insert(make_pair(TENG_CHARACTER_NAME_TAG_2, 0));
	char_status.insert(make_pair(TENG_CHARACTER_NAME_TAG_3, 0));
	char_status.insert(make_pair(TENG_CHARACTER_NAME_TAG_4, 0));
	char_status.insert(make_pair(TENG_CHARACTER_NAME_TAG_5, 0));
	char_status.insert(make_pair(TENG_CHARACTER_NAME_TAG_6, 0));
	char_status.insert(make_pair(TENG_CHARACTER_NAME_TAG_7, 0));
	char_status.insert(make_pair(TENG_CHARACTER_NAME_TAG_8, 0));
	char_status.insert(make_pair(TENG_CHARACTER_NAME_TAG_9, 0));
}

//好感度の加減算処理
void Teng::Scenario::setLove(char (*command)[TENG_SCRIPT_COMMAND_MAX_LENGTH]){
	if(command[2][0] == NC){
		DEBUG printf("[ERROR] @char.love 引数が不足しています\n");
		throw ENGINE_SCRIPT_LOVE_ARGS_NULL;
	}

	string name = string(command[1]);
	if(!checkCharName(name)){
		DEBUG printf("[ERROR] @char.love キャラクター指定が不正です。\n");
		throw ENGINE_SCRIPT_LOVE_NO_SUCH_CHARACTER;
	}

	if(!checkLoveArg(string(command[2]))){
		DEBUG printf("[ERROR] @char.love 好感度の変更値指定が不正です。\n");
		throw ENGINE_SCRIPT_LOVE_INVALID_NUMBER;
	}

	DEBUG printf("[INFO] @char.love %s の好感度 %d ", command[1], char_status[name]);
	int num = getNumber(command[2]);
	switch(command[2][0]){
	case '+':
		char_status[name] += num;
		DEBUG printf("+ %d → %d\n", num, char_status[name]);
		break;
	case '-':
		char_status[name] -= num;
		DEBUG printf("- %d → %d\n", num, char_status[name]);
		break;
	default:
		char_status[name] = num;
		DEBUG printf("→ %d\n", num, char_status[name]);
		break;
	}

}

//有効なキャラクター名かどうかを判定
bool Teng::Scenario::checkCharName(string name){
	bool result;
	if(char_status.find(name) == char_status.end()){
		result = false;
	}else{
		result = true;
	}
	return result;
}

//加減算に使う値の有効判定
bool Teng::Scenario::checkLoveArg(string love_arg2){
	//第２引数は 数字　記号＋数字
	regex pattern("^[+-]?[0-9]+$");
	if(regex_match(love_arg2,pattern)){
		return true;
	}else{
		return false;
	}
}

//符号付の文字列から数値を取り出す
int Teng::Scenario::getNumber(const char* number_with_sign){
	regex pattern("[0-9]+");
	cmatch match;
	regex_search(number_with_sign, match, pattern);
	return stoi(match.str());
}

int* Teng::Scenario::backupLove(){
	int  status[9];
	status[0] = char_status[TENG_CHARACTER_NAME_TAG_1];
	status[1] = char_status[TENG_CHARACTER_NAME_TAG_2];
	status[2] = char_status[TENG_CHARACTER_NAME_TAG_3];
	status[3] = char_status[TENG_CHARACTER_NAME_TAG_4];
	status[4] = char_status[TENG_CHARACTER_NAME_TAG_5];
	status[5] = char_status[TENG_CHARACTER_NAME_TAG_6];
	status[6] = char_status[TENG_CHARACTER_NAME_TAG_7];
	status[7] = char_status[TENG_CHARACTER_NAME_TAG_8];
	status[8] = char_status[TENG_CHARACTER_NAME_TAG_9];
	return status;
}

void Teng::Scenario::clearLove(){
	char_status[TENG_CHARACTER_NAME_TAG_1] = 0;
	char_status[TENG_CHARACTER_NAME_TAG_2] = 0;
	char_status[TENG_CHARACTER_NAME_TAG_3] = 0;
	char_status[TENG_CHARACTER_NAME_TAG_4] = 0;
	char_status[TENG_CHARACTER_NAME_TAG_5] = 0;
	char_status[TENG_CHARACTER_NAME_TAG_6] = 0;
	char_status[TENG_CHARACTER_NAME_TAG_7] = 0;
	char_status[TENG_CHARACTER_NAME_TAG_8] = 0;
	char_status[TENG_CHARACTER_NAME_TAG_9] = 0;
}

void Teng::Scenario::restoreLove(const int* love_array){
	char_status[TENG_CHARACTER_NAME_TAG_1] = love_array[0];
	char_status[TENG_CHARACTER_NAME_TAG_2] = love_array[1];
	char_status[TENG_CHARACTER_NAME_TAG_3] = love_array[2];
	char_status[TENG_CHARACTER_NAME_TAG_4] = love_array[3];
	char_status[TENG_CHARACTER_NAME_TAG_5] = love_array[4];
	char_status[TENG_CHARACTER_NAME_TAG_6] = love_array[5];
	char_status[TENG_CHARACTER_NAME_TAG_7] = love_array[6];
	char_status[TENG_CHARACTER_NAME_TAG_8] = love_array[7];
	char_status[TENG_CHARACTER_NAME_TAG_9] = love_array[8];
}

string Teng::Scenario::getTopLoveCharName(){
	string top_char = "";
	int    top_point = INT_MIN;
	for(auto it = char_status.begin(); it != char_status.end(); it++){
		if(top_point > it->second){
			// 何もしない
		}else if(top_point == it->second){
			// 何もしない
		}else{
			top_point = it->second;
			top_char = it->first;
		}
	}
	return top_char;
}

void Teng::Scenario::splitCommandAndCharName(string cmd_charname, string& result_before, string& result_after){
	std::istringstream iss(cmd_charname);
	string splitted_cmd;
	string arg1_a = "";
	string arg1_b = "";
	while(getline(iss, splitted_cmd, '.')){
		if(arg1_a == ""){
			arg1_a = splitted_cmd;
		}else{
			arg1_b = splitted_cmd;
		}
	}
	result_before = arg1_a;
	result_after  = arg1_b;
}