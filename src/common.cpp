﻿#include "common.h"

using namespace Teng;

bool Common::cursorIn(int x_left, int x_right, int y_top, int y_bottom){
	int cursor_x, cursor_y;
	//double power = Window::getPow();
	DxLib::GetMousePoint(&cursor_x, &cursor_y);
	if((cursor_x >= x_left && cursor_x <= x_right) &&
		(cursor_y >= y_top && cursor_y <= y_bottom)){
			return true;
	}
	return false;
}

void Common::putCursorPoint(){
	int cursor_x, cursor_y;
	DEBUG DxLib::GetMousePoint(&cursor_x, &cursor_y);
	DEBUG printf("x:%d, y:%d\n", cursor_x, cursor_y);
}