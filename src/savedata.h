﻿//セーブデータ処理
#pragma once
#include "define.h"
#include "scenario.h"
#include "tag\cmd_message.h"
#include "graphic\graphic_manager.h"
#include "sound\sound_manager.h"
#include <vector>
#include "select.h"
#include <string>

namespace Teng{
	typedef struct CharacterGraphic_tag{

	}CharacterGraphicStruct;

	typedef struct SaveDataStruct_tag{
		short id;
		//Scenario
		char scenario_file[TENG_FILE_NAME_MAX];//シナリオファイル名
		bool is_import_scenario;//@import内にいる場合はtrue、通常時はfalse
		int  current_line;//現在のシナリオ行
		//CharacterGraphic
		//CharacterGraphicStruct character_structs[character_graphic_num];
		GraphicManager g_manager;
		char visible_facename[TENG_FILE_NAME_MAX];
		//背景用
		int bg_before_trans_arrnum;
		int bg_after_trans_arrnum;
		//ルール用
		int rule_using_arrnum;
		SoundManager s_manager;
		//画像
		unsigned char thumbnail[TENG_SAVE_SCREENSHOT_WIDTH*TENG_SAVE_SCREENSHOT_HEIGHT*3+10000];//スクリーンショット(縦*横*24bit+予備)

		//std::string select_msg[5];
		//std::string select_label[5];
		char select_msg[5][100];
		char select_label[5][100];

		//選択肢内の場合
		bool is_selection_drawing;

		//好感度
		int character_loves[9];

		//キャラクター名
		char character_name[TENG_MESSAGE_CHARACTER_NAME_MAXLENGTH];

		//ウィンドウタイトル
		char window_title[128];

		//メッセージ
		char messages[3][128];

		//日付
		char date[32];
	}SaveDataStruct;

	class SaveData{
	public:
		SaveData(bool initialize = true);
		~SaveData();
		//セーブ用データ一覧を作る
		int save(int id, Scenario* scenario, GraphicManager* graphic_manager, SoundManager* sound_manager, Window* wnd, CmdMessage* message, Scene* scene);

		//ロード処理
		//numはセーブデータ番号
		errno_t load(int num, Scenario* scenario, CmdMessage* msg, GraphicManager* graphic_manager, SoundManager* sound_manager, Window* wnd, Config* config, Scene* scene);

		//セーブデータファイルを出力
		errno_t create(SaveDataStruct& strct, Scene* scene);

		SaveDataStruct save_struct;

		//スクリーンショットを取得、縮小して出力
		//失敗する場合は前後非同期をOFFにするとよい
		static void makeScreenShotShumbnail();

		//accessor
		static void setThumbnailHandle(int i, int handle){ thumbnail_handle[i] = handle;}
		static int  getThumbnailHandle(int i){ return thumbnail_handle[i];}
		static void setSavedataExist(int id, bool exist){ savedata_exist[id] = exist;}
		static bool getSavedataExist(int id){ return savedata_exist[id];}
		static const SaveDataStruct get(int id);

	private:
		short id;
		//Scenario
		char scenario_file[TENG_FILE_NAME_MAX];//シナリオファイル名
		bool is_import_scenario;
		int  current_line;//現在のシナリオ行
		static int thumbnail_handle[TENG_SAVE_SAVEDATA_MAX];
		static bool savedata_exist[TENG_SAVE_SAVEDATA_MAX];

		//ロード確認ダイアログ表示時に、何番のデータをクリックしたか保存しておく
		//static int keep_load_number;

	};
}