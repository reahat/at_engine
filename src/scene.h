﻿//シーン管理クラス
#pragma once
#include "graphic.h"
//#include "tag\cmd_message.h"
#include "graphic\graphic_manager.h"
#include "window.h"
#include "config.h"
#include "sound\sound_manager.h"
#include <Windows.h>
#include <string>
#include <thread>
//#include "input.h"
//#include "scenario.h"

using namespace std;
namespace Teng{
	class Scenario;
	class Input;
	class CmdMessage;

	class Scene{
	public:

		enum eScene{
			eScene_none,
			eScene_logo,
			eScene_title,
			eScene_title_load,
			eScene_scenario,
			eScene_scenario_save,
			eScene_scenario_load,
			eScene_minigame,
			eScene_movie,
			eScene_title_config,
			eScene_scenario_config,
			eScene_scenario_backlog,
			eScene_scenario_movie,
			eScene_scenario_menu,
			eScene_gallery,
			eScene_hyperskip
		};

		typedef enum eFadeType{
			stop,
			no_fade,
			fadein,
			fadeout,
			crossfade,
			use_rule
		} FadeType;
		typedef enum eSceneType{
			no_change,
			scene_exit,
			logo,
			title,
			title_load,
			title_config,
			menu,
			gallery,
			scenario,
			scenario_load,
			scenario_config,
			backlog,
			movie,
			save
		} SceneType;


		Scene();
		~Scene();
		void afterInit();
		void addFrame();
		unsigned long getFrame();
		void resetFrame(){ frame = 0; }
		void drawScreen(Teng::GraphicManager* graphic_manager, Teng::Scenario* scenario, Teng::CmdMessage* message, Teng::Config* config, Teng::Input* input, Teng::SoundManager* s_manager, Teng::Window* window);

		void doScenario(
			CmdMessage* message,
			Window* window,
			Config* config,
			Input* input,
			GraphicManager* g_manager,
			SoundManager* s_manager,
			Scenario* scenario,
			Scenario* subscenario);

		void doTitle(
			Window* window,
			Config* config,
			Input* input,
			GraphicManager* g_manager,
			SoundManager* s_manager);
		void resetTitleButtonFlag();
		void sceneScenarioInitialize(GraphicManager* g_manager);

		void doLogo(
			Window* window,
			Config* config,
			Input* input,
			GraphicManager* g_manager,
			SoundManager* s_manager);

		void doScenarioLoad(
			Window* window,
			Config* config,
			Input* input,
			GraphicManager* g_manager,
			SoundManager* s_manager,
			Scenario* scenario,
			CmdMessage* message);

		void doTitleLoad(
			Window* window,
			Config* config,
			Input* input,
			GraphicManager* g_manager,
			SoundManager* s_manager,
			Scenario* scenario,
			CmdMessage* message);

		void doScenarioSave(
			Window* window,
			Config* config,
			Input* input,
			GraphicManager* g_manager,
			SoundManager* s_manager,
			Scenario* scenario,
			CmdMessage* message);

		//ScenarioLoadとTitleLoadの共通セーブ処理
		void doLoadCommon(
			Window* window,
			Config* config,
			Input* input,
			GraphicManager* g_manager,
			SoundManager* s_manager,
			Scenario* scenario,
			CmdMessage* message);

		//セーブデータサムネイルの展開
		void decodeSavedataThumbnail(
			Window* window,
			Config* config,
			Input* input,
			GraphicManager* g_manager,
			SoundManager* s_manager);

		//セーブデータサムネイルの展開
		//クリックしたところのみ
		void decodeSavedataThumbnailOnlyClicked(
			Window* window,
			Config* config,
			Input* input,
			GraphicManager* g_manager,
			SoundManager* s_manager,
			int number);

		// 非同期ロード用
		// 1-4を完了次第順に使用する
		void decodeSavedataThumbnail1(
			Window* window,
			Config* config,
			Input* input,
			GraphicManager* g_manager,
			SoundManager* s_manager);

		void decodeSavedataThumbnail2(
			Config* config,
			Input* input,
			GraphicManager* g_manager,
			SoundManager* s_manager);

		void decodeSavedataThumbnail3(
			Config* config,
			Input* input,
			GraphicManager* g_manager,
			SoundManager* s_manager);

		void decodeSavedataThumbnail4(
			Config* config,
			Input* input,
			GraphicManager* g_manager,
			SoundManager* s_manager);

		void doScenarioBacklog(
			Window* window,
			Config* config,
			Input* input,
			GraphicManager* g_manager,
			SoundManager* s_manager,
			Scenario* scenario,
			CmdMessage* message);

		void doScenarioMenu(
			Window* window,
			Config* config,
			Input* input,
			GraphicManager* g_manager,
			SoundManager* s_manager,
			Scenario* scenario,
			CmdMessage* message);

		void doTitleConfig(
			Window* window,
			Config* config,
			Input* input,
			GraphicManager* g_manager,
			SoundManager* s_manager);

		void doScenarioConfig(
			Window* window,
			Config* config,
			Input* input,
			GraphicManager* g_manager,
			SoundManager* s_manager);

		void doConfigCommon(
			Window* window,
			Config* config,
			Input* input,
			GraphicManager* g_manager,
			SoundManager* s_manager);

		void doMovie(
			CmdMessage* message,
			Window* window,
			Config* config,
			Input* input,
			GraphicManager* g_manager,
			SoundManager* s_manager);

		void doHyperskip(
			CmdMessage* message,
			Window* window,
			Config* config,
			Input* input,
			GraphicManager* g_manager,
			SoundManager* s_manager,
			Scenario* scenario,
			Scenario* subscenario);
		
		bool isExecuteTargetTagForHyperskip(const int tag);
		bool isStopTargetTagForHyperskip(const int tag);

		void reloadHyperSkip(Scenario* scenario, CmdMessage* message, GraphicManager* g_manager, SoundManager* s_manager, Window* window, Config* config, Scene* scene);

		bool checkAutoMsg(Config* config, Input* input);


		void setNowScene(int scene){ now_scene = scene;}
		int  getNowScene(){ return now_scene;}
		void setBeforeScene(int scene){ before_scene = scene;}
		int  getBeforeScene(){ return before_scene;}
		void setFadeType(FadeType fade_type){ this->fade_type = fade_type; }

		//オートモード用
		unsigned long getAutoFrame(){return auto_frame;}
		void addAutoFrame(){if(auto_frame==ULONG_MAX){auto_frame=0;}else{auto_frame++;}}
		void resetAutoFrame(){auto_frame=0;}
		bool getSceneChanging(){ return scene_changing; }
		void setSceneChanging(const bool is_change){ scene_changing = is_change; }
		bool cursorOnButton(){ return cursor_on_button; }
		void cursorOnButton(bool is_on_button){ cursor_on_button = is_on_button; }

	private:
		int now_scene;
		int before_scene;
		unsigned long frame;
		unsigned long auto_frame;
		int color_black;
		int color_white;
		bool scene_changing;
		int screenshot_handle;

		//画像などのロード完了かどうかの確認用
		bool loaded;

		//タイトル画面のボタン色判定
		bool button_1_clicked;
		bool button_2_clicked;
		bool button_3_clicked;
		bool button_4_clicked;
		bool button_5_clicked;
		bool cursor_on_button;

	/********** cmd_settitle **********/
	public:
		void setTitle(char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER + 1][TENG_SCRIPT_COMMAND_MAX_LENGTH]);
		char* getWindowTitle(){ return window_title; }
	private:
		char window_title[128];

	/********** cmd_resettitle **********/
	public:
		void resetTitle();

	/********** cmd_wait **********/
	public:
		//void wait(const int frame);
		void wait(CmdMessage* msg, char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER + 1][TENG_SCRIPT_COMMAND_MAX_LENGTH]);
		void resetWait(){ wait_frame = 0; }
		void setWaitFrame(int f){ wait_frame = f; }
	private:
		unsigned long wait_frame;
		int  calcWait();
	
	/********** 動画再生 *********/
	public:
		void setPlayMovie(CmdMessage* message, SoundManager* s_manager, const char* command);
		int  playMovie();
		bool getMoviePlaying(){ return movie_playing; }
		void setMoviePlaying(bool playing){ movie_playing = playing; }
		void checkFadeForMovie(CmdMessage* message, SoundManager* s_manager);


	private:
		//シナリオ画面でのフェードも含める
		bool movie_playing;
		int  movie_handle;
		bool movie_fading;

	/********** ギャラリー *********/
	public:
		static int isGalleryPicture(const string filename);
		void doGallery(
			Window* window,
			Config* config,
			Input* input,
			GraphicManager* g_manager,
			SoundManager* s_manager,
			Scenario* scenario,
			CmdMessage* message);
		void drawPictures(
			Config* config,
			Input* input,
			GraphicManager* g_manager);
		int  loadPictures(GraphicManager* g_manager);
		int nextPictureExist(Config* config, GraphicManager* g_manager, arrnum now_pic);
	private:
		enum eGallery{
			cg_00, cg_01, cg_02, cg_03, cg_04, cg_05, cg_06, cg_07, cg_08, cg_09,
			cg_10, cg_11, cg_12, cg_13, cg_14, cg_15, cg_16, cg_17, cg_18, cg_19,
			cg_20, cg_21, cg_22, cg_23, cg_24, cg_25, cg_26, cg_27, cg_28, cg_29,
			cg_30, cg_31, cg_32, cg_33, cg_34, cg_35, cg_36, cg_37, cg_38, cg_39,
			cg_40, cg_41, cg_42, cg_43, cg_44, cg_45, cg_46, cg_47, cg_48, cg_49,
			cg_50, cg_51, cg_52, cg_53, cg_54, cg_55, cg_56, cg_57, cg_58, cg_59,
			cg_60, cg_61, cg_62, cg_63
		};
		void makeGalleryFilesList();
		static pair<int, string> gallery_files[TENG_GALLERY_MAX_NUMBER];
		int gallery_page_num;
		int gallery_page_num_before;//切り替え前のページ番号
		bool gallery_large_mode;
		int gallery_large_mode_picno;
		bool page_changing;


	/********** シナリオ画面用 *************/
	private:
		int clicked_button;
	public:
		void setClickedButton(int button_type){ clicked_button = button_type; }
	/********** セーブ・ロード *********/
	public:
		const int getDecodeFileHandle(int num){ return decode_filehandle[num]; }

	private:
		int saveload_pagenum;
		int saveload_before_pagenum;
		bool saveload_page_changing;
		bool saveload_confirm_fadein;
		bool saveload_confirm_fadeout;
		bool saveload_confirm_fadeout_reverse;
		const static int SAVEDATA_X_1 = 197;
		const static int SAVEDATA_X_2 = 405;
		const static int SAVEDATA_X_3 = 612;
		const static int SAVEDATA_X_4 = 820;
		const static int SAVEDATA_Y_1 = 91;
		const static int SAVEDATA_Y_2 = 243;
		const static int SAVEDATA_Y_3 = 395;
		const static int SAVEDATA_WIDTH  = 170;
		const static int SAVEDATA_HEIGHT = 130;
		const static DWORD SAVEDATA_CONFIRM_DIALOG_COLOR;
		const static DWORD SAVEDATA_CONFIRM_MSG_COLOR;
		const static DWORD SAVEDATA_STRING_DISABLED;
		const static DWORD SAVEDATA_STRING_ENABLE;
		const static DWORD SAVEDATA_STRING_FOCUS;
		const static int SAVEDATA_CONFIRM_DIALOG_HEIGHT = 158;
		const static int SAVEDATA_CONFIRM_DIALOG_Y = 554;
		const static int SAVEDATA_CONFIRM_MSG_DATE_X = 48;
		const static int SAVEDATA_CONFIRM_MSG_DATE_Y = 566 - SAVEDATA_CONFIRM_DIALOG_Y;
		const static int SAVEDATA_CONFIRM_MSG_TEXT_X = 53;
		const static int SAVEDATA_CONFIRM_MSG_TEXT_Y = 616 - SAVEDATA_CONFIRM_DIALOG_Y;
		const static int SAVEDATA_CONFIRM_MSG_QUESTION_X = 713;
		const static int SAVEDATA_CONFIRM_MSG_QUESTION_Y = 586 - SAVEDATA_CONFIRM_DIALOG_Y;
		const static int SAVEDATA_CONFIRM_MSG_YES_X = 767;
		const static int SAVEDATA_CONFIRM_MSG_NO_X = 872;
		const static int SAVEDATA_CONFIRM_MSG_YESNO_Y = 637 - SAVEDATA_CONFIRM_DIALOG_Y;
		const static int SAVEDATA_CONFIRM_ARROW_YES_X = 740;
		const static int SAVEDATA_CONFIRM_ARROW_NO_X = 845;
		const static int SAVEDATA_CONFIRM_ARROW_YESNO_Y = 645 - SAVEDATA_CONFIRM_DIALOG_Y;

		const int getThumbnailX(const int& data_in_page);
		const int getThumbnailY(const int& data_in_page);
		int saveload_confirm_dialog;
		//int saveload_dialog_back;
		int saveload_dialog_dummy;
		bool saveload_cursor_is_hand;
		bool is_dialog_draw;
		

		//セーブロードのサムネイルデコード非同期処理用
		int process_status;
		char decode_savefile[TENG_SAVE_SAVEDATA_MAX][TENG_FILE_NAME_MAX];
		LONGLONG decode_filesize[TENG_SAVE_SAVEDATA_MAX];
		char* decode_buffer[TENG_SAVE_SAVEDATA_MAX];
		int decode_filehandle[TENG_SAVE_SAVEDATA_MAX];
		int decode_img_handle[TENG_SAVE_SAVEDATA_MAX];
		int keep_load_number;

		/************** ハイパースキップ ******************/
		private:
		bool hyperskip_doing;



		/********** コンフィグ画面 *********/
		thread thread;
		static const int BACK_X = 27;
		static const int BACK_Y = 696;
		static const int SKIP_ALL_X = 450;
		static const int SKIP_Y = 251;
		static const int SKIP_ALREADY_X = 579;
		static const int MINUS_X = SKIP_ALL_X;
		static const int PLUS_X = 886;
		static const int MINUS_Y1 = 313;
		static const int MINUS_Y2 = 366;
		static const int MINUS_Y3 = 419;
		static const int MINUS_Y4 = 476;
		static const int MINUS_Y5 = 526;
		static const int BAR_BASE_X = 487;
		static const int BAR_BASE_Y1 = MINUS_Y1 + 9;
		static const int BAR_BASE_Y2 = MINUS_Y2 + 9;
		static const int BAR_BASE_Y3 = MINUS_Y3 + 9;
		static const int BAR_BASE_Y4 = MINUS_Y4 + 9;
		static const int BAR_BASE_Y5 = MINUS_Y5 + 9;
		static const int BAR_Y1 = MINUS_Y1 + 4;
		static const int BAR_Y2 = MINUS_Y2 + 4;
		static const int BAR_Y3 = MINUS_Y3 + 4;
		static const int BAR_Y4 = MINUS_Y4 + 4;
		static const int BAR_Y5 = MINUS_Y5 + 4;
		static const int VOL_0 = 0;
		static const int VOL_1 = 70;
		static const int VOL_2 = 90;
		static const int VOL_3 = 110;
		static const int VOL_4 = 140;
		static const int VOL_5 = 170;
		static const int VOL_6 = 190;
		static const int VOL_7 = 210;
		static const int VOL_8 = 230;
		static const int VOL_9 = 250;

		typedef enum eButtonState{
			btn_st_normal,
			btn_st_focus,
			btn_st_click
		}ButtonState;

		struct Btn{
			Btn();
			int now;
			int normal;
			int focus;
			int click;
			int x;
			int y;
			int width;
			int height;
			int power;
			int status;
		};
		int background;
		int bar_base;
		int bar_base_width;
		bool mouse_left_clicked;
		bool mouse_right_clicked;

		Btn btn_all;
		Btn btn_already;
		Btn btn_minus1, btn_minus2, btn_minus3, btn_minus4, btn_minus5;
		Btn btn_plus1, btn_plus2, btn_plus3, btn_plus4, btn_plus5;
		Btn btn_back;
		Btn bar1, bar2, bar3, bar4, bar5;
		int tmp_bar3_vol, tmp_bar4_vol, tmp_bar5_vol;
		int keep_btn;

		int bar1x, bar2x, bar3x, bar4x, bar5x;

		int skip_status;
		enum{ sk_already, sk_all };
		bool back_clicked;

		//Resources* resources;
		//Input* input;
		//Config* config;
		//ULONG frame;
		int   fade_type;
		int next_scene;
		bool cursorIn(const Btn& button)const;
		void adjustBarLoc(Btn& btn);
		int volumeToPower(const int& vol);
		int powerToVolume(const int& pow);
		int powerToX(const int& pow);
		int XtoPower(const int& x);
		void clickPlusButton(Btn& btn);
		void clickMinusButton(Btn& btn);
		void writeConfig();
		void checkVolume();
		int  calcPlayBgmVolume(SoundManager* s_manager, Config* config, BackGroundMusic& bgm, string scene="title");
		int  calcPlaySeVolume(SoundManager* s_manager, Config* config, arrnum arrnum);
		int  calcPlayBgmVolumeWithBgmVol(SoundManager* s_manager, Config* config, int volume);
		int  calcPlaySeVolumeWithSeVol(SoundManager* s_manager, Config* config, int volume);

		Input* input;
		Config* config;
		SoundManager* s_manager;
		void drawConfigGraphic();
		void execFadeConfig();
		void addFrameConfig(int add_count);

		public:
		void sceneConfigInitialize(
			Window* window,
			Config* config,
			Input* input,
			GraphicManager* g_manager,
			SoundManager* s_manager);

		/*****************右クリックメニュー***************/
		public:
			void sceneMenuInitialize(
				Window* window,
				Config* config,
				Input* input,
				GraphicManager* g_manager,
				SoundManager* s_manager);
			void resetButtons();
		private:
			Btn  btn_save, btn_load, btn_hskip, btn_config;
			Btn  btn_backlog, btn_title, btn_exit;
			int  menu_base;
			int  back_color;
			int  back_alpha;
			bool first_load_flg;
			int  tmp_next_scene;
			bool button_clicked;
			bool fade_completed;

		// バックログ
		public:
			void sceneBacklogInitialize(
				Window* window,
				Config* config,
				Input* input,
				GraphicManager* g_manager,
				SoundManager* s_manager);
			bool isBacklogEnable(){ return is_backlog_enable; }
		private:
			bool is_backlog_enable;
			Btn  btn_backlog_base, btn_backlog_bar_base;
			Btn  btn_backlog_bar, btn_backlog_up, btn_backlog_down;
			Btn  btn_backlog_back;
			int  font_color_backlog;
		// 背景エフェクト
		public:
			void setEffectFlash(const char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER + 1][TENG_SCRIPT_COMMAND_MAX_LENGTH]);
			void setEffectSepia(const char command[TENG_SCRIPT_ARGUMENT_MAX_NUMBER + 1][TENG_SCRIPT_COMMAND_MAX_LENGTH]);
			bool doBackEffect(GraphicManager* g_manager, Input* input);
			int  effect_flash_fadein_power;
			int  effect_flash_fadeout_power;
			int  effect_flash_frame;
			bool is_effect_do;
			string effect_type;
			string effect_status;

		private:
			int tmp_screen;
	};




}