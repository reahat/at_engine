#include "scene.h"
#include "scenario.h"
#include "DxLib.h"
#include "global.h"
#include "common.h"
#include "savedata.h"
#include "error_code_define.h"
#include <fstream>
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")
#include <list>

#include <typeinfo>
#include <iostream>
#include <cmath>


extern char common_dir[256];
extern bool is_hyper_skip;
using namespace Teng;

void Teng::Scene::doScenarioBacklog(
	Window* window,
	Config* config,
	Input* input,
	GraphicManager* g_manager,
	SoundManager* s_manager,
	Scenario* scenario,
	CmdMessage* message)
{





	//右クリックでメニューへ
	if (now_scene == eScene_scenario){
		if (input->getKeepMouseRight() != 0 && !window->getDialogDrawing() && !is_backlog_enable && message->isMsgDrawCompleted()){
			//画面遷移前にスクリーンショットを取得しておく
			DxLib::SetUseASyncLoadFlag(FALSE);
			SaveData::makeScreenShotShumbnail();




			//DxLib::SetUseASyncLoadFlag(FALSE);
			//スクリーンショットを作成して、メニュー画面の背景にする
			screenshot_handle = DxLib::MakeGraph(1024, 768);
			DxLib::GetDrawScreenGraph(0, 0, 1024, 768, screenshot_handle);
			now_scene = eScene_scenario_menu;
			before_scene = eScene_scenario;
			DxLib::SetUseASyncLoadFlag(TRUE);
			sceneMenuInitialize(window, config, input, g_manager, s_manager);
			resetButtons();
			//シナリオに戻った時の復元用
			//resources->tmpData()->tmpScriptFileName(command_queue->getScriptFilename());
			//resources->tmpData()->tmpScenarioExist(true);

			//for (auto it = cmd_list.begin(); it != cmd_list.end(); it++){
			//	if (!(*it)->isExecuted()){
			//		it--;
			//		(*it)->isExecuted(false);
			//		break;
			//	}
			//}


		}
	}


















	static bool is_backlog_initialized = false;
	if (!is_backlog_enable) return;
	if (!is_backlog_initialized){
		sceneBacklogInitialize(window, config, input, g_manager, s_manager);
		is_backlog_initialized = true;
	}

	// 強制的に最下段を表示する
	bool force_bottom = false;

	btn_backlog_up.now = btn_backlog_up.normal;
	btn_backlog_down.now = btn_backlog_down.normal;
	btn_backlog_bar.now = btn_backlog_bar.normal;


	//初回表示・上下ボタン押下時に一度だけログのポインタ計算をする
	static bool seeklog_flg = false; // falseの時のみ計算

	//g_manager->drawBackGroundGraphic(input);
	//g_manager->drawCharacterGraphic(input);
	////g_manager->drawSystemGraphic(now_scene);
	//g_manager->drawFaceGraphic();

	//画面に透過グレーをかぶせる
	DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, 128);
	DxLib::DrawBox(0, 0, 1724, 768, back_color, TRUE);
	DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 128);
	// ベース
	DxLib::DrawGraph(btn_backlog_base.x, btn_backlog_base.y, btn_backlog_base.normal, TRUE);

	static int block_cnt = 0;
	static int line_cnt = 0;
	Backlog logs = message->getLog();
	list<BacklogMessage> a_log = logs.logs();
	list<BacklogMessage>::iterator a_log_it = a_log.end();

	list<string>::iterator msg_it;
	list<string> log_msgs;

	bool is_mouse_left_clicked = (input->getKeepMouseLeft() != 0);

	static int say_cnt = 0;

	// 17行をカウントしたら打ち止め
	int log_line_cnt;
	// バックログ画面初期時
	if (!seeklog_flg){
		say_cnt = 0;
		block_cnt = 0;
		line_cnt = 0;
		logs = message->getLog();
		a_log = logs.logs();
		a_log_it = a_log.end();
		log_line_cnt = 0;
		while (a_log_it != a_log.begin() && log_line_cnt < 17){
			// A-3を指す
			if (log_line_cnt < 17){
				a_log_it--;
				block_cnt++;
			}
			log_msgs = *a_log_it;
			msg_it = log_msgs.end();
			line_cnt = 0;
			while (msg_it != log_msgs.begin() && log_line_cnt < 17){
				log_line_cnt += 1;
				msg_it--;
				line_cnt++;
			}
		}
		seeklog_flg = true;
		for (auto l : logs.logs()){
			if (!l.getSpeaker().empty()){
				say_cnt++;
			}
		}
	}


	if (block_cnt > 0 || line_cnt > 0){ //ログが無いときのエラー回避
		//表示
		log_line_cnt = 0;

		a_log_it = a_log.end();
		std::advance(a_log_it, block_cnt*(-1));
		log_msgs = *a_log_it;
		msg_it = log_msgs.end();

		if (log_msgs.size() < line_cnt){
			std::advance(msg_it, -1);
		}
		else{
			std::advance(msg_it, line_cnt*(-1));
		}


		DxLib::SetFontSpaceToHandle(-2, message->getFontHandle(CmdMessage::MsgType::BACKLOG));

		while (a_log_it != a_log.end() && log_line_cnt < 17){
			log_msgs = *a_log_it;
			while (msg_it != log_msgs.end() && log_line_cnt < 17){

				if (!a_log_it->getSpeaker().empty() && msg_it == log_msgs.begin()){

					string speaker = a_log_it->getSpeaker();

					string name = "【" + string(speaker.c_str()) + "】";
					log_msgs.insert(msg_it, name.c_str());
					msg_it--;

					//DxLib::DrawStringToHandle(
					//	TENG_BACKLOG_POINT_X - 80,
					//	TENG_BACKLOG_POINT_Y + (TENG_MESSAGE_FONT_SIZE + TENG_BACKLOG_LINE_SPACE) * log_line_cnt,
					//	a_log_it->getSpeaker().c_str(),
					//	font_color_backlog,
					//	message->getFontHandle(CmdMessage::MsgType::BACKLOG));
				}

				DxLib::DrawStringToHandle(
					TENG_BACKLOG_POINT_X,
					TENG_BACKLOG_POINT_Y + (TENG_MESSAGE_FONT_SIZE + TENG_BACKLOG_LINE_SPACE) * log_line_cnt,
					(*msg_it).c_str(),
					font_color_backlog,
					message->getFontHandle(CmdMessage::MsgType::BACKLOG));
				msg_it++;
				log_line_cnt += 1;
			}
			if (log_line_cnt < 17) a_log_it++;
			if (a_log_it == a_log.end()) break;
			log_msgs = *a_log_it;
			if (log_line_cnt == 17) break;
			msg_it = log_msgs.begin();
		}
	}

	////// スクロールバー表示位置の計算 //////
	//ログは全部で何行あるか
	int all_log_lines = 0;//ログ全体の行数
	for each(list<string> log_block in a_log){
		for each(string line in log_block){
			all_log_lines++;
		}
	}
	all_log_lines += say_cnt;
	//ログ全体の行数から、最下の９行を除いた数
	int lines_for_bar = all_log_lines - 16;
	//ブロック数は先頭から数えたいが、block_cntは下からの数なので逆順にする
	//行数は逆順にしなくても問題はない
	list<BacklogMessage> reverse_log_blocks = a_log;
	reverse_log_blocks.reverse();

	//現在は全体の何行目を表示しているか
	int blocks = 0;
	int lines = 0;
	int line_sum = 0;

	bool break_flg = false;
	for each(list<string> log_block in reverse_log_blocks){
		blocks++;
		for each(string line in log_block){
			lines++;
			line_sum++;
			if (blocks == block_cnt && lines == line_cnt){
				break_flg = true;
				break;
			}
		}
		if (break_flg) break;
		lines = 0;
	}
	line_sum -= 17;
	int now_top_line = lines_for_bar - line_sum;

	//最下画面の１０行を除き、全部でlines_for_bar行ある。
	//現在表示中のトップ行はnow_top_line
	//現在、全体のbar_ratio * 170%の位置にいる

	double bar_ratio = (double)(now_top_line) / (double)lines_for_bar;
	if (bar_ratio < 0) bar_ratio = 1.0;
	//DEBUG printf("bar-ratio:%f\n", bar_ratio);
	//上ボタンの下部は250(隙間を開けて260ということにする), 下ボタンの上部は630、余裕を持って600。差分は
	//int button_diff_y = 600 - 260;
	int button_diff_y = (btn_backlog_down.y) - (btn_backlog_up.y + btn_backlog_up.height) - btn_backlog_bar.height;
	//ログのカーソル位置が１行目のとき、強制的にバーを最上部にする
	//(割り算の結果bar_ratioは絶対にゼロにならないため誤魔化す）
	if (now_top_line == 1) bar_ratio = 0;
	static int before_bar_point = -1;
	int bar_point = (int)((double)button_diff_y * bar_ratio) + (btn_backlog_up.y + btn_backlog_up.height);

	//if (before_bar_point > 0 && bar_point == button_diff_y + btn_backlog_up.y + btn_backlog_up.height - 4){
	//	//bar_point = before_bar_point;
	//	bar_point = button_diff_y + btn_backlog_up.y + btn_backlog_up.height - 4;
	//}

	if (bar_point > button_diff_y + (btn_backlog_up.y + btn_backlog_up.height) - 5){
		bar_point = button_diff_y + (btn_backlog_up.y + btn_backlog_up.height + 3);
		force_bottom = true;
	}
	//before_bar_point = bar_point;

	static string clicking_button = "";

	////// スクロールバー表示位置ここまで //////
	

	int cursor_x, cursor_y;
	DxLib::GetMousePoint(&cursor_x, &cursor_y);
	if (block_cnt > 0 || line_cnt > 0){

		////// バークリック //////
		//if (Common::cursorIn(930, 980, 260, 617)){
		if (Common::cursorIn(
			btn_backlog_bar_base.x,
			btn_backlog_bar_base.x + btn_backlog_bar_base.width,
			btn_backlog_bar_base.y,
			btn_backlog_bar_base.y + btn_backlog_bar_base.height))
		{
			if (input->getMouseLeft() != 0){
				int click_point = cursor_y;
				//クリック地点は全体の何％の位置にあるか
				//double clicked_per = (double)(click_point - 260) / (double)(600 - 260);
				double clicked_per = (double)(click_point - (btn_backlog_up.y + btn_backlog_up.height)) / (double)(btn_backlog_down.y - (btn_backlog_up.y + btn_backlog_up.height) - btn_backlog_bar.height);
				//先頭からprint_top_line行目を先頭行として表示する
				int print_top_line = (int)((double)lines_for_bar * clicked_per);
				//これを後ろから数えた行数に変換
				int print_top_line_reverse = all_log_lines - print_top_line + 1;
				if (print_top_line_reverse < 1) print_top_line_reverse = 1;
				if (print_top_line_reverse > all_log_lines) print_top_line_reverse = all_log_lines;
				int b_block = 0;
				int b_line = 0;
				int line_total = 0;
				for each(list<string> block in reverse_log_blocks){
					bool flag = false;
					b_block++;
					for each(string li in block){
						b_line++;
						line_total++;
						if (line_total == print_top_line_reverse){
							flag = true;
							break;
						}
					}
					if (flag) break;
					b_line = 0;
				}
				block_cnt = b_block;
				line_cnt = b_line;
			}
		}
		////// バークリックここまで //////


		////// スクロール可能箇所を掴んだ場合 //////
		static bool scroll_bar_hold_flg = false;
		// スクロールバーのどこを掴んだのか
		static int  scroll_bar_hold_point = 0;
		//バーを初めて掴んだ
		//if ((Common::cursorIn(930, 980, bar_point, bar_point + 17)) && input->getKeepMouseLeft()){
		if (Common::cursorIn(
			btn_backlog_bar.x,
			btn_backlog_bar.x + btn_backlog_bar.width,
			bar_point,
			bar_point + btn_backlog_bar.height))
		{
			btn_backlog_bar.now = btn_backlog_bar.focus;
			if (is_mouse_left_clicked){
				clicking_button = "bar";
				scroll_bar_hold_flg = true;
				scroll_bar_hold_point = cursor_y - bar_point;
			}
		}
		//バーから手を離した
		if (input->getMouseLeft() == 0){
			scroll_bar_hold_flg = false;
		}
		//バーを掴んだままの状態
		if (scroll_bar_hold_flg){
			int cursor_x, cursor_y;
			DxLib::GetMousePoint(&cursor_x, &cursor_y);
			//bar_point = cursor_y - 5;
			bar_point = cursor_y - scroll_bar_hold_point;
			//バーが上に行き過ぎないようにする
			double rat = 1.0f / (double)lines_for_bar;
			//ログのカーソル位置が１行目のとき、強制的にバーを最上部にする
			//(割り算の結果bar_ratioは絶対にゼロにならないため誤魔化す）
			if (now_top_line == 1) rat = 0;
			//int pot = (int)((double)button_diff_y * rat) + 260;
			int pot = (int)((double)button_diff_y * rat) + (btn_backlog_up.y + btn_backlog_up.height);
			if (bar_point < pot){
				bar_point = pot;
			}
			//バーが下に行き過ぎないようにする
			//if (bar_point > button_diff_y + 260) bar_point = button_diff_y + 260;
			if (bar_point > button_diff_y + (btn_backlog_up.y + btn_backlog_up.height) + 4){
				bar_point = button_diff_y + (btn_backlog_up.y + btn_backlog_up.height) + 4;
			}

			//バーの位置をもとにログの表示位置を再計算
			int click_point = bar_point;

			//クリック地点は全体の何％の位置にあるか
			// double clicked_per = (double)(click_point - 260) / (double)(600 - 260);

			// 0.05は微調整
			double clicked_per = (double)(click_point - (btn_backlog_up.y + btn_backlog_up.height) + 0.08) / (double)(btn_backlog_down.y - (btn_backlog_up.y + btn_backlog_up.height) - btn_backlog_bar.height - 30);

			//先頭からprint_top_line行目を先頭行として表示する
			
			int print_top_line = (int)((double)lines_for_bar * clicked_per + 0.5);//四捨五入つき





			//これを後ろから数えた行数に変換
			int print_top_line_reverse = all_log_lines - print_top_line + 1;
			if (print_top_line_reverse < 1) print_top_line_reverse = 1;
			if (print_top_line_reverse > all_log_lines) print_top_line_reverse = all_log_lines;
			int b_block = 0;
			int b_line = 0;
			int line_total = 0;
			for each(list<string> block in reverse_log_blocks){
				bool flag = false;
				b_block++;
				for each(string li in block){
					b_line++;
					line_total++;
					if (line_total == print_top_line_reverse){
						flag = true;
						break;
					}
				}
				if (flag) break;
				b_line = 0;
			}
			block_cnt = b_block;
			line_cnt = b_line;


		}
	}

	////// スクロールバー可能箇所を掴んだ場合ここまで //////
	bool is_input_wheel_up = (input->getKeepWheelUp() != 0);
	bool is_input_wheel_down = (input->getKeepWheelDown() != 0);


	// 上ボタン
	if (block_cnt > 0 || line_cnt > 0){
		if (cursorIn(btn_backlog_up) || is_input_wheel_up){
			if(!is_input_wheel_up) btn_backlog_up.now = btn_backlog_up.focus;
			if (is_mouse_left_clicked || is_input_wheel_up){
				if(!is_input_wheel_up) btn_backlog_up.now = btn_backlog_up.click;
				clicking_button = "up";
				a_log_it = a_log.end();
				std::advance(a_log_it, block_cnt*(-1));
				log_msgs = *a_log_it;
				msg_it = log_msgs.end();
				std::advance(msg_it, line_cnt*(-1));

				if (msg_it != log_msgs.begin()){
					line_cnt++;
				}
				else{
					if (a_log_it != a_log.begin()){
						block_cnt++;
						log_msgs = *a_log_it;
						msg_it = log_msgs.end();
						line_cnt = 1;
					}
				}
			}
		}
		//下ボタン
		if (cursorIn(btn_backlog_down) || is_input_wheel_down){
			if(!is_input_wheel_down) btn_backlog_down.now = btn_backlog_down.focus;
			if (is_mouse_left_clicked || is_input_wheel_down){
				if(!is_input_wheel_down) btn_backlog_down.now = btn_backlog_down.click;
				clicking_button = "down";

				a_log_it = a_log.end();
				std::advance(a_log_it, block_cnt*(-1));
				log_msgs = *a_log_it;
				msg_it = log_msgs.end();
				std::advance(msg_it, line_cnt*(-1));

				if (log_line_cnt == 17){
					int tmp_block_cnt = block_cnt;
					int tmp_line_cnt = line_cnt;
					msg_it++;
					if (msg_it == log_msgs.end()){
						a_log_it++;
						block_cnt--;
						log_msgs = *a_log_it;
						line_cnt = log_msgs.size();
					}
					else{
						line_cnt--;
					}




					//限界までスクロールすると、一行不足するので再計算
					//常に17件は画面に表示するようにする
					int buttoned_block_cnt = block_cnt;
					int buttoned_line_cnt = line_cnt;

					log_line_cnt = 0;
					a_log_it = a_log.end();
					std::advance(a_log_it, block_cnt*(-1));
					log_msgs = *a_log_it;
					msg_it = log_msgs.end();
					std::advance(msg_it, line_cnt*(-1));

					while (a_log_it != a_log.end() && log_line_cnt < 17){
						log_msgs = *a_log_it;
						while (msg_it != log_msgs.end() && log_line_cnt < 17){
							msg_it++;
							log_line_cnt += 1;
						}
						if (log_line_cnt < 17) a_log_it++;
						if (a_log_it == a_log.end()) break;
						log_msgs = *a_log_it;
						if (log_line_cnt == 17) break;
						msg_it = log_msgs.begin();
					}
					if (log_line_cnt == 9){
						block_cnt = tmp_block_cnt;
						line_cnt = tmp_line_cnt;
					}
					else{
						block_cnt = buttoned_block_cnt;
						line_cnt = buttoned_line_cnt;
					}
				}
			}
		}

	}

	if (!is_input_wheel_up && !is_input_wheel_down){
		if (clicking_button == "up") btn_backlog_up.now = btn_backlog_up.click;
		else if (clicking_button == "down") btn_backlog_down.now = btn_backlog_down.click;
		else if (clicking_button == "bar") btn_backlog_bar.now = btn_backlog_bar.click;
	}

	if (input->getMouseLeft() == 0) clicking_button = "";
	

	//スクロールバー
	DxLib::DrawGraph(btn_backlog_bar_base.x, btn_backlog_bar_base.y, btn_backlog_bar_base.now, TRUE);
	DxLib::DrawGraph(btn_backlog_bar.x,	bar_point, btn_backlog_bar.now, TRUE);
	//上ボタン
	DxLib::DrawGraph(btn_backlog_up.x, btn_backlog_up.y, btn_backlog_up.now, TRUE);
	//下ボタン
	DxLib::DrawGraph(btn_backlog_down.x, btn_backlog_down.y, btn_backlog_down.now, TRUE);
	// 戻るボタン
	DxLib::DrawGraph(btn_backlog_back.x, btn_backlog_back.y, btn_backlog_back.now, TRUE);

	if (cursorIn(btn_backlog_back)){
		btn_backlog_back.now = btn_backlog_back.click;
		if (is_mouse_left_clicked){
			is_backlog_enable = false;
		}
	}
	else{
		btn_backlog_back.now = btn_backlog_back.normal;
	}

	//右クリックで前画面に戻る
	if (input->getKeepMouseRight() != 0){
		seeklog_flg = false;
		//now_scene = eScene_scenario;
		//before_scene = eScene_scenario_backlog;
		is_backlog_enable = false;
	}

}



void Scene::sceneBacklogInitialize(
	Window* window,
	Config* config,
	Input* input,
	GraphicManager* g_manager,
	SoundManager* s_manager)
{
	this->input = input;
	//frame = 0;
	fade_type = FadeType::fadein;
	//back_alpha = 0;
	button_clicked = false;
	fade_completed = false;

	// 背景色 #1b0805 透過60%
	back_color = DxLib::GetColor(27, 8, 5);

	font_color_backlog = DxLib::GetColor(44, 34, 30);

	btn_backlog_base = Btn();
	btn_backlog_bar_base = Btn();
	btn_backlog_bar = Btn();
	btn_backlog_up = Btn();
	btn_backlog_down = Btn();
	btn_backlog_back = Btn();

	btn_backlog_base.now = btn_backlog_base.normal = g_manager->system_graphics[SystemGraphic::arrnum_backlog_base].getHandle();
	btn_backlog_bar_base.now = btn_backlog_bar_base.normal = g_manager->system_graphics[SystemGraphic::arrnum_backlog_base_bar].getHandle();
	btn_backlog_bar.now = btn_backlog_bar.normal = g_manager->system_graphics[SystemGraphic::arrnum_backlog_bar_normal].getHandle();
	btn_backlog_bar.focus = g_manager->system_graphics[SystemGraphic::arrnum_backlog_bar_focus].getHandle();
	btn_backlog_bar.click = g_manager->system_graphics[SystemGraphic::arrnum_backlog_bar_click].getHandle();
	btn_backlog_up.now = btn_backlog_up.normal = g_manager->system_graphics[SystemGraphic::arrnum_backlog_up_normal].getHandle();
	btn_backlog_up.focus = g_manager->system_graphics[SystemGraphic::arrnum_backlog_up_focus].getHandle();
	btn_backlog_up.click = g_manager->system_graphics[SystemGraphic::arrnum_backlog_up_click].getHandle();
	btn_backlog_down.now = btn_backlog_down.normal = g_manager->system_graphics[SystemGraphic::arrnum_backlog_down_normal].getHandle();
	btn_backlog_down.focus = g_manager->system_graphics[SystemGraphic::arrnum_backlog_down_focus].getHandle();
	btn_backlog_down.click = g_manager->system_graphics[SystemGraphic::arrnum_backlog_down_click].getHandle();
	btn_backlog_back.now = btn_backlog_back.normal = g_manager->system_graphics[SystemGraphic::arrnum_backlog_back_normal].getHandle();
	btn_backlog_back.click = g_manager->system_graphics[SystemGraphic::arrnum_backlog_back_click].getHandle();

	btn_backlog_base.x = 232;
	btn_backlog_base.y = 0;
	btn_backlog_bar_base.x = 754;
	btn_backlog_bar_base.y = 75;
	btn_backlog_up.x = 746;
	btn_backlog_up.y = 71;
	btn_backlog_down.x = 746;
	btn_backlog_down.y = 678;
	btn_backlog_bar.x = 750;
	btn_backlog_bar.y = 350;
	btn_backlog_back.x = 254;
	btn_backlog_back.y = 722;

	DxLib::GetGraphSize(btn_backlog_up.normal, &btn_backlog_up.width, &btn_backlog_up.height);
	DxLib::GetGraphSize(btn_backlog_down.normal, &btn_backlog_down.width, &btn_backlog_down.height);
	DxLib::GetGraphSize(btn_backlog_bar.normal, &btn_backlog_bar.width, &btn_backlog_bar.height);
	DxLib::GetGraphSize(btn_backlog_back.normal, &btn_backlog_back.width, &btn_backlog_back.height);
}

