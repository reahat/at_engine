﻿#include "input.h"
#include "define.h"
#include "DxLib.h"
#include "error_code_define.h"
#include "scene.h"

Teng::Input::Input(){
	key_space = FALSE;
	key_return = FALSE;
	key_control = FALSE;
	key_escape = FALSE;
	key_alt = FALSE;
	key_f4 = FALSE;
	key_f5 = FALSE;
	key_f9 = FALSE;
	mouse_left = FALSE;
	mouse_right = FALSE;
	mouse_wheel_up = FALSE;
	mouse_wheel_down = FALSE;
}

Teng::Input::~Input(){

}

void Teng::Input::checkKeyInput(){
	int rot;
	key_space = DxLib::CheckHitKey(KEY_INPUT_SPACE);
	key_return = DxLib::CheckHitKey(KEY_INPUT_RETURN);
	key_control = DxLib::CheckHitKey(KEY_INPUT_LCONTROL);
	key_escape = DxLib::CheckHitKey(KEY_INPUT_ESCAPE);
	key_alt = DxLib::CheckHitKey(KEY_INPUT_LALT) | DxLib::CheckHitKey(KEY_INPUT_RALT);
	key_f4 = DxLib::CheckHitKey(KEY_INPUT_F4);
	key_f5 = DxLib::CheckHitKey(KEY_INPUT_F5);
	key_f9 = DxLib::CheckHitKey(KEY_INPUT_F9);

	mouse_left = DxLib::GetMouseInput() & MOUSE_INPUT_LEFT;
	mouse_right = DxLib::GetMouseInput() & MOUSE_INPUT_RIGHT;

	rot = DxLib::GetMouseWheelRotVol();
	if(rot == 0){
		mouse_wheel_down = FALSE;
		mouse_wheel_up = FALSE;
	}else if(rot < 0){
		mouse_wheel_down = TRUE;
		mouse_wheel_up = FALSE;
	}else if(rot > 0){
		mouse_wheel_down = FALSE;
		mouse_wheel_up = TRUE;
	}

	DxLib::GetMousePoint(&cursor_x, &cursor_y);

	if(key_alt & key_f4){
		DEBUG printf("強制終了します。\n");
		throw ENGINE_INPUT_TERMINATE;
	}
}

int Teng::Input::checkKeepKeyReturn(Teng::Scene* scn){
	static unsigned long before_input_frame;

	if(getKeyReturn() != 0){

		//前フレームで押されていなかった場合または初回入力時
		if(before_input_frame != scn->getFrame()-1 || before_input_frame == 0){
			before_input_frame = scn->getFrame();
			//printf("before:%d, frame-1:%d\n", before_input_frame, scn.getFrame()-1);
			keep_key_return = 1;
			return 1;
		}
		before_input_frame = scn->getFrame();
	}
	//入力が無効な場合
	keep_key_return = 0;
	return 0;
}

int Teng::Input::checkKeepMouseLeft(Teng::Scene* scn){
	static unsigned long before_input_frame;

	if(getMouseLeft() != 0){

		//前フレームで押されていなかった場合または初回入力時
		if(before_input_frame != scn->getFrame()-1 || before_input_frame == 0){
			before_input_frame = scn->getFrame();
			//printf("before:%d, frame-1:%d\n", before_input_frame, scn.getFrame()-1);
			keep_mouse_left = 1;
			return 1;
		}
		before_input_frame = scn->getFrame();
	}
	//入力が無効な場合
	keep_mouse_left = 0;
	return 0;
}

int Teng::Input::checkKeepMouseRight(Teng::Scene* scn){
	static unsigned long before_input_frame;

	if(getMouseRight() != 0){

		//前フレームで押されていなかった場合または初回入力時
		if(before_input_frame != scn->getFrame()-1 || before_input_frame == 0){
			before_input_frame = scn->getFrame();
			//printf("before:%d, frame-1:%d\n", before_input_frame, scn.getFrame()-1);
			keep_mouse_right = 1;
			return 1;
		}
		before_input_frame = scn->getFrame();
	}
	//入力が無効な場合
	keep_mouse_right = 0;
	return 0;
}

int Teng::Input::checkKeepKeyF5(Teng::Scene* scn){
	static unsigned long before_input_frame;

	if(getKeyF5() != 0){

		//前フレームで押されていなかった場合または初回入力時
		if(before_input_frame != scn->getFrame()-1 || before_input_frame == 0){
			before_input_frame = scn->getFrame();
			//printf("before:%d, frame-1:%d\n", before_input_frame, scn.getFrame()-1);
			keep_key_f5 = 1;
			return 1;
		}
		before_input_frame = scn->getFrame();
	}
	//入力が無効な場合
	keep_key_f5 = 0;
	return 0;
}

int Teng::Input::checkKeepKeyF9(Teng::Scene* scn){
	static unsigned long before_input_frame;

	if(getKeyF9() != 0){

		//前フレームで押されていなかった場合または初回入力時
		if(before_input_frame != scn->getFrame()-1 || before_input_frame == 0){
			before_input_frame = scn->getFrame();
			//printf("before:%d, frame-1:%d\n", before_input_frame, scn.getFrame()-1);
			keep_key_f9 = 1;
			return 1;
		}
		before_input_frame = scn->getFrame();
	}
	//入力が無効な場合
	keep_key_f9 = 0;
	return 0;
}

int Teng::Input::checkKeepWheelUp(Teng::Scene* scn){
	static unsigned long before_input_frame;

	if(getMouseWheelUp() != 0){

		//前フレームで押されていなかった場合または初回入力時
		if(before_input_frame != scn->getFrame()-1 || before_input_frame == 0){
			before_input_frame = scn->getFrame();
			keep_wheel_up = 1;
			return 1;
		}
		before_input_frame = scn->getFrame();
	}
	//入力が無効な場合
	keep_wheel_up = 0;
	return 0;
}

int Teng::Input::checkKeepWheelDown(Teng::Scene* scn){
	static unsigned long before_input_frame;

	if(getMouseWheelDown() != 0){

		//前フレームで押されていなかった場合または初回入力時
		if(before_input_frame != scn->getFrame()-1 || before_input_frame == 0){
			before_input_frame = scn->getFrame();
			keep_wheel_down = 1;
			return 1;
		}
		before_input_frame = scn->getFrame();
	}
	//入力が無効な場合
	keep_wheel_down = 0;
	return 0;
}

void Teng::Input::disableAllKeys(){
	key_space = 0;
	key_return = 0;
	key_control = 0;
	key_escape = 0;
	key_alt = 0;
	key_f4 = 0;
	key_f5 = 0;
	key_f9 = 0;
	mouse_left = 0;
	mouse_right = 0;
	mouse_wheel_up = 0;
	mouse_wheel_down = 0;
	keep_mouse_left = 0;
	keep_mouse_right = 0;
	keep_key_return = 0;
	keep_key_f5 = 0;
	keep_key_f9 = 0;
	keep_wheel_up = 0;
	keep_wheel_down = 0;
}

//getter

int Teng::Input::getKeySpace(){
	return key_space;
}
int Teng::Input::getKeyReturn(){
	return key_return;
}
int Teng::Input::getKeyControl(){
	return key_control;
}
int Teng::Input::getKeyEscape(){
	return key_escape;
}
int Teng::Input::getKeyAlt(){
	return key_alt;
}
int Teng::Input::getKeyF4(){
	return key_f4;
}
int Teng::Input::getKeyF5(){
	return key_f5;
}
int Teng::Input::getKeyF9(){
	return key_f9;
}
int Teng::Input::getMouseLeft(){
	return mouse_left;
}
int Teng::Input::getMouseRight(){
	return mouse_right;
}
int Teng::Input::getMouseWheelUp(){
	return mouse_wheel_up;
}
int Teng::Input::getMouseWheelDown(){
	return mouse_wheel_down;
}

int Teng::Input::getKeepMouseLeft(){
	return keep_mouse_left;
}
int Teng::Input::getKeepMouseRight(){
	return keep_mouse_right;
}
int Teng::Input::getKeepKeyReturn(){
	return keep_key_return;
}
int Teng::Input::getKeepKeyF5(){
	return keep_key_f5;
}
int Teng::Input::getKeepKeyF9(){
	return keep_key_f9;
}
int Teng::Input::getKeepWheelUp(){
	return keep_wheel_up;
}
int Teng::Input::getKeepWheelDown(){
	return keep_wheel_down;
}

int Teng::Input::cursorX(){
	return cursor_x;
}
int Teng::Input::cursorY(){
	return cursor_y;
}
bool Teng::Input::cursorIn(const int left, const int right, const int top, const int bottom){
	if (cursor_x < left)    return false;
	if (cursor_x >= right)  return false;
	if (cursor_y < top)     return false;
	if (cursor_y >= bottom) return false;
	return true;
}