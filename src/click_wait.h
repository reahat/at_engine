﻿// クリック待ちのバラ
#pragma once
//#include "graphic\graphic_manager.h"


namespace Teng{
	class GraphicManager;
	class ClickWait
	{
	public:
		ClickWait(GraphicManager* g_manager);
		~ClickWait();
		void drawFall();
		void drawFade(const int x, const int y);
	private:
		int handle_now;
		int handle_1, handle_2, handle_3, handle_4, handle_5;
		int handle_6, handle_7, handle_8, handle_9, handle_10;
		int handle_11, handle_12, handle_off;
		int f;
	};


}