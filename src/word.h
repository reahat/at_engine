﻿//キー入力処理クラス
#pragma once

#include "tag/cmd_message.h"
#include <string>
using std::string;

namespace Teng{
	class Word{

	public:
		Word();
		virtual ~Word();
		void    addBaseText(const char character);
		void    addRubiText(const char character);
		void    makeNameWord(string name);
		bool    isBlank();
		bool    isPresent();
		bool    isRubiGtBaseWidth();
		void    useRubiWidth();
		void    clear();
		void    calc(const int font_handle);
		string  baseText(){return base_text;}
		string  rubiText(){return rubi;}
		bool    rubiExist(){return rubi_exists;}
		void    setRubiXPos(int x_pos){rubi_x_pos = x_pos;}
		int     rubiXPos(){return rubi_x_pos;}
		void    setLineNum(int line){line_num = line;}
		int     getLineNum(){return line_num;}
		void    setRubiBaseLineCharNum(int num){rubi_base_line_char_num = num;}
		int     getRubiBaseLineCharNum(){return rubi_base_line_char_num;}
		void    setSpeakerName(string name){speaker_name = name;}
		string  getSpeakerName(){return speaker_name;}
		void    setOnStop(bool onstop){on_stop = onstop;}
		bool    isOnStop(){return on_stop;}
		bool    withBracket() const { return with_bracket; }
		void    withBracket(const bool is_with_bracket){ with_bracket = is_with_bracket; }
	private:
		string base_text;
		string rubi;
		int    witdh;
		int    x_pos;
		bool   use_rubi_width;
		bool   rubi_exists;
		int    rubi_x_pos;
		int    space;
		int    line_num;
		int    rubi_base_line_char_num;
		string speaker_name;
		bool   on_stop;
		bool   with_bracket;
	};
}